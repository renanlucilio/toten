object dmCon: TdmCon
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 418
  Width = 551
  object Con: TFDConnection
    Params.Strings = (
      'User_Name=sa'
      'Password=root'
      'Server=.\SQLEXPRESS'
      'Database=TOTEM_GERENCIADOR'
      'DriverID=MSSQL')
    Connected = True
    LoginPrompt = False
    Left = 8
    Top = 8
  end
  object Script: TFDScript
    SQLScripts = <
      item
        Name = 'CriacaoBD'
        SQL.Strings = (
          'USE [master]'
          'GO'
          
            '/****** Object:  Database [TOTEM_GERENCIADOR]    Script Date: 12' +
            '/04/2019 08:58:57 ******/'
          'IF NOT EXISTS ('
          #9'SELECT name'
          'FROM sys.databases'
          'WHERE name = N'#39'TOTEM_GERENCIADOR'#39
          ')'
          #9'CREATE DATABASE [TOTEM_GERENCIADOR]'
          #9'CONTAINMENT = NONE'
          #9'ON  PRIMARY '
          
            #9'( NAME = N'#39'TOTEM_GERENCIADOR'#39', FILENAME = N'#39'C:\Program Files\Mi' +
            'crosoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\TOTEM_GERENCIAD' +
            'OR.mdf'#39' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 6553' +
            '6KB )'
          #9'LOG ON '
          
            #9'( NAME = N'#39'TOTEM_GERENCIADOR_log'#39', FILENAME = N'#39'C:\Program File' +
            's\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\TOTEM_GEREN' +
            'CIADOR_log.ldf'#39' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH ' +
            '= 65536KB )'
          'GO'
          ''
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET COMPATIBILITY_LEVEL = 140'
          'GO'
          'IF (1 = FULLTEXTSERVICEPROPERTY('#39'IsFullTextInstalled'#39'))'
          'begin'
          
            #9'EXEC [TOTEM_GERENCIADOR].[dbo].[sp_fulltext_database] @action =' +
            ' '#39'enable'#39
          'end'
          'GO'
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET ANSI_NULL_DEFAULT OFF '
          'GO'
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET ANSI_NULLS OFF '
          'GO'
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET ANSI_PADDING OFF '
          'GO'
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET ANSI_WARNINGS OFF '
          'GO'
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET ARITHABORT OFF '
          'GO'
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET AUTO_CLOSE ON '
          'GO'
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET AUTO_SHRINK OFF '
          'GO'
          
            'ALTER DATABASE [TOTEM_GERENCIADOR] SET AUTO_UPDATE_STATISTICS ON' +
            ' '
          'GO'
          
            'ALTER DATABASE [TOTEM_GERENCIADOR] SET CURSOR_CLOSE_ON_COMMIT OF' +
            'F '
          'GO'
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET CURSOR_DEFAULT  GLOBAL '
          'GO'
          
            'ALTER DATABASE [TOTEM_GERENCIADOR] SET CONCAT_NULL_YIELDS_NULL O' +
            'FF '
          'GO'
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET NUMERIC_ROUNDABORT OFF '
          'GO'
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET QUOTED_IDENTIFIER OFF '
          'GO'
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET RECURSIVE_TRIGGERS OFF '
          'GO'
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET  ENABLE_BROKER '
          'GO'
          
            'ALTER DATABASE [TOTEM_GERENCIADOR] SET AUTO_UPDATE_STATISTICS_AS' +
            'YNC OFF '
          'GO'
          
            'ALTER DATABASE [TOTEM_GERENCIADOR] SET DATE_CORRELATION_OPTIMIZA' +
            'TION OFF '
          'GO'
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET TRUSTWORTHY OFF '
          'GO'
          
            'ALTER DATABASE [TOTEM_GERENCIADOR] SET ALLOW_SNAPSHOT_ISOLATION ' +
            'OFF '
          'GO'
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET PARAMETERIZATION SIMPLE '
          'GO'
          
            'ALTER DATABASE [TOTEM_GERENCIADOR] SET READ_COMMITTED_SNAPSHOT O' +
            'N '
          'GO'
          
            'ALTER DATABASE [TOTEM_GERENCIADOR] SET HONOR_BROKER_PRIORITY OFF' +
            ' '
          'GO'
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET RECOVERY SIMPLE '
          'GO'
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET  MULTI_USER '
          'GO'
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET PAGE_VERIFY CHECKSUM  '
          'GO'
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET DB_CHAINING OFF '
          'GO'
          
            'ALTER DATABASE [TOTEM_GERENCIADOR] SET FILESTREAM( NON_TRANSACTE' +
            'D_ACCESS = OFF ) '
          'GO'
          
            'ALTER DATABASE [TOTEM_GERENCIADOR] SET TARGET_RECOVERY_TIME = 60' +
            ' SECONDS '
          'GO'
          
            'ALTER DATABASE [TOTEM_GERENCIADOR] SET DELAYED_DURABILITY = DISA' +
            'BLED '
          'GO'
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET QUERY_STORE = OFF'
          'GO'
          'USE [TOTEM_GERENCIADOR]'
          'GO'
          ''
          ''
          
            '/****** Object:  User [root]    Script Date: 12/04/2019 08:58:57' +
            ' ******/'
          'IF NOT EXISTS ('
          #9'SELECT name'
          'FROM sys.syslogins'
          'WHERE name = N'#39'root'#39')'
          #9'CREATE USER [root] FOR LOGIN [root] WITH DEFAULT_SCHEMA=[dbo]'
          'GO'
          
            '/****** Object:  Table [dbo].[Categoria]    Script Date: 12/04/2' +
            '019 08:58:57 ******/'
          'SET ANSI_NULLS ON'
          'GO'
          'SET QUOTED_IDENTIFIER ON'
          'GO'
          ''
          ''
          'IF OBJECT_ID('#39'dbo.Categoria'#39', '#39'U'#39') IS NULL'
          'CREATE TABLE [dbo].[Categoria]'
          '('
          #9'[ID] [int] IDENTITY(1,1) NOT NULL,'
          #9'[Descricao] [nvarchar](450) NULL,'
          #9'[LocalLogo] [nvarchar](max) NULL,'
          #9'[Status] [int] NOT NULL,'
          #9'CONSTRAINT [PK_Categoria] PRIMARY KEY CLUSTERED '
          '('
          #9'[ID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP' +
            '_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRI' +
            'MARY]'
          ') ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Table [dbo].[Cliente]    Script Date: 12/04/201' +
            '9 08:58:58 ******/'
          'SET ANSI_NULLS ON'
          'GO'
          'SET QUOTED_IDENTIFIER ON'
          'GO'
          ''
          'IF OBJECT_ID('#39'dbo.Cliente'#39', '#39'U'#39') IS NULL'
          'CREATE TABLE [dbo].[Cliente]'
          '('
          #9'[ID] [int] IDENTITY(1,1) NOT NULL,'
          #9'[Nome] [nvarchar](max) NULL,'
          #9'[Documento] [nvarchar](max) NULL,'
          #9'[EnderecoID] [int] NOT NULL,'
          #9'[ContatoID] [int] NOT NULL,'
          #9'CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED '
          '('
          #9'[ID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP' +
            '_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRI' +
            'MARY]'
          ') ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Table [dbo].[COFINS]    Script Date: 12/04/2019' +
            ' 08:58:58 ******/'
          'SET ANSI_NULLS ON'
          'GO'
          'SET QUOTED_IDENTIFIER ON'
          'GO'
          'IF OBJECT_ID('#39'dbo.COFINS'#39', '#39'U'#39') IS NULL'
          'CREATE TABLE [dbo].[COFINS]'
          '('
          #9'[ID] [int] IDENTITY(1,1) NOT NULL,'
          #9'[BaseCalculo] [decimal](18, 2) NOT NULL,'
          #9'[Porcento] [float] NOT NULL,'
          #9'[Valor] [decimal](18, 2) NOT NULL,'
          #9'[BaseCalculoPorProduto] [decimal](18, 2) NOT NULL,'
          #9'[Aliquota] [float] NOT NULL,'
          #9'[CST] [int] NOT NULL,'
          #9'CONSTRAINT [PK_COFINS] PRIMARY KEY CLUSTERED '
          '('
          #9'[ID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP' +
            '_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRI' +
            'MARY]'
          ') ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Table [dbo].[COFINSST]    Script Date: 12/04/20' +
            '19 08:58:59 ******/'
          'SET ANSI_NULLS ON'
          'GO'
          'SET QUOTED_IDENTIFIER ON'
          'GO'
          'IF OBJECT_ID('#39'dbo.COFINSST'#39', '#39'U'#39') IS NULL'
          'CREATE TABLE [dbo].[COFINSST]'
          '('
          #9'[ID] [int] IDENTITY(1,1) NOT NULL,'
          #9'[BaseCalculo] [decimal](18, 2) NOT NULL,'
          #9'[Porcento] [float] NOT NULL,'
          #9'[Valor] [decimal](18, 2) NOT NULL,'
          #9'[BaseCalculoPorProduto] [decimal](18, 2) NOT NULL,'
          #9'[Aliquota] [float] NOT NULL,'
          #9'CONSTRAINT [PK_COFINSST] PRIMARY KEY CLUSTERED '
          '('
          #9'[ID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP' +
            '_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRI' +
            'MARY]'
          ') ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Table [dbo].[Contato]    Script Date: 12/04/201' +
            '9 08:58:59 ******/'
          'SET ANSI_NULLS ON'
          'GO'
          'SET QUOTED_IDENTIFIER ON'
          'GO'
          'IF OBJECT_ID('#39'dbo.Contato'#39', '#39'U'#39') IS NULL'
          'CREATE TABLE [dbo].[Contato]'
          '('
          #9'[ID] [int] IDENTITY(1,1) NOT NULL,'
          #9'[OhContato] [nvarchar](max) NULL,'
          #9'[Tipo] [nvarchar](max) NULL,'
          #9'CONSTRAINT [PK_Contato] PRIMARY KEY CLUSTERED '
          '('
          #9'[ID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP' +
            '_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRI' +
            'MARY]'
          ') ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Table [dbo].[Endereco]    Script Date: 12/04/20' +
            '19 08:58:59 ******/'
          'SET ANSI_NULLS ON'
          'GO'
          'SET QUOTED_IDENTIFIER ON'
          'GO'
          'IF OBJECT_ID('#39'dbo.Endereco'#39', '#39'U'#39') IS NULL'
          'CREATE TABLE [dbo].[Endereco]'
          '('
          #9'[ID] [int] IDENTITY(1,1) NOT NULL,'
          #9'[Logradouro] [nvarchar](max) NULL,'
          #9'[Numero] [nvarchar](max) NULL,'
          #9'[Bairro] [nvarchar](max) NULL,'
          #9'[Cidade] [nvarchar](max) NULL,'
          #9'[UF] [nvarchar](max) NULL,'
          #9'[CEP] [nvarchar](max) NULL,'
          #9'[Complemento] [nvarchar](max) NULL,'
          #9'[Referencia] [nvarchar](max) NULL,'
          #9'CONSTRAINT [PK_Endereco] PRIMARY KEY CLUSTERED '
          '('
          #9'[ID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP' +
            '_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRI' +
            'MARY]'
          ') ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Table [dbo].[ICMS]    Script Date: 12/04/2019 0' +
            '8:58:59 ******/'
          'SET ANSI_NULLS ON'
          'GO'
          'SET QUOTED_IDENTIFIER ON'
          'GO'
          'IF OBJECT_ID('#39'dbo.ICMS'#39', '#39'U'#39') IS NULL'
          'CREATE TABLE [dbo].[ICMS]'
          '('
          #9'[ID] [int] IDENTITY(1,1) NOT NULL,'
          #9'[Origem] [int] NOT NULL,'
          #9'[CST] [int] NOT NULL,'
          #9'[Porcento] [float] NOT NULL,'
          #9'[Valor] [decimal](18, 2) NOT NULL,'
          #9'CONSTRAINT [PK_ICMS] PRIMARY KEY CLUSTERED '
          '('
          #9'[ID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP' +
            '_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRI' +
            'MARY]'
          ') ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Table [dbo].[Impostos]    Script Date: 12/04/20' +
            '19 08:58:59 ******/'
          'SET ANSI_NULLS ON'
          'GO'
          'SET QUOTED_IDENTIFIER ON'
          'GO'
          'IF OBJECT_ID('#39'dbo.Impostos'#39', '#39'U'#39') IS NULL'
          'CREATE TABLE [dbo].[Impostos]'
          '('
          #9'[ID] [int] IDENTITY(1,1) NOT NULL,'
          #9'[Descricao] [nvarchar](max) NULL,'
          #9'[ICMSID] [int] NOT NULL,'
          #9'[PISID] [int] NOT NULL,'
          #9'[COFINSID] [int] NOT NULL,'
          #9'[PISSTID] [int] NOT NULL,'
          #9'[COFINSSTID] [int] NOT NULL,'
          #9'[ISSQNID] [int] NOT NULL,'
          #9'[TaxasID] [int] NOT NULL,'
          #9'CONSTRAINT [PK_Impostos] PRIMARY KEY CLUSTERED '
          '('
          #9'[ID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP' +
            '_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRI' +
            'MARY]'
          ') ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Table [dbo].[ISSQN]    Script Date: 12/04/2019 ' +
            '08:58:59 ******/'
          'SET ANSI_NULLS ON'
          'GO'
          'SET QUOTED_IDENTIFIER ON'
          'GO'
          'IF OBJECT_ID('#39'dbo.ISSQN'#39', '#39'U'#39') IS NULL'
          'CREATE TABLE [dbo].[ISSQN]'
          '('
          #9'[ID] [int] IDENTITY(1,1) NOT NULL,'
          #9'[BaseCalculo] [decimal](18, 2) NOT NULL,'
          #9'[Deducao] [decimal](18, 2) NOT NULL,'
          #9'[CodigoIBGE] [nvarchar](max) NULL,'
          #9'[BaseCalculoProduto] [decimal](18, 2) NOT NULL,'
          #9'[ItemDaListaServico] [nvarchar](max) NULL,'
          #9'[CodigoTributarioISSQN] [nvarchar](max) NULL,'
          #9'[Natureza] [int] NOT NULL,'
          #9'CONSTRAINT [PK_ISSQN] PRIMARY KEY CLUSTERED '
          '('
          #9'[ID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP' +
            '_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRI' +
            'MARY]'
          ') ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Table [dbo].[Item]    Script Date: 12/04/2019 0' +
            '8:58:59 ******/'
          'SET ANSI_NULLS ON'
          'GO'
          'SET QUOTED_IDENTIFIER ON'
          'GO'
          'IF OBJECT_ID('#39'dbo.Item'#39', '#39'U'#39') IS NULL'
          'CREATE TABLE [dbo].[Item]'
          '('
          #9'[ID] [int] IDENTITY(1,1) NOT NULL,'
          #9'CONSTRAINT [PK_Item] PRIMARY KEY CLUSTERED '
          '('
          #9'[ID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP' +
            '_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRI' +
            'MARY]'
          ') ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Table [dbo].[ItemExtra]    Script Date: 12/04/2' +
            '019 08:58:59 ******/'
          'SET ANSI_NULLS ON'
          'GO'
          'SET QUOTED_IDENTIFIER ON'
          'GO'
          'IF OBJECT_ID('#39'dbo.ItemExtra'#39', '#39'U'#39') IS NULL'
          'CREATE TABLE [dbo].[ItemExtra]'
          '('
          #9'[ID] [int] IDENTITY(1,1) NOT NULL,'
          #9'[Descricao] [nvarchar](max) NULL,'
          #9'[Valor] [float] NOT NULL,'
          #9'[Status] [int] NOT NULL,'
          #9'CONSTRAINT [PK_ItemExtra] PRIMARY KEY CLUSTERED '
          '('
          #9'[ID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP' +
            '_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRI' +
            'MARY]'
          ') ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Table [dbo].[ItemExtraCategoia]    Script Date:' +
            ' 12/04/2019 08:58:59 ******/'
          'SET ANSI_NULLS ON'
          'GO'
          'SET QUOTED_IDENTIFIER ON'
          'GO'
          'IF OBJECT_ID('#39'dbo.ItemExtraCategoia'#39', '#39'U'#39') IS NULL'
          'CREATE TABLE [dbo].[ItemExtraCategoia]'
          '('
          #9'[ItemExtraID] [int] NOT NULL,'
          #9'[CategoriaID] [int] NOT NULL,'
          #9'CONSTRAINT [PK_ItemExtraCategoia] PRIMARY KEY CLUSTERED '
          '('
          #9'[CategoriaID] ASC,'
          #9'[ItemExtraID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP' +
            '_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRI' +
            'MARY]'
          ') ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Table [dbo].[PIS]    Script Date: 12/04/2019 08' +
            ':58:59 ******/'
          'SET ANSI_NULLS ON'
          'GO'
          'SET QUOTED_IDENTIFIER ON'
          'GO'
          'IF OBJECT_ID('#39'dbo.PIS'#39', '#39'U'#39') IS NULL'
          'CREATE TABLE [dbo].[PIS]'
          '('
          #9'[ID] [int] IDENTITY(1,1) NOT NULL,'
          #9'[BaseCalculo] [decimal](18, 2) NOT NULL,'
          #9'[Porcento] [float] NOT NULL,'
          #9'[Valor] [decimal](18, 2) NOT NULL,'
          #9'[BaseCalculoPorProduto] [decimal](18, 2) NOT NULL,'
          #9'[Aliquota] [float] NOT NULL,'
          #9'[CST] [int] NOT NULL,'
          #9'CONSTRAINT [PK_PIS] PRIMARY KEY CLUSTERED '
          '('
          #9'[ID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP' +
            '_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRI' +
            'MARY]'
          ') ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Table [dbo].[PISST]    Script Date: 12/04/2019 ' +
            '08:58:59 ******/'
          'SET ANSI_NULLS ON'
          'GO'
          'SET QUOTED_IDENTIFIER ON'
          'GO'
          'IF OBJECT_ID('#39'dbo.PISST'#39', '#39'U'#39') IS NULL'
          'CREATE TABLE [dbo].[PISST]'
          '('
          #9'[ID] [int] IDENTITY(1,1) NOT NULL,'
          #9'[BaseCalculo] [decimal](18, 2) NOT NULL,'
          #9'[Porcento] [float] NOT NULL,'
          #9'[Valor] [decimal](18, 2) NOT NULL,'
          #9'[BaseCalculoPorProduto] [decimal](18, 2) NOT NULL,'
          #9'[Aliquota] [float] NOT NULL,'
          #9'CONSTRAINT [PK_PISST] PRIMARY KEY CLUSTERED '
          '('
          #9'[ID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP' +
            '_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRI' +
            'MARY]'
          ') ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Table [dbo].[Produto]    Script Date: 12/04/201' +
            '9 08:58:59 ******/'
          'SET ANSI_NULLS ON'
          'GO'
          'SET QUOTED_IDENTIFIER ON'
          'GO'
          'IF OBJECT_ID('#39'dbo.Produto'#39', '#39'U'#39') IS NULL'
          'CREATE TABLE [dbo].[Produto]'
          '('
          #9'[ID] [int] IDENTITY(1,1) NOT NULL,'
          #9'[Descricao] [nvarchar](max) NULL,'
          #9'[CategoriaID] [int] NOT NULL,'
          #9'[ImpostosID] [int] NULL,'
          #9'[UnidadeID] [int] NOT NULL,'
          #9'[CodigoBarras] [nvarchar](max) NULL,'
          #9'[ValorID] [int] NOT NULL,'
          #9'[NCM] [nvarchar](max) NULL,'
          #9'[CEST] [nvarchar](max) NULL,'
          #9'[CFOP] [nvarchar](max) NULL,'
          #9'[Ingredientes] [nvarchar](max) NULL,'
          #9'[Status] [int] NOT NULL,'
          #9'CONSTRAINT [PK_Produto] PRIMARY KEY CLUSTERED '
          '('
          #9'[ID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP' +
            '_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRI' +
            'MARY]'
          ') ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Table [dbo].[TaxasFiscais]    Script Date: 12/0' +
            '4/2019 08:58:59 ******/'
          'SET ANSI_NULLS ON'
          'GO'
          'SET QUOTED_IDENTIFIER ON'
          'GO'
          'IF OBJECT_ID('#39'dbo.TaxasFiscais'#39', '#39'U'#39') IS NULL'
          'CREATE TABLE [dbo].[TaxasFiscais]'
          '('
          #9'[ID] [int] IDENTITY(1,1) NOT NULL,'
          #9'[TaxaEstadual] [decimal](18, 2) NOT NULL,'
          #9'[TaxaFederal] [decimal](18, 2) NOT NULL,'
          #9'CONSTRAINT [PK_TaxasFiscais] PRIMARY KEY CLUSTERED '
          '('
          #9'[ID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP' +
            '_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRI' +
            'MARY]'
          ') ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Table [dbo].[Totem]    Script Date: 12/04/2019 ' +
            '08:58:59 ******/'
          'SET ANSI_NULLS ON'
          'GO'
          'SET QUOTED_IDENTIFIER ON'
          'GO'
          'IF OBJECT_ID('#39'dbo.Totem'#39', '#39'U'#39') IS NULL'
          'CREATE TABLE [dbo].[Totem]'
          '('
          #9'[ID] [int] IDENTITY(1,1) NOT NULL,'
          #9'[Local] [nvarchar](max) NULL,'
          #9'[Nome] [nvarchar](max) NULL,'
          #9'[Ip] [nvarchar](max) NULL,'
          #9'[Mapeamento] [nvarchar](max) NULL,'
          #9'[Status] [int] NOT NULL,'
          #9'CONSTRAINT [PK_Totem] PRIMARY KEY CLUSTERED '
          '('
          #9'[ID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP' +
            '_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRI' +
            'MARY]'
          ') ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Table [dbo].[Unidade]    Script Date: 12/04/201' +
            '9 08:59:00 ******/'
          'SET ANSI_NULLS ON'
          'GO'
          'SET QUOTED_IDENTIFIER ON'
          'GO'
          'IF OBJECT_ID('#39'dbo.Unidade'#39', '#39'U'#39') IS NULL'
          'CREATE TABLE [dbo].[Unidade]'
          '('
          #9'[ID] [int] IDENTITY(1,1) NOT NULL,'
          #9'[Descricao] [nvarchar](max) NULL,'
          #9'[Sigla] [nvarchar](max) NULL,'
          #9'[TemDecimal] [bit] NOT NULL,'
          #9'CONSTRAINT [PK_Unidade] PRIMARY KEY CLUSTERED '
          '('
          #9'[ID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP' +
            '_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRI' +
            'MARY]'
          ') ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Table [dbo].[ValorEspecial]    Script Date: 12/' +
            '04/2019 08:59:00 ******/'
          'SET ANSI_NULLS ON'
          'GO'
          'SET QUOTED_IDENTIFIER ON'
          'GO'
          'IF OBJECT_ID('#39'dbo.ValorEspecial'#39', '#39'U'#39') IS NULL'
          'CREATE TABLE [dbo].[ValorEspecial]'
          '('
          #9'[ID] [int] IDENTITY(1,1) NOT NULL,'
          #9'[ValorPequeno] [decimal](18, 2) NOT NULL,'
          #9'[ValorMedia] [decimal](18, 2) NOT NULL,'
          #9'[ValorGigante] [decimal](18, 2) NOT NULL,'
          #9'[ValorNormal] [decimal](18, 2) NOT NULL,'
          #9'CONSTRAINT [PK_ValorEspecial] PRIMARY KEY CLUSTERED '
          '('
          #9'[ID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP' +
            '_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRI' +
            'MARY]'
          ') ON [PRIMARY]'
          'GO'
          'SET ANSI_PADDING ON'
          'GO'
          
            '/****** Object:  Index [IX_Categoria_Descricao]    Script Date: ' +
            '12/04/2019 08:59:00 ******/'
          'IF 0 = (SELECT COUNT(*) as index_count'
          'FROM sys.indexes'
          'WHERE object_id = OBJECT_ID('#39'dbo.Categoria'#39')'
          #9'AND name='#39'IX_Categoria_Descricao'#39')'
          
            'CREATE UNIQUE NONCLUSTERED INDEX [IX_Categoria_Descricao] ON [db' +
            'o].[Categoria]'
          '('
          #9'[Descricao] ASC'
          ')'
          'WHERE ([Descricao] IS NOT NULL)'
          
            'WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEM' +
            'PDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = O' +
            'FF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Index [IX_Cliente_ContatoID]    Script Date: 12' +
            '/04/2019 08:59:00 ******/'
          'IF 0 = (SELECT COUNT(*) as index_count'
          'FROM sys.indexes'
          'WHERE object_id = OBJECT_ID('#39'dbo.Cliente'#39')'
          #9'AND name='#39'IX_Cliente_ContatoID'#39')'
          
            'CREATE NONCLUSTERED INDEX [IX_Cliente_ContatoID] ON [dbo].[Clien' +
            'te]'
          '('
          #9'[ContatoID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TE' +
            'MPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS =' +
            ' ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Index [IX_Cliente_EnderecoID]    Script Date: 1' +
            '2/04/2019 08:59:00 ******/'
          'IF 0 = (SELECT COUNT(*) as index_count'
          'FROM sys.indexes'
          'WHERE object_id = OBJECT_ID('#39'dbo.Cliente'#39')'
          #9'AND name='#39'IX_Cliente_EnderecoID'#39')'
          
            'CREATE NONCLUSTERED INDEX [IX_Cliente_EnderecoID] ON [dbo].[Clie' +
            'nte]'
          '('
          #9'[EnderecoID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TE' +
            'MPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS =' +
            ' ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Index [IX_Impostos_COFINSID]    Script Date: 12' +
            '/04/2019 08:59:00 ******/'
          'IF 0 = (SELECT COUNT(*) as index_count'
          'FROM sys.indexes'
          'WHERE object_id = OBJECT_ID('#39'dbo.Impostos'#39')'
          #9'AND name='#39'IX_Impostos_COFINSID'#39')'
          
            'CREATE NONCLUSTERED INDEX [IX_Impostos_COFINSID] ON [dbo].[Impos' +
            'tos]'
          '('
          #9'[COFINSID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TE' +
            'MPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS =' +
            ' ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Index [IX_Impostos_COFINSSTID]    Script Date: ' +
            '12/04/2019 08:59:00 ******/'
          'IF 0 = (SELECT COUNT(*) as index_count'
          'FROM sys.indexes'
          'WHERE object_id = OBJECT_ID('#39'dbo.Impostos'#39')'
          #9'AND name='#39'IX_Impostos_COFINSSTID'#39')'
          
            'CREATE NONCLUSTERED INDEX [IX_Impostos_COFINSSTID] ON [dbo].[Imp' +
            'ostos]'
          '('
          #9'[COFINSSTID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TE' +
            'MPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS =' +
            ' ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Index [IX_Impostos_ICMSID]    Script Date: 12/0' +
            '4/2019 08:59:00 ******/'
          'IF 0 = (SELECT COUNT(*) as index_count'
          'FROM sys.indexes'
          'WHERE object_id = OBJECT_ID('#39'dbo.Impostos'#39')'
          #9'AND name='#39'IX_Impostos_ICMSID'#39')'
          
            'CREATE NONCLUSTERED INDEX [IX_Impostos_ICMSID] ON [dbo].[Imposto' +
            's]'
          '('
          #9'[ICMSID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TE' +
            'MPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS =' +
            ' ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Index [IX_Impostos_ISSQNID]    Script Date: 12/' +
            '04/2019 08:59:00 ******/'
          'IF 0 = (SELECT COUNT(*) as index_count'
          'FROM sys.indexes'
          'WHERE object_id = OBJECT_ID('#39'dbo.Impostos'#39')'
          #9'AND name='#39'IX_Impostos_ISSQNID'#39')'
          
            'CREATE NONCLUSTERED INDEX [IX_Impostos_ISSQNID] ON [dbo].[Impost' +
            'os]'
          '('
          #9'[ISSQNID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TE' +
            'MPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS =' +
            ' ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Index [IX_Impostos_PISID]    Script Date: 12/04' +
            '/2019 08:59:00 ******/'
          'IF 0 = (SELECT COUNT(*) as index_count'
          'FROM sys.indexes'
          'WHERE object_id = OBJECT_ID('#39'dbo.Impostos'#39')'
          #9'AND name='#39'IX_Impostos_PISID'#39')'
          
            'CREATE NONCLUSTERED INDEX [IX_Impostos_PISID] ON [dbo].[Impostos' +
            ']'
          '('
          #9'[PISID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TE' +
            'MPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS =' +
            ' ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Index [IX_Impostos_PISSTID]    Script Date: 12/' +
            '04/2019 08:59:00 ******/'
          'IF 0 = (SELECT COUNT(*) as index_count'
          'FROM sys.indexes'
          'WHERE object_id = OBJECT_ID('#39'dbo.Impostos'#39')'
          #9'AND name='#39'IX_Impostos_PISSTID'#39')'
          
            'CREATE NONCLUSTERED INDEX [IX_Impostos_PISSTID] ON [dbo].[Impost' +
            'os]'
          '('
          #9'[PISSTID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TE' +
            'MPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS =' +
            ' ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Index [IX_Impostos_TaxasID]    Script Date: 12/' +
            '04/2019 08:59:00 ******/'
          'IF 0 = (SELECT COUNT(*) as index_count'
          'FROM sys.indexes'
          'WHERE object_id = OBJECT_ID('#39'dbo.Impostos'#39')'
          #9'AND name='#39'IX_Impostos_TaxasID'#39')'
          
            'CREATE NONCLUSTERED INDEX [IX_Impostos_TaxasID] ON [dbo].[Impost' +
            'os]'
          '('
          #9'[TaxasID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TE' +
            'MPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS =' +
            ' ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Index [IX_ItemExtraCategoia_ItemExtraID]    Scr' +
            'ipt Date: 12/04/2019 08:59:00 ******/'
          'IF 0 = (SELECT COUNT(*) as index_count'
          'FROM sys.indexes'
          'WHERE object_id = OBJECT_ID('#39'dbo.ItemExtraCategoia'#39')'
          #9'AND name='#39'IX_ItemExtraCategoia_ItemExtraID'#39')'
          
            'CREATE NONCLUSTERED INDEX [IX_ItemExtraCategoia_ItemExtraID] ON ' +
            '[dbo].[ItemExtraCategoia]'
          '('
          #9'[ItemExtraID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TE' +
            'MPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS =' +
            ' ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Index [IX_Produto_CategoriaID]    Script Date: ' +
            '12/04/2019 08:59:00 ******/'
          'IF 0 = (SELECT COUNT(*) as index_count'
          'FROM sys.indexes'
          'WHERE object_id = OBJECT_ID('#39'dbo.Produto'#39')'
          #9'AND name='#39'IX_Produto_CategoriaID'#39')'
          
            'CREATE NONCLUSTERED INDEX [IX_Produto_CategoriaID] ON [dbo].[Pro' +
            'duto]'
          '('
          #9'[CategoriaID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TE' +
            'MPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS =' +
            ' ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Index [IX_Produto_ImpostosID]    Script Date: 1' +
            '2/04/2019 08:59:00 ******/'
          'IF 0 = (SELECT COUNT(*) as index_count'
          'FROM sys.indexes'
          'WHERE object_id = OBJECT_ID('#39'dbo.Produto'#39')'
          #9'AND name='#39'IX_Produto_ImpostosID'#39')'
          
            'CREATE NONCLUSTERED INDEX [IX_Produto_ImpostosID] ON [dbo].[Prod' +
            'uto]'
          '('
          #9'[ImpostosID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TE' +
            'MPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS =' +
            ' ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Index [IX_Produto_UnidadeID]    Script Date: 12' +
            '/04/2019 08:59:00 ******/'
          'IF 0 = (SELECT COUNT(*) as index_count'
          'FROM sys.indexes'
          'WHERE object_id = OBJECT_ID('#39'dbo.Produto'#39')'
          #9'AND name='#39'IX_Produto_UnidadeID'#39')'
          
            'CREATE NONCLUSTERED INDEX [IX_Produto_UnidadeID] ON [dbo].[Produ' +
            'to]'
          '('
          #9'[UnidadeID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TE' +
            'MPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS =' +
            ' ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
          'GO'
          
            '/****** Object:  Index [IX_Produto_ValorID]    Script Date: 12/0' +
            '4/2019 08:59:00 ******/'
          'IF 0 = (SELECT COUNT(*) as index_count'
          'FROM sys.indexes'
          'WHERE object_id = OBJECT_ID('#39'dbo.Produto'#39')'
          #9'AND name='#39'IX_Produto_ValorID'#39')'
          
            'CREATE NONCLUSTERED INDEX [IX_Produto_ValorID] ON [dbo].[Produto' +
            ']'
          '('
          #9'[ValorID] ASC'
          
            ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TE' +
            'MPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS =' +
            ' ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Cliente_Contato_ContatoI' +
            'D'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Cliente'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Cliente]  WITH CHECK ADD  CONSTRAINT [FK_Clie' +
            'nte_Contato_ContatoID] FOREIGN KEY([ContatoID])'
          'REFERENCES [dbo].[Contato] ([ID])'
          'ON DELETE CASCADE'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Cliente_Contato_ContatoI' +
            'D'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Cliente'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_Contato' +
            '_ContatoID]'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Cliente_Endereco_Enderec' +
            'oID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Cliente'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Cliente]  WITH CHECK ADD  CONSTRAINT [FK_Clie' +
            'nte_Endereco_EnderecoID] FOREIGN KEY([EnderecoID])'
          'REFERENCES [dbo].[Endereco] ([ID])'
          'ON DELETE CASCADE'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Cliente_Endereco_Enderec' +
            'oID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Cliente'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_Enderec' +
            'o_EnderecoID]'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Impostos_COFINS_COFINSID' +
            #39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Impostos'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Impostos]  WITH CHECK ADD  CONSTRAINT [FK_Imp' +
            'ostos_COFINS_COFINSID] FOREIGN KEY([COFINSID])'
          'REFERENCES [dbo].[COFINS] ([ID])'
          'ON DELETE CASCADE'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Impostos_COFINS_COFINSID' +
            #39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Impostos'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Impostos] CHECK CONSTRAINT [FK_Impostos_COFIN' +
            'S_COFINSID]'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Impostos_COFINSST_COFINS' +
            'STID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Impostos'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Impostos]  WITH CHECK ADD  CONSTRAINT [FK_Imp' +
            'ostos_COFINSST_COFINSSTID] FOREIGN KEY([COFINSSTID])'
          'REFERENCES [dbo].[COFINSST] ([ID])'
          'ON DELETE CASCADE'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Impostos_COFINSST_COFINS' +
            'STID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Impostos'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Impostos] CHECK CONSTRAINT [FK_Impostos_COFIN' +
            'SST_COFINSSTID]'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Impostos_ICMS_ICMSID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Impostos'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Impostos]  WITH CHECK ADD  CONSTRAINT [FK_Imp' +
            'ostos_ICMS_ICMSID] FOREIGN KEY([ICMSID])'
          'REFERENCES [dbo].[ICMS] ([ID])'
          'ON DELETE CASCADE'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Impostos_ICMS_ICMSID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Impostos'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Impostos] CHECK CONSTRAINT [FK_Impostos_ICMS_' +
            'ICMSID]'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Impostos_ISSQN_ISSQNID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Impostos'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Impostos]  WITH CHECK ADD  CONSTRAINT [FK_Imp' +
            'ostos_ISSQN_ISSQNID] FOREIGN KEY([ISSQNID])'
          'REFERENCES [dbo].[ISSQN] ([ID])'
          'ON DELETE CASCADE'
          'GO'
          
            'ALTER TABLE [dbo].[Impostos] CHECK CONSTRAINT [FK_Impostos_ICMS_' +
            'ICMSID]'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Impostos_ISSQN_ISSQNID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Impostos'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Impostos] CHECK CONSTRAINT [FK_Impostos_ISSQN' +
            '_ISSQNID]'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Impostos_PIS_PISID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Impostos'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Impostos]  WITH CHECK ADD  CONSTRAINT [FK_Imp' +
            'ostos_PIS_PISID] FOREIGN KEY([PISID])'
          'REFERENCES [dbo].[PIS] ([ID])'
          'ON DELETE CASCADE'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Impostos_PIS_PISID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Impostos'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Impostos] CHECK CONSTRAINT [FK_Impostos_PIS_P' +
            'ISID]'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Impostos_PISST_PISSTID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Impostos'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Impostos]  WITH CHECK ADD  CONSTRAINT [FK_Imp' +
            'ostos_PISST_PISSTID] FOREIGN KEY([PISSTID])'
          'REFERENCES [dbo].[PISST] ([ID])'
          'ON DELETE CASCADE'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Impostos_PISST_PISSTID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Impostos'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Impostos] CHECK CONSTRAINT [FK_Impostos_PISST' +
            '_PISSTID]'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Impostos_TaxasFiscais_Ta' +
            'xasID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Impostos'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Impostos]  WITH CHECK ADD  CONSTRAINT [FK_Imp' +
            'ostos_TaxasFiscais_TaxasID] FOREIGN KEY([TaxasID])'
          'REFERENCES [dbo].[TaxasFiscais] ([ID])'
          'ON DELETE CASCADE'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Impostos_TaxasFiscais_Ta' +
            'xasID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Impostos'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Impostos] CHECK CONSTRAINT [FK_Impostos_Taxas' +
            'Fiscais_TaxasID]'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_ItemExtraCategoia_Catego' +
            'ria_CategoriaID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.ItemExtraCategoia'#39')'
          ')'
          
            'ALTER TABLE [dbo].[ItemExtraCategoia]  WITH CHECK ADD  CONSTRAIN' +
            'T [FK_ItemExtraCategoia_Categoria_CategoriaID] FOREIGN KEY([Cate' +
            'goriaID])'
          'REFERENCES [dbo].[Categoria] ([ID])'
          'ON DELETE CASCADE'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_ItemExtraCategoia_Catego' +
            'ria_CategoriaID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.ItemExtraCategoia'#39')'
          ')'
          
            'ALTER TABLE [dbo].[ItemExtraCategoia] CHECK CONSTRAINT [FK_ItemE' +
            'xtraCategoia_Categoria_CategoriaID]'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_ItemExtraCategoia_ItemEx' +
            'tra_ItemExtraID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.ItemExtraCategoia'#39')'
          ')'
          
            'ALTER TABLE [dbo].[ItemExtraCategoia]  WITH CHECK ADD  CONSTRAIN' +
            'T [FK_ItemExtraCategoia_ItemExtra_ItemExtraID] FOREIGN KEY([Item' +
            'ExtraID])'
          'REFERENCES [dbo].[ItemExtra] ([ID])'
          'ON DELETE CASCADE'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_ItemExtraCategoia_ItemEx' +
            'tra_ItemExtraID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.ItemExtraCategoia'#39')'
          ')'
          
            'ALTER TABLE [dbo].[ItemExtraCategoia] CHECK CONSTRAINT [FK_ItemE' +
            'xtraCategoia_ItemExtra_ItemExtraID]'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Produto_Categoria_Catego' +
            'riaID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Produto'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Produto]  WITH CHECK ADD  CONSTRAINT [FK_Prod' +
            'uto_Categoria_CategoriaID] FOREIGN KEY([CategoriaID])'
          'REFERENCES [dbo].[Categoria] ([ID])'
          'ON DELETE CASCADE'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Produto_Categoria_Catego' +
            'riaID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Produto'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Produto] CHECK CONSTRAINT [FK_Produto_Categor' +
            'ia_CategoriaID]'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Produto_Impostos_Imposto' +
            'sID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Produto'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Produto]  WITH CHECK ADD  CONSTRAINT [FK_Prod' +
            'uto_Impostos_ImpostosID] FOREIGN KEY([ImpostosID])'
          'REFERENCES [dbo].[Impostos] ([ID])'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Produto_Impostos_Imposto' +
            'sID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Produto'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Produto] CHECK CONSTRAINT [FK_Produto_Imposto' +
            's_ImpostosID]'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Produto_Unidade_UnidadeI' +
            'D'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Produto'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Produto]  WITH CHECK ADD  CONSTRAINT [FK_Prod' +
            'uto_Unidade_UnidadeID] FOREIGN KEY([UnidadeID])'
          'REFERENCES [dbo].[Unidade] ([ID])'
          'ON DELETE CASCADE'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Produto_Unidade_UnidadeI' +
            'D'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Produto'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Produto] CHECK CONSTRAINT [FK_Produto_Unidade' +
            '_UnidadeID]'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Produto_ValorEspecial_Va' +
            'lorID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Produto'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Produto]  WITH CHECK ADD  CONSTRAINT [FK_Prod' +
            'uto_ValorEspecial_ValorID] FOREIGN KEY([ValorID])'
          'REFERENCES [dbo].[ValorEspecial] ([ID])'
          'ON DELETE CASCADE'
          'GO'
          'IF NOT EXISTS (SELECT * '
          '  FROM sys.foreign_keys '
          
            '   WHERE object_id = OBJECT_ID(N'#39'dbo.FK_Produto_ValorEspecial_Va' +
            'lorID'#39')'
          '   AND parent_object_id = OBJECT_ID(N'#39'dbo.Produto'#39')'
          ')'
          
            'ALTER TABLE [dbo].[Produto] CHECK CONSTRAINT [FK_Produto_ValorEs' +
            'pecial_ValorID]'
          'GO'
          'USE [master]'
          'GO'
          'ALTER DATABASE [TOTEM_GERENCIADOR] SET  READ_WRITE '
          'GO')
      end>
    Connection = Con
    ScriptOptions.FileEncoding = ecUTF8
    Params = <>
    Macros = <>
    FetchOptions.AssignedValues = [evItems, evAutoClose, evAutoFetchAll]
    FetchOptions.AutoClose = False
    FetchOptions.Items = [fiBlobs, fiDetails]
    ResourceOptions.AssignedValues = [rvMacroCreate, rvMacroExpand, rvDirectExecute, rvPersistent]
    ResourceOptions.MacroCreate = False
    ResourceOptions.DirectExecute = True
    Left = 144
    Top = 8
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 72
    Top = 8
  end
  object tableCategoria: TFDTable
    Active = True
    IndexFieldNames = 'ID'
    Connection = Con
    UpdateOptions.UpdateTableName = 'TOTEM_GERENCIADOR.dbo.Categoria'
    TableName = 'TOTEM_GERENCIADOR.dbo.Categoria'
    Left = 24
    Top = 136
  end
  object tableCliente: TFDTable
    CachedUpdates = True
    IndexFieldNames = 'ID'
    Connection = Con
    UpdateOptions.UpdateTableName = 'TOTEM_GERENCIADOR.dbo.Cliente'
    TableName = 'TOTEM_GERENCIADOR.dbo.Cliente'
    Left = 24
    Top = 192
  end
  object tableItemExtra: TFDTable
    Active = True
    IndexFieldNames = 'ID'
    Connection = Con
    UpdateOptions.UpdateTableName = 'TOTEM_GERENCIADOR.dbo.ItemExtra'
    TableName = 'TOTEM_GERENCIADOR.dbo.ItemExtra'
    Left = 104
    Top = 192
  end
  object tableTributacao: TFDTable
    Active = True
    IndexFieldNames = 'ID'
    Connection = Con
    UpdateOptions.UpdateTableName = 'TOTEM_GERENCIADOR.dbo.Impostos'
    TableName = 'TOTEM_GERENCIADOR.dbo.Impostos'
    Left = 192
    Top = 136
  end
  object tableTotem: TFDTable
    Active = True
    IndexFieldNames = 'ID'
    Connection = Con
    UpdateOptions.UpdateTableName = 'TOTEM_GERENCIADOR.dbo.Totem'
    TableName = 'TOTEM_GERENCIADOR.dbo.Totem'
    Left = 192
    Top = 192
  end
  object tableProduto: TFDQuery
    Connection = Con
    FormatOptions.AssignedValues = [fvMapRules]
    FormatOptions.OwnMapRules = True
    FormatOptions.MapRules = <
      item
        SourceDataType = dtBCD
        TargetDataType = dtCurrency
      end>
    SQL.Strings = (
      'select p.ID, p.descricao, p.Status, v.ValorNormal from produto p'
      'inner join ValorEspecial v on p.ValorID = v.ID')
    Left = 104
    Top = 136
  end
end
