object dmRotas: TdmRotas
  OldCreateOrder = False
  Encoding = esASCII
  Height = 150
  Width = 215
  object dweRotasGet: TDWServerEvents
    IgnoreInvalidParams = False
    Events = <
      item
        Routes = [crAll]
        DWParams = <>
        JsonMode = jmPureJSON
        Name = 'categorias'
        OnReplyEvent = dweRotasGetEventscategoriasReplyEvent
      end
      item
        Routes = [crAll]
        DWParams = <>
        JsonMode = jmPureJSON
        Name = 'itensExtra'
        OnReplyEvent = dweRotasGetEventsitensExtraReplyEvent
      end
      item
        Routes = [crAll]
        DWParams = <>
        JsonMode = jmPureJSON
        Name = 'produtos'
        OnReplyEvent = dweRotasGetEventsprodutosReplyEvent
      end
      item
        Routes = [crAll]
        DWParams = <>
        JsonMode = jmPureJSON
        Name = 'pizzaTipo'
        OnReplyEvent = dweRotasGetEventspizzaTipoReplyEvent
      end
      item
        Routes = [crAll]
        DWParams = <>
        JsonMode = jmPureJSON
        Name = 'pizzaTamanho'
        OnReplyEvent = dweRotasGetEventspizzaTamanhoReplyEvent
      end
      item
        Routes = [crAll]
        DWParams = <>
        JsonMode = jmPureJSON
        Name = 'estabelecimento'
        OnReplyEvent = dweRotasGetEventsestabelecimentoReplyEvent
      end>
    Left = 88
    Top = 56
  end
end
