unit uDmServe;

interface

uses
  System.SysUtils, System.Classes, uDWAbout, uRESTDWBase, FMX.Types;

type
  TdmServe = class(TDataModule)
    dwServe: TRESTServicePooler;
    tmrAtualizaStatus: TTimer;
    procedure tmrAtualizaStatusTimer(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure GetStatus();
    { Private declarations }
  public
    procedure ConectarDesconectar();
    { Public declarations }
  end;

var
  dmServe: TdmServe;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

uses ufrmPrincipal, System.Types, uDmRotas;

{$R *.dfm}

{ TdmServe }

procedure TdmServe.ConectarDesconectar;
begin
  dwServe.Active:= not dwServe.Active;
end;

procedure TdmServe.DataModuleCreate(Sender: TObject);
begin
  ConectarDesconectar;
  dwServe.ServerMethodClass:= TdmRotas;
end;

procedure TdmServe.GetStatus;
begin
  if dwServe.Active then
  begin
    formPrincipal.imgStatuServer.Bitmap:= formPrincipal.imgList.Bitmap(TSizeF.Create(33, 33), 8);
    formPrincipal.txtStatusServer.Text:= 'Servidor Ligado';
  end
  else
  begin
    formPrincipal.imgStatuServer.Bitmap:= formPrincipal.imgList.Bitmap(TSizeF.Create(33, 33), 7);
    formPrincipal.txtStatusServer.Text:= 'Servidor Desligado';
  end;
end;

procedure TdmServe.tmrAtualizaStatusTimer(Sender: TObject);
begin
  GetStatus;
end;

end.
