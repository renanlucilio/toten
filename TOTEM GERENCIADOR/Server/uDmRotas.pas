unit uDmRotas;

interface

uses
  System.SysUtils, System.Classes, uDWDatamodule, uDWAbout,
  uRESTDWServerEvents, uDWJSONObject;

type
  TdmRotas = class(TServerMethodDataModule)
    dweRotasGet: TDWServerEvents;
    procedure dweRotasGetEventsprodutosReplyEvent(var Params: TDWParams;
      var Result: string);
    procedure dweRotasGetEventsitensExtraReplyEvent(var Params: TDWParams;
      var Result: string);
    procedure dweRotasGetEventscategoriasReplyEvent(var Params: TDWParams;
      var Result: string);
    procedure dweRotasGetEventspizzaTipoReplyEvent(var Params: TDWParams;
      var Result: string);
    procedure dweRotasGetEventspizzaTamanhoReplyEvent(
      var Params: TDWParams; var Result: string);
    procedure dweRotasGetEventsestabelecimentoReplyEvent(
      var Params: TDWParams; var Result: string);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmRotas: TdmRotas;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

uses uProdutoRepository, uItemExtraRepository, uCategoriaRepository,
  uPizzasConfig, Estabelecimento;

{$R *.dfm}

procedure TdmRotas.dweRotasGetEventscategoriasReplyEvent(
  var Params: TDWParams; var Result: string);
var
  repo: TCategoriaRepository;
begin
  repo:= TCategoriaRepository.Create;
  try
    result:= repo.GetCategoriasJson;
  finally
    repo.Free;
  end;
end;

procedure TdmRotas.dweRotasGetEventsestabelecimentoReplyEvent(
  var Params: TDWParams; var Result: string);
var
  esta: TEstabelecimento;
begin
  esta:= TEstabelecimento.Create;
  try
    esta.Carregar;
    Result:= esta.GetEstabelecimentoJson;
  finally
    esta.Free;
  end;
end;

procedure TdmRotas.dweRotasGetEventsitensExtraReplyEvent(
  var Params: TDWParams; var Result: string);
var
  repo: TItemExtraRepository;
begin
  repo:= TItemExtraRepository.Create();
  try
    result:= repo.GetItensExtraJson;
  finally
    repo.free;
  end;

end;

procedure TdmRotas.dweRotasGetEventspizzaTamanhoReplyEvent(
  var Params: TDWParams; var Result: string);
var
  pizzaConfig: TPizzasConfig;
begin
  pizzaConfig:= TPizzasConfig.Create;
  try
    pizzaConfig.Carregar;
    Result:= pizzaConfig.GetCobrancaTamanhoJson;
  finally
    pizzaConfig.Free;
  end;
end;

procedure TdmRotas.dweRotasGetEventspizzaTipoReplyEvent(
  var Params: TDWParams; var Result: string);
var
  pizzaConfig: TPizzasConfig;
begin
  pizzaConfig:= TPizzasConfig.Create;
  try
    pizzaConfig.Carregar;
    Result:= pizzaConfig.GetCobrancaTipoJson;
  finally
    pizzaConfig.Free;
  end;
end;

procedure TdmRotas.dweRotasGetEventsprodutosReplyEvent(
  var Params: TDWParams; var Result: string);
var
  repo: TProdutoRepository;
begin
  repo:= TProdutoRepository.Create();
  try
    result:= repo.GetProdutosJson;
  finally
    repo.Free;

  end;
end;

end.
