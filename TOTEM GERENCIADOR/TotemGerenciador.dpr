program TotemGerenciador;

uses
  System.StartUpCopy,
  FMX.Forms,
  TratandoErro,
  ufrmPrincipal in 'View\ufrmPrincipal.pas' {formPrincipal},
  Categoria in 'Model\Estoque\Categoria.pas',
  Item in 'Model\Estoque\Item.pas',
  ItemExtra in 'Model\Estoque\ItemExtra.pas',
  ItemExtraCategoia in 'Model\Estoque\ItemExtraCategoia.pas',
  Produto in 'Model\Estoque\Produto.pas',
  Unidade in 'Model\Estoque\Unidade.pas',
  ValorEspecial in 'Model\Estoque\ValorEspecial.pas',
  Cliente in 'Model\Sistema\Cliente.pas',
  Contato in 'Model\Sistema\Contato.pas',
  Endereco in 'Model\Sistema\Endereco.pas',
  Totem in 'Model\Sistema\Totem.pas',
  COFINS in 'Model\Tributação\COFINS.pas',
  COFINSST in 'Model\Tributação\COFINSST.pas',
  ICMS in 'Model\Tributação\ICMS.pas',
  Impostos in 'Model\Tributação\Impostos.pas',
  ISSQN in 'Model\Tributação\ISSQN.pas',
  PIS in 'Model\Tributação\PIS.pas',
  PISST in 'Model\Tributação\PISST.pas',
  TaxasFiscais in 'Model\Tributação\TaxasFiscais.pas',
  uDmCon in 'uDmCon.pas' {dmCon: TDataModule},
  uContextoDB in 'Repository\uContextoDB.pas',
  uClienteRepository in 'Repository\uClienteRepository.pas',
  uTotemRepository in 'Repository\uTotemRepository.pas',
  uCategoriaRepository in 'Repository\uCategoriaRepository.pas',
  uTributacaoRepository in 'Repository\uTributacaoRepository.pas',
  uItemExtraRepository in 'Repository\uItemExtraRepository.pas',
  uProdutoRepository in 'Repository\uProdutoRepository.pas',
  uGridData in 'uGridData.pas',
  uDmServe in 'Server\uDmServe.pas' {dmServe: TDataModule},
  uDmRotas in 'Server\uDmRotas.pas' {dmRotas: TDataModule},
  uPizzasConfig in 'Model\Estoque\uPizzasConfig.pas',
  GerenciadorTotem in 'Model\Sistema\GerenciadorTotem.pas',
  Estabelecimento in 'Model\Sistema\Estabelecimento.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TdmCon, dmCon);
  Application.CreateForm(TformPrincipal, formPrincipal);
  Application.CreateForm(TdmServe, dmServe);
  Application.Run;
end.
