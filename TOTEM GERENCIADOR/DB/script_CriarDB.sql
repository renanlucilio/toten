-- Create a new database called '[Totem_GERENCIADOR]'
-- Pasta tem que está criada já
USE master
GO
IF NOT EXISTS (
    SELECT name
FROM sys.databases
WHERE name = N'Totem_GERENCIADOR'
)
CREATE DATABASE [Totem_GERENCIADOR] ON PRIMARY (
     NAME = N'Totem_GERENCIADOR',
     FILENAME = N'C:\SICLOP\Totem_GERENCIADOR\DB\Totem_GERENCIADOR.mdf',
     SIZE = 8192KB ,
     FILEGROWTH = 65536KB 
) LOG ON (
     NAME = N'Totem_GERENCIADOR_log', 
     FILENAME = N'C:\SICLOP\Totem_GERENCIADOR\DB\Totem_GERENCIADOR_log.ldf' , 
     SIZE = 8192KB , 
     FILEGROWTH = 65536KB )
GO

-- Usando o banco de dados
USE [Totem_GERENCIADOR];
GO

-- Create a new table called '[Categoria]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[Categoria]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[Categoria_ItemExtra] DROP CONSTRAINT FK_CategoriaItemExtra_Categoria;
     ALTER TABLE [DBO].[Produto] DROP CONSTRAINT FK_Produto_Categoria;
     DROP TABLE [DBO].[Categoria];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[Categoria]
(
     [ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [Descricao] [varchar](150) NOT NULL,
     [LocalDoLogo] [varchar](200)
);
GO

-- Create a new table called '[ICMS]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[ICMS]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[Imposto] DROP CONSTRAINT FK_Imposto_ICMS;
     DROP TABLE [DBO].[ICMS];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[ICMS]
(
     [ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [Origem] [tinyint] NOT NULL,
     [CST] [varchar](40) NOT NULL,
     [Porcento] [real],
     [Valor] [money]
);
GO

-- Create a new table called '[COFINS]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[COFINS]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[Imposto] DROP CONSTRAINT FK_Imposto_COFINS;
     DROP TABLE [DBO].[COFINS];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[COFINS]
(
     [ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [CST] [varchar](40) NOT NULL,
     [BaseCalculo] [money],
     [Porcento] [real],
     [Valor] [money],
     [BaseCalculoProduto] [money],
     [Aliquota] [real]
);
GO

-- Create a new table called '[PIS]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[PIS]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[Imposto] DROP CONSTRAINT FK_Imposto_PIS;
     DROP TABLE [DBO].[PIS];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[PIS]
(
     [PIS_ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [CST] [varchar](40) NOT NULL,
     [BaseCalculo] [money],
     [Porcento] [real],
     [Valor] [money],
     [BaseCalculoProduto] [money],
     [Aliquota] [real]
);
GO

-- Create a new table called '[COFINSST]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[COFINSST]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[Imposto] DROP CONSTRAINT FK_Imposto_COFINSST;
     DROP TABLE [DBO].[COFINSST];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[COFINSST]
(
     [ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [BaseCalculo] [money],
     [Porcento] [real],
     [Valor] [money],
     [BaseCalculoProduto] [money],
     [Aliquota] [real]
);
GO

IF OBJECT_ID('[DBO].[ISSQN]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[Imposto] DROP CONSTRAINT FK_Imposto_ISSQN;
     DROP TABLE [DBO].[ISSQN];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[ISSQN]
(
     [ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [BaseCalculo] [money],
     [Deducao] [money],
     [CodigoIbge ] [varchar](50),
     [BaseCalculoProduto] [money],
     [ItemDaListaServico] [varchar](50),
     [CodigoTributacaoMunicipio] [varchar](50),
     [NaturezaOperacao] [tinyint],
     [Aliquota] [real]
);
GO

-- Create a new table called '[PISST]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[PISST]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[Imposto] DROP CONSTRAINT FK_Imposto_PISST;
     DROP TABLE [DBO].[PISST];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[PISST]
(
     [ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [BaseCalculo] [money],
     [Porcento] [real],
     [Valor] [money],
     [BaseCalculoProduto] [money],
     [Aliquota] [real]
);
GO

-- Create a new table called '[Imposto]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[Imposto]', 'U') IS NOT NULL 
BEGIN
     ALTER TABLE [DBO].[Produto] DROP CONSTRAINT FK_Produto_Imposto;
     ALTER TABLE [DBO].[ItemExtra] DROP CONSTRAINT FK_ITEMEXTRA_Imposto;
     DROP TABLE [DBO].[Imposto];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[Imposto]
(
     [IMP_ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [ICMS_ID] [bigint] NOT NULL,
     [PIS_ID] [bigint] NOT NULL,
     [ISSQN_ID] [bigint] NOT NULL,
     [COFINS_ID] [bigint] NOT NULL,
     [PISST_ID] [bigint] NOT NULL,
     [COFINSST_ID] [bigint] NOT NULL,
     [Descricao] [varchar](150),
     [TaxaFederal] [money],
     [TaxaEstadual] [money],
);
GO

-- Create a new table called '[ItemExtra]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[ItemExtra]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[TotemProduto] DROP CONSTRAINT FK_TotemProduto_ITEMEXTRA;
     ALTER TABLE [DBO].[Categoria_ItemExtra] DROP CONSTRAINT FK_CategoriaITEMEXTRA_ITEMEXTRA;
     DROP TABLE [DBO].[ItemExtra];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[ItemExtra]
(
     [ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [Descricao] [varchar(200)] NOT NULL
);
GO

-- Create a new table called '[Categoria_ItemExtra]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[Categoria_ItemExtra]', 'U') IS NOT NULL
BEGIN
     DROP TABLE [DBO].[Categoria_ItemExtra];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[Categoria_ItemExtra]
(
     [ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [ITE_ID] [bigint] NOT NULL,
     [CAT_ID] [bigint] NOT NULL
);
GO

-- Create a new table called '[Unidade]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[Unidade]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[Produto] DROP CONSTRAINT FK_Produto_Unidade;
     DROP TABLE [DBO].[Unidade];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[Unidade]
(
     [ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [Descricao] [varchar](150) NOT NULL,
     [Sigla] [varchar](5) NOT NULL,
     [TemDecimal] [bit] NOT NULL
);
GO

-- Create a new table called '[ValorProduto]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[ValorProduto]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[Produto] DROP CONSTRAINT FK_Produto_ValorProduto;
     DROP TABLE [DBO].[ValorProduto];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[ValorProduto]
(
     [ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [Pequeno] [money],
     [Medio] [money],
     [Gigante] [money],
     [Normal] [money] NOT NULL
);
GO

-- Create a new table called '[Produto]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[Produto]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[OPCIONAL] DROP CONSTRAINT FK_OPCIONAL_Produto;
     ALTER TABLE [DBO].[INGREDIENTE] DROP CONSTRAINT FK_INGREDIENTE_Produto;
     ALTER TABLE [DBO].[TotemProduto] DROP CONSTRAINT FK_TotemProduto_Produto;
     DROP TABLE [DBO].[Produto];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[Produto]
(
     [ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [Descricao] [varchar](150) NOT NULL,
     [CAT_ID] [bigint] NOT NULL,
     [IMP_ID] [bigint],
     [UNI_ID] [bigint] NOT NULL,
     [CodigoBarras] [varchar](150),
     [Ingrediente] [varchar](300) NOT NULL,
     [VP_ID] [bigint] NOT NULL,
     [NCM] [varchar](30),
     [CEST] [varchar](30),
     [CFOP] [varchar](30)
);
GO

-- Create a new table called '[Totem]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[Totem]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[Totem] DROP CONSTRAINT FK_Totem_Totem;
     DROP TABLE [DBO].[Totem];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[Totem]
(
     [TOT_ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [LOCAL] [varchar](200) NOT NULL,
     [NOME] [varchar](150) NOT NULL,
     [IP] [varchar](30) NOT NULL
);
GO

-- Create a new table called '[ENDERECO]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[ENDERECO]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[ESTABELECIMENTO] DROP CONSTRAINT FK_ESTABELECIMENTO_ENDERECO;
     ALTER TABLE [DBO].[Totem] DROP CONSTRAINT FK_Totem_ENDERECO;
     ALTER TABLE [DBO].[CLIENTE] DROP CONSTRAINT FK_CLIENTE_ENDERECO;
     DROP TABLE [DBO].[ENDERECO];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[ENDERECO]
(
     [END_ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [LOGRADOURO] [varchar](200) NOT NULL,
     [NUMERO] [varchar](100) NOT NULL,
     [BAIRRO] [varchar](150) NOT NULL,
     [CIDADE] [varchar](150) NOT NULL,
     [IBGE] [varchar](100) ,
     [GIA] [varchar](100) ,
     [UF] [varchar](2) NOT NULL,
     [CEP] [varchar](10) NOT NULL,
     [COMPLEMENTO] [varchar](150) ,
     [REFERENCIA] [varchar](150)
);
GO

-- Create a new table called '[CONTATO]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[CONTATO]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[CLIENTE] DROP CONSTRAINT FK_CLIENTE_CONTATO;
     ALTER TABLE [DBO].[ESTABELECIMENTO] DROP CONSTRAINT FK_ESTABELECIMENTO_CONTATO;
     DROP TABLE [DBO].[CONTATO];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[CONTATO]
(
     [CTT_ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [CONTATO] [varchar](150) NOT NULL,
     [TIPO] [varchar](70) NOT NULL
);
GO

-- Create a new table called '[CLIENTE]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[CLIENTE]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[Totem] DROP CONSTRAINT FK_Totem_CLIENTE;
     DROP TABLE [DBO].[CLIENTE];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[CLIENTE]
(
     [CLI_ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [NOME] [varchar](150) NOT NULL,
     [DOCUMENTO] [varchar](150) NOT NULL,
     [END_ID] [bigint],
     [CTT_ID] [bigint]
);
GO

-- Create a new table called '[Totem]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[Totem]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[TotemProduto] DROP CONSTRAINT FK_TotemProduto_Totem;
     ALTER TABLE [DBO].[VENDA] DROP CONSTRAINT FK_VENDA_Totem;
     DROP TABLE [DBO].[Totem];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[Totem]
(
     [PED_ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [Valor] [money] NOT NULL,
     [DATA] [date] NOT NULL,
     [HORA] [time] NOT NULL,
     [CLI_ID] [bigint],
     [END_ID] [bigint],
     [TOT_ID] [bigint] NOT NULL
);
GO

-- Create a new table called '[TotemProduto]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[TotemProduto]', 'U') IS NOT NULL
BEGIN
     DROP TABLE [DBO].[TotemProduto];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[TotemProduto]
(
     [PP_ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [ITE_ID] [bigint],
     [PRO_ID] [bigint] NOT NULL,
     [Valor] [money] NOT NULL,
     [QUANTIDADE] [real] NOT NULL,
     [OPC_ID] [bigint],
     [PED_ID] [bigint] NOT NULL
);
GO

-- Create a new table called '[ESTABELECIMENTO]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[ESTABELECIMENTO]', 'U') IS NOT NULL
BEGIN
     DROP TABLE [DBO].[ESTABELECIMENTO];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[ESTABELECIMENTO]
(
     [EST_ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [END_ID] [bigint],
     [CNPJ] [varchar](30) NOT NULL,
     [ABERTO] [bit] NOT NULL,
     [CTT_ID] [bigint]
);
GO

-- Create a new table called '[PAGAMENTO]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[PAGAMENTO]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[VENDA] DROP CONSTRAINT FK_VENDA_PAGAMENTO;
     DROP TABLE [DBO].[PAGAMENTO];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[PAGAMENTO]
(
     [PAG_ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [TIPO] [varchar](150) NOT NULL,
     [Valor] [money] NOT NULL,
     [PAGO] [bit] NOT NULL,
     [TROCO] [money]
);
GO

-- Create a new table called '[PERMISSAO]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[PERMISSAO]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[USUARIO] DROP CONSTRAINT FK_USUARIO_PERMISSAO;
     ALTER TABLE [DBO].[SISTEMA] DROP CONSTRAINT FK_SISTEMA_PERMISSAO;
     DROP TABLE [DBO].[PERMISSAO];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[PERMISSAO]
(
     [PER_ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [Descricao] [varchar](150) NOT NULL,
     [NIVEL] [tinyint] NOT NULL
);
GO

-- Create a new table called '[USUARIO]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[USUARIO]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[VENDA] DROP CONSTRAINT FK_VENDA_USUARIO;
     DROP TABLE [DBO].[USUARIO];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[USUARIO]
(
     [USU_ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [NOME] [varchar](100) NOT NULL,
     [LOGIN] [varchar](100) NOT NULL,
     [SENHA] [varchar](100) NOT NULL,
     [PER_ID] [bigint] NOT NULL
);
GO


-- Create a new table called '[PERIODO]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[PERIODO]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[CAIXA] DROP CONSTRAINT FK_CAIXA_PERIODO;
     DROP TABLE [DBO].[PERIODO]
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[PERIODO]
(
     [PEI_ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [Descricao] [varchar](100) NOT NULL,
     [HORARIO DE COMECO] [time] NOT NULL,
     [HORARIO DE FIM] [time] NOT NULL,
);
GO


-- Create a new table called '[CAIXA]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[CAIXA]', 'U') IS NOT NULL
BEGIN
     ALTER TABLE [DBO].[VENDA] DROP CONSTRAINT FK_VENDA_CAIXA;
     DROP TABLE [DBO].[CAIXA];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[CAIXA]
(
     [CAI_ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [PEI_ID] [bigint] NOT NULL,
     [DATA HORA ABERTURA] [datetime] NOT NULL,
     [DATA HORA FECHAMENTO] [datetime] NOT NULL
);
GO

-- Create a new table called '[VENDA]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[VENDA]', 'U') IS NOT NULL
BEGIN
     DROP TABLE [DBO].[VENDA];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[VENDA]
(
     [VEN_ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [CODIGO] [varchar](150) NOT NULL,
     [NUMERO DA VENDA][int] NOT NULL,
     [PAG_ID] [bigint] NOT NULL,
     [CAI_ID] [bigint] NOT NULL,
     [PED_ID] [bigint] NOT NULL,
     [EH NOTA FISCAL] [bit] NOT NULL,
     [USU_ID] [bigint] NOT NULL,
     UNIQUE([CODIGO])
);
GO

-- Create a new table called '[SISTEMA]' in schema '[DBO]'
-- Drop the table if it already exists
IF OBJECT_ID('[DBO].[SISTEMA]', 'U') IS NOT NULL
BEGIN
     DROP TABLE [DBO].[SISTEMA];
END
GO
-- Create the table in the specified schema
CREATE TABLE [DBO].[SISTEMA]
(
     [SIS_ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
     [FUNCIONALIDADE] [varchar](150) NOT NULL,
     [PER_ID] [bigint] NOT NULL
);
GO

GO
ALTER TABLE [DBO].[Imposto] 
ADD CONSTRAINT 
     FK_Imposto_ICMS FOREIGN KEY ([ICMS_ID])     
     REFERENCES [ICMS] ([ICMS_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE    
GO  

GO
ALTER TABLE [DBO].[Imposto] 
ADD CONSTRAINT 
     FK_Imposto_PIS FOREIGN KEY ([PIS_ID])     
     REFERENCES [PIS] ([PIS_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE   
GO  

GO
ALTER TABLE [DBO].[Imposto] 
ADD CONSTRAINT 
     FK_Imposto_ISSQN FOREIGN KEY ([ISSQN_ID])     
     REFERENCES [ISSQN] ([ISSQN_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE   
GO  

GO
ALTER TABLE [DBO].[Imposto] 
ADD CONSTRAINT 
     FK_Imposto_COFINS FOREIGN KEY ([COFINS_ID])     
     REFERENCES [COFINS] ([COFINS_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE 
GO  

GO
ALTER TABLE [DBO].[Imposto] 
ADD CONSTRAINT 
     FK_Imposto_PISST FOREIGN KEY ([PISST_ID])     
     REFERENCES [PISST] ([PISST_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE
GO  

GO
ALTER TABLE [DBO].[Imposto] 
ADD CONSTRAINT 
     FK_Imposto_COFINSST FOREIGN KEY ([COFINSST_ID])     
     REFERENCES [COFINSST] ([COFINSST_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE
GO       

GO
ALTER TABLE [DBO].[ItemExtra] 
ADD CONSTRAINT 
     FK_ITEMEXTRA_Imposto FOREIGN KEY ([IMP_ID])     
     REFERENCES [Imposto] ([IMP_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE
GO    

GO
ALTER TABLE [DBO].[Categoria_ItemExtra] 
ADD CONSTRAINT 
     FK_CategoriaITEMEXTRA_ITEMEXTRA FOREIGN KEY ([ITE_ID])     
     REFERENCES [ItemExtra] ([ITE_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE
GO   

GO
ALTER TABLE [DBO].[Categoria_ItemExtra] 
ADD CONSTRAINT 
     FK_CategoriaITEMEXTRA_Categoria FOREIGN KEY ([CAT_ID])     
     REFERENCES [Categoria] ([CAT_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE
GO    

GO
ALTER TABLE [DBO].[Produto] 
ADD CONSTRAINT 
     FK_Produto_Categoria FOREIGN KEY ([CAT_ID])     
     REFERENCES [Categoria] ([CAT_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE
GO

GO
ALTER TABLE [DBO].[Produto] 
ADD CONSTRAINT 
     FK_Produto_Imposto FOREIGN KEY ([IMP_ID])     
     REFERENCES [Imposto] ([IMP_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE
GO

GO
ALTER TABLE [DBO].[Produto] 
ADD CONSTRAINT 
     FK_Produto_Unidade FOREIGN KEY ([UNI_ID])     
     REFERENCES [Unidade] ([UNI_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE
GO     

GO
ALTER TABLE [DBO].[Produto] 
ADD CONSTRAINT 
     FK_Produto_ValorProduto FOREIGN KEY ([VP_ID])     
     REFERENCES [ValorProduto] ([VP_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE
GO    

GO
ALTER TABLE [DBO].[INGREDIENTE] 
ADD CONSTRAINT 
     FK_INGREDIENTE_Produto FOREIGN KEY ([PRO_ID])     
     REFERENCES [Produto] ([PRO_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE
GO  

GO
ALTER TABLE [DBO].[OPCIONAL] 
ADD CONSTRAINT 
     FK_OPCIONAL_Produto FOREIGN KEY ([PRO_ID])     
     REFERENCES [Produto] ([PRO_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE
GO  

GO
ALTER TABLE [DBO].[CLIENTE] 
ADD CONSTRAINT 
     FK_CLIENTE_ENDERECO FOREIGN KEY ([END_ID])     
     REFERENCES [ENDERECO] ([END_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE
GO  

GO
ALTER TABLE [DBO].[CLIENTE] 
ADD CONSTRAINT 
     FK_CLIENTE_CONTATO FOREIGN KEY ([CTT_ID])     
     REFERENCES [CONTATO] ([CTT_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE
GO  

GO
ALTER TABLE [DBO].[Totem] 
ADD CONSTRAINT 
     FK_Totem_CLIENTE FOREIGN KEY ([CLI_ID])     
     REFERENCES [CLIENTE] ([CLI_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE
GO  

GO
ALTER TABLE [DBO].[Totem] 
ADD CONSTRAINT 
     FK_Totem_ENDERECO FOREIGN KEY ([END_ID])     
     REFERENCES [ENDERECO] ([END_ID])     
GO  

GO
ALTER TABLE [DBO].[Totem] 
ADD CONSTRAINT 
     FK_Totem_Totem FOREIGN KEY ([TOT_ID])     
     REFERENCES [Totem] ([TOT_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE   
GO  

GO
ALTER TABLE [DBO].[TotemProduto] 
ADD CONSTRAINT 
     FK_TotemProduto_ITEMEXTRA FOREIGN KEY ([ITE_ID])     
     REFERENCES [ItemExtra] ([ITE_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE    
GO  

GO
ALTER TABLE [DBO].[TotemProduto] 
ADD CONSTRAINT 
     FK_TotemProduto_Produto FOREIGN KEY ([PRO_ID])     
     REFERENCES [Produto] ([PRO_ID])     
GO 

GO
ALTER TABLE [DBO].[TotemProduto] 
ADD CONSTRAINT 
     FK_TotemProduto_OPCIONAL FOREIGN KEY ([OPC_ID])     
     REFERENCES [OPCIONAL] ([OPC_ID])     
GO  

GO
ALTER TABLE [DBO].[TotemProduto] 
ADD CONSTRAINT 
     FK_TotemProduto_Totem FOREIGN KEY ([PED_ID])     
     REFERENCES [Totem] ([PED_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE      
GO 

GO
ALTER TABLE [DBO].[ESTABELECIMENTO] 
ADD CONSTRAINT 
     FK_ESTABELECIMENTO_ENDERECO FOREIGN KEY ([END_ID])     
     REFERENCES [ENDERECO] ([END_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE        
GO         

GO
ALTER TABLE [DBO].[ESTABELECIMENTO] 
ADD CONSTRAINT 
     FK_ESTABELECIMENTO_CONTATO FOREIGN KEY ([CTT_ID])     
     REFERENCES [CONTATO] ([CTT_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE        
GO         


GO
ALTER TABLE [DBO].[USUARIO] 
ADD CONSTRAINT 
     FK_USUARIO_PERMISSAO FOREIGN KEY ([PER_ID])     
     REFERENCES [PERMISSAO] ([PER_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE         
GO 

GO
ALTER TABLE [DBO].[VENDA] 
ADD CONSTRAINT 
     FK_VENDA_PAGAMENTO FOREIGN KEY ([PAG_ID])     
     REFERENCES [PAGAMENTO] ([PAG_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE         
GO 

GO
ALTER TABLE [DBO].[VENDA] 
ADD CONSTRAINT 
     FK_VENDA_Totem FOREIGN KEY ([PED_ID])     
     REFERENCES [Totem] ([PED_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE         
GO 

GO
ALTER TABLE [DBO].[VENDA] 
ADD CONSTRAINT 
     FK_VENDA_CAIXA FOREIGN KEY ([CAI_ID])     
     REFERENCES [CAIXA] ([CAI_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE      
GO 

GO
ALTER TABLE [DBO].[VENDA] 
ADD CONSTRAINT 
     FK_VENDA_USUARIO FOREIGN KEY ([USU_ID])     
     REFERENCES [USUARIO] ([USU_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE        
GO  

GO
ALTER TABLE [DBO].[SISTEMA] 
ADD CONSTRAINT 
     FK_SISTEMA_PERMISSAO FOREIGN KEY ([PER_ID])     
     REFERENCES [PERMISSAO] ([PER_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE        
GO  

GO
ALTER TABLE [DBO].[CAIXA] 
ADD CONSTRAINT 
     FK_CAIXA_PERIODO FOREIGN KEY ([PEI_ID])     
     REFERENCES [PERIODO] ([PEI_ID])     
     ON DELETE CASCADE    
     ON UPDATE CASCADE        
GO  