USE [master]
GO
/****** Object:  Database [TOTEM_GERENCIADOR]    Script Date: 12/04/2019 08:58:57 ******/
IF NOT EXISTS (
	SELECT name
FROM sys.databases
WHERE name = N'TOTEM_GERENCIADOR'
)
	CREATE DATABASE [TOTEM_GERENCIADOR]
	CONTAINMENT = NONE
	ON  PRIMARY 
	( NAME = N'TOTEM_GERENCIADOR', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\TOTEM_GERENCIADOR.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
	LOG ON 
	( NAME = N'TOTEM_GERENCIADOR_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\TOTEM_GERENCIADOR_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO

ALTER DATABASE [TOTEM_GERENCIADOR] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
	EXEC [TOTEM_GERENCIADOR].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET ARITHABORT OFF 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET  ENABLE_BROKER 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET  MULTI_USER 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET QUERY_STORE = OFF
GO
USE [TOTEM_GERENCIADOR]
GO


/****** Object:  User [root]    Script Date: 12/04/2019 08:58:57 ******/
IF NOT EXISTS (
	SELECT name
FROM sys.syslogins
WHERE name = N'root')
	CREATE USER [root] FOR LOGIN [root] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[Categoria]    Script Date: 12/04/2019 08:58:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.Categoria', 'U') IS NULL
CREATE TABLE [dbo].[Categoria]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Descricao] [nvarchar](450) NULL,
	[LocalLogo] [nvarchar](max) NULL,
	[Status] [int] NOT NULL,
	CONSTRAINT [PK_Categoria] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 12/04/2019 08:58:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.Cliente', 'U') IS NULL
CREATE TABLE [dbo].[Cliente]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nome] [nvarchar](max) NULL,
	[Documento] [nvarchar](max) NULL,
	[EnderecoID] [int] NOT NULL,
	[ContatoID] [int] NOT NULL,
	CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[COFINS]    Script Date: 12/04/2019 08:58:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.COFINS', 'U') IS NULL
CREATE TABLE [dbo].[COFINS]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BaseCalculo] [decimal](18, 2) NOT NULL,
	[Porcento] [float] NOT NULL,
	[Valor] [decimal](18, 2) NOT NULL,
	[BaseCalculoPorProduto] [decimal](18, 2) NOT NULL,
	[Aliquota] [float] NOT NULL,
	[CST] [int] NOT NULL,
	CONSTRAINT [PK_COFINS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[COFINSST]    Script Date: 12/04/2019 08:58:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.COFINSST', 'U') IS NULL
CREATE TABLE [dbo].[COFINSST]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BaseCalculo] [decimal](18, 2) NOT NULL,
	[Porcento] [float] NOT NULL,
	[Valor] [decimal](18, 2) NOT NULL,
	[BaseCalculoPorProduto] [decimal](18, 2) NOT NULL,
	[Aliquota] [float] NOT NULL,
	CONSTRAINT [PK_COFINSST] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contato]    Script Date: 12/04/2019 08:58:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.Contato', 'U') IS NULL
CREATE TABLE [dbo].[Contato]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OhContato] [nvarchar](max) NULL,
	[Tipo] [nvarchar](max) NULL,
	CONSTRAINT [PK_Contato] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Endereco]    Script Date: 12/04/2019 08:58:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.Endereco', 'U') IS NULL
CREATE TABLE [dbo].[Endereco]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Logradouro] [nvarchar](max) NULL,
	[Numero] [nvarchar](max) NULL,
	[Bairro] [nvarchar](max) NULL,
	[Cidade] [nvarchar](max) NULL,
	[UF] [nvarchar](max) NULL,
	[CEP] [nvarchar](max) NULL,
	[Complemento] [nvarchar](max) NULL,
	[Referencia] [nvarchar](max) NULL,
	CONSTRAINT [PK_Endereco] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ICMS]    Script Date: 12/04/2019 08:58:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.ICMS', 'U') IS NULL
CREATE TABLE [dbo].[ICMS]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Origem] [int] NOT NULL,
	[CST] [int] NOT NULL,
	[Porcento] [float] NOT NULL,
	[Valor] [decimal](18, 2) NOT NULL,
	CONSTRAINT [PK_ICMS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Impostos]    Script Date: 12/04/2019 08:58:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.Impostos', 'U') IS NULL
CREATE TABLE [dbo].[Impostos]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Descricao] [nvarchar](max) NULL,
	[ICMSID] [int] NOT NULL,
	[PISID] [int] NOT NULL,
	[COFINSID] [int] NOT NULL,
	[PISSTID] [int] NOT NULL,
	[COFINSSTID] [int] NOT NULL,
	[ISSQNID] [int] NOT NULL,
	[TaxasID] [int] NOT NULL,
	CONSTRAINT [PK_Impostos] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ISSQN]    Script Date: 12/04/2019 08:58:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.ISSQN', 'U') IS NULL
CREATE TABLE [dbo].[ISSQN]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BaseCalculo] [decimal](18, 2) NOT NULL,
	[Deducao] [decimal](18, 2) NOT NULL,
	[CodigoIBGE] [nvarchar](max) NULL,
	[BaseCalculoProduto] [decimal](18, 2) NOT NULL,
	[ItemDaListaServico] [nvarchar](max) NULL,
	[CodigoTributarioISSQN] [nvarchar](max) NULL,
	[Natureza] [int] NOT NULL,
	CONSTRAINT [PK_ISSQN] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Item]    Script Date: 12/04/2019 08:58:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.Item', 'U') IS NULL
CREATE TABLE [dbo].[Item]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	CONSTRAINT [PK_Item] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ItemExtra]    Script Date: 12/04/2019 08:58:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.ItemExtra', 'U') IS NULL
CREATE TABLE [dbo].[ItemExtra]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Descricao] [nvarchar](max) NULL,
	[Valor] [float] NOT NULL,
	[Status] [int] NOT NULL,
	CONSTRAINT [PK_ItemExtra] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ItemExtraCategoia]    Script Date: 12/04/2019 08:58:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.ItemExtraCategoia', 'U') IS NULL
CREATE TABLE [dbo].[ItemExtraCategoia]
(
	[ItemExtraID] [int] NOT NULL,
	[CategoriaID] [int] NOT NULL,
	CONSTRAINT [PK_ItemExtraCategoia] PRIMARY KEY CLUSTERED 
(
	[CategoriaID] ASC,
	[ItemExtraID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PIS]    Script Date: 12/04/2019 08:58:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.PIS', 'U') IS NULL
CREATE TABLE [dbo].[PIS]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BaseCalculo] [decimal](18, 2) NOT NULL,
	[Porcento] [float] NOT NULL,
	[Valor] [decimal](18, 2) NOT NULL,
	[BaseCalculoPorProduto] [decimal](18, 2) NOT NULL,
	[Aliquota] [float] NOT NULL,
	[CST] [int] NOT NULL,
	CONSTRAINT [PK_PIS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PISST]    Script Date: 12/04/2019 08:58:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.PISST', 'U') IS NULL
CREATE TABLE [dbo].[PISST]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BaseCalculo] [decimal](18, 2) NOT NULL,
	[Porcento] [float] NOT NULL,
	[Valor] [decimal](18, 2) NOT NULL,
	[BaseCalculoPorProduto] [decimal](18, 2) NOT NULL,
	[Aliquota] [float] NOT NULL,
	CONSTRAINT [PK_PISST] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Produto]    Script Date: 12/04/2019 08:58:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.Produto', 'U') IS NULL
CREATE TABLE [dbo].[Produto]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Descricao] [nvarchar](max) NULL,
	[CategoriaID] [int] NOT NULL,
	[ImpostosID] [int] NULL,
	[UnidadeID] [int] NOT NULL,
	[CodigoBarras] [nvarchar](max) NULL,
	[ValorID] [int] NOT NULL,
	[NCM] [nvarchar](max) NULL,
	[CEST] [nvarchar](max) NULL,
	[CFOP] [nvarchar](max) NULL,
	[Ingredientes] [nvarchar](max) NULL,
	[Status] [int] NOT NULL,
	CONSTRAINT [PK_Produto] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TaxasFiscais]    Script Date: 12/04/2019 08:58:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.TaxasFiscais', 'U') IS NULL
CREATE TABLE [dbo].[TaxasFiscais]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TaxaEstadual] [decimal](18, 2) NOT NULL,
	[TaxaFederal] [decimal](18, 2) NOT NULL,
	CONSTRAINT [PK_TaxasFiscais] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Totem]    Script Date: 12/04/2019 08:58:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.Totem', 'U') IS NULL
CREATE TABLE [dbo].[Totem]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Local] [nvarchar](max) NULL,
	[Nome] [nvarchar](max) NULL,
	[Ip] [nvarchar](max) NULL,
	[Mapeamento] [nvarchar](max) NULL,
	[Status] [int] NOT NULL,
	CONSTRAINT [PK_Totem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Unidade]    Script Date: 12/04/2019 08:59:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.Unidade', 'U') IS NULL
CREATE TABLE [dbo].[Unidade]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Descricao] [nvarchar](max) NULL,
	[Sigla] [nvarchar](max) NULL,
	[TemDecimal] [bit] NOT NULL,
	CONSTRAINT [PK_Unidade] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ValorEspecial]    Script Date: 12/04/2019 08:59:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.ValorEspecial', 'U') IS NULL
CREATE TABLE [dbo].[ValorEspecial]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ValorPequeno] [decimal](18, 2) NOT NULL,
	[ValorMedia] [decimal](18, 2) NOT NULL,
	[ValorGigante] [decimal](18, 2) NOT NULL,
	[ValorNormal] [decimal](18, 2) NOT NULL,
	CONSTRAINT [PK_ValorEspecial] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Categoria_Descricao]    Script Date: 12/04/2019 08:59:00 ******/
IF 0 = (SELECT COUNT(*) as index_count
FROM sys.indexes
WHERE object_id = OBJECT_ID('dbo.Categoria')
	AND name='IX_Categoria_Descricao')
CREATE UNIQUE NONCLUSTERED INDEX [IX_Categoria_Descricao] ON [dbo].[Categoria]
(
	[Descricao] ASC
)
WHERE ([Descricao] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Cliente_ContatoID]    Script Date: 12/04/2019 08:59:00 ******/
IF 0 = (SELECT COUNT(*) as index_count
FROM sys.indexes
WHERE object_id = OBJECT_ID('dbo.Cliente')
	AND name='IX_Cliente_ContatoID')
CREATE NONCLUSTERED INDEX [IX_Cliente_ContatoID] ON [dbo].[Cliente]
(
	[ContatoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Cliente_EnderecoID]    Script Date: 12/04/2019 08:59:00 ******/
IF 0 = (SELECT COUNT(*) as index_count
FROM sys.indexes
WHERE object_id = OBJECT_ID('dbo.Cliente')
	AND name='IX_Cliente_EnderecoID')
CREATE NONCLUSTERED INDEX [IX_Cliente_EnderecoID] ON [dbo].[Cliente]
(
	[EnderecoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Impostos_COFINSID]    Script Date: 12/04/2019 08:59:00 ******/
IF 0 = (SELECT COUNT(*) as index_count
FROM sys.indexes
WHERE object_id = OBJECT_ID('dbo.Impostos')
	AND name='IX_Impostos_COFINSID')
CREATE NONCLUSTERED INDEX [IX_Impostos_COFINSID] ON [dbo].[Impostos]
(
	[COFINSID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Impostos_COFINSSTID]    Script Date: 12/04/2019 08:59:00 ******/
IF 0 = (SELECT COUNT(*) as index_count
FROM sys.indexes
WHERE object_id = OBJECT_ID('dbo.Impostos')
	AND name='IX_Impostos_COFINSSTID')
CREATE NONCLUSTERED INDEX [IX_Impostos_COFINSSTID] ON [dbo].[Impostos]
(
	[COFINSSTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Impostos_ICMSID]    Script Date: 12/04/2019 08:59:00 ******/
IF 0 = (SELECT COUNT(*) as index_count
FROM sys.indexes
WHERE object_id = OBJECT_ID('dbo.Impostos')
	AND name='IX_Impostos_ICMSID')
CREATE NONCLUSTERED INDEX [IX_Impostos_ICMSID] ON [dbo].[Impostos]
(
	[ICMSID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Impostos_ISSQNID]    Script Date: 12/04/2019 08:59:00 ******/
IF 0 = (SELECT COUNT(*) as index_count
FROM sys.indexes
WHERE object_id = OBJECT_ID('dbo.Impostos')
	AND name='IX_Impostos_ISSQNID')
CREATE NONCLUSTERED INDEX [IX_Impostos_ISSQNID] ON [dbo].[Impostos]
(
	[ISSQNID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Impostos_PISID]    Script Date: 12/04/2019 08:59:00 ******/
IF 0 = (SELECT COUNT(*) as index_count
FROM sys.indexes
WHERE object_id = OBJECT_ID('dbo.Impostos')
	AND name='IX_Impostos_PISID')
CREATE NONCLUSTERED INDEX [IX_Impostos_PISID] ON [dbo].[Impostos]
(
	[PISID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Impostos_PISSTID]    Script Date: 12/04/2019 08:59:00 ******/
IF 0 = (SELECT COUNT(*) as index_count
FROM sys.indexes
WHERE object_id = OBJECT_ID('dbo.Impostos')
	AND name='IX_Impostos_PISSTID')
CREATE NONCLUSTERED INDEX [IX_Impostos_PISSTID] ON [dbo].[Impostos]
(
	[PISSTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Impostos_TaxasID]    Script Date: 12/04/2019 08:59:00 ******/
IF 0 = (SELECT COUNT(*) as index_count
FROM sys.indexes
WHERE object_id = OBJECT_ID('dbo.Impostos')
	AND name='IX_Impostos_TaxasID')
CREATE NONCLUSTERED INDEX [IX_Impostos_TaxasID] ON [dbo].[Impostos]
(
	[TaxasID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ItemExtraCategoia_ItemExtraID]    Script Date: 12/04/2019 08:59:00 ******/
IF 0 = (SELECT COUNT(*) as index_count
FROM sys.indexes
WHERE object_id = OBJECT_ID('dbo.ItemExtraCategoia')
	AND name='IX_ItemExtraCategoia_ItemExtraID')
CREATE NONCLUSTERED INDEX [IX_ItemExtraCategoia_ItemExtraID] ON [dbo].[ItemExtraCategoia]
(
	[ItemExtraID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Produto_CategoriaID]    Script Date: 12/04/2019 08:59:00 ******/
IF 0 = (SELECT COUNT(*) as index_count
FROM sys.indexes
WHERE object_id = OBJECT_ID('dbo.Produto')
	AND name='IX_Produto_CategoriaID')
CREATE NONCLUSTERED INDEX [IX_Produto_CategoriaID] ON [dbo].[Produto]
(
	[CategoriaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Produto_ImpostosID]    Script Date: 12/04/2019 08:59:00 ******/
IF 0 = (SELECT COUNT(*) as index_count
FROM sys.indexes
WHERE object_id = OBJECT_ID('dbo.Produto')
	AND name='IX_Produto_ImpostosID')
CREATE NONCLUSTERED INDEX [IX_Produto_ImpostosID] ON [dbo].[Produto]
(
	[ImpostosID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Produto_UnidadeID]    Script Date: 12/04/2019 08:59:00 ******/
IF 0 = (SELECT COUNT(*) as index_count
FROM sys.indexes
WHERE object_id = OBJECT_ID('dbo.Produto')
	AND name='IX_Produto_UnidadeID')
CREATE NONCLUSTERED INDEX [IX_Produto_UnidadeID] ON [dbo].[Produto]
(
	[UnidadeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Produto_ValorID]    Script Date: 12/04/2019 08:59:00 ******/
IF 0 = (SELECT COUNT(*) as index_count
FROM sys.indexes
WHERE object_id = OBJECT_ID('dbo.Produto')
	AND name='IX_Produto_ValorID')
CREATE NONCLUSTERED INDEX [IX_Produto_ValorID] ON [dbo].[Produto]
(
	[ValorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Cliente_Contato_ContatoID')
   AND parent_object_id = OBJECT_ID(N'dbo.Cliente')
)
ALTER TABLE [dbo].[Cliente]  WITH CHECK ADD  CONSTRAINT [FK_Cliente_Contato_ContatoID] FOREIGN KEY([ContatoID])
REFERENCES [dbo].[Contato] ([ID])
ON DELETE CASCADE
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Cliente_Contato_ContatoID')
   AND parent_object_id = OBJECT_ID(N'dbo.Cliente')
)
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_Contato_ContatoID]
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Cliente_Endereco_EnderecoID')
   AND parent_object_id = OBJECT_ID(N'dbo.Cliente')
)
ALTER TABLE [dbo].[Cliente]  WITH CHECK ADD  CONSTRAINT [FK_Cliente_Endereco_EnderecoID] FOREIGN KEY([EnderecoID])
REFERENCES [dbo].[Endereco] ([ID])
ON DELETE CASCADE
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Cliente_Endereco_EnderecoID')
   AND parent_object_id = OBJECT_ID(N'dbo.Cliente')
)
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_Endereco_EnderecoID]
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Impostos_COFINS_COFINSID')
   AND parent_object_id = OBJECT_ID(N'dbo.Impostos')
)
ALTER TABLE [dbo].[Impostos]  WITH CHECK ADD  CONSTRAINT [FK_Impostos_COFINS_COFINSID] FOREIGN KEY([COFINSID])
REFERENCES [dbo].[COFINS] ([ID])
ON DELETE CASCADE
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Impostos_COFINS_COFINSID')
   AND parent_object_id = OBJECT_ID(N'dbo.Impostos')
)
ALTER TABLE [dbo].[Impostos] CHECK CONSTRAINT [FK_Impostos_COFINS_COFINSID]
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Impostos_COFINSST_COFINSSTID')
   AND parent_object_id = OBJECT_ID(N'dbo.Impostos')
)
ALTER TABLE [dbo].[Impostos]  WITH CHECK ADD  CONSTRAINT [FK_Impostos_COFINSST_COFINSSTID] FOREIGN KEY([COFINSSTID])
REFERENCES [dbo].[COFINSST] ([ID])
ON DELETE CASCADE
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Impostos_COFINSST_COFINSSTID')
   AND parent_object_id = OBJECT_ID(N'dbo.Impostos')
)
ALTER TABLE [dbo].[Impostos] CHECK CONSTRAINT [FK_Impostos_COFINSST_COFINSSTID]
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Impostos_ICMS_ICMSID')
   AND parent_object_id = OBJECT_ID(N'dbo.Impostos')
)
ALTER TABLE [dbo].[Impostos]  WITH CHECK ADD  CONSTRAINT [FK_Impostos_ICMS_ICMSID] FOREIGN KEY([ICMSID])
REFERENCES [dbo].[ICMS] ([ID])
ON DELETE CASCADE
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Impostos_ICMS_ICMSID')
   AND parent_object_id = OBJECT_ID(N'dbo.Impostos')
)
ALTER TABLE [dbo].[Impostos] CHECK CONSTRAINT [FK_Impostos_ICMS_ICMSID]
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Impostos_ISSQN_ISSQNID')
   AND parent_object_id = OBJECT_ID(N'dbo.Impostos')
)
ALTER TABLE [dbo].[Impostos]  WITH CHECK ADD  CONSTRAINT [FK_Impostos_ISSQN_ISSQNID] FOREIGN KEY([ISSQNID])
REFERENCES [dbo].[ISSQN] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Impostos] CHECK CONSTRAINT [FK_Impostos_ICMS_ICMSID]
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Impostos_ISSQN_ISSQNID')
   AND parent_object_id = OBJECT_ID(N'dbo.Impostos')
)
ALTER TABLE [dbo].[Impostos] CHECK CONSTRAINT [FK_Impostos_ISSQN_ISSQNID]
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Impostos_PIS_PISID')
   AND parent_object_id = OBJECT_ID(N'dbo.Impostos')
)
ALTER TABLE [dbo].[Impostos]  WITH CHECK ADD  CONSTRAINT [FK_Impostos_PIS_PISID] FOREIGN KEY([PISID])
REFERENCES [dbo].[PIS] ([ID])
ON DELETE CASCADE
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Impostos_PIS_PISID')
   AND parent_object_id = OBJECT_ID(N'dbo.Impostos')
)
ALTER TABLE [dbo].[Impostos] CHECK CONSTRAINT [FK_Impostos_PIS_PISID]
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Impostos_PISST_PISSTID')
   AND parent_object_id = OBJECT_ID(N'dbo.Impostos')
)
ALTER TABLE [dbo].[Impostos]  WITH CHECK ADD  CONSTRAINT [FK_Impostos_PISST_PISSTID] FOREIGN KEY([PISSTID])
REFERENCES [dbo].[PISST] ([ID])
ON DELETE CASCADE
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Impostos_PISST_PISSTID')
   AND parent_object_id = OBJECT_ID(N'dbo.Impostos')
)
ALTER TABLE [dbo].[Impostos] CHECK CONSTRAINT [FK_Impostos_PISST_PISSTID]
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Impostos_TaxasFiscais_TaxasID')
   AND parent_object_id = OBJECT_ID(N'dbo.Impostos')
)
ALTER TABLE [dbo].[Impostos]  WITH CHECK ADD  CONSTRAINT [FK_Impostos_TaxasFiscais_TaxasID] FOREIGN KEY([TaxasID])
REFERENCES [dbo].[TaxasFiscais] ([ID])
ON DELETE CASCADE
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Impostos_TaxasFiscais_TaxasID')
   AND parent_object_id = OBJECT_ID(N'dbo.Impostos')
)
ALTER TABLE [dbo].[Impostos] CHECK CONSTRAINT [FK_Impostos_TaxasFiscais_TaxasID]
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_ItemExtraCategoia_Categoria_CategoriaID')
   AND parent_object_id = OBJECT_ID(N'dbo.ItemExtraCategoia')
)
ALTER TABLE [dbo].[ItemExtraCategoia]  WITH CHECK ADD  CONSTRAINT [FK_ItemExtraCategoia_Categoria_CategoriaID] FOREIGN KEY([CategoriaID])
REFERENCES [dbo].[Categoria] ([ID])
ON DELETE CASCADE
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_ItemExtraCategoia_Categoria_CategoriaID')
   AND parent_object_id = OBJECT_ID(N'dbo.ItemExtraCategoia')
)
ALTER TABLE [dbo].[ItemExtraCategoia] CHECK CONSTRAINT [FK_ItemExtraCategoia_Categoria_CategoriaID]
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_ItemExtraCategoia_ItemExtra_ItemExtraID')
   AND parent_object_id = OBJECT_ID(N'dbo.ItemExtraCategoia')
)
ALTER TABLE [dbo].[ItemExtraCategoia]  WITH CHECK ADD  CONSTRAINT [FK_ItemExtraCategoia_ItemExtra_ItemExtraID] FOREIGN KEY([ItemExtraID])
REFERENCES [dbo].[ItemExtra] ([ID])
ON DELETE CASCADE
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_ItemExtraCategoia_ItemExtra_ItemExtraID')
   AND parent_object_id = OBJECT_ID(N'dbo.ItemExtraCategoia')
)
ALTER TABLE [dbo].[ItemExtraCategoia] CHECK CONSTRAINT [FK_ItemExtraCategoia_ItemExtra_ItemExtraID]
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Produto_Categoria_CategoriaID')
   AND parent_object_id = OBJECT_ID(N'dbo.Produto')
)
ALTER TABLE [dbo].[Produto]  WITH CHECK ADD  CONSTRAINT [FK_Produto_Categoria_CategoriaID] FOREIGN KEY([CategoriaID])
REFERENCES [dbo].[Categoria] ([ID])
ON DELETE CASCADE
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Produto_Categoria_CategoriaID')
   AND parent_object_id = OBJECT_ID(N'dbo.Produto')
)
ALTER TABLE [dbo].[Produto] CHECK CONSTRAINT [FK_Produto_Categoria_CategoriaID]
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Produto_Impostos_ImpostosID')
   AND parent_object_id = OBJECT_ID(N'dbo.Produto')
)
ALTER TABLE [dbo].[Produto]  WITH CHECK ADD  CONSTRAINT [FK_Produto_Impostos_ImpostosID] FOREIGN KEY([ImpostosID])
REFERENCES [dbo].[Impostos] ([ID])
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Produto_Impostos_ImpostosID')
   AND parent_object_id = OBJECT_ID(N'dbo.Produto')
)
ALTER TABLE [dbo].[Produto] CHECK CONSTRAINT [FK_Produto_Impostos_ImpostosID]
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Produto_Unidade_UnidadeID')
   AND parent_object_id = OBJECT_ID(N'dbo.Produto')
)
ALTER TABLE [dbo].[Produto]  WITH CHECK ADD  CONSTRAINT [FK_Produto_Unidade_UnidadeID] FOREIGN KEY([UnidadeID])
REFERENCES [dbo].[Unidade] ([ID])
ON DELETE CASCADE
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Produto_Unidade_UnidadeID')
   AND parent_object_id = OBJECT_ID(N'dbo.Produto')
)
ALTER TABLE [dbo].[Produto] CHECK CONSTRAINT [FK_Produto_Unidade_UnidadeID]
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Produto_ValorEspecial_ValorID')
   AND parent_object_id = OBJECT_ID(N'dbo.Produto')
)
ALTER TABLE [dbo].[Produto]  WITH CHECK ADD  CONSTRAINT [FK_Produto_ValorEspecial_ValorID] FOREIGN KEY([ValorID])
REFERENCES [dbo].[ValorEspecial] ([ID])
ON DELETE CASCADE
GO
IF NOT EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_Produto_ValorEspecial_ValorID')
   AND parent_object_id = OBJECT_ID(N'dbo.Produto')
)
ALTER TABLE [dbo].[Produto] CHECK CONSTRAINT [FK_Produto_ValorEspecial_ValorID]
GO
USE [master]
GO
ALTER DATABASE [TOTEM_GERENCIADOR] SET  READ_WRITE 
GO
