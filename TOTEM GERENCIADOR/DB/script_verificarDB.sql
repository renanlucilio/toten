USE master;

IF EXISTS (
    SELECT name
        FROM sys.databases
        WHERE name = N'TOTEM GERENCIADOR')
	SELECT 'TRUE' as [EXISTE];
ELSE
	SELECT 'FALSE' as [EXISTE];