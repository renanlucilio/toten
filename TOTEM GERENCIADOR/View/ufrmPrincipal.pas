unit ufrmPrincipal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Objects, FMX.Effects, FMX.Layouts, FMX.Controls.Presentation,
  FMX.StdCtrls, System.ImageList, FMX.ImgList, FMX.TabControl, System.Rtti,
  FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.Edit, uDmCon, Fmx.Bind.Grid,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.Grid,
  Data.Bind.DBScope, FMX.Memo, FMX.ComboEdit, Cliente, Contato, Endereco,  Categoria,
  Totem, uTotemRepository,
  Impostos, ItemExtra, ItemExtraCategoia, System.Generics.Collections,
  uItemExtraRepository, Produto, Unidade, ValorEspecial, uProdutoRepository,
  Data.DB, uGridData, MaskUtils, FMX.ListView.Types,
  FMX.ListView.Appearances, FMX.ListView.Adapters.Base, FMX.ListView;

type
  TformPrincipal = class(TForm)
    rectMain: TRectangle;
    rectHeader: TRectangle;
    rectAside: TRectangle;
    txtTituloUm: TText;
    txtTituloDois: TText;
    vsbMenuLateral: TVertScrollBox;
    StyleBook: TStyleBook;
    btnSair: TButton;
    ShadowEffect9: TShadowEffect;
    btnCadastro: TButton;
    ShadowEffect2: TShadowEffect;
    btnRelatorios: TButton;
    ShadowEffect3: TShadowEffect;
    btnTotens: TButton;
    ShadowEffect4: TShadowEffect;
    btnMovimento: TButton;
    ShadowEffect5: TShadowEffect;
    btnInicio: TButton;
    ShadowEffect6: TShadowEffect;
    btnConfiguracoes: TButton;
    ShadowEffect7: TShadowEffect;
    imgList: TImageList;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    Image6: TImage;
    btnImgTotens: TImage;
    Layout1: TLayout;
    Rectangle4: TRectangle;
    ShadowEffect1: TShadowEffect;
    txtH1: TText;
    tbcMain: TTabControl;
    TabIInicio: TTabItem;
    TabIMovimento: TTabItem;
    TabITotens: TTabItem;
    TabIRelatorios: TTabItem;
    tabCadastro: TTabItem;
    TabIConfiguracoes: TTabItem;
    tbcMovimentos: TTabControl;
    TabIMovimentos: TTabItem;
    TabIPedidos: TTabItem;
    Rectangle5: TRectangle;
    Text4: TText;
    Text5: TText;
    Rectangle6: TRectangle;
    lgAcoesPedido: TGridLayout;
    btnFazerPedidos: TButton;
    ShadowEffect10: TShadowEffect;
    btnVerPedidos: TButton;
    ShadowEffect8: TShadowEffect;
    btnCancelarPedidos: TButton;
    ShadowEffect11: TShadowEffect;
    btnVerCaixa: TButton;
    ShadowEffect12: TShadowEffect;
    btnFecharCaixa: TButton;
    ShadowEffect13: TShadowEffect;
    Layout2: TLayout;
    Layout3: TLayout;
    Layout4: TLayout;
    Line1: TLine;
    Line2: TLine;
    Layout5: TLayout;
    Layout6: TLayout;
    Layout7: TLayout;
    Image10: TImage;
    Image9: TImage;
    Image8: TImage;
    Layout8: TLayout;
    Layout9: TLayout;
    Layout10: TLayout;
    Text6: TText;
    Text8: TText;
    Text7: TText;
    txtQtdVendas: TText;
    txtPedidosPendentes: TText;
    txtValorVendido: TText;
    Text12: TText;
    VertScrollBox2: TVertScrollBox;
    lgPedidos: TGridLayout;
    Rectangle7: TRectangle;
    tbcCadastros: TTabControl;
    tabCliente: TTabItem;
    VertScrollBox3: TVertScrollBox;
    Text18: TText;
    Text16: TText;
    Text19: TText;
    Text20: TText;
    Text21: TText;
    Text22: TText;
    tabCategoria: TTabItem;
    tabProduto: TTabItem;
    tabItemExtra: TTabItem;
    tabTributacao: TTabItem;
    tabTotem: TTabItem;
    tbcFormCategoria: TTabControl;
    TabCategoriaLista: TTabItem;
    txtCategoriaId: TText;
    Layout13: TLayout;
    btnCategoriaNovo: TSpeedButton;
    btnCategoriaAtualizar: TSpeedButton;
    gridCategoria: TGrid;
    TabCategoriaCad: TTabItem;
    tbcFormProduto: TTabControl;
    tabProdutoLista: TTabItem;
    txtProdutoId: TText;
    Layout14: TLayout;
    btnProdutoNovo: TSpeedButton;
    btnProdutoAtualizar: TSpeedButton;
    gridProduto: TGrid;
    tabProdutoCad: TTabItem;
    tbcItemExtra: TTabControl;
    tabItemExtraLista: TTabItem;
    txtItemExtraId: TText;
    Layout15: TLayout;
    btnItemExtraNovo: TSpeedButton;
    btnItemExtraAtualizar: TSpeedButton;
    gridItemExtra: TGrid;
    tabItemExtraCad: TTabItem;
    tbcTributacoes: TTabControl;
    tabTributacaoLista: TTabItem;
    txtTributacaoId: TText;
    Layout16: TLayout;
    btnTributacaoNovo: TSpeedButton;
    btnTributacaoAtualizar: TSpeedButton;
    gridTributacao: TGrid;
    tabTributacaoCad: TTabItem;
    tbcTotens: TTabControl;
    tabTotemLista: TTabItem;
    txtTotemId: TText;
    Layout17: TLayout;
    btnTotemNovo: TSpeedButton;
    btnTotemAtualizar: TSpeedButton;
    gridTotem: TGrid;
    tabTotemCad: TTabItem;
    txtCategoriaTipoForm: TText;
    txtProdutoTipoForm: TText;
    txtItemExtraTipoForm: TText;
    txtTributacaoTipoForm: TText;
    txtTotemTipoForm: TText;
    tbcClientes: TTabControl;
    tabClienteLista: TTabItem;
    txtClienteId: TText;
    Layout22: TLayout;
    btnClienteNovo: TSpeedButton;
    btnClienteAtualizar: TSpeedButton;
    GridClientes: TGrid;
    tabClienteCad: TTabItem;
    txtClienteTipoForm: TText;
    VertScrollBox4: TVertScrollBox;
    bindTableCliente: TBindSourceDB;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    bindList: TBindingsList;
    btnClienteApagar: TSpeedButton;
    btnClienteEditar: TSpeedButton;
    btnCategoriaApagar: TSpeedButton;
    btnCategoriaEditar: TSpeedButton;
    btnProdutoApagar: TSpeedButton;
    btnProdutoEditar: TSpeedButton;
    btnItemExtraApagar: TSpeedButton;
    btnItemExtraEditar: TSpeedButton;
    btnTributacaoApagar: TSpeedButton;
    btnTributacaoEditar: TSpeedButton;
    btnTotemApagar: TSpeedButton;
    btnTotemEditar: TSpeedButton;
    tabOpcoesDeCadastro: TTabItem;
    lgBtnsCadastro: TGridLayout;
    btnCadastroTotem: TButton;
    ShadowEffect14: TShadowEffect;
    btnCadastroCliente: TButton;
    ShadowEffect15: TShadowEffect;
    btnCadastroCategoria: TButton;
    ShadowEffect16: TShadowEffect;
    btnCadastroProduto: TButton;
    ShadowEffect17: TShadowEffect;
    btnCadastroItemExtra: TButton;
    ShadowEffect18: TShadowEffect;
    btnCadastroTributacao: TButton;
    ShadowEffect19: TShadowEffect;
    bindTableProduto: TBindSourceDB;
    LinkGridToDataSourceBindSourceDB7: TLinkGridToDataSource;
    Rectangle8: TRectangle;
    Text35: TText;
    edtClienteNome: TEdit;
    Rectangle9: TRectangle;
    edtClienteDocumento: TEdit;
    Text36: TText;
    Rectangle10: TRectangle;
    edtClienteContato: TEdit;
    txtClienteContatoID: TText;
    Rectangle11: TRectangle;
    edtClienteCep: TEdit;
    txtClienteEnderecoId: TText;
    Rectangle12: TRectangle;
    Text39: TText;
    Rectangle13: TRectangle;
    edtClienteLogradouro: TEdit;
    Text40: TText;
    Rectangle14: TRectangle;
    edtClienteNumero: TEdit;
    Text41: TText;
    Rectangle15: TRectangle;
    edtClienteComplemento: TEdit;
    Text42: TText;
    Rectangle16: TRectangle;
    edtClienteBairro: TEdit;
    Text43: TText;
    Rectangle17: TRectangle;
    edtClienteCidade: TEdit;
    Text44: TText;
    Rectangle18: TRectangle;
    edtClienteReferencia: TEdit;
    Text45: TText;
    Rectangle19: TRectangle;
    Text46: TText;
    btnClienteCancelar: TSpeedButton;
    btnClienteSalvar: TSpeedButton;
    VertScrollBox5: TVertScrollBox;
    Rectangle20: TRectangle;
    edtCategoriaDescricao: TEdit;
    Text47: TText;
    Rectangle24: TRectangle;
    Text51: TText;
    Rectangle30: TRectangle;
    edtCategoriaLocalLogo: TEdit;
    Text57: TText;
    btnCancelarCategoria: TSpeedButton;
    btnCategoriaSalvar: TSpeedButton;
    btnSelecionarLogo: TButton;
    ShadowEffect20: TShadowEffect;
    imgCategoriaLogo: TImage;
    VertScrollBox6: TVertScrollBox;
    Rectangle21: TRectangle;
    edtProdutoDescricao: TEdit;
    Text48: TText;
    Rectangle22: TRectangle;
    Text49: TText;
    Rectangle23: TRectangle;
    Text50: TText;
    Rectangle25: TRectangle;
    edtProdutoValor: TEdit;
    txtProdutoValorEspecialId: TText;
    Rectangle26: TRectangle;
    txtProdutoUnidadeId: TText;
    Rectangle27: TRectangle;
    edtProdutoValorPequena: TEdit;
    Text54: TText;
    Rectangle28: TRectangle;
    edtProdutoValorMedia: TEdit;
    Text55: TText;
    Rectangle29: TRectangle;
    edtProdutoValorGigante: TEdit;
    Text56: TText;
    Rectangle31: TRectangle;
    edtProdutoNCM: TEdit;
    Text58: TText;
    Rectangle32: TRectangle;
    edtProdutoCEST: TEdit;
    Text59: TText;
    Rectangle33: TRectangle;
    edtProdutoCFOP: TEdit;
    Text60: TText;
    Rectangle34: TRectangle;
    Text61: TText;
    btnProdutoCancelar: TSpeedButton;
    btnProdutoSalvar: TSpeedButton;
    Text62: TText;
    swProdutoTemDecimal: TSwitch;
    Rectangle35: TRectangle;
    edtProdutoDescricaoUnidade: TEdit;
    Text63: TText;
    Rectangle36: TRectangle;
    edtProdutoCodigoBarras: TEdit;
    Text64: TText;
    Rectangle37: TRectangle;
    Text65: TText;
    mmoIngredientes: TMemo;
    VertScrollBox7: TVertScrollBox;
    Rectangle38: TRectangle;
    edtItemExtraDescricao: TEdit;
    Text66: TText;
    Rectangle41: TRectangle;
    edtItemExtraValor: TEdit;
    Text69: TText;
    Rectangle42: TRectangle;
    Text70: TText;
    btnItemExtraCancelar: TSpeedButton;
    btnItemExtraSalvar: TSpeedButton;
    VertScrollBox8: TVertScrollBox;
    Rectangle39: TRectangle;
    edtTributacaoDescricao: TEdit;
    Text67: TText;
    Rectangle40: TRectangle;
    edtICMSOrigem: TEdit;
    Text68: TText;
    Rectangle43: TRectangle;
    edtICMSCST: TEdit;
    Text71: TText;
    Rectangle46: TRectangle;
    edtTaxaTaxaEstadual: TEdit;
    Text74: TText;
    Rectangle47: TRectangle;
    edtTaxaTaxaFederal: TEdit;
    Text75: TText;
    Rectangle48: TRectangle;
    edtPisCst: TEdit;
    Text76: TText;
    Rectangle49: TRectangle;
    edtPisBc: TEdit;
    Text77: TText;
    Rectangle50: TRectangle;
    edtPisPorcentagem: TEdit;
    Text78: TText;
    Rectangle51: TRectangle;
    edtPisValor: TEdit;
    Text79: TText;
    btnTributacaoCancelar: TSpeedButton;
    btnTributacaoSalvar: TSpeedButton;
    txtIcmsID: TText;
    Rectangle44: TRectangle;
    edtICMSPorcentagem: TEdit;
    Text73: TText;
    Rectangle45: TRectangle;
    edtICMSValor: TEdit;
    Text81: TText;
    txtIssqnId: TText;
    Rectangle53: TRectangle;
    edtISSQNNatureza: TEdit;
    Text83: TText;
    Rectangle54: TRectangle;
    edtIssqnBC: TEdit;
    Text84: TText;
    Rectangle55: TRectangle;
    edtIssqnDeducao: TEdit;
    Text85: TText;
    Rectangle56: TRectangle;
    edtIssqnCodigoIbge: TEdit;
    Text86: TText;
    Rectangle57: TRectangle;
    edtIssqnBcp: TEdit;
    Text87: TText;
    Rectangle58: TRectangle;
    edtIssqnItemServico: TEdit;
    Text88: TText;
    txtTaxasId: TText;
    txtPisId: TText;
    Rectangle52: TRectangle;
    edtPisBcp: TEdit;
    Text80: TText;
    Rectangle59: TRectangle;
    edtPisAliquota: TEdit;
    Text91: TText;
    txtPisstId: TText;
    Rectangle60: TRectangle;
    edtPisstBc: TEdit;
    Text93: TText;
    Rectangle61: TRectangle;
    edtPisstPorcentagem: TEdit;
    Text94: TText;
    Rectangle62: TRectangle;
    edtPisstValor: TEdit;
    Text95: TText;
    Rectangle63: TRectangle;
    edtPisstBcp: TEdit;
    Text96: TText;
    Rectangle64: TRectangle;
    edtPisstAliquota: TEdit;
    Text97: TText;
    txtCofinsId: TText;
    Rectangle65: TRectangle;
    edtCofinsCST: TEdit;
    Text99: TText;
    Rectangle66: TRectangle;
    edtCofinsBc: TEdit;
    Text100: TText;
    Rectangle67: TRectangle;
    Text101: TText;
    Rectangle68: TRectangle;
    Text102: TText;
    Rectangle69: TRectangle;
    Text103: TText;
    Rectangle70: TRectangle;
    Text104: TText;
    txtCofinsstId: TText;
    Rectangle71: TRectangle;
    edtCofinsstBc: TEdit;
    Text106: TText;
    Rectangle72: TRectangle;
    edtCofinsstPorcentagem: TEdit;
    Text107: TText;
    Rectangle73: TRectangle;
    edtCofinsstValor: TEdit;
    Text108: TText;
    Rectangle74: TRectangle;
    edtCofinsstBcp: TEdit;
    Text109: TText;
    Rectangle75: TRectangle;
    edtCofinsstAliquota: TEdit;
    Text110: TText;
    VertScrollBox9: TVertScrollBox;
    Rectangle76: TRectangle;
    edtTotemLocal: TEdit;
    Text111: TText;
    Rectangle78: TRectangle;
    Text113: TText;
    btnTotemCancelar: TSpeedButton;
    btnTotemSalvar: TSpeedButton;
    btnMapearTotem: TButton;
    ShadowEffect21: TShadowEffect;
    Rectangle77: TRectangle;
    edtTotemMapeamento: TEdit;
    Text112: TText;
    Rectangle79: TRectangle;
    edtTotemIp: TEdit;
    Text114: TText;
    Rectangle80: TRectangle;
    edtTotemNome: TEdit;
    Text115: TText;
    Rectangle81: TRectangle;
    Text117: TText;
    btnAdicionarCategoria: TButton;
    ShadowEffect22: TShadowEffect;
    edtCofinsPorcentagem: TEdit;
    edtCofinsValor: TEdit;
    edtCofinsBcp: TEdit;
    edtCofinsAliquota: TEdit;
    Text17: TText;
    Text14: TText;
    Text15: TText;
    Text13: TText;
    Layout11: TLayout;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    cedClienteFormaContato: TComboEdit;
    cedClienteUf: TComboEdit;
    cedTotemStatus: TComboEdit;
    bindTableTotem: TBindSourceDB;
    LinkGridToDataSourceBindSourceDB12: TLinkGridToDataSource;
    LinkPropertyToFieldTag2: TLinkPropertyToField;
    cedCategoriaStatus: TComboEdit;
    bindTableCategoria: TBindSourceDB;
    LinkGridToDataSourceBindSourceDB13: TLinkGridToDataSource;
    LinkPropertyToFieldTag3: TLinkPropertyToField;
    Rectangle1: TRectangle;
    edtIssqnCodigoTributario: TEdit;
    Text1: TText;
    bindTableTributacao: TBindSourceDB;
    LinkGridToDataSourceBindSourceDB14: TLinkGridToDataSource;
    LinkPropertyToFieldTag4: TLinkPropertyToField;
    cedItemExtraStatus: TComboEdit;
    Rectangle2: TRectangle;
    Text2: TText;
    cedItemExtraCategoria: TComboEdit;
    Text3: TText;
    bindTableItemExtra: TBindSourceDB;
    LinkGridToDataSourceBindSourceDB15: TLinkGridToDataSource;
    LinkPropertyToFieldTag5: TLinkPropertyToField;
    cedProdutoCategorias: TComboEdit;
    cedProdutoTributacao: TComboEdit;
    cedProdutoSiglaDaUnidade: TComboEdit;
    cedProdutoStatus: TComboEdit;
    LinkPropertyToFieldTag6: TLinkPropertyToField;
    LinkPropertyToFieldTag: TLinkPropertyToField;
    lsvItensCategorias: TListView;
    btnLimparCateroriaItemExtra: TButton;
    ShadowEffect23: TShadowEffect;
    imgStatuServer: TImage;
    txtStatusServer: TText;
    tbcConfiguracaoes: TTabControl;
    tabConfiguracoes: TTabItem;
    VertScrollBox1: TVertScrollBox;
    GridLayout1: TGridLayout;
    btnAjustesProdutos: TButton;
    ShadowEffect27: TShadowEffect;
    tabConfigDefinicoesPizzas: TTabItem;
    VertScrollBox10: TVertScrollBox;
    Rectangle3: TRectangle;
    edtTamanhoPizzaBroto: TEdit;
    Text9: TText;
    Rectangle82: TRectangle;
    edtTamanhoPizzaMedia: TEdit;
    Text10: TText;
    Rectangle83: TRectangle;
    edtTamanhoPizzaGigante: TEdit;
    Text11: TText;
    Rectangle85: TRectangle;
    Text24: TText;
    cedFormaCobrancaTamanhoPizza: TComboEdit;
    Rectangle92: TRectangle;
    Text31: TText;
    cedFormaCobrancaTipoPizza: TComboEdit;
    btnDefinicoesPizzasCancelar: TSpeedButton;
    btnDefinicoesPizzasSalvar: TSpeedButton;
    Text32: TText;
    txtTipoCobrancaTamanhoPizza: TText;
    tbcAjustes: TTabControl;
    tabAjustesProdutos: TTabItem;
    GridLayout3: TGridLayout;
    btnDefinicoesPizzas: TSpeedButton;
    lsvTotens: TListView;
    btnAtualizarTotens: TSpeedButton;
    LinkListControlToField1: TLinkListControlToField;
    btnAjustesEstabelecimento: TButton;
    ShadowEffect24: TShadowEffect;
    Text25: TText;
    tabAjustesEstabelecimento: TTabItem;
    btnDefiEstabelecimento: TSpeedButton;
    tabConfigDefinicoesEstabelecimento: TTabItem;
    Text26: TText;
    VertScrollBox11: TVertScrollBox;
    Rectangle84: TRectangle;
    edtEstabelecimentoNome: TEdit;
    Text23: TText;
    Rectangle87: TRectangle;
    edtEstabelecimentoLogo: TEdit;
    Text28: TText;
    imgLogoEstabelecimento: TImage;
    btnCancelarEstabelecimento: TSpeedButton;
    btnSalvarEstabelecimento: TSpeedButton;
    btnSelecionarLogoEstabelecimento: TButton;
    ShadowEffect25: TShadowEffect;
    procedure btnInicioClick(Sender: TObject);
    procedure btnMovimentoClick(Sender: TObject);
    procedure btnTotensClick(Sender: TObject);
    procedure btnCadastroClick(Sender: TObject);
    procedure btnRelatoriosClick(Sender: TObject);
    procedure btnConfiguracoesClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnVerPedidosClick(Sender: TObject);
    procedure btnCadastroTotemClick(Sender: TObject);
    procedure btnCadastroClienteClick(Sender: TObject);
    procedure btnCadastroCategoriaClick(Sender: TObject);
    procedure btnCadastroProdutoClick(Sender: TObject);
    procedure btnCadastroItemExtraClick(Sender: TObject);
    procedure btnCadastroTributacaoClick(Sender: TObject);
    procedure btnClienteNovoClick(Sender: TObject);
    procedure btnClienteEditarClick(Sender: TObject);
    procedure btnCategoriaNovoClick(Sender: TObject);
    procedure btnCategoriaEditarClick(Sender: TObject);
    procedure btnProdutoNovoClick(Sender: TObject);
    procedure btnProdutoEditarClick(Sender: TObject);
    procedure btnItemExtraNovoClick(Sender: TObject);
    procedure btnItemExtraEditarClick(Sender: TObject);
    procedure btnTributacaoNovoClick(Sender: TObject);
    procedure btnTributacaoEditarClick(Sender: TObject);
    procedure btnTotemNovoClick(Sender: TObject);
    procedure btnTotemEditarClick(Sender: TObject);
    procedure btnClienteSalvarClick(Sender: TObject);
    procedure btnClienteAtualizarClick(Sender: TObject);
    procedure btnClienteCacelarClick(Sender: TObject);
    procedure btnClienteApagarClick(Sender: TObject);
    procedure btnTotemCancelarClick(Sender: TObject);
    procedure btnTotemSalvarClick(Sender: TObject);
    procedure btnTotemAtualizarClick(Sender: TObject);
    procedure btnTotemApagarClick(Sender: TObject);
    procedure btnCancelarCategoriaClick(Sender: TObject);
    procedure btnCategoriaSalvarClick(Sender: TObject);
    procedure btnCategoriaAtualizarClick(Sender: TObject);
    procedure btnCategoriaApagarClick(Sender: TObject);
    procedure btnTributacaoAtualizarClick(Sender: TObject);
    procedure btnTributacaoApagarClick(Sender: TObject);
    procedure btnTributacaoCancelarClick(Sender: TObject);
    procedure btnTributacaoSalvarClick(Sender: TObject);
    procedure btnAdicionarCategoriaClick(Sender: TObject);
    procedure btnItemExtraCancelarClick(Sender: TObject);
    procedure btnItemExtraSalvarClick(Sender: TObject);
    procedure btnItemExtraAtualizarClick(Sender: TObject);
    procedure btnItemExtraApagarClick(Sender: TObject);
    procedure btnProdutoCancelarClick(Sender: TObject);
    procedure btnProdutoSalvarClick(Sender: TObject);
    procedure btnProdutoAtualizarClick(Sender: TObject);
    procedure btnProdutoApagarClick(Sender: TObject);
    procedure btnMapearTotemClick(Sender: TObject);
    procedure LinkGridToDataSourceBindSourceDB12AssigningValue(
      Sender: TObject; AssignValueRec: TBindingAssignValueRec;
      var Value: TValue; var Handled: Boolean);
    procedure LinkGridToDataSourceBindSourceDB1AssigningValue(
      Sender: TObject; AssignValueRec: TBindingAssignValueRec;
      var Value: TValue; var Handled: Boolean);
    procedure btnSelecionarLogoClick(Sender: TObject);
    procedure LinkGridToDataSourceBindSourceDB13AssigningValue(
      Sender: TObject; AssignValueRec: TBindingAssignValueRec;
      var Value: TValue; var Handled: Boolean);
    procedure LinkGridToDataSourceBindSourceDB7AssigningValue(
      Sender: TObject; AssignValueRec: TBindingAssignValueRec;
      var Value: TValue; var Handled: Boolean);
    procedure btnLimparCateroriaItemExtraClick(Sender: TObject);
    procedure LinkGridToDataSourceBindSourceDB15AssigningValue(
      Sender: TObject; AssignValueRec: TBindingAssignValueRec;
      var Value: TValue; var Handled: Boolean);
    procedure btnAjustesProdutosClick(Sender: TObject);
    procedure btnDefinicoesPizzasClick(Sender: TObject);
    procedure btnDefinicoesPizzasCancelarClick(Sender: TObject);
    procedure btnDefinicoesPizzasSalvarClick(Sender: TObject);
    procedure cedFormaCobrancaTamanhoPizzaChange(Sender: TObject);
    procedure btnAtualizarTotensClick(Sender: TObject);
    procedure btnDefiEstabelecimentoClick(Sender: TObject);
    procedure btnCancelarEstabelecimentoClick(Sender: TObject);
    procedure btnSalvarEstabelecimentoClick(Sender: TObject);
    procedure btnAjustesEstabelecimentoClick(Sender: TObject);
    procedure btnSelecionarLogoEstabelecimentoClick(Sender: TObject);
  private
    procedure SetarAsTabControl();
    { Private declarations }

    function CriarCliente(): TCliente;
    function CriarTotem(): TTotem;
    function CriarCategoria(): TCategoria;
    function CriarTributacao(): TImpostos;
    function CriarItemExtra(): TItemExtra;
    function CriarItemCategorias(): TObjectList<TItemExtraCategoia>;
    function CriarProduto(): TProduto;

    function validarTotem(totem: TTotem): Boolean;
    function validarCliente(cliente: TCliente): Boolean;
    function validarCategoria(categoria: TCategoria): Boolean;
    function validarTributacao(impostos: TImpostos): Boolean;
    function ValidarProduto(produto: TProduto): Boolean;
    function ValidarItemExtra(itemExtra: TItemExtra): Boolean;
  public
    procedure AddItemCategoria();
    { Public declarations }
  end;

var
  formPrincipal: TformPrincipal;

implementation

{$R *.fmx}

uses uClienteRepository, uCategoriaRepository, uTributacaoRepository, PIS,
  PISST, COFINS, COFINSST, TaxasFiscais, ISSQN, ICMS, Model.ViewUtil,
  uPizzasConfig, GerenciadorTotem, Estabelecimento;

procedure TformPrincipal.AddItemCategoria();
var
  repo: TItemExtraRepository;
  parans: TArray<string>;
begin
  repo := TItemExtraRepository.Create;
  try
    parans:= cedItemExtraCategoria.Text.Split(['|']);

    repo.AddItemCategoriaLista(parans[0].ToInteger());
  finally
    repo.Free;
  end;
end;


procedure TformPrincipal.btnAdicionarCategoriaClick(Sender: TObject);
begin
  if not cedItemExtraCategoria.Text.IsEmpty then
  begin
    AddItemCategoria();
    cedItemExtraCategoria.Text:= '';
  end;
end;

procedure TformPrincipal.btnAjustesProdutosClick(Sender: TObject);
begin
  tbcAjustes.ActiveTab:= tabAjustesProdutos;
end;

procedure TformPrincipal.btnAtualizarTotensClick(Sender: TObject);
var
  gerenciador: TGerenciadorTotem;
begin

  gerenciador := TGerenciadorTotem.Create;
  try
    gerenciador.GerarConfiguracoes;
  finally
    gerenciador.Free;
  end;
end;

procedure TformPrincipal.btnCadastroCategoriaClick(Sender: TObject);
var
  repo: TCategoriaRepository;
begin
  tbcCadastros.ActiveTab:= tabCategoria;
  tbcFormCategoria.ActiveTab:= TabCategoriaLista;

  repo:= TCategoriaRepository.Create();
  try
    repo.CarregaCategorias();
  finally
    repo.Free;
  end;
end;

procedure TformPrincipal.btnCadastroClick(Sender: TObject);
begin
  tbcMain.ActiveTab:= tabCadastro;
  tbcCadastros.ActiveTab:= tabOpcoesDeCadastro;
end;

procedure TformPrincipal.btnCadastroClienteClick(Sender: TObject);
var
  repo: TClienteRepository;
begin
  tbcCadastros.ActiveTab:= tabCliente;
  tbcClientes.ActiveTab:= tabClienteLista;

  repo:= TClienteRepository.Create();
  try
    repo.CarregaClientes();
  finally
    repo.Free;
  end;
end;

procedure TformPrincipal.btnCadastroItemExtraClick(Sender: TObject);
var
  repo: TItemExtraRepository;
begin
  tbcCadastros.ActiveTab:= tabItemExtra;
  tbcItemExtra.ActiveTab:= tabItemExtraLista;

  repo:= TItemExtraRepository.Create();
  try
    repo.CarregaItensExtra();
  finally
    repo.Free;
  end;
end;

procedure TformPrincipal.btnCadastroProdutoClick(Sender: TObject);
var
  repo: TProdutoRepository;
begin
  tbcCadastros.ActiveTab:= tabProduto;
  tbcFormProduto.ActiveTab:= tabProdutoLista;

  repo:= TProdutoRepository.Create();
  try
    repo.CarregaProdutos();
  finally
    repo.Free;
  end;
end;

procedure TformPrincipal.btnCadastroTotemClick(Sender: TObject);
var
  repo: TTotemRepository;
begin
  tbcCadastros.ActiveTab:= tabTotem;
  tbcTotens.ActiveTab:= tabTotemLista;

  repo:= TTotemRepository.Create();
  try
    repo.CarregaTotens();
  finally
    repo.Free;
  end;
end;

procedure TformPrincipal.btnCadastroTributacaoClick(Sender: TObject);
var
  repo: TTributacaoRepository;
begin
  tbcCadastros.ActiveTab:= tabTributacao;
  tbcTributacoes.ActiveTab:= tabTributacaoLista;

  repo:= TTributacaoRepository.Create();
  try
    repo.CarregarTributacoes();
  finally
    repo.Free;
  end;
end;

procedure TformPrincipal.btnCancelarCategoriaClick(Sender: TObject);
begin
  tbcFormCategoria.ActiveTab:= TabCategoriaLista;
end;

procedure TformPrincipal.btnCancelarEstabelecimentoClick(Sender: TObject);
begin
  tbcConfiguracaoes.ActiveTab:= tabConfiguracoes;
end;

procedure TformPrincipal.btnCategoriaApagarClick(Sender: TObject);
var
  repo: TCategoriaRepository;
begin
  repo:= TCategoriaRepository.Create();
  try
    repo.Delete(txtCategoriaId.Tag);
  finally
    repo.Free;
  end;
end;

procedure TformPrincipal.btnCategoriaAtualizarClick(Sender: TObject);
var
  repo: TCategoriaRepository;
begin
  repo:= TCategoriaRepository.Create();
  try
    repo.CarregaCategorias();
  finally
    repo.Free;
  end;
end;

procedure TformPrincipal.btnCategoriaEditarClick(Sender: TObject);
var
  repo: TCategoriaRepository;
  id: integer;
begin
  try
    repo:= TCategoriaRepository.Create();

    repo.InitEdit(txtCategoriaId.Tag);
  finally
    FreeAndNil(repo);
  end;
end;

procedure TformPrincipal.btnCategoriaNovoClick(Sender: TObject);
var
  repo: TCategoriaRepository;
begin
  try
    repo:= TCategoriaRepository.Create();
    repo.InitInsert();
  finally
    FreeAndNil(repo);
  end;
end;
procedure TformPrincipal.btnCategoriaSalvarClick(Sender: TObject);
var
  repo: TCategoriaRepository;
  categoria: TCategoria;
begin
  categoria := CriarCategoria();
  repo := TCategoriaRepository.Create;
  if validarCategoria(categoria) then
  begin
    try
      if txtCategoriaTipoForm.Tag = 0 then
        repo.Insert(categoria)
      else
      begin
        categoria.ID:= txtCategoriaId.Tag;
        repo.Edit(categoria);
      end;
    finally
      repo.Free;
    end;
  end
  else
    categoria.Free;

end;

procedure TformPrincipal.btnClienteApagarClick(Sender: TObject);
var
  repo: TClienteRepository;
begin
  repo:= TClienteRepository.Create();
  try
    repo.Delete(txtClienteId.Tag);
  finally
    repo.Free;
  end;
end;

procedure TformPrincipal.btnClienteAtualizarClick(Sender: TObject);
var
  repo: TClienteRepository;
begin
  repo:= TClienteRepository.Create();
  try
    repo.CarregaClientes();
  finally
    repo.Free;
  end;
end;

procedure TformPrincipal.btnClienteCacelarClick(Sender: TObject);
begin
  tbcClientes.ActiveTab:= tabClienteLista;
end;

procedure TformPrincipal.btnClienteEditarClick(Sender: TObject);
var
  repo: TClienteRepository;
  id: integer;
begin
  try
    repo:= TClienteRepository.Create();

    repo.InitEdit(txtClienteId.Tag);
  finally
    FreeAndNil(repo);
  end;
end;

procedure TformPrincipal.btnClienteNovoClick(Sender: TObject);
var
  repo: TClienteRepository;
begin
  try
    repo:= TClienteRepository.Create();
    repo.InitInsert();
  finally
    FreeAndNil(repo);
  end;
end;

procedure TformPrincipal.btnClienteSalvarClick(Sender: TObject);
var
  repo: TClienteRepository;
  cliente: TCliente;
begin
  cliente := CriarCliente();
  repo := TClienteRepository.Create;
  if validarCliente(cliente) then
  begin
    try
      if txtClienteTipoForm.Tag = 0 then
        repo.Insert(cliente)
      else
      begin
        cliente.ID:= txtClienteID.Tag;
        repo.Edit(cliente);
      end;
    finally
      repo.Free;
    end;
  end
  else
    cliente.Free;

end;

procedure TformPrincipal.btnConfiguracoesClick(Sender: TObject);
begin
  tbcConfiguracaoes.ActiveTab:= tabConfiguracoes;
  tbcMain.ActiveTab:= TabIConfiguracoes;
  txtH1.Text:= 'Configurações';
end;

procedure TformPrincipal.btnDefinicoesPizzasCancelarClick(Sender: TObject);
begin
  tbcConfiguracaoes.ActiveTab:= tabConfiguracoes;
end;

procedure TformPrincipal.btnDefinicoesPizzasClick(Sender: TObject);
var
  ajustesPizzas: TPizzasConfig;
begin
  tbcConfiguracaoes.ActiveTab:= tabConfigDefinicoesPizzas;

  ajustesPizzas := TPizzasConfig.Create;
  try
    ajustesPizzas.Carregar;
    ajustesPizzas.SetDadosForm;
  finally
    ajustesPizzas.Free;
  end;
end;

procedure TformPrincipal.btnDefinicoesPizzasSalvarClick(Sender: TObject);
var
  ajustesPizzas: TPizzasConfig;
begin
  tbcConfiguracaoes.ActiveTab:= tabConfigDefinicoesPizzas;

  ajustesPizzas := TPizzasConfig.Create;
  try
    ajustesPizzas.GetDadosForm;
    ajustesPizzas.Salvar;

    tbcConfiguracaoes.ActiveTab:= tabConfiguracoes;
  finally
    ajustesPizzas.Free;
  end;
end;

procedure TformPrincipal.btnInicioClick(Sender: TObject);
begin
  tbcMain.ActiveTab:= TabIInicio;
  txtH1.Text:= 'Inicio';
end;

procedure TformPrincipal.btnItemExtraApagarClick(Sender: TObject);
var
  repo: TItemExtraRepository;
begin
  repo:= TItemExtraRepository.Create();
  try
    repo.Delete(txtItemExtraId.Tag);
  finally
    repo.Free;
  end;
end;

procedure TformPrincipal.btnItemExtraAtualizarClick(Sender: TObject);
var
  repo: TItemExtraRepository;
begin
  repo:= TItemExtraRepository.Create();
  try
    repo.CarregaItensExtra();
  finally
    repo.Free;
  end;
end;

procedure TformPrincipal.btnItemExtraCancelarClick(Sender: TObject);
begin
  tbcItemExtra.ActiveTab:= tabItemExtraLista;
end;

procedure TformPrincipal.btnItemExtraEditarClick(Sender: TObject);
var
  repo: TItemExtraRepository;
  id: integer;
begin
  try
    repo:= TItemExtraRepository.Create();

    repo.InitEdit(txtItemExtraId.Tag);
  finally
    FreeAndNil(repo);
  end;
end;

procedure TformPrincipal.btnItemExtraNovoClick(Sender: TObject);
var
  repo: TItemExtraRepository;
begin
  try
    repo:= TItemExtraRepository.Create();
    repo.InitInsert();
  finally
    FreeAndNil(repo);
  end;
end;

procedure TformPrincipal.btnItemExtraSalvarClick(Sender: TObject);
var
  repo: TItemExtraRepository;
  itemExtra: TItemExtra;
  itensCategoria: TObjectList<TItemExtraCategoia>;
  listaCategorias: TObjectList<TCategoria>;
  it: TItemExtraCategoia;
begin
  itemExtra := CriarItemExtra();
  itensCategoria:= CriarItemCategorias();
  listaCategorias:= TObjectList<TCategoria>.Create(True);
  repo := TItemExtraRepository.Create;
  if ValidarItemExtra(itemExtra) then
  begin
    try
      if txtItemExtraTipoForm.Tag = 0 then
      begin
        itensCategoria.OwnsObjects:= false;
        for it in itensCategoria do
        begin
          listaCategorias.Add(it.Categoria);
        end;

        try
          repo.Insert(itemExtra, listaCategorias);
        finally
          itensCategoria.Free;
        end;
      end
      else
      begin
        itemExtra.ID:= txtItemExtraId.Tag;
        repo.Edit(itemExtra, itensCategoria);
      end;
    finally
      repo.Free;
    end;
  end;
end;


procedure TformPrincipal.btnLimparCateroriaItemExtraClick(Sender: TObject);
begin
  lsvItensCategorias.Items.Clear;
end;

procedure TformPrincipal.btnMapearTotemClick(Sender: TObject);
var
  caminho: string;
begin
  caminho:=SelecionarCaminhoDoArquivo('Executável|Totem.exe', 'Selecione o Executável do Totem.exe');

  if not caminho.IsEmpty then
    edtTotemMapeamento.Text:= caminho;
end;

procedure TformPrincipal.btnMovimentoClick(Sender: TObject);
begin
  tbcMain.ActiveTab:= TabIMovimento;
  tbcMovimentos.ActiveTab:= TabIMovimentos;
  txtH1.Text:= 'Movimento';
end;

procedure TformPrincipal.btnProdutoApagarClick(Sender: TObject);
var
  repo: TProdutoRepository;
begin
  repo:= TProdutoRepository.Create();
  try
    repo.Delete(txtProdutoId.Tag);
  finally
    repo.Free;
  end;
end;

procedure TformPrincipal.btnProdutoAtualizarClick(Sender: TObject);
var
  repo: TProdutoRepository;
begin
  repo:= TProdutoRepository.Create();
  try
    repo.CarregaProdutos();
  finally
    repo.Free;
  end;
end;

procedure TformPrincipal.btnProdutoCancelarClick(Sender: TObject);
begin
  tbcFormProduto.ActiveTab:= tabProdutoLista;
end;

procedure TformPrincipal.btnProdutoEditarClick(Sender: TObject);
var
  repo: TProdutoRepository;
begin
  try
    repo:= TProdutoRepository.Create();

    repo.InitEdit(txtProdutoId.Tag);
  finally
    FreeAndNil(repo);
  end;
end;

procedure TformPrincipal.btnProdutoNovoClick(Sender: TObject);
var
  repo: TProdutoRepository;
begin
  try
    repo:= TProdutoRepository.Create();
    repo.InitInsert();
  finally
    FreeAndNil(repo);
  end;
end;

procedure TformPrincipal.btnProdutoSalvarClick(Sender: TObject);
var
  repo: TProdutoRepository;
  produto: TProduto;
begin
  produto := CriarProduto();
  repo := TProdutoRepository.Create;
  if ValidarProduto(produto) then
  begin
    try
      if txtProdutoTipoForm.Tag = 0 then
        repo.Insert(produto)
      else
      begin
        produto.ID:= txtProdutoId.Tag;
        repo.Edit(produto);
      end;
    finally
      repo.Free;
    end;
  end
  else
    produto.Free;

end;


procedure TformPrincipal.btnRelatoriosClick(Sender: TObject);
begin
  tbcMain.ActiveTab:= TabIRelatorios;
  txtH1.Text:= 'Relatórios';
end;

procedure TformPrincipal.btnSalvarEstabelecimentoClick(Sender: TObject);
var
  esta: TEstabelecimento;
begin
  tbcConfiguracaoes.ActiveTab:= tabConfigDefinicoesEstabelecimento;

  esta := TEstabelecimento.Create;
  try
    esta.GetDadosForm;
    esta.Salvar;

    tbcConfiguracaoes.ActiveTab:= tabConfiguracoes;
  finally
    esta.Free;
  end;
end;

procedure TformPrincipal.btnSelecionarLogoClick(Sender: TObject);
var
  caminho: string;
begin
  caminho:=SelecionarCaminhoDoArquivo('Imagem|*.jpg;*.jpeg;*.png', 'Selecione o Logo dessa categoria');

  if not caminho.IsEmpty then
    edtCategoriaLocalLogo.Text:= caminho;

  if FileExists(caminho) then
    imgCategoriaLogo.Bitmap.LoadFromFile(caminho);
end;

procedure TformPrincipal.btnTotemApagarClick(Sender: TObject);
var
  repo: TTotemRepository;
begin
  repo:= TTotemRepository.Create();
  try
    repo.Delete(txtTotemId.Tag);
  finally
    repo.Free;
  end;
end;

procedure TformPrincipal.btnTotemAtualizarClick(Sender: TObject);
var
  repo: TTotemRepository;
begin
  repo:= TTotemRepository.Create();
  try
    repo.CarregaTotens();
  finally
    repo.Free;
  end;
end;

procedure TformPrincipal.btnTotemCancelarClick(Sender: TObject);
begin
  tbcTotens.ActiveTab:= tabTotemLista;
end;

procedure TformPrincipal.btnTotemEditarClick(Sender: TObject);
var
  repo: TTotemRepository;
  id: integer;
begin
  try
    repo:= TTotemRepository.Create();

    repo.InitEdit(txtTotemId.Tag);
  finally
    FreeAndNil(repo);
  end;
end;

procedure TformPrincipal.btnTotemNovoClick(Sender: TObject);
var
  repo: TTotemRepository;
begin
  try
    repo:= TTotemRepository.Create();
    repo.InitInsert();
  finally
    FreeAndNil(repo);
  end;
end;

procedure TformPrincipal.btnTotemSalvarClick(Sender: TObject);
var
  repo: TTotemRepository;
  totem: TTotem;
begin
  totem := CriarTotem();
  repo := TTotemRepository.Create;
  if validarTotem(totem) then
  begin
    try
      if txtTotemTipoForm.Tag = 0 then
        repo.Insert(totem)
      else
      begin
        totem.ID:= txtTotemId.Tag;
        repo.Edit(totem);
      end;
    finally
      repo.Free;
    end;
  end
  else
    totem.Free;

end;

procedure TformPrincipal.btnTotensClick(Sender: TObject);
var
  repo: TTotemRepository;
begin
  tbcMain.ActiveTab:= TabITotens;
  txtH1.Text:= 'Totens';

  repo := TTotemRepository.Create;
  try
    repo.CarregaTotens;
  finally
    repo.Free;
  end;
end;

procedure TformPrincipal.btnTributacaoApagarClick(Sender: TObject);
var
  repo: TTributacaoRepository;
begin
  repo:= TTributacaoRepository.Create();
  try
    repo.Delete(txtTributacaoId.Tag);
  finally
    repo.Free;
  end;
end;

procedure TformPrincipal.btnTributacaoAtualizarClick(Sender: TObject);
var
  repo: TTributacaoRepository;
begin
  repo:= TTributacaoRepository.Create();
  try
    repo.CarregarTributacoes();
  finally
    repo.Free;
  end;
end;

procedure TformPrincipal.btnTributacaoCancelarClick(Sender: TObject);
begin
  tbcTributacoes.ActiveTab:= tabTributacaoLista;
end;

procedure TformPrincipal.btnTributacaoEditarClick(Sender: TObject);
var
  repo: TTributacaoRepository;
  id: integer;
begin
  try
    repo:= TTributacaoRepository.Create();

    repo.InitEdit(txtTributacaoId.Tag);
  finally
    FreeAndNil(repo);
  end;
end;


procedure TformPrincipal.btnTributacaoNovoClick(Sender: TObject);
var
  repo: TTributacaoRepository;
begin
  try
    repo:= TTributacaoRepository.Create();
    repo.InitInsert();
  finally
    FreeAndNil(repo);
  end;
end;

procedure TformPrincipal.btnTributacaoSalvarClick(Sender: TObject);
var
  repo: TTributacaoRepository;
  tributacao: TImpostos;
begin
  tributacao := CriarTributacao();
  repo := TTributacaoRepository.Create;
  if validarTributacao(tributacao) then
  begin
    try
      if txtTributacaoTipoForm.Tag = 0 then
        repo.Insert(tributacao)
      else
      begin
        tributacao.ID:= txtTributacaoId.Tag;
        repo.Edit(tributacao);
      end;
    finally
      repo.Free;
    end;
  end
  else
    tributacao.Free;

end;

procedure TformPrincipal.btnVerPedidosClick(Sender: TObject);
begin
  tbcMovimentos.ActiveTab:= TabIPedidos;
end;



procedure TformPrincipal.btnAjustesEstabelecimentoClick(Sender: TObject);
begin
  tbcAjustes.ActiveTab:= tabAjustesEstabelecimento;
end;

procedure TformPrincipal.btnSelecionarLogoEstabelecimentoClick(Sender: TObject);
var
  caminho: string;
begin
  caminho:=SelecionarCaminhoDoArquivo('Imagem|*.jpg;*.jpeg;*.png', 'Selecione o Logo dessa categoria');

  if not caminho.IsEmpty then
    edtEstabelecimentoLogo.Text:= caminho;

  if FileExists(caminho) then
    imgLogoEstabelecimento.Bitmap.LoadFromFile(caminho);
end;

procedure TformPrincipal.cedFormaCobrancaTamanhoPizzaChange(
  Sender: TObject);
begin
  if cedFormaCobrancaTamanhoPizza.ItemIndex = 0 then
    txtTipoCobrancaTamanhoPizza.Text:= 'Valor Fixo'
  else if cedFormaCobrancaTamanhoPizza.ItemIndex = 1 then
     txtTipoCobrancaTamanhoPizza.Text:= 'Porcentagem'
  else if cedFormaCobrancaTamanhoPizza.ItemIndex = 2 then
     txtTipoCobrancaTamanhoPizza.Text:= 'Valor fixo a Mais, Valor fixo a Menos';
end;

function TformPrincipal.CriarCategoria: TCategoria;
begin
  result:= TCategoria.Create();

  Result.Descricao:= edtCategoriaDescricao.Text;
  Result.LocalLogo:= edtCategoriaLocalLogo.Text;
  Result.Status:= cedCategoriaStatus.ItemIndex;
end;

function TformPrincipal.CriarCliente(): TCliente;
var
  contato: TContato;
  endereco: TEndereco;
begin
  Result:= TCliente.Create();

  contato:= TContato.Create();
  endereco:= TEndereco.Create();

  Result.Nome:= edtClienteNome.Text;
  Result.Documento:= edtClienteDocumento.Text;
  Result.EnderecoID:= txtClienteEnderecoId.Tag;
  Result.ContatoID:= txtClienteContatoID.Tag;

  Contato.OhContato:= edtClienteContato.Text;
  Contato.Tipo:= cedClienteFormaContato.Text;

  Endereco.Logradouro:= edtClienteLogradouro.Text;
  Endereco.Numero:= edtClienteNumero.Text;
  Endereco.Bairro:= edtClienteBairro.Text;
  Endereco.Cidade:= edtClienteCidade.Text;
  Endereco.UF:= cedClienteUf.Text;
  Endereco.CEP:= edtClienteCep.Text;
  Endereco.Complemento:= edtClienteComplemento.Text;
  Endereco.Referencia:= edtClienteReferencia.Text;

  result.Contato:= Contato;
  result.Endereco:= Endereco;
end;

function TformPrincipal.CriarItemCategorias: TObjectList<TItemExtraCategoia>;
var
  I: Integer;
  item: TItemExtraCategoia;
  it: TListItem;
begin
  result:= TObjectList<TItemExtraCategoia>.Create();

  for I := 0 to lsvItensCategorias.Items.Count-1 do
  begin
    it:= lsvItensCategorias.Items.Item[I];


    item:= TItemExtraCategoia.Create;
    item.Categoria.ID:= it.Tag;
    item.CategoriaID:= it.Tag;
    item.ItemExtraID:= txtItemExtraId.Tag;
    result.Add(item);
  end;
end;

function TformPrincipal.CriarItemExtra: TItemExtra;
begin
  result:= TItemExtra.Create();

  result.Descricao:= edtItemExtraDescricao.Text;
  Result.Valor:= StrToFloatDef(edtItemExtraValor.Text, 0);
  Result.Status:= cedItemExtraStatus.ItemIndex;
end;

function TformPrincipal.CriarProduto: TProduto;
var
  params: TArray<string>;
begin
  Result:= TProduto.Create();
  Result.Unidade:= TUnidade.Create();
  Result.ValorEspecial:= TValorEspecial.Create();

  Result.Descricao:= edtProdutoDescricao.Text;
  Result.ValorEspecial.ValorNormal:= StrToFloatDef(edtProdutoValor.Text, 0);
  Result.ValorEspecial.ValorPequeno:= StrToFloatDef(edtProdutoValorPequena.Text, 0);
  Result.ValorEspecial.ValorMedia:= StrToFloatDef(edtProdutoValorMedia.Text, 0);
  Result.ValorEspecial.ValorGigante:= StrToFloatDef(edtProdutoValorGigante.Text, 0);
  Result.NCM:= edtProdutoNCM.Text;
  Result.CEST:= edtProdutoCEST.Text;
  Result.CFOP:= edtProdutoCFOP.Text;
  Result.Unidade.Descricao:= edtProdutoDescricaoUnidade.Text;
  Result.CodigoBarras:= edtProdutoCodigoBarras.Text;
  Result.Ingredientes:= mmoIngredientes.Lines.Text;

  Result.UnidadeID:= txtProdutoUnidadeId.Tag;
  Result.ValorID:= txtProdutoValorEspecialId.Tag;
  Result.Status:= cedProdutoStatus.ItemIndex;

  Result.Unidade.Sigla:= cedProdutoSiglaDaUnidade.Text;
  Result.Unidade.TemDecimal:= swProdutoTemDecimal.IsChecked;
  Result.CategoriaID:= -1;

  if not cedProdutoCategorias.Text.IsEmpty then
  begin
    params:= cedProdutoCategorias.Text.Split(['|']);
    Result.CategoriaID:= params[0].ToInteger;
  end;

  if not cedProdutoTributacao.Text.IsEmpty then
  begin
    params:= cedProdutoTributacao.Text.Split(['|']);
    Result.ImpostosID:= params[0].ToInteger;
  end;
end;

function TformPrincipal.CriarTotem: TTotem;
begin
  result:= TTotem.Create;

  Result.Local:= edtTotemLocal.Text;
  Result.Nome:= edtTotemNome.Text;
  Result.Ip:= edtTotemIp.Text;
  Result.Mapeamento:= edtTotemMapeamento.Text;
  result.Status:= cedTotemStatus.ItemIndex;
end;

function TformPrincipal.CriarTributacao: TImpostos;
begin
  result:= TImpostos.Create;

  Result.PIS:= TPIS.Create;
  Result.PISST:= TPISST.Create;
  Result.COFINS:= TCOFINS.Create;
  Result.COFINSST:= TCOFINSST.Create;
  Result.TaxasFiscais:= TTaxasFiscais.Create;
  Result.ISSQN:= TISSQN.Create;
  Result.ICMS:= TICMS.Create;

  Result.Descricao:= edtTributacaoDescricao.Text;

  Result.PIS.CST:=  StrToIntDef(edtPisCst.Text, 0);
  Result.PIS.BaseCalculo:= StrToFloatDef(edtPisBc.Text, 0);
  Result.PIS.Porcento := StrToFloatDef(edtPisPorcentagem.Text, 0);
  Result.PIS.Valor:= StrToFloatDef(edtPisValor.Text, 0);
  Result.PIS.BaseCalculoPorProduto:= StrToFloatDef(edtPisBcp.Text, 0);
  Result.PIS.Aliquota:= StrToFloatDef(edtPisAliquota.Text, 0);

  Result.PISST.BaseCalculo:= StrToFloatDef(edtPisstBc.Text, 0);
  Result.PISST.Porcento:= StrToFloatDef(edtPisstPorcentagem.Text, 0);
  Result.PISST.Valor:= StrToFloatDef(edtPisstValor.Text, 0);
  Result.PISST.BaseCalculoPorProduto:= StrToFloatDef(edtPisstBcp.Text, 0);
  Result.PISST.Aliquota:= StrToFloatDef(edtPisstAliquota.Text, 0);

  Result.COFINS.CST:= StrToIntDef(edtCofinsCST.Text, 0);
  Result.COFINS.BaseCalculo:= StrToFloatDef(edtCofinsBc.Text, 0);
  Result.COFINS.Porcento:= StrToFloatDef(edtCofinsPorcentagem.Text, 0);
  Result.COFINS.Valor:= StrToFloatDef(edtCofinsValor.Text, 0);
  Result.COFINS.BaseCalculoPorProduto:= StrToFloatDef(edtCofinsBcp.Text, 0);
  Result.COFINS.Aliquota:= StrToFloatDef(edtCofinsAliquota.Text, 0);

  Result.COFINSST.BaseCalculo:= StrToFloatDef(edtCofinsstBc.Text, 0);
  Result.COFINSST.Porcento := StrToFloatDef(edtCofinsstPorcentagem.Text, 0);
  Result.COFINSST.Valor:= StrToFloatDef(edtCofinsstValor.Text, 0);
  Result.COFINSST.BaseCalculoPorProduto:= StrToFloatDef(edtCofinsstBcp.Text, 0);
  Result.COFINSST.Aliquota:= StrToFloatDef(edtCofinsstAliquota.Text, 0);

  Result.ICMS.Origem:= StrToIntDef(edtICMSOrigem.Text, -1);
  Result.ICMS.CST:= StrToIntDef(edtICMSCST.Text, 0);
  Result.ICMS.Porcento:= StrToFloatDef(edtICMSPorcentagem.Text, 0);
  Result.ICMS.Valor:= StrToFloatDef(edtICMSValor.Text, 0);

  Result.ISSQN.Natureza:= StrToIntDef(edtISSQNNatureza.Text, 0);
  Result.ISSQN.BaseCalculo:= StrToFloatDef(edtIssqnBC.Text, 0);
  Result.ISSQN.Deducao:= StrToFloatDef(edtIssqnDeducao.Text, 0);
  Result.ISSQN.CodigoIBGE:= edtIssqnCodigoIbge.Text;
  Result.ISSQN.BaseCalculoProduto:= StrToFloatDef(edtIssqnBcp.Text, 0);
  Result.ISSQN.ItemDaListaServico:= edtIssqnItemServico.Text;
  Result.ISSQN.CodigoTributarioISSQN:= edtIssqnCodigoTributario.Text;

  Result.TaxasFiscais.TaxaEstadual:= StrToFloatDef(edtTaxaTaxaEstadual.Text, 0);
  Result.TaxasFiscais.TaxaFederal:= StrToFloatDef(edtTaxaTaxaFederal.Text, 0);

  Result.ICMSID:= txtIcmsID.Tag;
  Result.ISSQNID:= txtIssqnId.Tag;
  Result.PISID:= txtPisId.Tag;
  Result.PISSTID:= txtPisstId.Tag;
  Result.COFINSID:= txtCofinsId.Tag;
  Result.COFINSSTID:= txtCofinsstId.Tag;
  Result.TaxasID:= txtTaxasId.Tag;
end;

procedure TformPrincipal.FormCreate(Sender: TObject);
begin
  SetarAsTabControl();
end;

procedure TformPrincipal.LinkGridToDataSourceBindSourceDB12AssigningValue(
  Sender: TObject; AssignValueRec: TBindingAssignValueRec;
  var Value: TValue; var Handled: Boolean);
var
  coluna: TGridDataObject;
begin
    if Assigned(AssignValueRec.OutObj) then
      if AssignValueRec.OutObj.ClassNameIs('TGridDataObject') then
      begin
        coluna:= TGridDataObject(AssignValueRec.OutObj);

        if coluna.Column = 5 then
        begin
          if Value.AsString.Equals('-1') then
            Value:= 'Sem Status';

        end;
      end;

end;

procedure TformPrincipal.LinkGridToDataSourceBindSourceDB13AssigningValue(
  Sender: TObject; AssignValueRec: TBindingAssignValueRec;
  var Value: TValue; var Handled: Boolean);
var
  coluna: TGridDataObject;
begin
    if Assigned(AssignValueRec.OutObj) then
      if AssignValueRec.OutObj.ClassNameIs('TGridDataObject') then
      begin
        coluna:= TGridDataObject(AssignValueRec.OutObj);

        if coluna.Column = 3 then
        begin
          if Value.AsString.Equals('0') then
            Value:= 'Ativado'
          else if Value.AsString.Equals('1') then
               Value:= 'Bloqueado';
        end;
      end;

end;

procedure TformPrincipal.LinkGridToDataSourceBindSourceDB15AssigningValue(
  Sender: TObject; AssignValueRec: TBindingAssignValueRec;
  var Value: TValue; var Handled: Boolean);
var
  coluna: TGridDataObject;
begin
   if Assigned(AssignValueRec.OutObj) then
    if AssignValueRec.OutObj.ClassNameIs('TGridDataObject') then
    begin
      coluna:= TGridDataObject(AssignValueRec.OutObj);

      if coluna.Column = 3 then
      begin
        if value.AsString.Equals('0') then
          value:= 'Ativado'
        else if value.AsString.Equals('1') then
          value:= 'Bloqueado';
      end;

      if coluna.Column = 2 then
      begin
        value:= 'R$ '+ value.AsString;
      end;
    end;

end;

procedure TformPrincipal.LinkGridToDataSourceBindSourceDB1AssigningValue(
  Sender: TObject; AssignValueRec: TBindingAssignValueRec;
  var Value: TValue; var Handled: Boolean);
var
  coluna: TGridDataObject;
begin
   if Assigned(AssignValueRec.OutObj) then
    if AssignValueRec.OutObj.ClassNameIs('TGridDataObject') then
    begin
      coluna:= TGridDataObject(AssignValueRec.OutObj);

      if coluna.Column = 2 then
      begin
        if Value.AsString.Length = 11 then
          value:= FormatMaskText('000\.000\.000\-00;0;', value.AsString)
        else if Value.AsString.Length = 14 then
          value:= FormatMaskText('00\.000\.000\/0000\-00;0;', value.AsString);

      end;
    end;

end;

procedure TformPrincipal.LinkGridToDataSourceBindSourceDB7AssigningValue(
  Sender: TObject; AssignValueRec: TBindingAssignValueRec;
  var Value: TValue; var Handled: Boolean);
var
  coluna: TGridDataObject;
begin
   if Assigned(AssignValueRec.OutObj) then
    if AssignValueRec.OutObj.ClassNameIs('TGridDataObject') then
    begin
      coluna:= TGridDataObject(AssignValueRec.OutObj);

      if coluna.Column = 2 then
      begin
        if value.AsString.Equals('0') then
          value:= 'Ativado'
        else if value.AsString.Equals('1') then
          value:= 'Bloqueado';
      end;

      if coluna.Column = 3 then
      begin
        value:= 'R$ '+ value.AsString;
      end;
    end;

end;

procedure TformPrincipal.SetarAsTabControl;
var
  I: Integer;
begin
  for I := 0 to Self.ComponentCount-1 do
  begin
    if Components[I] is TTabControl then
    begin
      (Components[I] as TTabControl).TabPosition:= TTabPosition.None;
      (Components[I] as TTabControl).TabIndex:= 0;
    end;
  end;
end;

procedure TformPrincipal.btnDefiEstabelecimentoClick(Sender: TObject);
var
  esta: TEstabelecimento;
begin
  tbcConfiguracaoes.ActiveTab:= tabConfigDefinicoesEstabelecimento;

  esta := TEstabelecimento.Create;
  try
    esta.Carregar;
    esta.SetDadosForm;
  finally
    esta.Free;
  end;
end;


function TformPrincipal.validarCategoria(categoria: TCategoria): Boolean;
var
  retorno: TStringBuilder;
begin

  retorno := TStringBuilder.Create;
  try
    if categoria.Descricao.Value.IsEmpty then
      retorno.Append('Campo "Descrição" deve ser preenchido').AppendLine;

    if not retorno.ToString.IsEmpty then
    begin
      ShowMessage(retorno.ToString);
      result:= false;
    end
    else
      result:= true;



  finally
    retorno.Free;
  end;


end;

function TformPrincipal.validarCliente(cliente: TCliente): Boolean;
var
  retorno: TStringBuilder;
begin

  retorno := TStringBuilder.Create;
  try
    if cliente.Nome.Value.IsEmpty then
      retorno.Append('Campo "Nome" deve ser preenchido').AppendLine;

    if cliente.Documento.Value.IsEmpty then
      retorno.Append('Campo "Documento" deve ser preenchido').AppendLine;

    if not retorno.ToString.IsEmpty then
    begin
      ShowMessage(retorno.ToString);
      result:= false;
    end
    else
      result:= true;



  finally
    retorno.Free;
  end;


end;

function TformPrincipal.ValidarItemExtra(itemExtra: TItemExtra): Boolean;
var
  retorno: TStringBuilder;
begin

  retorno := TStringBuilder.Create;
  try
    if itemExtra.Descricao.Value.IsEmpty then
      retorno.Append('Campo "Descrição" deve ser preenchido').AppendLine;

    if not retorno.ToString.IsEmpty then
    begin
      ShowMessage(retorno.ToString);
      result:= false;
    end
    else
      result:= true;



  finally
    retorno.Free;
  end;


end;

function TformPrincipal.ValidarProduto(produto: TProduto): Boolean;
var
  retorno: TStringBuilder;
begin

  retorno := TStringBuilder.Create;
  try
    if produto.Descricao.Value.IsEmpty then
      retorno.Append('Campo "Descrição" deve ser preenchido').AppendLine;

    if produto.CategoriaID = -1 then
      retorno.Append('Campo "Categoria" deve ser preenchido').AppendLine;

    if produto.ValorEspecial.ValorNormal = 0 then
      retorno.Append('Campo "Valor" deve ser preenchido').AppendLine;

    if not retorno.ToString.IsEmpty then
    begin
      ShowMessage(retorno.ToString);
      result:= false;
    end
    else
      result:= true;



  finally
    retorno.Free;
  end;


end;

function TformPrincipal.validarTotem(totem: TTotem): Boolean;
var
  retorno: TStringBuilder;
begin

  retorno := TStringBuilder.Create;
  try
    if totem.Local.Value.IsEmpty then
      retorno.Append('Campo "Local" deve ser preenchido').AppendLine;

    if totem.Nome.Value.IsEmpty then
      retorno.Append('Campo "Nome" deve ser preenchido').AppendLine;

    if totem.Mapeamento.Value.IsEmpty then
      retorno.Append('Campo "Mapeamente" deve ser preenchido').AppendLine;

    if not retorno.ToString.IsEmpty then
    begin
      ShowMessage(retorno.ToString);
      result:= false;
    end
    else
      result:= true;



  finally
    retorno.Free;
  end;


end;

function TformPrincipal.validarTributacao(impostos: TImpostos): Boolean;
var
  retorno: TStringBuilder;
begin

  retorno := TStringBuilder.Create;
  try
    if impostos.Descricao.Value.IsEmpty then
      retorno.Append('Campo "Descrição" deve ser preenchido').AppendLine;

    if impostos.PIS.CST = 0 then
      retorno.Append('Campo "PIS CST" deve ser preenchido').AppendLine;

    if impostos.COFINS.CST = 0 then
      retorno.Append('Campo "COFINS CST" deve ser preenchido').AppendLine;

    if impostos.ICMS.CST = 0 then
      retorno.Append('Campo "ICMS CST" deve ser preenchido').AppendLine;

    if impostos.ICMS.Origem = -1 then
      retorno.Append('Campo "ICMS ORIGEM" deve ser preenchido').AppendLine;

    if not retorno.ToString.IsEmpty then
    begin
      ShowMessage(retorno.ToString);
      result:= false;
    end
    else
      result:= true;



  finally
    retorno.Free;
  end;


end;

end.
