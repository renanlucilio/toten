unit uDmCon;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MSSQL,
  FireDAC.Phys.MSSQLDef, FireDAC.FMXUI.Wait, Data.DB, FireDAC.Comp.Client,
  FireDAC.Comp.ScriptCommands, FireDAC.Stan.Util, FireDAC.Comp.Script,
  FireDAC.Comp.UI, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet;

type
  TdmCon = class(TDataModule)
    Con: TFDConnection;
    Script: TFDScript;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    tableCategoria: TFDTable;
    tableCliente: TFDTable;
    tableItemExtra: TFDTable;
    tableTributacao: TFDTable;
    tableTotem: TFDTable;
    tableProduto: TFDQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure CriarBancoDados();
    function VerificaSeBancoExiste(): Boolean;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmCon: TdmCon;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

procedure TdmCon.CriarBancoDados;
begin
  Con.Params.Database:= '';
  if not VerificaSeBancoExiste then
  begin
    with Script do
    begin
        Con.Connected := True;
        ValidateAll;
        ExecuteAll;

        Con.Connected := False;

    end;
  end
  else
    Con.Params.Database:= 'TOTEM_GERENCIADOR';
end;

procedure TdmCon.DataModuleCreate(Sender: TObject);
begin
  CriarBancoDados;
end;

function TdmCon.VerificaSeBancoExiste: Boolean;
var
  return: Variant;
begin
  return:= Con.ExecSQLScalar('SELECT name FROM sys.databases WHERE name = N'+QuotedStr('TOTEM_GERENCIADOR'));

  result:= return = 'TOTEM_GERENCIADOR';

end;

end.
