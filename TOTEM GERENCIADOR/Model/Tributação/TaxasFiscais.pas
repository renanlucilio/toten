unit taxasfiscais;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('TaxasFiscais', '')]
  [PrimaryKey('ID', NotInc, NoSort, False, 'Chave prim�ria')]
  TTaxasFiscais = class
  private
    { Private declarations } 
    FID: Nullable<Integer>;
    FTaxaEstadual: Double;
    FTaxaFederal: Double;
  public 
    { Public declarations } 
    [Column('ID', ftInteger)]
    [Dictionary('ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ID: Nullable<Integer> read FID write FID;

    [Restrictions([NotNull])]
    [Column('TaxaEstadual', ftBCD, 18, 2)]
    [Dictionary('TaxaEstadual', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property TaxaEstadual: Double read FTaxaEstadual write FTaxaEstadual;

    [Restrictions([NotNull])]
    [Column('TaxaFederal', ftBCD, 18, 2)]
    [Dictionary('TaxaFederal', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property TaxaFederal: Double read FTaxaFederal write FTaxaFederal;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TTaxasFiscais)

end.
