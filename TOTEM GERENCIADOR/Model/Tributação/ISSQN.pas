unit issqn;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('ISSQN', '')]
  [PrimaryKey('ID', NotInc, NoSort, False, 'Chave prim�ria')]
  TISSQN = class
  private
    { Private declarations } 
    FID: Nullable<Integer>;
    FBaseCalculo: Double;
    FDeducao: Double;
    FBaseCalculoProduto: Double;
    FNatureza: Integer;
    FCodigoIBGE: Nullable<string>;
    FItemDaListaServico: Nullable<string>;
    FCodigoTributarioISSQN: Nullable<string>;
  public 
    { Public declarations } 
    [Column('ID', ftInteger)]
    [Dictionary('ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ID: Nullable<Integer> read FID write FID;

    [Restrictions([NotNull])]
    [Column('BaseCalculo', ftBCD, 18, 2)]
    [Dictionary('BaseCalculo', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property BaseCalculo: Double read FBaseCalculo write FBaseCalculo;

    [Restrictions([NotNull])]
    [Column('Deducao', ftBCD, 18, 2)]
    [Dictionary('Deducao', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Deducao: Double read FDeducao write FDeducao;

    [Column('CodigoIBGE', ftString)]
    [Dictionary('CodigoIBGE', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property CodigoIBGE: Nullable<string> read FCodigoIBGE write FCodigoIBGE;

    [Restrictions([NotNull])]
    [Column('BaseCalculoProduto', ftBCD, 18, 2)]
    [Dictionary('BaseCalculoProduto', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property BaseCalculoProduto: Double read FBaseCalculoProduto write FBaseCalculoProduto;

    [Column('ItemDaListaServico', ftString)]
    [Dictionary('ItemDaListaServico', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property ItemDaListaServico: Nullable<string> read FItemDaListaServico write FItemDaListaServico;

    [Column('CodigoTributarioISSQN', ftString)]
    [Dictionary('CodigoTributarioISSQN', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property CodigoTributarioISSQN: Nullable<string> read FCodigoTributarioISSQN write FCodigoTributarioISSQN;

    [Restrictions([NotNull])]
    [Column('Natureza', ftInteger)]
    [Dictionary('Natureza', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Natureza: Integer read FNatureza write FNatureza;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TISSQN)

end.
