unit icms;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('ICMS', '')]
  [PrimaryKey('ID', NotInc, NoSort, False, 'Chave prim�ria')]
  TICMS = class
  private
    { Private declarations } 
    FID: Nullable<Integer>;
    FOrigem: Integer;
    FCST: Integer;
    FPorcento: Currency;
    FValor: Double;
  public 
    { Public declarations } 
    [Column('ID', ftInteger)]
    [Dictionary('ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ID: Nullable<Integer> read FID write FID;

    [Restrictions([NotNull])]
    [Column('Origem', ftInteger)]
    [Dictionary('Origem', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Origem: Integer read FOrigem write FOrigem;

    [Restrictions([NotNull])]
    [Column('CST', ftInteger)]
    [Dictionary('CST', 'Mensagem de valida��o', '', '', '', taCenter)]
    property CST: Integer read FCST write FCST;

    [Restrictions([NotNull])]
    [Column('Porcento', ftCurrency)]
    [Dictionary('Porcento', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Porcento: Currency read FPorcento write FPorcento;

    [Restrictions([NotNull])]
    [Column('Valor', ftBCD, 18, 2)]
    [Dictionary('Valor', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Valor: Double read FValor write FValor;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TICMS)

end.
