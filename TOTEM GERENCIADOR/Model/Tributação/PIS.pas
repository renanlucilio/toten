unit pis;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('PIS', '')]
  [PrimaryKey('ID', NotInc, NoSort, False, 'Chave prim�ria')]
  TPIS = class
  private
    { Private declarations } 
    FID: Nullable<Integer>;
    FBaseCalculo: Double;
    FPorcento: Currency;
    FValor: Double;
    FBaseCalculoPorProduto: Double;
    FAliquota: Currency;
    FCST: Integer;
  public 
    { Public declarations } 
    [Column('ID', ftInteger)]
    [Dictionary('ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ID: Nullable<Integer> read FID write FID;

    [Restrictions([NotNull])]
    [Column('BaseCalculo', ftBCD, 18, 2)]
    [Dictionary('BaseCalculo', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property BaseCalculo: Double read FBaseCalculo write FBaseCalculo;

    [Restrictions([NotNull])]
    [Column('Porcento', ftCurrency)]
    [Dictionary('Porcento', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Porcento: Currency read FPorcento write FPorcento;

    [Restrictions([NotNull])]
    [Column('Valor', ftBCD, 18, 2)]
    [Dictionary('Valor', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Valor: Double read FValor write FValor;

    [Restrictions([NotNull])]
    [Column('BaseCalculoPorProduto', ftBCD, 18, 2)]
    [Dictionary('BaseCalculoPorProduto', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property BaseCalculoPorProduto: Double read FBaseCalculoPorProduto write FBaseCalculoPorProduto;

    [Restrictions([NotNull])]
    [Column('Aliquota', ftCurrency)]
    [Dictionary('Aliquota', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Aliquota: Currency read FAliquota write FAliquota;

    [Restrictions([NotNull])]
    [Column('CST', ftInteger)]
    [Dictionary('CST', 'Mensagem de valida��o', '', '', '', taCenter)]
    property CST: Integer read FCST write FCST;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TPIS)

end.
