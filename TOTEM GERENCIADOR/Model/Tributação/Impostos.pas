unit impostos;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  cofins,
  cofinsst,
  icms,
  issqn,
  pis,
  pisst,
  taxasfiscais,
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('Impostos', '')]
  [PrimaryKey('ID', NotInc, NoSort, False, 'Chave prim�ria')]
  TImpostos = class
  private
    { Private declarations } 
    FID: Nullable<Integer>;
    FICMSID: Integer;
    FPISID: Integer;
    FCOFINSID: Integer;
    FPISSTID: Integer;
    FCOFINSSTID: Integer;
    FISSQNID: Integer;
    FTaxasID: Integer;

    FCOFINS_0:  TCOFINS  ;
    FCOFINSST_1:  TCOFINSST  ;
    FICMS_2:  TICMS  ;
    FISSQN_3:  TISSQN  ;
    FPIS_4:  TPIS  ;
    FPISST_5:  TPISST  ;
    FTaxasFiscais_6:  TTaxasFiscais  ;
    FDescricao: Nullable<string>;
  public 
    { Public declarations } 
    constructor Create;
    destructor Destroy; override;
    [Column('ID', ftInteger)]
    [Dictionary('ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ID: Nullable<Integer> read FID write FID;

    [Column('Descricao', ftString)]
    [Dictionary('Descricao', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Descricao: Nullable<string> read FDescricao write FDescricao;

    [Restrictions([NotNull])]
    [Column('ICMSID', ftInteger)]
    [ForeignKey('FK_Impostos_ICMS_ICMSID', 'ICMSID', 'ICMS', 'ID', Cascade, SetNull)]
    [Dictionary('ICMSID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ICMSID: Integer read FICMSID write FICMSID;

    [Restrictions([NotNull])]
    [Column('PISID', ftInteger)]
    [ForeignKey('FK_Impostos_PIS_PISID', 'PISID', 'PIS', 'ID', Cascade, SetNull)]
    [Dictionary('PISID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property PISID: Integer read FPISID write FPISID;

    [Restrictions([NotNull])]
    [Column('COFINSID', ftInteger)]
    [ForeignKey('FK_Impostos_COFINS_COFINSID', 'COFINSID', 'COFINS', 'ID', Cascade, SetNull)]
    [Dictionary('COFINSID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property COFINSID: Integer read FCOFINSID write FCOFINSID;

    [Restrictions([NotNull])]
    [Column('PISSTID', ftInteger)]
    [ForeignKey('FK_Impostos_PISST_PISSTID', 'PISSTID', 'PISST', 'ID', Cascade, SetNull)]
    [Dictionary('PISSTID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property PISSTID: Integer read FPISSTID write FPISSTID;

    [Restrictions([NotNull])]
    [Column('COFINSSTID', ftInteger)]
    [ForeignKey('FK_Impostos_COFINSST_COFINSSTID', 'COFINSSTID', 'COFINSST', 'ID', Cascade, SetNull)]
    [Dictionary('COFINSSTID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property COFINSSTID: Integer read FCOFINSSTID write FCOFINSSTID;

    [Restrictions([NotNull])]
    [Column('ISSQNID', ftInteger)]
    [ForeignKey('FK_Impostos_ISSQN_ISSQNID', 'ISSQNID', 'ISSQN', 'ID', Cascade, SetNull)]
    [Dictionary('ISSQNID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ISSQNID: Integer read FISSQNID write FISSQNID;

    [Restrictions([NotNull])]
    [Column('TaxasID', ftInteger)]
    [ForeignKey('FK_Impostos_TaxasFiscais_TaxasID', 'TaxasID', 'TaxasFiscais', 'ID', Cascade, SetNull)]
    [Dictionary('TaxasID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property TaxasID: Integer read FTaxasID write FTaxasID;

    [Association(OneToOne,'COFINSID','COFINS','ID')]
    property COFINS: TCOFINS read FCOFINS_0 write FCOFINS_0;

    [Association(OneToOne,'COFINSSTID','COFINSST','ID')]
    property COFINSST: TCOFINSST read FCOFINSST_1 write FCOFINSST_1;

    [Association(OneToOne,'ICMSID','ICMS','ID')]
    property ICMS: TICMS read FICMS_2 write FICMS_2;

    [Association(OneToOne,'ISSQNID','ISSQN','ID')]
    property ISSQN: TISSQN read FISSQN_3 write FISSQN_3;

    [Association(OneToOne,'PISID','PIS','ID')]
    property PIS: TPIS read FPIS_4 write FPIS_4;

    [Association(OneToOne,'PISSTID','PISST','ID')]
    property PISST: TPISST read FPISST_5 write FPISST_5;

    [Association(OneToOne,'TaxasID','TaxasFiscais','ID')]
    property TaxasFiscais: TTaxasFiscais read FTaxasFiscais_6 write FTaxasFiscais_6;

  end;

implementation

constructor TImpostos.Create;
begin
  FCOFINS_0 := TCOFINS.Create;
  FCOFINSST_1 := TCOFINSST.Create;
  FICMS_2 := TICMS.Create;
  FISSQN_3 := TISSQN.Create;
  FPIS_4 := TPIS.Create;
  FPISST_5 := TPISST.Create;
  FTaxasFiscais_6 := TTaxasFiscais.Create;
end;

destructor TImpostos.Destroy;
begin
  if Assigned(FCOFINS_0) then
    FCOFINS_0.Free;

  if Assigned(FCOFINSST_1) then
    FCOFINSST_1.Free;

  if Assigned(FICMS_2) then
    FICMS_2.Free;

  if Assigned(FISSQN_3) then
    FISSQN_3.Free;

  if Assigned(FPIS_4) then
    FPIS_4.Free;

  if Assigned(FPISST_5) then
    FPISST_5.Free;

  if Assigned(FTaxasFiscais_6) then
    FTaxasFiscais_6.Free;

  inherited;
end;

initialization

  TRegisterClass.RegisterEntity(TImpostos)

end.
