unit itemextracategoia;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  categoria,
  itemextra,
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('ItemExtraCategoia', '')]
  [PrimaryKey('ItemExtraID', NotInc, NoSort, False, 'Chave prim�ria')]
  [PrimaryKey('CategoriaID', NotInc, NoSort, False, 'Chave prim�ria')]
  TItemExtraCategoia = class
  private
    { Private declarations } 
    FItemExtraID: Integer;
    FCategoriaID: Integer;

    FCategoria_0:  TCategoria  ;
    FItemExtra_1:  TItemExtra  ;
  public 
    { Public declarations } 
    constructor Create;
    destructor Destroy; override;
    [Restrictions([NotNull])]
    [Column('ItemExtraID', ftInteger)]
    [ForeignKey('FK_ItemExtraCategoia_ItemExtra_ItemExtraID', 'ItemExtraID', 'ItemExtra', 'ID', Cascade, SetNull)]
    [Dictionary('ItemExtraID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ItemExtraID: Integer read FItemExtraID write FItemExtraID;

    [Restrictions([NotNull])]
    [Column('CategoriaID', ftInteger)]
    [ForeignKey('FK_ItemExtraCategoia_Categoria_CategoriaID', 'CategoriaID', 'Categoria', 'ID', Cascade, SetNull)]
    [Dictionary('CategoriaID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property CategoriaID: Integer read FCategoriaID write FCategoriaID;

    [Association(OneToOne,'CategoriaID','Categoria','ID')]
    property Categoria: TCategoria read FCategoria_0 write FCategoria_0;

    [Association(OneToOne,'ItemExtraID','ItemExtra','ID')]
    property ItemExtra: TItemExtra read FItemExtra_1 write FItemExtra_1;



  end;

implementation

constructor TItemExtraCategoia.Create;
begin
  FCategoria_0 := TCategoria.Create;
  FItemExtra_1 := TItemExtra.Create;
end;

destructor TItemExtraCategoia.Destroy;
begin
  if Assigned(FCategoria_0) then
    FCategoria_0.Free;

  if Assigned(FItemExtra_1) then
    FItemExtra_1.Free;

  inherited;
end;

initialization

  TRegisterClass.RegisterEntity(TItemExtraCategoia)

end.
