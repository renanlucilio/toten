unit uPizzasConfig;

interface

uses
  PersistenciaConfiguracao.Ini,
    PersistenciaConfiguracao.Interfaces,
    Factory.PersistenciaConfigucao,
    System.SysUtils;

type
  TCobrancaTamanho = (valorFixo, porcentagem, valorMM);
  TCobrancaTipo = (media, valorMaior);


  TPizzasConfig = class(TInterfacedObject, iConfiguravel)
  private
    FPorcentagemMedia: Double;
    FTipoCobranca: TCobrancaTipo;
    FPorcentagemGigante: Double;
    FValorBroto: Currency;
    FTamanhoPizza: TCobrancaTamanho;
    FValorFixoMBroto: Currency;
    FValorMedia: Currency;
    FPorcentagemBroto: Double;
    FValorGigante: Currency;
    FValorFixoMMedia: Currency;
    FValorFixoMGigante: Currency;
    
  public
    property CobrancaTamanho:    TCobrancaTamanho        read FTamanhoPizza   write FTamanhoPizza;
    property ValorBroto:         Currency                read FValorBroto   write FValorBroto;
    property ValorMedia:         Currency                read FValorMedia write FValorMedia;
    property ValorGigante:       Currency                read FValorGigante write FValorGigante;
    property ValorFixoMBroto:    Currency                read FValorFixoMBroto write FValorFixoMBroto;
    property ValorFixoMMedia:    Currency                read FValorFixoMMedia write FValorFixoMMedia;
    property ValorFixoMGigante:  Currency                read FValorFixoMGigante write FValorFixoMGigante;
    property PorcentagemBroto:   Double                  read FPorcentagemBroto write FPorcentagemBroto;
    property PorcentagemMedia:   Double                  read FPorcentagemMedia write FPorcentagemMedia;
    property PorcentagemGigante: Double                  read FPorcentagemGigante write FPorcentagemGigante;
    property CobrancaTipo:       TCobrancaTipo           read FTipoCobranca write FTipoCobranca; 

    procedure Salvar;
    procedure Carregar;
    function GetConfigurado: boolean;     

    constructor Create();

    procedure GetDadosForm();
    procedure SetDadosForm();

    function GetCobrancaTamanhoJson(): string;
    function GetCobrancaTipoJson(): string;
  end;

implementation

{ TPizzasConfig }

uses ufrmPrincipal, uCriadorJson;

procedure TPizzasConfig.Carregar;
var
  ini: iSalvaCarregaConfiguracao;
begin
  ini:= TFactoryPersistenciaConfiguracao.getPersistenciaConfiguracao(tpcIni);

  ini.carrega(self);

  ValorBroto:= StrToFloatDef(ini.getResultado.Items['ValorBroto'], 0);  
  ValorMedia:= StrToFloatDef(ini.getResultado.Items['ValorMedia'], 0);  
  ValorGigante:= StrToFloatDef(ini.getResultado.Items['ValorGigante'], 0);  
  ValorFixoMBroto:= StrToFloatDef(ini.getResultado.Items['ValorFixoMBroto'], 0);  
  ValorFixoMMedia:= StrToFloatDef(ini.getResultado.Items['ValorFixoMMedia'], 0);
  ValorFixoMGigante:= StrToFloatDef(ini.getResultado.Items['ValorFixoMGigante'], 0);  
  PorcentagemBroto:= StrToFloatDef(ini.getResultado.Items['PorcentagemBroto'], 0);  
  PorcentagemMedia:= StrToFloatDef(ini.getResultado.Items['PorcentagemMedia'], 0);  
  PorcentagemGigante:= StrToFloatDef(ini.getResultado.Items['PorcentagemGigante'], 0);  

  if ini.getResultado.Items['CobrancaTamanho'] = 'valorFixo' then
    CobrancaTamanho:= valorFixo
  else if ini.getResultado.Items['CobrancaTamanho'] = 'porcentagem' then
    CobrancaTamanho:= porcentagem
  else if ini.getResultado.Items['CobrancaTamanho'] = 'valorMM' then
    CobrancaTamanho:= valorMM;
    
  if ini.getResultado.Items['CobrancaTipo'] = 'media' then
    CobrancaTipo:= media
  else if ini.getResultado.Items['CobrancaTipo'] = 'valorMaior' then
    CobrancaTipo:= valorMaior;  

    
end;

constructor TPizzasConfig.Create;
begin
  CobrancaTamanho:= valorFixo;
  CobrancaTipo:= media;     
  
end;

function TPizzasConfig.GetCobrancaTipoJson: string;
var
  criadorJson: TCriadorJson;
begin
  criadorJson := TCriadorJson.Create;
  try
    criadorJson.Inicio;

    case CobrancaTipo of
      media: criadorJson.AddValue('tipoCobranca', 'media');
      valorMaior: criadorJson.AddValue('tipoCobranca', 'valorMaior');
    end;

    criadorJson.Fim;

    Result:= criadorJson.AsJsonStr;
  finally
    criadorJson.Free;
  end;
end;

function TPizzasConfig.GetCobrancaTamanhoJson: string;
var
  criadorJson: TCriadorJson;
begin
  criadorJson := TCriadorJson.Create;
  try
    criadorJson.Inicio;

    criadorJson.AddValue('valorBroto', ValorBroto);
    criadorJson.AddValue('valorMedia', ValorMedia);
    criadorJson.AddValue('valorGigante', ValorGigante);
    criadorJson.AddValue('valorFixoMBroto', ValorFixoMBroto);
    criadorJson.AddValue('valorFixoMMedia', ValorFixoMMedia);
    criadorJson.AddValue('valorFixoMGigante', ValorFixoMGigante);
    criadorJson.AddValue('porcentagemMedia', PorcentagemMedia);
    criadorJson.AddValue('porcentagemBroto', PorcentagemBroto);
    criadorJson.AddValue('porcentagemGigante', PorcentagemGigante);

    case CobrancaTamanho of
      valorFixo: criadorJson.AddValue('tipoCobranca', 'valorFixo');
      porcentagem: criadorJson.AddValue('tipoCobranca', 'porcentagem');
      valorMM: criadorJson.AddValue('tipoCobranca', 'valorMM');
    end;

    criadorJson.Fim;

    Result:= criadorJson.AsJsonStr;
  finally
    criadorJson.Free;
  end;
end;

function TPizzasConfig.GetConfigurado: boolean;
begin
  result:= true;
end;

procedure TPizzasConfig.GetDadosForm;
begin
  if formPrincipal.cedFormaCobrancaTamanhoPizza.ItemIndex = 0 then
  begin
    CobrancaTamanho:= valorFixo;
    ValorBroto:= StrToFloatDef(formPrincipal.edtTamanhoPizzaBroto.Text, 0); 
    ValorMedia:= StrToFloatDef(formPrincipal.edtTamanhoPizzaMedia.Text, 0);
    ValorGigante:= StrToFloatDef(formPrincipal.edtTamanhoPizzaGigante.Text, 0);
    ValorFixoMBroto:= 0;
    ValorFixoMMedia:= 0;
    ValorFixoMGigante:= 0;
    PorcentagemBroto:= 0;
    PorcentagemMedia:= 0;
    PorcentagemGigante:= 0;
  end
  else if formPrincipal.cedFormaCobrancaTamanhoPizza.ItemIndex = 1 then
  begin
    CobrancaTamanho:= porcentagem;
    ValorBroto:= 0;  
    ValorMedia:= 0;
    ValorGigante:= 0;
    ValorFixoMBroto:= 0;
    ValorFixoMMedia:= 0;
    ValorFixoMGigante:= 0;
    PorcentagemBroto:= StrToFloatDef(formPrincipal.edtTamanhoPizzaBroto.Text, 0); 
    PorcentagemMedia:= StrToFloatDef(formPrincipal.edtTamanhoPizzaMedia.Text, 0);
    PorcentagemGigante:= StrToFloatDef(formPrincipal.edtTamanhoPizzaGigante.Text, 0);   
  end
  else if formPrincipal.cedFormaCobrancaTamanhoPizza.ItemIndex = 2 then
  begin
    CobrancaTamanho:= valorMM;
    ValorBroto:= 0;  
    ValorMedia:= 0;
    ValorGigante:= 0;
    ValorFixoMBroto:= StrToFloatDef(formPrincipal.edtTamanhoPizzaBroto.Text, 0);
    ValorFixoMMedia:= StrToFloatDef(formPrincipal.edtTamanhoPizzaMedia.Text, 0);
    ValorFixoMGigante:= StrToFloatDef(formPrincipal.edtTamanhoPizzaGigante.Text, 0); 
    PorcentagemBroto:= 0;
    PorcentagemMedia:= 0;
    PorcentagemGigante:= 0;   
  end;

  if formPrincipal.cedFormaCobrancaTipoPizza.ItemIndex = 0 then
    CobrancaTipo:= media
  else if formPrincipal.cedFormaCobrancaTipoPizza.ItemIndex = 1 then
    CobrancaTipo:= valorMaior;   
  
       
end;

procedure TPizzasConfig.Salvar;
var
  ini: iSalvaCarregaConfiguracao;
begin
  ini:= TFactoryPersistenciaConfiguracao.getPersistenciaConfiguracao(tpcIni);

  ini.salva(self);

end;

procedure TPizzasConfig.SetDadosForm;
begin
   if CobrancaTamanho = valorFixo then
  begin
    formPrincipal.txtTipoCobrancaTamanhoPizza.Text:= 'Valor Fixo';
    formPrincipal.cedFormaCobrancaTamanhoPizza.ItemIndex := 0;
    formPrincipal.edtTamanhoPizzaBroto.Text:= CurrToStr(ValorBroto); 
    formPrincipal.edtTamanhoPizzaMedia.Text:= CurrToStr(ValorMedia);
    formPrincipal.edtTamanhoPizzaGigante.Text:= CurrToStr(ValorGigante);
  end
  else if CobrancaTamanho = porcentagem then
  begin
    formPrincipal.txtTipoCobrancaTamanhoPizza.Text:= 'Porcentagem';
    formPrincipal.cedFormaCobrancaTamanhoPizza.ItemIndex := 1;
    formPrincipal.edtTamanhoPizzaBroto.Text:= PorcentagemBroto.ToString; 
    formPrincipal.edtTamanhoPizzaMedia.Text:= PorcentagemMedia.ToString;
    formPrincipal.edtTamanhoPizzaGigante.Text:= PorcentagemMedia.ToString;
  end
  else if CobrancaTamanho = valorMM then
  begin
    formPrincipal.txtTipoCobrancaTamanhoPizza.Text:= 'Valor fixo a Mais, Valor fixo a Menos';
    formPrincipal.cedFormaCobrancaTamanhoPizza.ItemIndex := 2;
    formPrincipal.edtTamanhoPizzaBroto.Text:= CurrToStr(ValorFixoMBroto); 
    formPrincipal.edtTamanhoPizzaMedia.Text:= CurrToStr(ValorFixoMMedia);
    formPrincipal.edtTamanhoPizzaGigante.Text:= CurrToStr(ValorFixoMGigante);
  end;

  if CobrancaTipo = media then
    formPrincipal.cedFormaCobrancaTipoPizza.ItemIndex := 0
  else if CobrancaTipo= valorMaior then
    formPrincipal.cedFormaCobrancaTipoPizza.ItemIndex := 1;   
end;

end.
