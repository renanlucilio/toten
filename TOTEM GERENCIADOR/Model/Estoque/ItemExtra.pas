unit itemextra;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('ItemExtra', '')]
  [PrimaryKey('ID', NotInc, NoSort, False, 'Chave prim�ria')]
  TItemExtra = class
  private
    { Private declarations } 
    FID: Nullable<Integer>;
    FValor: Currency;
    FStatus: Integer;
    FDescricao: Nullable<String>;
  public 
    { Public declarations } 
    [Column('ID', ftInteger)]
    [Dictionary('ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ID: Nullable<Integer> read FID write FID;

    [Column('Descricao', ftString)]
    [Dictionary('Descricao', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Descricao: Nullable<String> read FDescricao write FDescricao;

    [Restrictions([NotNull])]
    [Column('Valor', ftCurrency)]
    [Dictionary('Valor', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Valor: Currency read FValor write FValor;

    [Restrictions([NotNull])]
    [Column('Status', ftInteger)]
    [Dictionary('Status', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Status: Integer read FStatus write FStatus;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TItemExtra)

end.
