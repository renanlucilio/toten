unit produto;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  categoria,
  impostos,
  unidade,
  valorespecial,
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('Produto', '')]
  [PrimaryKey('ID', NotInc, NoSort, False, 'Chave prim�ria')]
  TProduto = class
  private
    { Private declarations } 
    FID: Nullable<Integer>;
    FCategoriaID: Integer;
    FImpostosID: Nullable<Integer>;
    FUnidadeID: Integer;
    FValorID: Integer;
    FStatus: Integer;

    FCategoria_0:  TCategoria  ;
    FImpostos_1:  TImpostos  ;
    FUnidade_2:  TUnidade  ;
    FValorEspecial_3:  TValorEspecial  ;
    FDescricao: Nullable<String>;
    FCEST: Nullable<String>;
    FCodigoBarras: Nullable<String>;
    FCFOP: Nullable<String>;
    FNCM: Nullable<String>;
    FIngredientes: Nullable<String>;
  public 
    { Public declarations } 
    constructor Create;
    destructor Destroy; override;
    [Column('ID', ftInteger)]
    [Dictionary('ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ID: Nullable<Integer> read FID write FID;

    [Column('Descricao', ftString)]
    [Dictionary('Descricao', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Descricao: Nullable<String> read FDescricao write FDescricao;

    [Restrictions([NotNull])]
    [Column('CategoriaID', ftInteger)]
    [ForeignKey('FK_Produto_Categoria_CategoriaID', 'CategoriaID', 'Categoria', 'ID', Cascade, SetNull)]
    [Dictionary('CategoriaID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property CategoriaID: Integer read FCategoriaID write FCategoriaID;

    [Column('ImpostosID', ftInteger)]
    [ForeignKey('FK_Produto_Impostos_ImpostosID', 'ImpostosID', 'Impostos', 'ID', SetNull, SetNull)]
    [Dictionary('ImpostosID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ImpostosID: Nullable<Integer> read FImpostosID write FImpostosID;

    [Restrictions([NotNull])]
    [Column('UnidadeID', ftInteger)]
    [ForeignKey('FK_Produto_Unidade_UnidadeID', 'UnidadeID', 'Unidade', 'ID', Cascade, SetNull)]
    [Dictionary('UnidadeID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property UnidadeID: Integer read FUnidadeID write FUnidadeID;

    [Column('CodigoBarras', ftString)]
    [Dictionary('CodigoBarras', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property CodigoBarras: Nullable<String> read FCodigoBarras write FCodigoBarras;

    [Restrictions([NotNull])]
    [Column('ValorID', ftInteger)]
    [ForeignKey('FK_Produto_ValorEspecial_ValorID', 'ValorID', 'ValorEspecial', 'ID', Cascade, SetNull)]
    [Dictionary('ValorID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ValorID: Integer read FValorID write FValorID;

    [Column('NCM', ftString)]
    [Dictionary('NCM', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property NCM: Nullable<String> read FNCM write FNCM;

    [Column('CEST', ftString)]
    [Dictionary('CEST', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property CEST: Nullable<String> read FCEST write FCEST;

    [Column('CFOP', ftString)]
    [Dictionary('CFOP', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property CFOP: Nullable<String> read FCFOP write FCFOP;

    [Column('Ingredientes', ftString)]
    [Dictionary('Ingredientes', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Ingredientes: Nullable<String> read FIngredientes write FIngredientes;

    [Restrictions([NotNull])]
    [Column('Status', ftInteger)]
    [Dictionary('Status', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Status: Integer read FStatus write FStatus;

    [Association(OneToOne,'CategoriaID','Categoria','ID')]
    property Categoria: TCategoria read FCategoria_0 write FCategoria_0;

    [Association(OneToOne,'ImpostosID','Impostos','ID')]
    property Impostos: TImpostos read FImpostos_1 write FImpostos_1;

    [Association(OneToOne,'UnidadeID','Unidade','ID')]
    property Unidade: TUnidade read FUnidade_2 write FUnidade_2;

    [Association(OneToOne,'ValorID','ValorEspecial','ID')]
    property ValorEspecial: TValorEspecial read FValorEspecial_3 write FValorEspecial_3;

  end;

implementation

constructor TProduto.Create;
begin
  FCategoria_0 := TCategoria.Create;
  FImpostos_1 := TImpostos.Create;
  FUnidade_2 := TUnidade.Create;
  FValorEspecial_3 := TValorEspecial.Create;
end;

destructor TProduto.Destroy;
begin
  if Assigned(FCategoria_0) then
    FCategoria_0.Free;

  if Assigned(FImpostos_1) then
    FImpostos_1.Free;

  if Assigned(FUnidade_2) then
    FUnidade_2.Free;

  if Assigned(FValorEspecial_3) then
    FValorEspecial_3.Free;

  inherited;
end;

initialization

  TRegisterClass.RegisterEntity(TProduto)

end.
