unit categoria;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('Categoria', '')]
  [PrimaryKey('ID', NotInc, NoSort, False, 'Chave prim�ria')]
  TCategoria = class
  private
    { Private declarations } 
    FID: Nullable<Integer>;
    FDescricao: Nullable<String>;
    FStatus: Integer;
    FLocalLogo: Nullable<String>;
  public 
    { Public declarations } 
    [Column('ID', ftInteger)]
    [Dictionary('ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ID: Nullable<Integer> read FID write FID;

    [Column('Descricao', ftString, 450)]
    [Dictionary('Descricao', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Descricao: Nullable<String> read FDescricao write FDescricao;

    [Column('LocalLogo', ftString)]
    [Dictionary('LocalLogo', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property LocalLogo: Nullable<String> read FLocalLogo write FLocalLogo;

    [Restrictions([NotNull])]
    [Column('Status', ftInteger)]
    [Dictionary('Status', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Status: Integer read FStatus write FStatus;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TCategoria)

end.
