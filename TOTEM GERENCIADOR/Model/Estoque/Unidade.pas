unit unidade;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('Unidade', '')]
  [PrimaryKey('ID', NotInc, NoSort, False, 'Chave prim�ria')]
  TUnidade = class
  private
    { Private declarations }
    FID: Nullable<Integer>;

    FTemDecimal: Boolean;
    FDescricao: Nullable<String>;
    FSigla: Nullable<String>;
  public 
    { Public declarations } 
    [Column('ID', ftInteger)]
    [Dictionary('ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ID: Nullable<Integer> read FID write FID;

    [Column('Descricao', ftString)]
    [Dictionary('Descricao', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Descricao: Nullable<String> read FDescricao write FDescricao;

    [Column('Sigla', ftString)]
    [Dictionary('Sigla', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Sigla: Nullable<String> read FSigla write FSigla;

    [Restrictions([NotNull])]
    [Column('TemDecimal', ftBoolean)]
    [Dictionary('TemDecimal', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property TemDecimal: Boolean read FTemDecimal write FTemDecimal;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TUnidade)

end.
