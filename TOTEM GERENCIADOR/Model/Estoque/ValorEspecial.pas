unit valorespecial;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('ValorEspecial', '')]
  [PrimaryKey('ID', NotInc, NoSort, False, 'Chave prim�ria')]
  TValorEspecial = class
  private
    { Private declarations } 
    FID: Nullable<Integer>;
    FValorPequeno: Double;
    FValorMedia: Double;
    FValorGigante: Double;
    FValorNormal: Double;
  public 
    { Public declarations } 
    [Column('ID', ftInteger)]
    [Dictionary('ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ID: Nullable<Integer> read FID write FID;

    [Restrictions([NotNull])]
    [Column('ValorPequeno', ftBCD, 18, 2)]
    [Dictionary('ValorPequeno', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property ValorPequeno: Double read FValorPequeno write FValorPequeno;

    [Restrictions([NotNull])]
    [Column('ValorMedia', ftBCD, 18, 2)]
    [Dictionary('ValorMedia', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property ValorMedia: Double read FValorMedia write FValorMedia;

    [Restrictions([NotNull])]
    [Column('ValorGigante', ftBCD, 18, 2)]
    [Dictionary('ValorGigante', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property ValorGigante: Double read FValorGigante write FValorGigante;

    [Restrictions([NotNull])]
    [Column('ValorNormal', ftBCD, 18, 2)]
    [Dictionary('ValorNormal', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property ValorNormal: Double read FValorNormal write FValorNormal;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TValorEspecial)

end.
