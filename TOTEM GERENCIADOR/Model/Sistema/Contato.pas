unit contato;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('Contato', '')]
  [PrimaryKey('ID', NotInc, NoSort, False, 'Chave prim�ria')]
  TContato = class
  private
    { Private declarations } 
    FID: Nullable<Integer>;
    FTipo: Nullable<string>;
    FOhContato: Nullable<string>;
  public 
    { Public declarations } 
    [Column('ID', ftInteger)]
    [Dictionary('ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ID: Nullable<Integer> read FID write FID;

    [Column('OhContato', ftString)]
    [Dictionary('OhContato', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property OhContato: Nullable<string> read FOhContato write FOhContato;

    [Column('Tipo', ftString)]
    [Dictionary('Tipo', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Tipo: Nullable<string> read FTipo write FTipo;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TContato)

end.
