unit cliente;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  contato,
  endereco,
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('Cliente', '')]
  [PrimaryKey('ID', NotInc, NoSort, False, 'Chave prim�ria')]
  TCliente = class
  private
    { Private declarations } 
    FID: Nullable<Integer>;

    FEnderecoID: Integer;
    FContatoID: Integer;

    FContato_0:  TContato  ;
    FEndereco_1:  TEndereco  ;
    FDocumento: Nullable<String>;
    FNome: Nullable<String>;
  public 
    { Public declarations } 
    constructor Create;
    destructor Destroy; override;
    [Column('ID', ftInteger)]
    [Dictionary('ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ID: Nullable<Integer> read FID write FID;

    [Column('Nome', ftString)]
    [Dictionary('Nome', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Nome: Nullable<String> read FNome write FNome;

    [Column('Documento', ftString)]
    [Dictionary('Documento', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Documento: Nullable<String> read FDocumento write FDocumento;

    [Restrictions([NotNull])]
    [Column('EnderecoID', ftInteger)]
    [ForeignKey('FK_Cliente_Endereco_EnderecoID', 'EnderecoID', 'Endereco', 'ID', Cascade, SetNull)]
    [Dictionary('EnderecoID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property EnderecoID: Integer read FEnderecoID write FEnderecoID;

    [Restrictions([NotNull])]
    [Column('ContatoID', ftInteger)]
    [ForeignKey('FK_Cliente_Contato_ContatoID', 'ContatoID', 'Contato', 'ID', Cascade, SetNull)]
    [Dictionary('ContatoID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ContatoID: Integer read FContatoID write FContatoID;

    [Association(OneToOne,'ContatoID','Contato','ID')]
    property Contato: TContato read FContato_0 write FContato_0;

    [Association(OneToOne,'EnderecoID','Endereco','ID')]
    property Endereco: TEndereco read FEndereco_1 write FEndereco_1;

  end;

implementation

constructor TCliente.Create;
begin
  FContato_0 := TContato.Create;
  FEndereco_1 := TEndereco.Create;
end;

destructor TCliente.Destroy;
begin
  if Assigned(FContato_0) then
    FContato_0.Free;

  if Assigned(FEndereco_1) then
    FEndereco_1.Free;

  inherited;
end;

initialization

  TRegisterClass.RegisterEntity(TCliente)

end.
