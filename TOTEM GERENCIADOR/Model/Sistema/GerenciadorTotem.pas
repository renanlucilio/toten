unit GerenciadorTotem;

interface

uses
  System.Generics.Collections, Totem;

type
  TGerenciadorTotem = class
  private
    FIP: string;
    FPorta: string;
  public
    property IP: string read FIP ;
    property Porta: string read FPorta;
    constructor Create;

    procedure GerarConfiguracoes();
  end;

implementation

uses
  Model.LibUtil, uTotemRepository, System.IniFiles, System.SysUtils, System.IOUtils;
{ TGerenciadorTotem }

constructor TGerenciadorTotem.Create;
begin
  FIP:= getIP;
  FPorta:= '8082';
end;

procedure TGerenciadorTotem.GerarConfiguracoes();
var
  repo: TTotemRepository;
  list: TObjectList<TTotem>;
  it: TTotem;
  arquivoIni: TIniFile;
  rota: string;
  local: string;
begin
  repo := TTotemRepository.Create;
  try
    list:= repo.GetTotens();

    arquivoIni := TIniFile.Create(
      ExtractFilePath(ParamStr(0))+'totemServ.ini');
    try
      rota:= Concat(IP,':',Porta);
      arquivoIni.WriteString('serv', 'IP', rota);
    finally
      arquivoIni.Free;
    end;


    for it in list do
    begin
      local:= ExtractFilePath(it.Mapeamento) + 'totemServ.ini';

      if FileExists(local) then
        TFile.Delete(local);

      TFile.Copy(ExtractFilePath(ParamStr(0))+'totemServ.ini', local);
    end;

  finally
    repo.Free;
  end;
end;

end.
