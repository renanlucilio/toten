unit Estabelecimento;

interface

uses
  PersistenciaConfiguracao.Ini,
    PersistenciaConfiguracao.Interfaces,
    Factory.PersistenciaConfigucao;

type
  TEstabelecimento = class(TInterfacedObject, iConfiguravel)
  private
    FLocalLogo: string;
    FNomeFantasia: string;
  public
    property LocalLogo: string read FLocalLogo write FLocalLogo;
    property NomeFantasia: string read FNomeFantasia write FNomeFantasia;

    procedure Salvar;
    procedure Carregar;
    function GetConfigurado: boolean;

    procedure GetDadosForm();
    procedure SetDadosForm();

    function GetEstabelecimentoJson: string;
  end;

implementation

{ TEstabelecimento }

uses ufrmPrincipal, System.SysUtils, System.UITypes, uCriadorJson,
  FMX.Graphics, uLibUtil
;

procedure TEstabelecimento.Carregar;
var
  ini: iSalvaCarregaConfiguracao;
begin
  ini:= TFactoryPersistenciaConfiguracao.getPersistenciaConfiguracao(tpcIni);

  ini.carrega(self);

  LocalLogo:= ini.getResultado.Items['LocalLogo'];
  NomeFantasia:= ini.getResultado.Items['NomeFantasia'];
end;

function TEstabelecimento.GetConfigurado: boolean;
begin
  result:= true;
end;

procedure TEstabelecimento.GetDadosForm;
begin
  LocalLogo:= formPrincipal.edtEstabelecimentoLogo.Text;
  NomeFantasia:= formPrincipal.edtEstabelecimentoNome.Text;

end;

function TEstabelecimento.GetEstabelecimentoJson: string;
var
  criadorJson: TCriadorJson;
  logo: TBitmap;
begin
  criadorJson := TCriadorJson.Create;
  try
    criadorJson.Inicio;

    criadorJson.AddValue('id', 0);
    criadorJson.AddValue('nomeFantasia', NomeFantasia);

    if FileExists(LocalLogo) then
    begin
      logo:= TBitmap.Create();
      logo.LoadFromFile(LocalLogo);
      criadorJson.AddValue('logo', Base64FromBitmap(logo));
    end
    else
      criadorJson.AddValue('logo', '');

    criadorJson.Fim;

    Result:= criadorJson.AsJsonStr;
  finally
    criadorJson.Free;
  end;
end;

procedure TEstabelecimento.Salvar;
var
  ini: iSalvaCarregaConfiguracao;
begin
  ini:= TFactoryPersistenciaConfiguracao.getPersistenciaConfiguracao(tpcIni);

  ini.salva(self);

end;

procedure TEstabelecimento.SetDadosForm;
begin
  formPrincipal.edtEstabelecimentoLogo.Text:= LocalLogo;
  formPrincipal.edtEstabelecimentoNome.Text:= NomeFantasia;

  if FileExists(LocalLogo) then
    formPrincipal.imgLogoEstabelecimento.Bitmap.LoadFromFile(LocalLogo)
  else
    formPrincipal.imgLogoEstabelecimento.Bitmap.Clear(TColor($FFFFFF));

end;

end.
