unit totem;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('Totem', '')]
  [PrimaryKey('ID', NotInc, NoSort, False, 'Chave prim�ria')]
  TTotem = class
  private
    { Private declarations } 
    FID: Nullable<Integer>;
    FStatus: Integer;
    FLocal: Nullable<string>;
    FMapeamento: Nullable<string>;
    FIp: Nullable<string>;
    FNome: Nullable<string>;
  public 
    { Public declarations } 
    [Column('ID', ftInteger)]
    [Dictionary('ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ID: Nullable<Integer> read FID write FID;

    [Column('Local', ftString)]
    [Dictionary('Local', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Local: Nullable<string> read FLocal write FLocal;

    [Column('Nome', ftString)]
    [Dictionary('Nome', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Nome: Nullable<string> read FNome write FNome;

    [Column('Ip', ftString)]
    [Dictionary('Ip', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Ip: Nullable<string> read FIp write FIp;

    [Column('Mapeamento', ftString)]
    [Dictionary('Mapeamento', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Mapeamento: Nullable<string> read FMapeamento write FMapeamento;

    [Restrictions([NotNull])]
    [Column('Status', ftInteger)]
    [Dictionary('Status', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Status: Integer read FStatus write FStatus;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TTotem)

end.
