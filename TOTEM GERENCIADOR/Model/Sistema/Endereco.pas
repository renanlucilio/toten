unit endereco;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('Endereco', '')]
  [PrimaryKey('ID', NotInc, NoSort, False, 'Chave prim�ria')]
  TEndereco = class
  private
    { Private declarations } 
    FID: Nullable<Integer>;
    FLogradouro: Nullable<string>;
    FBairro: Nullable<string>;
    FUF: Nullable<string>;
    FCEP: Nullable<string>;
    FNumero: Nullable<string>;
    FComplemento: Nullable<string>;
    FReferencia: Nullable<string>;
    FCidade: Nullable<string>;
  public 
    { Public declarations } 
    [Column('ID', ftInteger)]
    [Dictionary('ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ID: Nullable<Integer> read FID write FID;

    [Column('Logradouro', ftString)]
    [Dictionary('Logradouro', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Logradouro: Nullable<string> read FLogradouro write FLogradouro;

    [Column('Numero', ftString)]
    [Dictionary('Numero', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Numero: Nullable<string> read FNumero write FNumero;

    [Column('Bairro', ftString)]
    [Dictionary('Bairro', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Bairro: Nullable<string> read FBairro write FBairro;

    [Column('Cidade', ftString)]
    [Dictionary('Cidade', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Cidade: Nullable<string> read FCidade write FCidade;

    [Column('UF', ftString)]
    [Dictionary('UF', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property UF: Nullable<string> read FUF write FUF;

    [Column('CEP', ftString)]
    [Dictionary('CEP', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property CEP: Nullable<string> read FCEP write FCEP;

    [Column('Complemento', ftString)]
    [Dictionary('Complemento', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Complemento: Nullable<string> read FComplemento write FComplemento;

    [Column('Referencia', ftString)]
    [Dictionary('Referencia', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Referencia: Nullable<string> read FReferencia write FReferencia;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TEndereco)

end.
