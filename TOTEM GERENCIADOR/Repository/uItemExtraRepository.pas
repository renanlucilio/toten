unit uItemExtraRepository;

interface

uses uContextoDB, ItemExtra, ItemExtraCategoia, uDmCon, Categoria, System.Generics.Collections;

type
  TItemExtraRepository = class
  private
    contexto: TContextoDB;
    procedure LimparDadosItemExtra();
    procedure SetarCampos(itemExtra: TItemExtra; listaCategorias: TObjectList<TItemExtraCategoia>);
    procedure RemoverDiferentes(itemExtra: TItemExtra; listaCategorias: TObjectList<TItemExtraCategoia>);
    procedure AddItemCategoria(itemCategoria: TItemExtraCategoia);

    function IsItemExiste(id: integer): Boolean;
    procedure CarregarCategorias();
  public
    constructor Create();

    procedure InitInsert();
    procedure Insert(itemExtra: TItemExtra; listaCategorias: TObjectList<TCategoria>);
    procedure InitEdit(id: Integer);
    procedure Edit(itemExtra: TItemExtra;listaCategorias: TObjectList<TItemExtraCategoia>);
    procedure Delete(id: Integer);
    procedure CarregaItensExtra();
    procedure AddItemCategoriaLista(id: integer);

    function GetItensExtraJson(): string;
  end;

implementation

{ TItemExtraRepository }

uses ufrmPrincipal, System.SysUtils, uConexao.Query, FMX.Controls, FMX.Layouts,
  FMX.Objects, FMX.StdCtrls, FMX.Types, System.UITypes,
  FMX.ListView.Appearances, FMX.ListView.Types;

procedure TItemExtraRepository.AddItemCategoriaLista(id: integer);
var
  categoria: TCategoria;
  itemTemp: TItemExtraCategoia;
begin
  if not IsItemExiste(id) then
  begin
    try
      itemTemp:= TItemExtraCategoia.Create;
      itemTemp.CategoriaID:= id;
      categoria:= contexto.ORM.DAO<TCategoria>().Find(id);

      itemTemp.Categoria:= categoria;

      AddItemCategoria(itemTemp);
    finally
      itemTemp.Free;
    end;                      
  end;
end;

procedure TItemExtraRepository.CarregaItensExtra;
begin
  dmCon.Con.Close();
  dmCon.tableItemExtra.Active:= false;
  dmCon.Con.Open();
  dmCon.tableItemExtra.Active:= true;
end;

procedure TItemExtraRepository.CarregarCategorias;
var
  categorias: TObjectList<TCategoria>;
  categoria: TCategoria;  
  linha: string;
begin

  categorias:= contexto.ORM.DAO<TCategoria>().Find();

  try
    formPrincipal.cedItemExtraCategoria.Items.Clear;
    for categoria in categorias do
    begin
      linha:= Concat(categoria.ID.Value.ToString, '|', categoria.Descricao);

      formPrincipal.cedItemExtraCategoria.Items.Add(linha);
          
    end;
  finally
    categorias.Free;
  end;

end;

procedure TItemExtraRepository.AddItemCategoria(itemCategoria: TItemExtraCategoia);
var
  novoItem: TListViewItem;
begin
  novoItem:= formPrincipal.lsvItensCategorias.Items.Add();

  novoItem.Tag:= itemCategoria.CategoriaID;
  novoItem.Text:= itemCategoria.Categoria.Descricao;

end;

constructor TItemExtraRepository.Create;
begin
  contexto := TContextoDB.Create(dmCon.Con);
end;

procedure TItemExtraRepository.Delete(id: Integer);
var
  itemExtra: TItemExtra;
begin

  itemExtra:= contexto
              .ORM
              .DAO<TItemExtra>()
              .Find(id);

  if Assigned(itemExtra) then
  begin
    try
      contexto
        .ORM
        .DAO<TItemExtra>()
        .Delete(itemExtra);

      formPrincipal.tbcItemExtra.ActiveTab:= formPrincipal.tabItemExtraLista;
      CarregaItensExtra();
    except
      formPrincipal.tbcItemExtra.ActiveTab:= formPrincipal.tabItemExtraCad;
    end;
  end;
end;

procedure TItemExtraRepository.Edit(itemExtra: TItemExtra; listaCategorias: TObjectList<TItemExtraCategoia>);
var
  qry: TQuery;
begin
  try
    if Assigned(itemExtra) then
    begin

      try
        qry:= TQuery.Create(dmCon.Con);
        qry.ref;

        RemoverDiferentes(itemExtra, listaCategorias);

        qry.SQL.Update('ItemExtra')
          .&Set('Descricao', itemExtra.Descricao.Value.QuotedString)
          .&Set('Valor', CurrToStr(itemExtra.Valor))
          .&Set('Status', itemExtra.Status.ToString)
          .Where('ID ='+ itemExtra.ID.GetValueOrDefault.ToString);

        qry.Exec();


        formPrincipal.tbcItemExtra.ActiveTab:= formPrincipal.tabItemExtraLista;
        CarregaItensExtra();
      except
        formPrincipal.tbcItemExtra.ActiveTab:= formPrincipal.tabItemExtraCad;
      end;

    end;
  finally

    if Assigned(itemExtra) then
      itemExtra.Free;
  end;
end;

function TItemExtraRepository.GetItensExtraJson: string;
var
  qry: TQuery;
begin
  qry:= TQuery.Create(dmCon.Con);
  qry.Ref;
  try
    qry.SQL.Select
        .Column('ID').&As('id')
        .Column('Descricao').&As('descricao')
        .Column('Valor').&As('valorUnitario')
      .From('ItemExtra')
      .Where('Status = 0');

    qry.Open();
    Result:= qry.ToJson;
  except
    result:= '{}';
  end;
end;

procedure TItemExtraRepository.InitEdit(id: Integer);
var
  itemExtra: TItemExtra;
  itensCategorias: TObjectList<TItemExtraCategoia>;
begin
  itemExtra:= contexto
              .ORM
              .DAO<TItemExtra>()
              .Find(id);

  itensCategorias:= contexto.ORM.DAO<TItemExtraCategoia>().FindWhere('ItemExtraID ='+ id.ToString);

  if Assigned(itemExtra) then
  begin
    SetarCampos(itemExtra, itensCategorias);
    CarregarCategorias();
    formPrincipal.txtItemExtraTipoForm.Tag:= 1;
    formPrincipal.tbcItemExtra.ActiveTab:= formPrincipal.tabItemExtraCad;
  end;
end;

procedure TItemExtraRepository.InitInsert;
begin

  LimparDadosItemExtra();
  CarregarCategorias();
  formPrincipal.txtItemExtraTipoForm.Tag:= 0;
  formPrincipal.tbcItemExtra.ActiveTab:= formPrincipal.tabItemExtraCad;
end;

procedure TItemExtraRepository.Insert(itemExtra: TItemExtra; listaCategorias: TObjectList<TCategoria>);
var
  itemCategoria: TItemExtraCategoia;
  itemExtraTemp: TItemExtra;
  listaDeItensExtra: TObjectList<TItemExtra>;
  categoria, categoriaTemp: TCategoria;
begin
  try
    if Assigned(itemExtra) then
    begin
      contexto
          .ORM
          .DAO<TItemExtra>()
          .Insert(itemExtra);

      listaDeItensExtra:= contexto.ORM.DAO<TItemExtra>().Find();
      itemExtraTemp:= listaDeItensExtra.Last;

      try
        if Assigned(listaCategorias) then
        begin
          for categoria in listaCategorias do
          begin
            try
              categoriaTemp:= contexto.ORM.DAO<TCategoria>().Find(categoria.ID);
              itemCategoria:= TItemExtraCategoia.Create();

              itemCategoria.ItemExtraID:= itemExtraTemp.ID.GetValueOrDefault;
              itemCategoria.CategoriaID:= categoria.ID;

              contexto
                .ORM
                .DAO<TItemExtraCategoia>()
                .Insert(itemCategoria);
            finally
              categoriaTemp.Free;
              itemCategoria.Free;
            end;
          end;
        end;



        formPrincipal.tbcItemExtra.ActiveTab:= formPrincipal.tabItemExtraLista;
        CarregaItensExtra();
      except
        formPrincipal.tbcItemExtra.ActiveTab:= formPrincipal.tabItemExtraCad;
      end;

    end;
  finally
    if Assigned(itemExtra) then
      itemExtra.Free;

    if Assigned(listaCategorias) then
      listaCategorias.Free;

    if Assigned(itemExtraTemp) then
      itemExtraTemp.Free;

  end;
end;

function TItemExtraRepository.IsItemExiste(id: integer): Boolean;
var
  i: integer;
  it: TListItem;
begin

  for I := 0 to formPrincipal.lsvItensCategorias.Items.Count-1 do
  begin
    it:= formPrincipal.lsvItensCategorias.Items.Item[I];

    if it.Tag = id then
      Exit(true);
  end;

  result:= false;
          
end;

procedure TItemExtraRepository.LimparDadosItemExtra;
var
  I: Integer;
begin
    formPrincipal.lsvItensCategorias.Items.Clear;

    formPrincipal.edtItemExtraDescricao.Text:= '';
    formPrincipal.edtItemExtraValor.Text:= '';
    formPrincipal.cedItemExtraCategoria.ItemIndex:= 0;
    formPrincipal.cedItemExtraStatus.ItemIndex:= 0;
end;

procedure TItemExtraRepository.RemoverDiferentes(itemExtra: TItemExtra;
  listaCategorias: TObjectList<TItemExtraCategoia>);
var
  qry: TQuery;
  it: TItemExtraCategoia;
  exist: string;
begin
  qry:= TQuery.Create(dmCon.Con);
  qry.Ref;
  qry.SQL.Delete.From('ItemExtraCategoia').Where('ItemExtraID = '+ ItemExtra.ID);
  qry.Exec;

  for it in listaCategorias do
  begin
    it.ItemExtraID:= itemExtra.ID;
    it.CategoriaID:= it.Categoria.ID;
    contexto.ORM.DAO<TItemExtraCategoia>().Insert(it);
  end;
end;

procedure TItemExtraRepository.SetarCampos(itemExtra: TItemExtra; listaCategorias: TObjectList<TItemExtraCategoia>);
var
  itemCategoria: TItemExtraCategoia;
begin
  formPrincipal.edtItemExtraDescricao.Text:= itemExtra.Descricao;
  formPrincipal.edtItemExtraValor.Text:= CurrToStr(itemExtra.Valor);
  formPrincipal.cedItemExtraStatus.ItemIndex:= itemExtra.Status;
  formPrincipal.lsvItensCategorias.Items.Clear;
  try
    for itemCategoria in listaCategorias do
    begin
      AddItemCategoria(itemCategoria);
    end;
  finally
    listaCategorias.Free;
  end;
end;

end.
