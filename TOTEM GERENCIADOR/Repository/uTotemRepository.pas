unit uTotemRepository;

interface

uses Totem, uContextoDB, uDmCon, uConexao.Query, System.Generics.Collections;

type
  TTotemRepository = class
  private
    contexto: TContextoDB;
    procedure LimparDadosTotem();
    procedure SetarCampos(totem: TTotem);
  public
    constructor Create();

    procedure InitInsert();
    procedure Insert(totem: TTotem);
    procedure InitEdit(id: Integer);
    procedure Edit(totem: TTotem);
    procedure Delete(id: Integer);
    procedure CarregaTotens();

    function GetTotens: TObjectList<TTotem>;
  end;

implementation

uses
  System.SysUtils, ufrmPrincipal;

{ TotemRepository }

procedure TTotemRepository.CarregaTotens;
begin
  dmCon.Con.Close();
  dmCon.tableTotem.Active:= false;
  dmCon.Con.Open();
  dmCon.tableTotem.Active:= true;
end;

constructor TTotemRepository.Create;
begin
  contexto := TContextoDB.Create(dmCon.Con);
end;

procedure TTotemRepository.Delete(id: Integer);
var
  totem: TTotem;
begin

  totem:= contexto
              .ORM
              .DAO<TTotem>()
              .Find(id);

  if Assigned(totem) then
  begin
    try
      contexto
        .ORM
        .DAO<TTotem>()
        .Delete(totem);

      formPrincipal.tbcTotens.ActiveTab:= formPrincipal.tabTotemLista;
      CarregaTotens();
    except
      formPrincipal.tbcTotens.ActiveTab:= formPrincipal.tabTotemCad;
    end;
  end;
end;

procedure TTotemRepository.Edit(totem: TTotem);
var
  qry: TQuery;
begin
  try
    if Assigned(totem) then
    begin

      try
        qry:= TQuery.Create(dmCon.Con);
        qry.ref;

//        totem.Mapeamento:= StringReplace(totem.Mapeamento, '\', '\\', [rfReplaceAll]);

        qry.SQL.Update('Totem')
          .&Set('Local', totem.Local.Value.QuotedString)
          .&Set('Nome', totem.Nome.Value.QuotedString)
          .&Set('Ip', totem.Ip.Value.QuotedString)
          .&Set('Mapeamento', totem.Mapeamento.Value.QuotedString)
          .&Set('Status', totem.Status.ToString)
        .Where('ID ='+totem.ID.Value.ToString);

        qry.Exec();


        formPrincipal.tbcTotens.ActiveTab:= formPrincipal.tabTotemLista;
        CarregaTotens();
      except
        formPrincipal.tbcTotens.ActiveTab:= formPrincipal.tabTotemCad;
      end;

    end;
  finally

    if Assigned(totem) then
      totem.Free;
  end;
end;

function TTotemRepository.GetTotens: TObjectList<TTotem>;
begin
  Result:= contexto
              .ORM
              .DAO<TTotem>()
              .Find();
end;

procedure TTotemRepository.InitEdit(id: Integer);
var
  totem: TTotem;
begin
  totem:= contexto
              .ORM
              .DAO<TTotem>()
              .Find(id);

  if Assigned(totem) then
  begin
    SetarCampos(totem);
    formPrincipal.txtTotemTipoForm.Tag:= 1;
    formPrincipal.tbcTotens.ActiveTab:= formPrincipal.tabTotemCad;
  end;
end;

procedure TTotemRepository.InitInsert;
begin
  LimparDadosTotem();
  formPrincipal.txtTotemTipoForm.Tag:= 0;
  formPrincipal.tbcTotens.ActiveTab:= formPrincipal.tabTotemCad;
end;

procedure TTotemRepository.Insert(totem: TTotem);
begin
   try
    if Assigned(totem) then
    begin

      try
        contexto
          .ORM
          .DAO<TTotem>()
          .Insert(totem);

        formPrincipal.tbcTotens.ActiveTab:= formPrincipal.tabTotemLista;
        CarregaTotens();
      except
        formPrincipal.tbcTotens.ActiveTab:= formPrincipal.tabTotemCad;
      end;

    end;
  finally
    if Assigned(totem) then
      totem.Free;

  end;
end;

procedure TTotemRepository.LimparDadosTotem;
begin
  formPrincipal.edtTotemLocal.Text:= '';
  formPrincipal.cedTotemStatus.Text:= '';
  formPrincipal.edtTotemMapeamento.Text:= '';
  formPrincipal.edtTotemIp.Text:= '';
  formPrincipal.edtTotemNome.Text:= '';
end;

procedure TTotemRepository.SetarCampos(totem: TTotem);
begin
  formPrincipal.edtTotemLocal.Text:= totem.Local;
  formPrincipal.edtTotemMapeamento.Text:= totem.Mapeamento;
  formPrincipal.edtTotemIp.Text:= totem.Ip;
  formPrincipal.edtTotemNome.Text:= totem.Nome;
  formPrincipal.cedTotemStatus.ItemIndex:= totem.Status;
end;

end.
