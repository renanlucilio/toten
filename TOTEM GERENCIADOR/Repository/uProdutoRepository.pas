unit uProdutoRepository;

interface

uses Produto, uContextoDB, Impostos, Categoria, uConexao.Query, System.SysUtils, uCriadorJson,
    uCriadorJson.Interfaces;

type
  TProdutoRepository = class
  private
    contexto: TContextoDB;
    procedure LimparDadosProdutos();
    procedure SetarCampos(produto: TProduto);
    procedure CarregarCategorias();
    procedure CarregarTributacoes();
    procedure UpdateProduto(produto: TProduto);
    procedure UpdateUnidade(produto: TProduto);
    procedure UpdateValorEspecial(produto: TProduto);
  public
    constructor Create();

    procedure InitInsert();
    procedure Insert(produto: TProduto);
    procedure InitEdit(id: Integer);
    procedure Edit(produto: TProduto);
    procedure Delete(id: Integer);
    procedure CarregaProdutos();

    function GetProdutosJson(): string;
  end;

implementation

uses ufrmPrincipal, uDmCon, System.Generics.Collections, ValorEspecial,
  Unidade;

{ TProdutoRepository }

procedure TProdutoRepository.CarregaProdutos;
begin
  dmCon.Con.Close();
  dmCon.tableProduto.Active:= false;
  dmCon.Con.Open();
  dmCon.tableProduto.Active:= true;
end;

procedure TProdutoRepository.CarregarCategorias;
var
  categorias: TObjectList<TCategoria>;
  categoria: TCategoria;
  linha: string;
begin
  formPrincipal.cedProdutoCategorias.Items.Clear;
  categorias:= contexto.ORM.DAO<TCategoria>().Find();

  try
    formPrincipal.cedProdutoCategorias.Items.Clear;
    for categoria in categorias do
    begin
      linha:= Concat(categoria.ID.Value.ToString, '|', categoria.Descricao);

      formPrincipal.cedProdutoCategorias.Items.Add(linha);

    end;
  finally
    categorias.Free;
  end;

end;

procedure TProdutoRepository.CarregarTributacoes;
var
  tributacoes: TObjectList<TImpostos>;
  tributacao: TImpostos;
  linha: string;
begin
  formPrincipal.cedProdutoTributacao.Items.Clear;
  tributacoes:= contexto.ORM.DAO<TImpostos>().Find();

  try

    for tributacao in tributacoes do
    begin
      linha:= Concat(tributacao.ID.Value.ToString, '|', tributacao.Descricao);

      formPrincipal.cedProdutoTributacao.Items.Add(linha);

    end;
  finally
    tributacoes.Free;
  end;

end;
constructor TProdutoRepository.Create;
begin
  contexto := TContextoDB.Create(dmCon.Con);
end;

procedure TProdutoRepository.Delete(id: Integer);
var
  produto: TProduto;
begin

  produto:= contexto
              .ORM
              .DAO<TProduto>()
              .Find(id);

  if Assigned(produto) then
  begin
    try
      contexto
        .ORM
        .DAO<TProduto>()
        .Delete(produto);

      formPrincipal.tbcFormProduto.ActiveTab:= formPrincipal.tabProdutoLista;
      CarregaProdutos();
    except
      formPrincipal.tbcFormProduto.ActiveTab:= formPrincipal.tabProdutoCad;
    end;
  end;
end;

procedure TProdutoRepository.Edit(produto: TProduto);
begin
  try
    if Assigned(produto) then
    begin

      try

        UpdateProduto(produto);
        UpdateUnidade(produto);
        UpdateValorEspecial(produto);

        formPrincipal.tbcFormProduto.ActiveTab:= formPrincipal.tabProdutoLista;
        CarregaProdutos();
      except
        formPrincipal.tbcFormProduto.ActiveTab:= formPrincipal.tabProdutoCad;
      end;

    end;
  finally

    if Assigned(produto) then
      produto.Free;
  end;
end;

function TProdutoRepository.GetProdutosJson: string;
var
  qry: TQuery;
  criadorJson: TCriadorJson;
begin
  qry:= TQuery.Create(dmCon.Con);
  qry.Ref;
  try
    qry.SQL.Select
        .Column('p.ID').&As('id')
        .Column('v.ValorNormal').&As('valorUnitario')
        .Column('p.Descricao').&As('descricao')
        .Column('p.Ingredientes').&As('detalhes')
        .Column('p.CategoriaID').&As('categoria')
        .Column('u.Descricao').&As('descricaoUnidade')
        .Column('u.Sigla').&As('siglaUnidade')
        .Column('u.TemDecimal').&As('temCasaDecimal')
      .From('Produto p')
      .InnerJoin('ValorEspecial v')
        .&On('p.ValorID = v.ID')
      .InnerJoin('Unidade u')
        .&On('p.UnidadeID = u.ID')
      .Where('p.Status = 0');


    qry.Open();
    Result:= qry.ToJson;
  except
    Result:= '{}';
  end;

end;

procedure TProdutoRepository.InitEdit(id: Integer);
var
  produto: TProduto;
begin
  produto:= contexto
              .ORM
              .DAO<TProduto>()
              .Find(id);

  if Assigned(produto) then
  begin
    CarregarCategorias();
    CarregarTributacoes();
    SetarCampos(produto);
    formPrincipal.txtProdutoTipoForm.Tag:= 1;
    formPrincipal.tbcFormProduto.ActiveTab:= formPrincipal.tabProdutoCad;
  end;
end;

procedure TProdutoRepository.InitInsert;
begin
  CarregarCategorias();
  CarregarTributacoes();
  LimparDadosProdutos();

  formPrincipal.txtProdutoTipoForm.Tag:= 0;
  formPrincipal.tbcFormProduto.ActiveTab:= formPrincipal.tabProdutoCad;
end;

procedure TProdutoRepository.Insert(produto: TProduto);
var
  listaDeUnidades: TObjectList<TUnidade>;
  listaDeValores: TObjectList<TValorEspecial>;
begin
  try
    if Assigned(produto) then
    begin

      if Assigned(produto.Unidade) then
      begin
        contexto
          .ORM
          .DAO<TUnidade>()
          .Insert(produto.Unidade);

        listaDeUnidades:= contexto
                          .ORM
                          .DAO<TUnidade>()
                          .Find();
        try
          produto.UnidadeID:= listaDeUnidades.Last.ID;
        finally
          listaDeUnidades.Free;
        end;

      end;

      if Assigned(produto.ValorEspecial) then
      begin
        contexto
          .ORM
          .DAO<TValorEspecial>()
          .Insert(produto.ValorEspecial);

        listaDeValores:= contexto
                          .ORM
                          .DAO<TValorEspecial>()
                          .Find();
        try
          produto.ValorID:= listaDeValores.Last.ID;
        finally
          listaDeValores.Free;
        end;

      end;

      try
        contexto
          .ORM
          .DAO<TProduto>()
          .Insert(produto);

        formPrincipal.tbcFormProduto.ActiveTab:= formPrincipal.tabProdutoLista;
        CarregaProdutos();
      except
        formPrincipal.tbcFormProduto.ActiveTab:= formPrincipal.tabProdutoCad;
      end;

    end;
  finally
    if Assigned(produto) then
      produto.Free;

  end;
end;

procedure TProdutoRepository.LimparDadosProdutos;
begin
  formPrincipal.edtProdutoDescricao.Text:= '';
  formPrincipal.edtProdutoValor.Text:= '';
  formPrincipal.edtProdutoValorPequena.Text:= '';
  formPrincipal.edtProdutoValorMedia.Text:= '';
  formPrincipal.edtProdutoValorGigante.Text:= '';
  formPrincipal.edtProdutoNCM.Text:= '';
  formPrincipal.edtProdutoCEST.Text:= '';
  formPrincipal.edtProdutoCFOP.Text:= '';
  formPrincipal.edtProdutoDescricaoUnidade.Text:= '';
  formPrincipal.edtProdutoCodigoBarras.Text:= '';

  formPrincipal.cedProdutoCategorias.ItemIndex:= -1;
  formPrincipal.cedProdutoTributacao.ItemIndex:= -1;
  formPrincipal.cedProdutoStatus.ItemIndex:= 0;
  formPrincipal.cedProdutoSiglaDaUnidade.ItemIndex:= 0;

  formPrincipal.swProdutoTemDecimal.IsChecked:= false;

  formPrincipal.mmoIngredientes.Lines.Clear;

end;

procedure TProdutoRepository.SetarCampos(produto: TProduto);
var
  linhaCategoria: string;
  linhaTributacao: string;
begin

  formPrincipal.edtProdutoDescricao.Text:= produto.Descricao;
  formPrincipal.edtProdutoValor.Text:= produto.ValorEspecial.ValorNormal.ToString;
  formPrincipal.edtProdutoValorPequena.Text:= produto.ValorEspecial.ValorPequeno.ToString;
  formPrincipal.edtProdutoValorMedia.Text:= produto.ValorEspecial.ValorMedia.ToString;
  formPrincipal.edtProdutoValorGigante.Text:= produto.ValorEspecial.ValorGigante.ToString;
  formPrincipal.edtProdutoNCM.Text:= produto.NCM;
  formPrincipal.edtProdutoCEST.Text:= produto.CEST;
  formPrincipal.edtProdutoCFOP.Text:= produto.CFOP;
  formPrincipal.edtProdutoDescricaoUnidade.Text:= produto.Unidade.Descricao;
  formPrincipal.edtProdutoCodigoBarras.Text:= produto.CodigoBarras;

  formPrincipal.cedProdutoStatus.ItemIndex:= produto.Status;
  formPrincipal.cedProdutoSiglaDaUnidade.ItemIndex:= formPrincipal.cedProdutoSiglaDaUnidade.Items.IndexOf(
    produto.Unidade.Sigla);
  formPrincipal.swProdutoTemDecimal.IsChecked:= produto.Unidade.TemDecimal;

  formPrincipal.mmoIngredientes.Lines.Clear;
  formPrincipal.mmoIngredientes.Lines.Add(produto.Ingredientes);

  formPrincipal.txtProdutoUnidadeId.Tag:= produto.UnidadeID;
  formPrincipal.txtProdutoValorEspecialId.Tag:= produto.ValorID;

  linhaCategoria:= Concat(produto.CategoriaID.ToString, '|', produto.Categoria.Descricao);
  linhaTributacao:= Concat(produto.ImpostosID.GetValueOrDefault.ToString, '|', produto.Impostos.Descricao.GetValueOrDefault);

  formPrincipal.cedProdutoCategorias.ItemIndex:= formPrincipal.cedProdutoCategorias.Items.IndexOf(
    linhaCategoria);
  formPrincipal.cedProdutoTributacao.ItemIndex:= formPrincipal.cedProdutoTributacao.Items.IndexOf(
    linhaTributacao);
end;

procedure TProdutoRepository.UpdateProduto(produto: TProduto);
var
  qry: TQuery;
begin
  qry:= TQuery.Create(dmCon.Con);
  qry.ref;

  qry.SQL.Update('Produto')
    .&Set('Descricao', produto.Descricao.GetValueOrDefault.QuotedString)
    .&Set('CategoriaID', produto.CategoriaID.ToString)
    .&Set('UnidadeID', produto.UnidadeID.ToString)
    .&Set('CodigoBarras', produto.CodigoBarras.GetValueOrDefault.QuotedString)
    .&Set('ValorID', produto.ValorID.ToString)
    .&Set('NCM', produto.NCM.GetValueOrDefault.QuotedString)
    .&Set('CEST', produto.CEST.Value.QuotedString)
    .&Set('CFOP', produto.CFOP.Value.QuotedString)
    .&Set('Ingredientes', produto.Ingredientes.GetValueOrDefault.QuotedString)
    .&Set('Status', produto.Status.ToString);

   if produto.ImpostosID.GetValueOrDefault <> 0 then
      qry.SQL
        .&Set('ImpostosID', produto.ImpostosID.GetValueOrDefault.ToString);

  qry.SQL
    .Where('ID = ' + produto.ID.GetValueOrDefault.ToString);

  qry.Exec();
  qry.SQL.ClearAll();
end;

procedure TProdutoRepository.UpdateUnidade(produto: TProduto);
var
  qry: TQuery;
begin
  qry:= TQuery.Create(dmCon.Con);
  qry.ref;

  qry.SQL.Update('Unidade')
    .&Set('Descricao', produto.Unidade.Descricao.Value.QuotedString)
    .&Set('Sigla', produto.Unidade.Sigla.Value.QuotedString)
    .&Set('TemDecimal', produto.Unidade.TemDecimal.ToInteger.ToString)
    .Where('ID = ' + produto.UnidadeID.ToString);

  qry.Exec();
end;

procedure TProdutoRepository.UpdateValorEspecial(produto: TProduto);
var
  qry: TQuery;
begin
  qry:= TQuery.Create(dmCon.Con);
  qry.ref;

  qry.SQL.Update('ValorEspecial')
    .&Set('ValorPequeno', produto.ValorEspecial.ValorPequeno.ToString)
    .&Set('ValorMedia', produto.ValorEspecial.ValorMedia.ToString)
    .&Set('ValorGigante', produto.ValorEspecial.ValorGigante.ToString)
    .&Set('ValorNormal', produto.ValorEspecial.ValorNormal.ToString)
    .Where('ID = ' + produto.ValorID.ToString);

  qry.Exec();
end;

end.
