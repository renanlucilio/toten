unit uContextoDB;

interface

uses
  uConexao.interfaces,
  uConexaoORM,
  ormbr.dml.generator.mssql,
  FireDAC.Stan.Intf,FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
   FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
   FireDAC.Stan.Async, FireDAC.Phys, FireDAC.FMXUI.Wait, Data.DB,
   FireDAC.Comp.Client, FireDAC.Comp.UI, System.Classes;

type
  TContextoDB = class
  private
    FORM:     TConexaoORM;
    connection: TFDConnection;

  public
    constructor Create(con: TFDConnection);
    destructor Destroy; override;

    property ORM:     TConexaoORM read FORM       write FORM;
  end;

implementation

uses
  ormbr.factory.interfaces,
  Model.LibUtil;

{ TConexao }

constructor TContextoDB.Create(con: TFDConnection);
begin
  connection :=  con;
  FORM:= TConexaoORM.Create(connection, dnMSSQL);
end;

destructor TContextoDB.Destroy;
begin
  FORM.Free;
  inherited;
end;

end.
