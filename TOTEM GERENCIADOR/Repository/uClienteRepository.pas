unit uClienteRepository;

interface

uses Cliente, uContextoDB, Contato, Endereco, uConexao.Query;

type
  TClienteRepository = class
  private
    contexto: TContextoDB;
    procedure LimparDadosCliente;
    procedure SetarCampos(cliente: TCliente);
    procedure UpdateCliente(cliente: TCliente);
    procedure UpdateClienteEndereco(cliente: TCliente);
    procedure UpdateContato(cliente: TCliente);
  public
    constructor Create();

    procedure InitInsert();
    procedure Insert(cliente: TCliente);
    procedure InitEdit(id: Integer);
    procedure Edit(cliente: TCliente);
    procedure Delete(id: Integer);
    procedure CarregaClientes();
  end;

implementation

{ TClienteRepository }

uses uDmCon, ufrmPrincipal, System.Generics.Collections, System.SysUtils;

procedure TClienteRepository.CarregaClientes;
begin
  dmCon.Con.Close();
  dmCon.tableCliente.Active:= false;
  dmCon.Con.Open();
  dmCon.tableCliente.Active:= true;
end;

procedure TClienteRepository.UpdateContato(cliente: TCliente);
var
  qry: TQuery;
begin
  qry:= TQuery.Create(dmCon.Con);
  qry.ref;

  qry.SQL.Update('Contato')
    .&Set('OhContato', cliente.Contato.OhContato.Value.QuotedString())
    .&Set('Tipo', cliente.Contato.Tipo.Value.QuotedString())
    .Where('ID = ' + cliente.ContatoID.ToString);

  qry.Exec();


end;

procedure TClienteRepository.UpdateClienteEndereco(cliente: TCliente);
var
  qry: TQuery;
begin
  qry:= TQuery.Create(dmCon.Con);
  qry.ref;

  qry.SQL.Update('Endereco')
    .&Set('Logradouro', cliente.Endereco.Logradouro.Value.QuotedString())
    .&Set('Numero', cliente.Endereco.Numero.Value.QuotedString())
    .&Set('Bairro', cliente.Endereco.Bairro.Value.QuotedString())
    .&Set('Cidade', cliente.Endereco.Cidade.Value.QuotedString())
    .&Set('UF', cliente.Endereco.UF.Value.QuotedString())
    .&Set('CEP', cliente.Endereco.CEP.Value.QuotedString())
    .&Set('Complemento', cliente.Endereco.Complemento.Value.QuotedString())
    .&Set('Referencia', cliente.Endereco.Referencia.Value.QuotedString())
    .Where('ID = ' + cliente.EnderecoID.ToString);

  qry.Exec();


end;

procedure TClienteRepository.UpdateCliente(cliente: TCliente);
var
  qry: TQuery;
begin
  qry:= TQuery.Create(dmCon.Con);
  qry.ref;

  qry.SQL.Update('Cliente')
    .&Set('Nome', cliente.Nome.Value.QuotedString())
    .&Set('Documento', cliente.Documento.Value.QuotedString())
    .&Set('EnderecoID', cliente.EnderecoID.ToString)
    .&Set('ContatoID', cliente.ContatoID.ToString)
    .Where('ID = ' + cliente.ID.Value.ToString);

  qry.Exec();

end;

constructor TClienteRepository.Create();
begin
  contexto := TContextoDB.Create(dmCon.Con);
end;

procedure TClienteRepository.Delete(id: Integer);
var
  cliente: TCliente;
begin

  cliente:= contexto
              .ORM
              .DAO<TCliente>()
              .Find(id);

  if Assigned(cliente) then
  begin
    try
      contexto
        .ORM
        .DAO<TCliente>()
        .Delete(cliente);

      formPrincipal.tbcClientes.ActiveTab:= formPrincipal.tabClienteLista;
      CarregaClientes();
    except
      formPrincipal.tbcClientes.ActiveTab:= formPrincipal.tabClienteCad;
    end;
  end;
end;

procedure TClienteRepository.SetarCampos(cliente: TCliente);
begin
  formPrincipal.edtClienteNome.Text := cliente.Nome.Value;
  formPrincipal.edtClienteDocumento.Text := cliente.Documento.Value;
  formPrincipal.edtClienteContato.Text := cliente.Contato.OhContato.Value;
  formPrincipal.edtClienteCep.Text := cliente.Endereco.CEP.Value;
  formPrincipal.cedClienteFormaContato.Text := cliente.Contato.Tipo.Value;
  formPrincipal.edtClienteLogradouro.Text := cliente.Endereco.Logradouro.Value;
  formPrincipal.edtClienteNumero.Text := cliente.Endereco.Numero.Value;
  formPrincipal.edtClienteComplemento.Text := cliente.Endereco.Complemento.Value;
  formPrincipal.edtClienteBairro.Text := cliente.Endereco.Bairro.Value;
  formPrincipal.edtClienteCidade.Text := cliente.Endereco.Cidade.Value;
  formPrincipal.edtClienteReferencia.Text := cliente.Endereco.Referencia.Value;
  formPrincipal.cedClienteUF.Text := cliente.Endereco.UF.Value;
  formPrincipal.txtClienteContatoID.Tag:= cliente.EnderecoID;
  formPrincipal.txtClienteEnderecoId.Tag:= cliente.ContatoID;
end;

procedure TClienteRepository.LimparDadosCliente;
begin
  formPrincipal.edtClienteNome.Text := '';
  formPrincipal.edtClienteDocumento.Text := '';
  formPrincipal.edtClienteContato.Text := '';
  formPrincipal.edtClienteCep.Text := '';
  formPrincipal.cedClienteFormaContato.Text:= '';
  formPrincipal.edtClienteLogradouro.Text := '';
  formPrincipal.edtClienteNumero.Text := '';
  formPrincipal.edtClienteComplemento.Text := '';
  formPrincipal.edtClienteBairro.Text := '';
  formPrincipal.edtClienteCidade.Text := '';
  formPrincipal.edtClienteReferencia.Text := '';
  formPrincipal.cedClienteUF.Text:= '';
end;

procedure TClienteRepository.Edit(cliente: TCliente);
begin
  try
    if Assigned(cliente) then
    begin

      try
        

        UpdateCliente(cliente);

        UpdateClienteEndereco(cliente);

        UpdateContato(cliente);


        formPrincipal.tbcClientes.ActiveTab:= formPrincipal.tabClienteLista;
        CarregaClientes();
      except
        formPrincipal.tbcClientes.ActiveTab:= formPrincipal.tabClienteCad;
      end;

    end;
  finally

    if Assigned(cliente) then
      cliente.Free;
  end;
end;

procedure TClienteRepository.InitEdit(id: integer);
var
  cliente: TCliente;
begin
  cliente:= contexto
              .ORM
              .DAO<TCliente>()
              .Find(id);

  if Assigned(cliente) then
  begin
    SetarCampos(cliente);
    formPrincipal.txtClienteTipoForm.Tag:= 1;
    formPrincipal.tbcClientes.ActiveTab:= formPrincipal.tabClienteCad;
  end;
end;

procedure TClienteRepository.InitInsert;
begin
  LimparDadosCliente();
  formPrincipal.txtClienteTipoForm.Tag:= 0;
  formPrincipal.tbcClientes.ActiveTab:= formPrincipal.tabClienteCad;
end;

procedure TClienteRepository.Insert(cliente: TCliente);
var
  listaContatos: TObjectList<TContato>;
  listaEndereco: TObjectList<TEndereco>;
begin
  try
    if Assigned(cliente) then
    begin

      if Assigned(cliente.Contato) then
      begin
        contexto
          .ORM
          .DAO<TContato>()
          .Insert(cliente.Contato);

        listaContatos:= contexto
                          .ORM
                          .DAO<TContato>()
                          .Find();
        try
          cliente.ContatoID:= listaContatos.Last.ID;
        finally
          listaContatos.Free;
        end;

      end;

      if Assigned(cliente.Endereco) then
      begin
        contexto
          .ORM
          .DAO<TEndereco>()
          .Insert(cliente.Endereco);

        listaEndereco:= contexto
                          .ORM
                          .DAO<TEndereco>()
                          .Find();
        try
          cliente.EnderecoID:= listaEndereco.Last.ID;
        finally
          listaEndereco.Free;
        end;

      end;

      try
        contexto
          .ORM
          .DAO<TCliente>()
          .Insert(cliente);

        formPrincipal.tbcClientes.ActiveTab:= formPrincipal.tabClienteLista;
        CarregaClientes();
      except
        formPrincipal.tbcClientes.ActiveTab:= formPrincipal.tabClienteCad;
      end;

    end;
  finally
    if Assigned(cliente) then
      cliente.Free;

  end;
end;

end.
