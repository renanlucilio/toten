unit uCategoriaRepository;

interface

uses Categoria,System.Json,Json.Helper, uContextoDB, uDmCon;

type
  TCategoriaRepository = class
  private
    contexto: TContextoDB;
    procedure LimparDadosCategoria();
    procedure SetarCampos(categoria: TCategoria);
  public
    constructor Create();

    procedure InitInsert();
    procedure Insert(categoria: TCategoria);
    procedure InitEdit(id: Integer);
    procedure Edit(categoria: TCategoria);
    procedure Delete(id: Integer);
    procedure CarregaCategorias();

    function GetCategoriasJson(): string;
  end;

implementation

{ TCategoriaRepository }

uses ufrmPrincipal,
uConexao.Query,uLibUtil, System.SysUtils, System.UITypes, FMX.Graphics;

procedure TCategoriaRepository.CarregaCategorias;
begin
  dmCon.Con.Close();
  dmCon.tableCategoria.Active:= false;
  dmCon.Con.Open();
  dmCon.tableCategoria.Active:= true;
end;

constructor TCategoriaRepository.Create;
begin
  contexto := TContextoDB.Create(dmCon.Con);
end;

procedure TCategoriaRepository.Delete(id: Integer);
var
  categoria: TCategoria;
begin

  categoria:= contexto
              .ORM
              .DAO<TCategoria>()
              .Find(id);

  if Assigned(categoria) then
  begin
    try
      contexto
        .ORM
        .DAO<TCategoria>()
        .Delete(categoria);

      formPrincipal.tbcFormCategoria.ActiveTab:= formPrincipal.TabCategoriaLista;
      CarregaCategorias();
    except
      formPrincipal.tbcFormCategoria.ActiveTab:= formPrincipal.TabCategoriaCad;
    end;
  end;
end;

procedure TCategoriaRepository.Edit(categoria: TCategoria);
var
  qry: TQuery;
begin
  try
    if Assigned(categoria) then
    begin

      try
        qry:= TQuery.Create(dmCon.Con);
        qry.ref;

        qry.SQL.Update('Categoria')
          .&Set('Descricao', categoria.Descricao.Value.QuotedString)
          .&Set('LocalLogo', categoria.LocalLogo.Value.QuotedString)
          .&Set('Status', categoria.Status.ToString)
        .Where('ID ='+categoria.ID.Value.ToString);

        qry.Exec();


        formPrincipal.tbcFormCategoria.ActiveTab:= formPrincipal.TabCategoriaLista;
        CarregaCategorias();
      except
        formPrincipal.tbcFormCategoria.ActiveTab:= formPrincipal.TabCategoriaCad;
      end;

    end;
  finally

    if Assigned(categoria) then
      categoria.Free;
  end;
end;

function TCategoriaRepository.GetCategoriasJson: string;
var
  qry: TQuery;
  retorno: TJSONArray;
  it: TJSONValue;
  objJson: TJSONObject;
  logo: TBitmap;
begin
  qry:= TQuery.Create(dmCon.Con);
  qry.Ref;
  try
    qry.SQL.Select
        .Column('ID')
        .Column('Descricao')
        .Column('LocalLogo')
      .From('Categoria')
      .Where('Status = 0');

    qry.Open();


    retorno:= TJSONArray.Create();

    qry.DataSet.First;

    while not qry.DataSet.Eof do
    begin

      objJson:= TJSONObject.Create();

      objJson.addPair('id', qry.DataSet.FieldByName('ID').AsInteger);
      objJson.addPair('descricao', qry.DataSet.FieldByName('Descricao').AsString);

      if FileExists(qry.DataSet.FieldByName('LocalLogo').AsString) then
      begin
        logo:= TBitmap.Create();
        logo.LoadFromFile(qry.DataSet.FieldByName('LocalLogo').AsString);
        objJson.addPair('logo', Base64FromBitmap(logo));
      end
      else
        objJson.addPair('logo', '');

      retorno.AddElement(objJson);

      qry.DataSet.Next;
    end;


    Result:= retorno.ToJSON;
  except
    Result:= '{}';
  end;
end;

procedure TCategoriaRepository.InitEdit(id: Integer);
var
  categoria: TCategoria;
begin
  categoria:= contexto
              .ORM
              .DAO<TCategoria>()
              .Find(id);

  if Assigned(categoria) then
  begin
    SetarCampos(categoria);
    formPrincipal.txtCategoriaTipoForm.Tag:= 1;
    formPrincipal.tbcFormCategoria.ActiveTab:= formPrincipal.TabCategoriaCad;
  end;
end;

procedure TCategoriaRepository.InitInsert;
begin
  LimparDadosCategoria();
  formPrincipal.txtCategoriaTipoForm.Tag:= 0;
  formPrincipal.tbcFormCategoria.ActiveTab:= formPrincipal.TabCategoriaCad;
end;

procedure TCategoriaRepository.Insert(categoria: TCategoria);
begin
  try
    if Assigned(categoria) then
    begin

      try
        contexto
          .ORM
          .DAO<TCategoria>()
          .Insert(categoria);

        formPrincipal.tbcFormCategoria.ActiveTab:= formPrincipal.TabCategoriaLista;
        CarregaCategorias();
      except
        formPrincipal.tbcFormCategoria.ActiveTab:= formPrincipal.TabCategoriaCad;
      end;

    end;
  finally
    if Assigned(categoria) then
      categoria.Free;

  end;
end;

procedure TCategoriaRepository.LimparDadosCategoria;
begin
  formPrincipal.edtCategoriaDescricao.Text:= '';
  formPrincipal.edtCategoriaLocalLogo.Text:= '';
  formPrincipal.cedCategoriaStatus.ItemIndex:= 0;
  formPrincipal.imgCategoriaLogo.Bitmap.Clear(TColor($FFFFFF));
end;

procedure TCategoriaRepository.SetarCampos(categoria: TCategoria);
begin
  formPrincipal.edtCategoriaDescricao.Text:= categoria.Descricao;
  formPrincipal.edtCategoriaLocalLogo.Text:= categoria.LocalLogo;
  formPrincipal.cedCategoriaStatus.ItemIndex:= categoria.Status;

  if FileExists(categoria.LocalLogo) then
    formPrincipal.imgCategoriaLogo.Bitmap.LoadFromFile(categoria.LocalLogo)
  else
    formPrincipal.imgCategoriaLogo.Bitmap.Clear(TColor($FFFFFF));
end;

end.
