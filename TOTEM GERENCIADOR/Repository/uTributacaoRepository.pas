unit uTributacaoRepository;

interface

uses Impostos, uContextoDB, uDmCon, uConexao.Query;

type
  TTributacaoRepository = class
  private
    contexto: TContextoDB;

    procedure LimparDadosTributacoes;
    procedure SetarCampos(imposto: TImpostos);
    procedure InsertICMS(var imposto: TImpostos);
    procedure InsertISSQN(var imposto: TImpostos);
    procedure InsertPIS(var imposto: TImpostos);
    procedure InsertPISST(var imposto: TImpostos);
    procedure InsertCOFINS(var imposto: TImpostos);
    procedure InsertCOFINSST(var imposto: TImpostos);
    procedure InsertTaxas(var imposto: TImpostos);
    procedure UpdateCofins(imposto: TImpostos);
    procedure UpdateCofinsst(imposto: TImpostos);
    procedure UpdatePis(imposto: TImpostos);
    procedure UpdatePisst(imposto: TImpostos);
    procedure UpdateIssqn(imposto: TImpostos);
    procedure UpdateIcms(imposto: TImpostos);
    procedure UpdateTaxas(imposto: TImpostos);
  public
    constructor Create();

    procedure InitInsert();
    procedure Insert(imposto: TImpostos);
    procedure InitEdit(id: Integer);
    procedure Edit(imposto: TImpostos);
    procedure Delete(id: Integer);
    procedure CarregarTributacoes();
  end;

implementation



{ TTributacaoRepository }

uses ufrmPrincipal, System.Generics.Collections, ICMS, ISSQN, PIS, PISST,
  COFINS, COFINSST, TaxasFiscais, System.SysUtils;


procedure TTributacaoRepository.CarregarTributacoes;
begin
  dmCon.Con.Close();
  dmCon.tableTributacao.Active:= false;
  dmCon.Con.Open();
  dmCon.tableTributacao.Active:= true;
end;

procedure TTributacaoRepository.UpdateCofins(imposto: TImpostos);
var
  qry: TQuery;
begin
  qry:= TQuery.Create(dmCon.Con);
  qry.ref;

  qry.SQL.Update('COFINS')
    .&Set('BaseCalculo', imposto.COFINS.BaseCalculo.ToString)
    .&Set('Porcento', CurrToStr(imposto.COFINS.Porcento))
    .&Set('Valor', imposto.COFINS.Valor.ToString)
    .&Set('BaseCalculoPorProduto', imposto.COFINS.BaseCalculoPorProduto.ToString)
    .&Set('Aliquota', CurrToStr(imposto.COFINS.Aliquota))
    .&Set('CST', imposto.COFINS.CST.ToString)
    .Where('ID = ' + imposto.COFINSID.ToString);
  qry.Exec;

end;

procedure TTributacaoRepository.UpdateCofinsst(imposto: TImpostos);
var
  qry: TQuery;
begin
  qry:= TQuery.Create(dmCon.Con);
  qry.ref;

  qry.SQL.Update('COFINSST')
    .&Set('BaseCalculo', imposto.COFINSST.BaseCalculo.ToString)
    .&Set('Porcento', CurrToStr(imposto.COFINSST.Porcento))
    .&Set('Valor', imposto.COFINSST.Valor.ToString)
    .&Set('BaseCalculoPorProduto', imposto.COFINSST.BaseCalculoPorProduto.ToString)
    .&Set('Aliquota', CurrToStr(imposto.COFINSST.Aliquota))
    .Where('ID = ' + imposto.COFINSSTID.ToString);
  qry.Exec;

end;

procedure TTributacaoRepository.UpdateIcms(imposto: TImpostos);
var
  qry: TQuery;
begin
  qry:= TQuery.Create(dmCon.Con);
  qry.ref;

  qry.SQL.Update('ICMS')
    .&Set('Origem', imposto.ICMS.Origem.ToString)
    .&Set('CST', imposto.ICMS.CST.ToString)
    .&Set('Porcento', CurrToStr(imposto.ICMS.Porcento))
    .&Set('Valor', imposto.ICMS.Valor.ToString)
    .Where('ID = ' + imposto.ICMSID.ToString);
  qry.Exec;

end;

procedure TTributacaoRepository.UpdateIssqn(imposto: TImpostos);
var
  qry: TQuery;
begin
  qry:= TQuery.Create(dmCon.Con);
  qry.ref;

  qry.SQL.Update('ISSQN')
    .&Set('BaseCalculo', imposto.ISSQN.BaseCalculo.ToString)
    .&Set('Deducao', imposto.ISSQN.Deducao.ToString)
    .&Set('CodigoIBGE', imposto.ISSQN.CodigoIBGE.Value.QuotedString)
    .&Set('BaseCalculoProduto', imposto.ISSQN.BaseCalculoProduto.ToString)
    .&Set('ItemDaListaServico', imposto.ISSQN.ItemDaListaServico.Value.QuotedString)
    .&Set('CodigoTributarioISSQN', imposto.ISSQN.CodigoTributarioISSQN.Value.QuotedString)
    .&Set('Natureza', imposto.ISSQN.Natureza.ToString)
    .Where('ID = ' + imposto.ISSQNID.ToString);
  qry.Exec;

end;

procedure TTributacaoRepository.UpdatePis(imposto: TImpostos);
var
  qry: TQuery;
begin
  qry:= TQuery.Create(dmCon.Con);
  qry.ref;

   qry.SQL.Update('PIS')
    .&Set('BaseCalculo', imposto.PIS.BaseCalculo.ToString)
    .&Set('Porcento', CurrToStr(imposto.PIS.Porcento))
    .&Set('Valor', imposto.PIS.Valor.ToString)
    .&Set('BaseCalculoPorProduto', imposto.PIS.BaseCalculoPorProduto.ToString)
    .&Set('Aliquota', CurrToStr(imposto.PIS.Aliquota))
    .&Set('CST', imposto.PIS.CST.ToString)
    .Where('ID = ' + imposto.PISID.ToString);
  qry.Exec;

end;

procedure TTributacaoRepository.UpdatePisst(imposto: TImpostos);
var
  qry: TQuery;
begin
  qry:= TQuery.Create(dmCon.Con);
  qry.ref;

  qry.SQL.Update('PISST')
    .&Set('BaseCalculo', imposto.PISST.BaseCalculo.ToString)
    .&Set('Porcento', CurrToStr(imposto.PISST.Porcento))
    .&Set('Valor', imposto.PISST.Valor.ToString)
    .&Set('BaseCalculoPorProduto', imposto.PISST.BaseCalculoPorProduto.ToString)
    .&Set('Aliquota', CurrToStr(imposto.PISST.Aliquota))
    .Where('ID = ' + imposto.PISSTID.ToString);
  qry.Exec;

end;

procedure TTributacaoRepository.UpdateTaxas(imposto: TImpostos);
var
  qry: TQuery;
begin
  qry:= TQuery.Create(dmCon.Con);
  qry.ref;

  qry.SQL.Update('TaxasFiscais')
    .&Set('TaxaEstadual', imposto.TaxasFiscais.TaxaEstadual.ToString)
    .&Set('TaxaFederal', imposto.TaxasFiscais.TaxaFederal.ToString)
    .Where('ID = ' + imposto.TaxasID.ToString);
  qry.Exec;

end;

procedure TTributacaoRepository.InsertTaxas(var imposto: TImpostos);
var
  listaTaxas: System.Generics.Collections.TObjectList<TTaxasFiscais>;
begin
  if Assigned(imposto.TaxasFiscais) then
  begin
    contexto.ORM.DAO<TTaxasFiscais>.Insert(imposto.TaxasFiscais);
    listaTaxas := contexto.ORM.DAO<TTaxasFiscais>.Find;
    try
      imposto.TaxasID := listaTaxas.Last.ID;
    finally
      listaTaxas.Free;
    end;
  end;
end;

procedure TTributacaoRepository.InsertCOFINSST(var imposto: TImpostos);
var
  listaCofinsst: System.Generics.Collections.TObjectList<TCOFINSST>;
begin
  if Assigned(imposto.COFINSST) then
  begin
    contexto.ORM.DAO<TCOFINSST>.Insert(imposto.COFINSST);
    listaCofinsst := contexto.ORM.DAO<TCOFINSST>.Find;
    try
      imposto.COFINSSTID := listaCofinsst.Last.ID;
    finally
      listaCofinsst.Free;
    end;
  end;
end;

procedure TTributacaoRepository.InsertCOFINS(var imposto: TImpostos);
var
  listaCofins: System.Generics.Collections.TObjectList<TCOFINS>;
begin
  if Assigned(imposto.COFINS) then
  begin
    contexto.ORM.DAO<TCOFINS>.Insert(imposto.COFINS);
    listaCofins := contexto.ORM.DAO<TCOFINS>.Find;
    try
      imposto.COFINSID := listaCofins.Last.ID;
    finally
      listaCofins.Free;
    end;
  end;
end;

procedure TTributacaoRepository.InsertPISST(var imposto: TImpostos);
var
  listaPisst: TObjectList<TPISST>;
begin
  if Assigned(imposto.PISST) then
  begin
    contexto.ORM.DAO<TPISST>.Insert(imposto.PISST);
    listaPisst := contexto.ORM.DAO<TPISST>.Find();
    try
      imposto.PISSTID := listaPisst.Last.ID;
    finally
      listaPisst.Free;
    end;
  end;
end;

procedure TTributacaoRepository.InsertPIS(var imposto: TImpostos);
var
  listaPis: System.Generics.Collections.TObjectList<TPIS>;
begin
  if Assigned(imposto.PIS) then
  begin
    contexto.ORM.DAO<TPIS>.Insert(imposto.PIS);
    listaPis := contexto.ORM.DAO<TPIS>.Find;
    try
      imposto.PISID := listaPis.Last.ID;
    finally
      listaPis.Free;
    end;
  end;
end;

procedure TTributacaoRepository.InsertISSQN(var imposto: TImpostos);
var
  listaIssqn: System.Generics.Collections.TObjectList<TISSQN>;
begin
  if Assigned(imposto.ISSQN) then
  begin
    contexto.ORM.DAO<TISSQN>.Insert(imposto.ISSQN);
    listaIssqn := contexto.ORM.DAO<TISSQN>.Find;
    try
      imposto.ISSQNID := listaIssqn.Last.ID;
    finally
      listaIssqn.Free;
    end;
  end;
end;

procedure TTributacaoRepository.InsertICMS(var imposto: TImpostos);
var
  listaIcms: System.Generics.Collections.TObjectList<TICMS>;
begin
  if Assigned(imposto.ICMS) then
  begin
    contexto.ORM.DAO<TICMS>.Insert(imposto.ICMS);
    listaIcms := contexto.ORM.DAO<TICMS>.Find;
    try
      imposto.ICMSID := listaIcms.Last.ID;
    finally
      listaIcms.Free;
    end;
  end;
end;

constructor TTributacaoRepository.Create;
begin
  contexto := TContextoDB.Create(dmCon.Con);
end;

procedure TTributacaoRepository.Delete(id: Integer);
var
  imposto: TImpostos;
begin
  imposto:= contexto
              .ORM
              .DAO<TImpostos>()
              .Find(id);

  if Assigned(imposto) then
  begin
    try
      contexto
        .ORM
        .DAO<TImpostos>()
        .Delete(imposto);

      formPrincipal.tbcTributacoes.ActiveTab:= formPrincipal.tabTributacaoLista;
      CarregarTributacoes();
    except
      formPrincipal.tbcTributacoes.ActiveTab:= formPrincipal.tabTributacaoCad;
    end;
  end;
end;

procedure TTributacaoRepository.Edit(imposto: TImpostos);
begin
  try
    if Assigned(imposto) then
    begin

      try
        UpdateCofins(imposto);
        UpdateCofinsst(imposto);
        UpdatePis(imposto);
        UpdatePisst(imposto);
        UpdateIssqn(imposto);
        UpdateIcms(imposto);
        UpdateTaxas(imposto);

        formPrincipal.tbcTributacoes.ActiveTab:= formPrincipal.tabTributacaoLista;
        CarregarTributacoes();
      except
        formPrincipal.tbcTributacoes.ActiveTab:= formPrincipal.tabTributacaoCad;
      end;

    end;
  finally

    if Assigned(imposto) then
      imposto.Free;
  end;
end;

procedure TTributacaoRepository.InitEdit(id: Integer);
var
  imposto: TImpostos;
begin
  imposto:= contexto
              .ORM
              .DAO<TImpostos>()
              .Find(id);

  if Assigned(imposto) then
  begin
    SetarCampos(imposto);
    formPrincipal.txtTributacaoTipoForm.Tag:= 1;
    formPrincipal.tbcTributacoes.ActiveTab:= formPrincipal.tabTributacaoCad;
  end;
end;


procedure TTributacaoRepository.InitInsert;
begin
  LimparDadosTributacoes();
  formPrincipal.txtTributacaoTipoForm.Tag:= 0;
  formPrincipal.tbcTributacoes.ActiveTab:= formPrincipal.tabTributacaoCad;
end;

procedure TTributacaoRepository.Insert(imposto: TImpostos);
begin
  try
    if Assigned(imposto) then
    begin
      InsertICMS(imposto);
      InsertISSQN(imposto);
      InsertPIS(imposto);
      InsertPISST(imposto);
      InsertCOFINS(imposto);
      InsertCOFINSST(imposto);
      InsertTaxas(imposto);

      try
        contexto
          .ORM
          .DAO<TImpostos>()
          .Insert(imposto);

        formPrincipal.tbcTributacoes.ActiveTab:= formPrincipal.tabTributacaoLista;
        CarregarTributacoes();
      except
        formPrincipal.tbcTributacoes.ActiveTab:= formPrincipal.tabTributacaoCad;
      end;

    end;
  finally
    if Assigned(imposto) then
      imposto.Free;

  end;
end;

procedure TTributacaoRepository.LimparDadosTributacoes;
begin
  formPrincipal.edtTributacaoDescricao.Text:= '';

  formPrincipal.edtPisCst.Text:= '';
  formPrincipal.edtPisBc.Text:= '';
  formPrincipal.edtPisPorcentagem.Text:= '';
  formPrincipal.edtPisValor.Text:= '';
  formPrincipal.edtPisBcp.Text:= '';
  formPrincipal.edtPisAliquota.Text:= '';

  formPrincipal.edtPisstBc.Text:= '';
  formPrincipal.edtPisstPorcentagem.Text:= '';
  formPrincipal.edtPisstValor.Text:= '';
  formPrincipal.edtPisstBcp.Text:= '';
  formPrincipal.edtPisstAliquota.Text:= '';

  formPrincipal.edtCofinsCST.Text:= '';
  formPrincipal.edtCofinsBc.Text:= '';
  formPrincipal.edtCofinsPorcentagem.Text:= '';
  formPrincipal.edtCofinsValor.Text:= '';
  formPrincipal.edtCofinsBcp.Text:= '';
  formPrincipal.edtCofinsAliquota.Text:= '';

  formPrincipal.edtCofinsstBc.Text:= '';
  formPrincipal.edtCofinsstPorcentagem.Text:= '';
  formPrincipal.edtCofinsstValor.Text:= '';
  formPrincipal.edtCofinsstBcp.Text:= '';
  formPrincipal.edtCofinsstAliquota.Text:= '';

  formPrincipal.edtICMSOrigem.Text:= '';
  formPrincipal.edtICMSCST.Text:= '';
  formPrincipal.edtICMSPorcentagem.Text:= '';
  formPrincipal.edtICMSValor.Text:= '';

  formPrincipal.edtISSQNNatureza.Text:= '';
  formPrincipal.edtIssqnBC.Text:= '';
  formPrincipal.edtIssqnDeducao.Text:= '';
  formPrincipal.edtIssqnCodigoIbge.Text:= '';
  formPrincipal.edtIssqnBcp.Text:= '';
  formPrincipal.edtIssqnItemServico.Text:= '';
  formPrincipal.edtIssqnCodigoTributario.Text:= '';

  formPrincipal.edtTaxaTaxaEstadual.Text:= '';
  formPrincipal.edtTaxaTaxaFederal.Text:= '';
end;

procedure TTributacaoRepository.SetarCampos(imposto: TImpostos);
begin
  formPrincipal.edtTributacaoDescricao.Text:= imposto.Descricao;

  formPrincipal.edtPisCst.Text:= imposto.PIS.CST.ToString;
  formPrincipal.edtPisBc.Text:= imposto.PIS.BaseCalculo.ToString;
  formPrincipal.edtPisPorcentagem.Text:= CurrToStr(imposto.PIS.Porcento);
  formPrincipal.edtPisValor.Text:= imposto.PIS.Valor.ToString;
  formPrincipal.edtPisBcp.Text:= imposto.PIS.BaseCalculoPorProduto.ToString;
  formPrincipal.edtPisAliquota.Text:= CurrToStr(imposto.PIS.Aliquota);

  formPrincipal.edtPisstBc.Text:= imposto.PISST.BaseCalculo.ToString;
  formPrincipal.edtPisstPorcentagem.Text:= CurrToStr(imposto.PISST.Porcento);
  formPrincipal.edtPisstValor.Text:= imposto.PISST.Valor.ToString;
  formPrincipal.edtPisstBcp.Text:= imposto.PISST.BaseCalculoPorProduto.ToString;
  formPrincipal.edtPisstAliquota.Text:= CurrToStr(imposto.PISST.Aliquota);

  formPrincipal.edtCofinsCST.Text:= imposto.COFINS.CST.ToString;
  formPrincipal.edtCofinsBc.Text:= imposto.COFINS.BaseCalculo.ToString;
  formPrincipal.edtCofinsPorcentagem.Text:= CurrToStr(imposto.COFINS.Porcento);
  formPrincipal.edtCofinsValor.Text:= imposto.COFINS.Valor.ToString;
  formPrincipal.edtCofinsBcp.Text:= imposto.COFINS.BaseCalculoPorProduto.ToString;
  formPrincipal.edtCofinsAliquota.Text:= CurrToStr(imposto.COFINS.Aliquota);

  formPrincipal.edtCofinsstBc.Text:= imposto.COFINSST.BaseCalculo.ToString;
  formPrincipal.edtCofinsstPorcentagem.Text:= CurrToStr(imposto.COFINSST.Porcento);
  formPrincipal.edtCofinsstValor.Text:= imposto.COFINSST.Valor.ToString;
  formPrincipal.edtCofinsstBcp.Text:= imposto.COFINSST.BaseCalculoPorProduto.ToString;
  formPrincipal.edtCofinsstAliquota.Text:= CurrToStr(imposto.COFINSST.Aliquota);

  formPrincipal.edtICMSOrigem.Text:= imposto.ICMS.Origem.ToString;
  formPrincipal.edtICMSCST.Text:= imposto.ICMS.CST.ToString;
  formPrincipal.edtICMSPorcentagem.Text:= CurrToStr(imposto.ICMS.Porcento);
  formPrincipal.edtICMSValor.Text:= imposto.ICMS.Valor.ToString;

  formPrincipal.edtISSQNNatureza.Text:= imposto.ISSQN.Natureza.ToString;
  formPrincipal.edtIssqnBC.Text:= imposto.ISSQN.BaseCalculo.ToString;
  formPrincipal.edtIssqnDeducao.Text:= imposto.ISSQN.Deducao.ToString;
  formPrincipal.edtIssqnCodigoIbge.Text:= imposto.ISSQN.CodigoIBGE.Value;
  formPrincipal.edtIssqnBcp.Text:= imposto.ISSQN.BaseCalculoProduto.ToString;
  formPrincipal.edtIssqnItemServico.Text:= imposto.ISSQN.ItemDaListaServico.Value;
  formPrincipal.edtIssqnCodigoTributario.Text:= imposto.ISSQN.CodigoTributarioISSQN.Value;

  formPrincipal.edtTaxaTaxaEstadual.Text:= imposto.TaxasFiscais.TaxaEstadual.ToString;
  formPrincipal.edtTaxaTaxaFederal.Text:= imposto.TaxasFiscais.TaxaFederal.ToString;

  formPrincipal.txtIcmsID.Tag:= imposto.ICMSID;
  formPrincipal.txtIssqnId.Tag:= imposto.ISSQNID;
  formPrincipal.txtPisId.Tag:= imposto.PISID;
  formPrincipal.txtPisstId.Tag:= imposto.PISSTID;
  formPrincipal.txtCofinsId.Tag:= imposto.COFINSID;
  formPrincipal.txtCofinsstId.Tag:= imposto.COFINSSTID;
  formPrincipal.txtTaxasId.Tag:= imposto.TaxasID;
end;

end.
