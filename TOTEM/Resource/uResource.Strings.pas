unit uResource.Strings;

interface

resourcestring

  MsgErroSemRota = 'N�o � poss�vel fazer uma requisi��o sem uma rota';

  MsgErroServNaoEncontrado = 'Servidor n�o encontrado';

  MsgErroCategoriaInv�lida = 'Categoria Inv�lida';

  MsgErroSaboresNaoPreenchidos = 'N�o foram preenchido todos os sabores de pizza';

  MsgErroProdutoNulo = 'Produto ou Venda Nulos';

  MsgErroComecaVenda = 'Erro ao come�a a venda';

  MsgErroVender = 'N�o foi poss�vel realizar a venda';

implementation

end.
