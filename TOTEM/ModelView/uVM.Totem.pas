unit uVM.Totem;

interface

uses
  System.SysUtils, System.Classes, uTotem, View.Principal;

type
  TdmTotem = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    _totem: TTotem;
    { Private declarations }
    procedure ExibirDadosDoEstabelecimento();
  public

    { Public declarations }
  end;

var
  dmTotem: TdmTotem;

implementation

uses
  FMX.Graphics;

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

procedure TdmTotem.DataModuleCreate(Sender: TObject);
begin
  _totem:= TTotem.Create();
  ExibirDadosDoEstabelecimento();
end;

procedure TdmTotem.DataModuleDestroy(Sender: TObject);
begin
  _totem.Free;
end;

procedure TdmTotem.ExibirDadosDoEstabelecimento;
var
  logo: TBitmap;
begin
  logo:= _totem.Estabelecimento.GetLogo();

  if Assigned(logo) then
  begin

    frmPrincipal.imgLogoDoEstabelecimento.Bitmap:= logo;
  end;

  frmPrincipal.txtEstabelecimento.Text:= _totem.Estabelecimento.NomeFantasia;
end;

end.
