unit uVM.Cardapio;

interface

uses
  {ListView}
    FMX.ListView.Types,
    FMX.ListView.Appearances,
    FMX.ListView.Adapters.Base,
    FMX.ListView,
  {Delphi}
    System.SysUtils,
    System.Classes,
    System.Generics.Collections,
    System.UITypes,
  {Model}
    uCardapio,
    uCategoria,
    uItem.Produto,
    uItem.ItemExtra,
    uItem.Produto.Pizza,
    uPizzaTamanho,
    uPizzaTipo,
    uCardapio.Manager,
    uItem.Produto.Combinado,
  {View}
    View.QtdItemExtraObs,
  {Delphi}
    System.Types;

type
  TdmCardapio = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    Cardapio: TCardapio;
    frmQtdItemExtraObs: TfrmQtdItemExtraObs;

    procedure MostraInfoPizza(tamanhoPizza: TTamanhoPizza;formaTamanho: TTipoCobrancaPorTamanho;
       formaTipo:TTipoCobrancaPorCombinada; qtdSabores: TTipos);

    procedure CarregarListViewCategorias();
    procedure CarregarListViewProdutos(produtos: TList<TProduto>);
    procedure CarregarListViewPizzas(produtos: TList<TProduto>);
    procedure CarregarListViewItensExtra(itens: TList<TItemExtra>);

    function DefinirTipoTamanhoPizza(produtos: TList<TProduto>): Boolean;

    procedure DoTipoPizzaAtualiza(tipoPizza:TTipos ; saborAtual: integer; produtoAtual: TPizza; valorAtual: Currency;
    Action: TCollectionNotification);
    procedure DoNotifyItensExtra(Sender: TObject; const Item: TItemExtra; Action: TCollectionNotification);
  public
    procedure IniciarCardapios();
    procedure ListaProdutosCategoria(aItemLsv: TListViewItem);
    procedure ListaCategorias();
    procedure ListaItensExtra();

    procedure AdcRemovePizza(aItemLsv: TListViewItem);

    procedure DefinirProduto(aItemLsv: TListViewItem); overload;
    function DefinirProduto(aProduto: TProduto): Boolean; overload;
    function ModificarProduto(aProduto: TProduto; quantidade: double): Double;
    function ModificaPizza: Boolean;

    procedure VoltarParaCategorias();
    procedure CancelarProdutosCombinados();
  end;

var
  dmCardapio: TdmCardapio;


implementation

uses
  {View}
    View.Principal,
    View.TipoTamanhoPizza,
  {ViewModel}
    uVM.Venda, FMX.Objects;

{$R *.dfm}

{ TdmCardapio }


procedure TdmCardapio.AdcRemovePizza(aItemLsv: TListViewItem);
var
  produto: TProduto;
  pizza: TPizza;
var
  img: TListItemImage;
begin
  if Cardapio.PizzaCombinada.VerificarSeTodosSaboresForamPreenchidos then
    exit();

  if aItemLsv.Checked then
  begin
    img:= TListItemImage(aItemLsv.Objects.FindDrawable('Selecionavel'));
    img.Bitmap:= frmPrincipal.imgListPizzas.Bitmap(TSizeF.Create(512, 512), 10);
    aItemLsv.Checked:= false;
    Cardapio.PizzaCombinada.RemoverPizza(aItemLsv.Tag);
  end
  else
  begin
    img:= TListItemImage(aItemLsv.Objects.FindDrawable('Selecionavel'));
    img.Bitmap:= frmPrincipal.imgListPizzas.Bitmap(TSizeF.Create(512, 512), 11);
    aItemLsv.Checked:= true;
    pizza:= TPizza(Cardapio.GetProduto(aItemLsv.Tag));
    Cardapio.pizzaCombinada.AdicionarPizza(pizza);
  end;

  if Cardapio.PizzaCombinada.VerificarSeTodosSaboresForamPreenchidos then
  begin

    frmPrincipal.tbcCategoriasProdutos.TabIndex:= 0;

    produto:= Cardapio.CriarPizzaCombinada();

    if not Assigned(produto) then
      raise Exception.Create('Produto n�o foi localizado');

    if not DefinirProduto(produto) then
      TProdutoCombinado(produto).Free;
  end;

end;

procedure TdmCardapio.CancelarProdutosCombinados;
begin
  Cardapio.PizzaCombinada.LimparPizzaCombinada();
end;

procedure TdmCardapio.CarregarListViewCategorias();
var
  novoItem: TListViewItem;
  it: TCategoria;
begin
  frmPrincipal.lsvCategorias.Items.Clear;

  for it in Cardapio.Categorias do
  begin
    novoItem:= frmPrincipal.lsvCategorias.Items.Add;

    novoItem.Text:= it.Descricao;
    novoItem.Tag:= it.ID;
    novoItem.Bitmap:= it.Logo;
  end;
end;

procedure TdmCardapio.CarregarListViewItensExtra(itens: TList<TItemExtra>);
var
  novoItem: TListViewItem;
  descricao, valor: TListItemText;
  it: TItemExtra;
begin
  try
    frmQtdItemExtraObs.lsvItensExtra.Items.Clear;
    for it in itens do
    begin
      novoItem:= frmQtdItemExtraObs.lsvItensExtra.Items.Add;
      descricao:= TListItemText(novoItem.Objects.FindDrawable('Descricao'));
      valor:= TListItemText(novoItem.Objects.FindDrawable('Valor'));

      frmQtdItemExtraObs.ItensExtra.Add(it);
      novoItem.Tag:= it.ID;
      descricao.Text:= it.Descricao;
      valor.Text:= FormatCurr('R$ ###,###,##0.00;1; ', it.ValorUnitario);
    end;
  finally
    itens.Free;
  end;
end;

procedure TdmCardapio.CarregarListViewPizzas(produtos: TList<TProduto>);
var
  novoItem: TListViewItem;
  descricao, detalhes, valor: TListItemText;
  it: TProduto;
begin
  frmPrincipal.lsvPizzas.Items.Clear;

  try
    for it in produtos do
    begin
      novoItem:= frmPrincipal.lsvPizzas.Items.Add;
      descricao:= TListItemText(novoItem.Objects.FindDrawable('Descricao'));
      detalhes:= TListItemText(novoItem.Objects.FindDrawable('Detalhes'));
      valor:= TListItemText(novoItem.Objects.FindDrawable('Valor'));

      novoItem.Tag:= it.ID;
      descricao.Text:= it.Descricao;
      detalhes.Text:= it.Detalhes;
      valor.Text:= FormatCurr('R$ ###,###,##0.00;1; ', TPizza(it).ValorPizza);
    end;
  finally
    produtos.Free;
  end;
end;

procedure TdmCardapio.CarregarListViewProdutos(produtos: TList<TProduto>);
var
  novoItem: TListViewItem;
  descricao, detalhes, valor: TListItemText;
  it: TProduto;
begin
  frmPrincipal.lsvProdutos.Items.Clear;
  try
    for it in produtos do
    begin
      novoItem:= frmPrincipal.lsvProdutos.Items.Add;
      descricao:= TListItemText(novoItem.Objects.FindDrawable('Descricao'));
      detalhes:= TListItemText(novoItem.Objects.FindDrawable('Detalhes'));
      valor:= TListItemText(novoItem.Objects.FindDrawable('Valor'));

      novoItem.Tag:= it.ID;
      descricao.Text:= it.Descricao;
      detalhes.Text:= it.Detalhes;
      valor.Text:= FormatCurr('R$ ###,###,##0.00;1; ', it.ValorUnitario);
    end;
  finally
    produtos.Free;
  end;
end;

procedure TdmCardapio.DataModuleCreate(Sender: TObject);
var
  cardapioManager: TCardapioManager;
begin
  cardapioManager:= TCardapioManager.Create();
  try
    Cardapio:= cardapioManager.CarregarCardapio();
    Cardapio.PizzaCombinada.OnAtualizaLista:= DoTipoPizzaAtualiza;
  finally
    cardapioManager.Free;
  end;
end;

procedure TdmCardapio.DataModuleDestroy(Sender: TObject);
begin
  Cardapio.Free;
end;

procedure TdmCardapio.DefinirProduto(aItemLsv: TListViewItem);
var
  produto: TProduto;
  it: TItemExtra;
  it2: string;
  itemExtra: TItemExtra;
begin
  produto:= Cardapio.GetProduto(aItemLsv.Tag);
  frmQtdItemExtraObs:= TfrmQtdItemExtraObs.Create(nil, produto);
  try
    frmQtdItemExtraObs.ItensAdicionado.OnNotify:= DoNotifyItensExtra;
    ListaItensExtra();
    frmPrincipal.btnCancelar.Visible:= false;
    frmPrincipal.btnConfirmar.Visible:= false;
    frmQtdItemExtraObs.ShowModal;
    frmPrincipal.btnCancelar.Visible:= true;
    frmPrincipal.btnConfirmar.Visible:= true;
    frmQtdItemExtraObs.ItensAdicionado.OnNotify:= nil;

    if frmQtdItemExtraObs.ModalResult = mrYes then
    begin

      for it in frmQtdItemExtraObs.ItensAdicionado do
      begin
        itemExtra:= Cardapio.GetItemExtra(it.ID);

        if not Assigned(itemExtra) then
          raise Exception.Create('Item extra n�o encontrado');

        produto.ItensExtra.Add(itemExtra);
      end;

      for it2 in frmQtdItemExtraObs.Linhas do
      begin
        produto.Observacao.Add(it2);   
      end;
    
      dmVenda.AdicionarProdutos(produto, frmQtdItemExtraObs.Quantidade);
    end
    else
      produto.Free;
  finally
    frmQtdItemExtraObs.Free
  end;
end;

function TdmCardapio.DefinirProduto(aProduto: TProduto): Boolean;
var
  it: TItemExtra;
  itemExtra: TItemExtra;
  it2: string;
begin
  frmQtdItemExtraObs:= TfrmQtdItemExtraObs.Create(nil, aProduto);
  try

    frmQtdItemExtraObs.ItensAdicionado.OnNotify:= DoNotifyItensExtra;
    ListaItensExtra();
    frmPrincipal.btnCancelar.Visible:= false;
    frmPrincipal.btnConfirmar.Visible:= false;
    frmQtdItemExtraObs.ShowModal;
    frmPrincipal.btnCancelar.Visible:= true;
    frmPrincipal.btnConfirmar.Visible:= true;
    frmQtdItemExtraObs.ItensAdicionado.OnNotify:= nil;
    if frmQtdItemExtraObs.ModalResult = mrYes then
    begin

      result:= true;
      for it in frmQtdItemExtraObs.ItensAdicionado do
      begin
        itemExtra:= Cardapio.GetItemExtra(it.ID);

        if not Assigned(itemExtra) then
          raise Exception.Create('Item extra n�o encontrado');

        aProduto.ItensExtra.Add(itemExtra);
      end;

      for it2 in frmQtdItemExtraObs.Linhas do
      begin
        aProduto.Observacao.Add(it2);   
      end;                                     

      dmVenda.AdicionarProdutos(aProduto, frmQtdItemExtraObs.Quantidade);
    end
    else
      result:= false;
  finally
    frmQtdItemExtraObs.Free
  end;
end;

function TdmCardapio.DefinirTipoTamanhoPizza(produtos: TList<TProduto>): Boolean;
var
  frmTamanhoTipoPizza: TfrmTamanhoTipoPizza;
  it: TProduto;
begin
  frmTamanhoTipoPizza:= TfrmTamanhoTipoPizza.Create(nil);
  try
    frmPrincipal.btnCancelar.Visible:= false;
    frmPrincipal.btnConfirmar.Visible:= false;
    frmTamanhoTipoPizza.ShowModal;
    frmPrincipal.btnCancelar.Visible:= true;
    frmPrincipal.btnConfirmar.Visible:= true;
    if frmTamanhoTipoPizza.ModalResult = mrYes then
    begin

      result:= true;
      for it in produtos do
      begin
        if frmTamanhoTipoPizza.rbnBroto.IsChecked then
          TPizza(it).TamanhoPizza.TamanhoPizza:= tpBroto
        else if frmTamanhoTipoPizza.rbnNormal.IsChecked then
          TPizza(it).TamanhoPizza.TamanhoPizza:= tpNormal
        else if frmTamanhoTipoPizza.rbnMedia.IsChecked then
          TPizza(it).TamanhoPizza.TamanhoPizza:= tpMedia
        else if frmTamanhoTipoPizza.rbnGigante.IsChecked then
          TPizza(it).TamanhoPizza.TamanhoPizza:= tpGigante;

        TPizza(it).DefinirTamanhoPizza;
      end;

       Cardapio.PizzaCombinada.IniciarEscolhaSabores;

      if frmTamanhoTipoPizza.rbnMeia.IsChecked then
      begin
        Cardapio.PizzaCombinada.Tipo:= tMeia
      end
      else if frmTamanhoTipoPizza.rbnTerco.IsChecked then
      begin
        Cardapio.PizzaCombinada.Tipo:= tTerco
      end
      else if frmTamanhoTipoPizza.rbnQuarto.IsChecked then
      begin
        Cardapio.PizzaCombinada.Tipo:= tQuadra
      end
      else if frmTamanhoTipoPizza.rbnUmSabor.IsChecked then
      begin
        Cardapio.PizzaCombinada.Tipo:= tNormal;
      end;

      Cardapio.PizzaCombinada.DefinirTipo;
      Cardapio.PizzaCombinada.IniciarEscolhaSabores;
      MostraInfoPizza(
        TPizza(produtos.First).TamanhoPizza.TamanhoPizza,
        TPizza(produtos.First).TamanhoPizza.TipoCobranca,
        Cardapio.PizzaCombinada.TipoCobranca,
        Cardapio.PizzaCombinada.Tipo
      );

      Cardapio.PizzaCombinada.LimparPizzaCombinada;
    end
    else
      result:= false;

    if not Result then
        produtos.Free;
  finally
    frmTamanhoTipoPizza.Free
  end;
end;

procedure TdmCardapio.IniciarCardapios;
begin
  CarregarListViewCategorias;
end;

procedure TdmCardapio.ListaCategorias;
begin
  CarregarListViewCategorias();
end;

procedure TdmCardapio.ListaItensExtra();
var
  itens: TList<TItemExtra>;
  it, itemExtra: TItemExtra;
begin
  itens:= TList<TItemExtra>.Create;
  for it in Cardapio.ItensExtra do
  begin
    itemExtra:= Cardapio.GetItemExtra(it.ID);

    if not Assigned(itemExtra) then
      raise Exception.Create('Item Extra n�o foi localizado');

    itens.Add(itemExtra);
  end;
  CarregarListViewItensExtra(itens);
end;

procedure TdmCardapio.ListaProdutosCategoria(aItemLsv: TListViewItem);
var
  categoriaTemp: TCategoria;
  cardapioManager: TCardapioManager;
  produtos: TObjectList<TProduto>;
begin
  categoriaTemp:= Cardapio.GetCategoria(aItemLsv.Tag);
  cardapioManager:= TCardapioManager.Create();
  try
    produtos:= cardapioManager.CarregarProdutosDeUmaCategoria(categoriaTemp, Cardapio);

    frmPrincipal.txtProdutoCategoria.Text:= categoriaTemp.Descricao;
    if LowerCase(categoriaTemp.Descricao).Contains('pizza') then
    begin
     if DefinirTipoTamanhoPizza(produtos) then
     begin
      CarregarListViewPizzas(produtos);
      frmPrincipal.tbcCategoriasProdutos.TabIndex:= 2;
      frmPrincipal.AjustaLsvProdutos(frmPrincipal.lsvPizzas);
     end;
    end
    else
    begin
      CarregarListViewProdutos(produtos);
      frmPrincipal.tbcCategoriasProdutos.TabIndex:= 1;
      frmPrincipal.AjustaLsvProdutos(frmPrincipal.lsvProdutos);
    end;
  finally
    categoriaTemp.Free;
    cardapioManager.Free;
  end;
end;

function TdmCardapio.ModificaPizza: Boolean;
var
  produtos: TList<TProduto>;
  cardapioManager: TCardapioManager;
  categoria: TCategoria;
  it: TCategoria;
  it2: TProduto;
  descricao: string;
begin
  cardapioManager:= TCardapioManager.Create();
  categoria:= nil;
  for it in Cardapio.Categorias do
  begin
    descricao:= LowerCase(it.Descricao);
    if descricao.Contains('pizza') then
    begin
      categoria:= Cardapio.GetCategoria(it.ID);
      break;
    end;
  end;

  try
    produtos:= cardapioManager.CarregarProdutosDeUmaCategoria(categoria, Cardapio);

    if DefinirTipoTamanhoPizza(produtos) then
    begin
     Result:= true;
     CarregarListViewPizzas(produtos);
     frmPrincipal.tbcCategoriasProdutos.TabIndex:= 2;
     frmPrincipal.AjustaLsvProdutos(frmPrincipal.lsvPizzas);
    end
    else
    begin
      for it2 in produtos do
       TPizza(it2).Free;

      produtos.Free;

      Result:= false;
    end;
  finally
    categoria.Free;
    cardapioManager.Free;
  end;
end;

function TdmCardapio.ModificarProduto(aProduto: TProduto; quantidade: double): double;
var
  it: TItemExtra;
  it2: string;
  itemExtra: TItemExtra;
begin
  frmQtdItemExtraObs:= TfrmQtdItemExtraObs.Create(nil, aProduto, quantidade);
  try

    ListaItensExtra();
    frmQtdItemExtraObs.IniciaItensAdicionados(aProduto.ItensExtra.ToArray);
    aProduto.ItensExtra.OwnsObjects:= false;
    aProduto.ItensExtra.Clear;
    aProduto.ItensExtra.OwnsObjects:= true;
    frmQtdItemExtraObs.ItensAdicionado.OnNotify:= DoNotifyItensExtra;
    frmPrincipal.btnCancelar.Visible:= false;
    frmPrincipal.btnConfirmar.Visible:= false;
    frmQtdItemExtraObs.ShowModal;
    frmPrincipal.btnCancelar.Visible:= true;
    frmPrincipal.btnConfirmar.Visible:= true;
    frmQtdItemExtraObs.ItensAdicionado.OnNotify:= nil;
    

    if frmQtdItemExtraObs.ModalResult = mrYes then
    begin

      for it in frmQtdItemExtraObs.ItensAdicionado do
      begin
        itemExtra:= Cardapio.GetItemExtra(it.ID);

        if not Assigned(itemExtra) then
          raise Exception.Create('Item extra n�o encontrado');

        aProduto.ItensExtra.Add(itemExtra);
      end;

      for it2 in frmQtdItemExtraObs.Linhas do
      begin
        aProduto.Observacao.Add(it2);   
      end;                  

      result:= frmQtdItemExtraObs.Quantidade;
    end
    else
    begin
      Result:= quantidade;
    end; 
  finally
    frmQtdItemExtraObs.Free;
  end;
end;

procedure TdmCardapio.MostraInfoPizza(tamanhoPizza: TTamanhoPizza;formaTamanho: TTipoCobrancaPorTamanho;
       formaTipo:TTipoCobrancaPorCombinada; qtdSabores: TTipos);
var
  tituloPizza: string;
  tamanhoStr: string;
  qtdSaboresStr: string;
begin
  tituloPizza:= 'Pizza';
  case tamanhoPizza of
    tpBroto:
    begin
       tituloPizza:=  tituloPizza + ' de 4 peda�os';
       tamanhoStr:= 'Broto';
    end;
    tpMedia:
    begin
      tituloPizza:=  tituloPizza + ' de 6 peda�os';
      tamanhoStr:= 'M�dia';
    end;
    tpGigante:
    begin
      tituloPizza:=  tituloPizza + ' de 10 peda�os';
      tamanhoStr:= 'Gigante';
    end;
  end;

  case qtdSabores of
    tMeia:
    begin
       tituloPizza:=  tituloPizza + ' com 2 sabores';
       qtdSaboresStr:= '2 Sabores';
    end;
    tTerco:
    begin
      tituloPizza:=  tituloPizza + ' com 3 sabores';
      qtdSaboresStr:= '3 Sabores';
    end;
    tQuadra:
    begin
      tituloPizza:=  tituloPizza + ' com 4 sabores';
      qtdSaboresStr:= '4 Sabores';
    end;
    tNormal:
    begin
      tituloPizza:=  tituloPizza + ' com 1 sabor';
    end;
  end;

  frmPrincipal.txtPizzas.Text:= tituloPizza;

  if not tamanhoStr.IsEmpty then
    case formaTamanho of
      tctValorFixo: frmPrincipal.txtCobrancaTamanho.Text:='O valor da pizza ' + tamanhoStr + ' � definida com um valor fixo';
      tctPorcentagem: frmPrincipal.txtCobrancaTamanho.Text:='O valor da pizza ' +  tamanhoStr + ' � definida com uma porcentagem';
      tctValorMM: frmPrincipal.txtCobrancaTamanho.Text:='O valor da pizza ' +  tamanhoStr + ' � definida com um valor maior ou menor fixo';
    end;

  if not qtdSaboresStr.IsEmpty then
    case formaTipo of
      tccMedia: frmPrincipal.txtCobrancaTipo.Text:='O valor de uma pizza com ' +  qtdSaboresStr + ' � feita pela m�dia dos valores dos sabores';
      tccValorDaMaior: frmPrincipal.txtCobrancaTipo.Text:='O valor de uma pizza com ' +  qtdSaboresStr + ' � feita pelo o sabor de maior valor';
    end;
end;

procedure TdmCardapio.VoltarParaCategorias;
begin
  Cardapio.PizzaCombinada.LimparPizzaCombinada;
  frmPrincipal.lsvProdutos.Items.Clear;
  frmPrincipal.lsvPizzas.Items.Clear;
end;

procedure TdmCardapio.DoNotifyItensExtra(Sender: TObject;
  const Item: TItemExtra; Action: TCollectionNotification);
var
  I: Integer;
  novoItem: TListViewItem;
  descricao, valor: TListItemText;
begin
  if Action = cnAdded then
  begin
    novoItem:= frmQtdItemExtraObs.lsvItensAdicionados.Items.Add;

    descricao:= TListItemText(novoItem.Objects.FindDrawable('Descricao'));
    valor:= TListItemText(novoItem.Objects.FindDrawable('Valor'));

    novoItem.Checked:= true;
    novoItem.Tag:= Item.ID;
    descricao.Text:= item.Descricao;
    valor.Text:=  FormatCurr('R$ ###,###,##0.00;1; ', Item.ValorUnitario);
  end else if Action = cnRemoved then
  begin
    for I := 0 to Pred(frmQtdItemExtraObs.lsvItensAdicionados.Items.Count) do
    begin
      if frmQtdItemExtraObs.lsvItensAdicionados.Items.Item[I].Tag = item.ID then
      begin
        frmQtdItemExtraObs.lsvItensAdicionados.Items.Delete(I);
        exit;
      end;
    end;
  end;
end;

procedure TdmCardapio.DoTipoPizzaAtualiza(tipoPizza:TTipos ; saborAtual: integer; produtoAtual: TPizza; valorAtual: Currency;
    Action: TCollectionNotification);
const
  strVazio = '';
var
  I: Integer;
begin                                        
    if saborAtual = 0 then
    begin
      frmPrincipal.imgTipoAtual.Bitmap:= frmPrincipal.imgListPizzas.Bitmap(TSizeF.Create(75,75), 12);

      frmPrincipal.txtPizza1.Text:= strVazio;
      frmPrincipal.txtPizza2.Text:= strVazio;
      frmPrincipal.txtPizza3.Text:= strVazio;
      frmPrincipal.txtPizza4.Text:= strVazio;
    end;

    case tipoPizza of
      tMeia:
      begin
        case saborAtual of
          1: frmPrincipal.imgTipoAtual.Bitmap:= frmPrincipal.imgListPizzas.Bitmap(TSizeF.Create(75,75), 6);
          2: frmPrincipal.imgTipoAtual.Bitmap:= frmPrincipal.imgListPizzas.Bitmap(TSizeF.Create(75,75), 3);
        end;
      end;
      tTerco:
      begin
        case saborAtual of
          1: frmPrincipal.imgTipoAtual.Bitmap:= frmPrincipal.imgListPizzas.Bitmap(TSizeF.Create(75,75), 4);
          2: frmPrincipal.imgTipoAtual.Bitmap:= frmPrincipal.imgListPizzas.Bitmap(TSizeF.Create(75,75), 5);
          3: frmPrincipal.imgTipoAtual.Bitmap:= frmPrincipal.imgListPizzas.Bitmap(TSizeF.Create(75,75), 3);
        end;
      end ;
      tQuadra:
      begin
        case saborAtual of
          1: frmPrincipal.imgTipoAtual.Bitmap:= frmPrincipal.imgListPizzas.Bitmap(TSizeF.Create(75,75), 7);
          2: frmPrincipal.imgTipoAtual.Bitmap:= frmPrincipal.imgListPizzas.Bitmap(TSizeF.Create(75,75), 8);
          3: frmPrincipal.imgTipoAtual.Bitmap:= frmPrincipal.imgListPizzas.Bitmap(TSizeF.Create(75,75), 9);
          4: frmPrincipal.imgTipoAtual.Bitmap:= frmPrincipal.imgListPizzas.Bitmap(TSizeF.Create(75,75), 3);
        end;
      end ;
      tNormal:
      begin
        case saborAtual of
          1: frmPrincipal.imgTipoAtual.Bitmap:= frmPrincipal.imgListPizzas.Bitmap(TSizeF.Create(75,75), 3);
        end;
      end ;
    end;

  if Assigned(produtoAtual) then
  begin
    if Action = cnAdded then
    begin
      for I := 0 to Pred(frmPrincipal.ComponentCount) do
      begin
        if frmPrincipal.Components[I] is TText then
        begin
          if ((frmPrincipal.Components[I] as TText).TagString = 'pizza') and 
            ((frmPrincipal.Components[I] as TText).Text = strVazio)then
          begin
            (frmPrincipal.Components[I] as TText).Text:= produtoAtual.Descricao;
            (frmPrincipal.Components[I] as TText).Tag:= produtoAtual.ID;
            break;
          end
          else
            Continue;
        end;     
      end;
    end
    else
    begin
      for I := 0 to Pred(frmPrincipal.ComponentCount) do
      begin
        if frmPrincipal.Components[I] is TText then
        begin
          if ((frmPrincipal.Components[I] as TText).Tag = produtoAtual.ID) and
            ((frmPrincipal.Components[I] as TText).TagString = 'pizza') then
          begin
            (frmPrincipal.Components[I] as TText).Text:= strVazio;
            (frmPrincipal.Components[I] as TText).Tag:=-1;
          end;
        end;
      end;
    end;
  end;
end;


end.
