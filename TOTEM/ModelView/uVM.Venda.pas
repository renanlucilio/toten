unit uVM.Venda;

interface

uses
  {ListView}
    FMX.ListView.Types,
    FMX.ListView.Appearances,
    FMX.ListView.Adapters.Base,
    FMX.ListView,
  {Model}
    uVendedor,
    uItem.Produto,
    uVendaProduto,
    uVM.Cardapio,
    uItem.Produto.Pizza,
    uItem.Produto.Combinado,
  {Delphi}
    System.SysUtils,
    System.Classes,
    System.Generics.Collections,
  {View}
    View.ModificarProduto;

type
  TdmVenda = class(TDataModule)
  private
    vendedor: TVendedor;
    FEmVenda: Boolean;
    procedure NotifyListComanda(Sender: TObject; const Item: TVendaProduto; Action: TCollectionNotification);
    procedure ModificarProdutoNormal(produto: TVendaProduto);
    procedure ModificarPizza(produto: TVendaProduto);
  public
    procedure AdicionarProdutos(produto: TProduto; quantidade: double);

    property EmVenda: Boolean read FEmVenda default false;

    procedure ComecaVenda;
    procedure CancelarVenda;
    procedure FinalizaVenda;
    function ProdutoJaExiste(produto: TProduto; out quantidade: double): Boolean;

    procedure EditarExcluirProduto(itemList: TListViewItem);
  end;

var
  dmVenda: TdmVenda;

implementation
{$R *.dfm}

uses
  {View}
    View.Principal,
  {Delphi}
    System.UITypes;

{ TdmVenda }

procedure TdmVenda.AdicionarProdutos(produto: TProduto; quantidade: double);
begin
  if Assigned(produto) then
    vendedor.Comanda.AdicionarProduto(produto, quantidade);


end;

procedure TdmVenda.EditarExcluirProduto(itemList: TListViewItem);
var
  produto: TVendaProduto;
begin
  produto:= vendedor.Comanda.GetProduto(itemList.TagString);

  if not Assigned(produto) then
    exit;

  if produto.Produto is TProdutoCombinado then
    ModificarPizza(produto)
  else
    ModificarProdutoNormal(produto);
end;

procedure TdmVenda.FinalizaVenda;
begin
  try
    if vendedor.Venda.GravarVenda then
    begin
      vendedor.Free;
      FEmVenda:= false;
      vendedor.Comanda.ItensVendidos.OnNotify:= nil;
      frmPrincipal.lsvComanda.Items.Clear;
    end;
  finally
    frmPrincipal.tbcPrincipal.TabIndex:= 2;
  end;
end;

procedure TdmVenda.CancelarVenda;
begin
  if not vendedor.Venda.CancelarVenda then
    raise Exception.Create('Houve um erro ao come�a a venda');
  vendedor.Free;
  FEmVenda:= false;
  vendedor.Comanda.ItensVendidos.OnNotify:= nil;
  frmPrincipal.lsvComanda.Items.Clear;
end;

procedure TdmVenda.ComecaVenda;
begin
  vendedor:= TVendedor.Create;
  if not vendedor.Venda.ComecaVender then
    raise Exception.Create('Houve um erro ao come�a a venda');
  FEmVenda:= true;
  frmPrincipal.lsvComanda.Items.Clear;
  vendedor.Comanda.ItensVendidos.OnNotify:= NotifyListComanda;
end;

procedure TdmVenda.ModificarPizza(produto: TVendaProduto);
var
  frmModificar: TfrmModificaProduto;
begin
  frmModificar:= TfrmModificaProduto.Create(nil, produto);
  try
    frmPrincipal.btnCancelar.Visible:= false;
    frmPrincipal.btnConfirmar.Visible:= false;
    frmModificar.ShowModal;
    frmPrincipal.btnCancelar.Visible:= true;
    frmPrincipal.btnConfirmar.Visible:= true;

    if frmModificar.ModalResult = mrAbort then
    begin
      vendedor.Comanda.RemoverProduto(produto.ID);
      frmPrincipal.lsvComanda.ItemIndex:= 0;
      produto.Free;
    end;

    if frmModificar.ModalResult = mrAll then
    begin
      vendedor.Comanda.RemoverProduto(produto.ID);
      frmPrincipal.lsvComanda.ItemIndex:= 0;
      produto.Free;
      dmCardapio.ModificaPizza;
    end;

    if frmModificar.ModalResult = mrOk then
    begin
      produto.Quantidade:= dmCardapio.ModificarProduto(produto.Produto, produto.Quantidade);
      vendedor.Comanda.AtualizarProduto(produto);
    end;

  finally
    frmModificar.Free;
  end;
end;

procedure TdmVenda.ModificarProdutoNormal(produto: TVendaProduto);
var
  frmModificar: TfrmModificaProduto;
begin
  frmModificar:= TfrmModificaProduto.Create(nil, produto);
  try
    frmPrincipal.btnCancelar.Visible:= false;
    frmPrincipal.btnConfirmar.Visible:= false;
    frmModificar.ShowModal;
    frmPrincipal.btnCancelar.Visible:= true;
    frmPrincipal.btnConfirmar.Visible:= true;

    if frmModificar.ModalResult = mrAbort then
    begin
      vendedor.Comanda.RemoverProduto(produto.ID);
      produto.Free;
    end;

    if frmModificar.ModalResult = mrOk then
    begin
      produto.Quantidade:= dmCardapio.ModificarProduto(produto.Produto, produto.Quantidade);
      vendedor.Comanda.AtualizarProduto(produto);
    end;

  finally
    frmModificar.Free;
  end;
end;

procedure TdmVenda.NotifyListComanda(Sender: TObject;const Item: TVendaProduto; Action: TCollectionNotification);
var
  I: Integer;
  novoItem: TListViewItem;
  descricao, valor, detalhes: TListItemText;
  it: TVendaProduto;
begin
  frmPrincipal.txtTotal.Text:= FormatCurr('R$ ###,###,##0.00;1; ', vendedor.Comanda.GetTotalVenda);
  frmPrincipal.lsvComanda.Hide;
  frmPrincipal.lsvComanda.BeginUpdate;
  frmPrincipal.lsvComanda.Items.Clear;
  try
    for it in vendedor.Comanda.ItensVendidos do
    begin
      novoItem:= frmPrincipal.lsvComanda.Items.Add;

      descricao:= TListItemText(novoItem.Objects.FindDrawable('Descricao'));
      valor:= TListItemText(novoItem.Objects.FindDrawable('Valor'));
      detalhes:= TListItemText(novoItem.Objects.FindDrawable('Detalhes'));

      novoItem.TagString:= it.ID;

      descricao.Text:= it.GetDescricaoProduto();
      detalhes.Text:= it.GetDetalhesProduto;
      valor.Text:=  FormatCurr('R$ ###,###,##0.00;1; ', it.GetValor);
    end;
  finally
    frmPrincipal.lsvComanda.Show;
    frmPrincipal.lsvComanda.EndUpdate;
  end;


end;

function TdmVenda.ProdutoJaExiste(produto: TProduto; out quantidade: double): Boolean;
begin
  result:= vendedor.Comanda.ProdutoJaEstaNaComanda(produto, quantidade);
end;

end.
