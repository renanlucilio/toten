program Totem;





{$R *.dres}

uses
  System.StartUpCopy,
  FMX.Forms,
  uLog,
  TratandoErro,
  uAtualizador,
  uItem in 'Model\uItem.pas',
  uItem.ItemExtra in 'Model\uItem.ItemExtra.pas',
  uCategoria in 'Model\uCategoria.pas',
  uItem.Produto in 'Model\uItem.Produto.pas',
  uItem.Produto.Pizza in 'Model\uItem.Produto.Pizza.pas',
  uPizzaTamanho in 'Model\uPizzaTamanho.pas',
  uPizzaTipo in 'Model\uPizzaTipo.pas',
  uVendaProduto in 'Model\uVendaProduto.pas',
  uVenda in 'Model\uVenda.pas',
  uCliente in 'Model\uCliente.pas',
  uPromocao in 'Model\uPromocao.pas',
  uCardapio in 'Model\uCardapio.pas',
  uRepositorio.Json in 'Model\Repositorio\uRepositorio.Json.pas',
  uRepositorio.Interfaces in 'Model\Repositorio\uRepositorio.Interfaces.pas',
  uRecuperador.Json in 'Model\Repositorio\uRecuperador.Json.pas',
  uPersistencia.Json in 'Model\Repositorio\uPersistencia.Json.pas',
  uTotem in 'Model\uTotem.pas',
  uCardapio.Manager in 'Model\uCardapio.Manager.pas',
  uComanda in 'Model\uComanda.pas',
  uVerificador in 'Model\uVerificador.pas',
  uInterfaces in 'Model\uInterfaces.pas',
  uCriadorJson in 'Model\Repositorio\uCriadorJson.pas',
  uCriadorJson.Interfaces in 'Model\Repositorio\uCriadorJson.Interfaces.pas',
  uConfiguracoes in 'Model\uConfiguracoes.pas',
  uLibUtil in 'Model\Utils\uLibUtil.pas',
  uResource.Strings in 'Resource\uResource.Strings.pas',
  View.QtdItemExtraObs in 'View\View.QtdItemExtraObs.pas' {frmQtdItemExtraObs},
  View.TipoTamanhoPizza in 'View\View.TipoTamanhoPizza.pas' {frmTamanhoTipoPizza},
  uVM.Cardapio in 'ModelView\uVM.Cardapio.pas' {dmCardapio: TDataModule},
  View.Principal in 'View\View.Principal.pas' {frmPrincipal},
  uVM.Venda in 'ModelView\uVM.Venda.pas' {dmVenda: TDataModule},
  uVendedor in 'Model\uVendedor.pas',
  uItem.Produto.Combinado in 'Model\uItem.Produto.Combinado.pas',
  uUnidadeMedida in 'Model\uUnidadeMedida.pas',
  Winapi.Windows,
  View.ModificarProduto in 'View\View.ModificarProduto.pas' {frmModificaProduto},
  uEstabelecimento in 'Model\uEstabelecimento.pas',
  uVM.Totem in 'ModelView\uVM.Totem.pas' {dmTotem: TDataModule};

{$R *.res}


var
   programa, atualizador: THandle;
   titulo: string;
   atualizadorTitulo: string;
begin
  titulo:= 'Totem SICLOP';
  atualizadorTitulo:= 'Atualizador SICLOP';
  Application.Title:=titulo;
  programa := FindWindow(nil, PWideChar(titulo));
  atualizador:= FindWindow(nil, PWideChar(atualizadorTitulo));

  if (programa = 0) and (atualizador = 0) then
  begin
    LogSystem.DoMsg('Iniciando o sistema');
    ReportMemoryLeaksOnShutdown:= true;
    Application.Initialize;
    Application.CreateForm(TdmCardapio, dmCardapio);
  Application.CreateForm(TdmVenda, dmVenda);
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.CreateForm(TdmTotem, dmTotem);
  Application.Run;
  end
  else if (programa = 0) then
    SetForegroundWindow(programa)
  else if (atualizador = 0) then
    SetForegroundWindow(atualizador);
end.
