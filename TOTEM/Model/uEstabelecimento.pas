unit uEstabelecimento;

interface

uses
  FMX.Graphics;

type
  TEstabelecimento = class
  private
    FNomeFantasia: string;
    FId:           Integer;
    FLogo:         TBitmap;
    FLogo64:       string;
  public
    function ToString: string; override;
    destructor Destroy; override;
    property Id:           Integer read FId           write FId;
    property NomeFantasia: string  read FNomeFantasia write FNomeFantasia;
    property Logo64:       string  read FLogo64       write FLogo64;

    function GetLogo(): TBitmap;


  end;


implementation

uses
    uLibUtil;


{ TEstabelecimento }



destructor TEstabelecimento.Destroy;
begin
  if Assigned(FLogo) then
    FLogo.Free;

  inherited;
end;

function TEstabelecimento.GetLogo: TBitmap;
begin
  if FLogo64 = '' then
    Exit(nil);

  if not Assigned(FLogo) then
    FLogo:=BitmapFromBase64(FLogo64);

  Result:= FLogo;
end;

function TEstabelecimento.ToString: string;
begin
  Result:= NomeFantasia;
end;

end.
