unit uUnidadeMedida;

interface

type
  TUnidadeMedida = record
  private
    FSigla: string;
    FDescricao: string;
    FTemCasaDecimal: Boolean;
  public
    constructor Create(aSigala, aDescricao: string; aTemCasaDecima: Boolean);   
  
    property Sigla:          string  read FSigla          write FSigla;
    property Descricao:      string  read FDescricao      write FDescricao;
    property TemCasaDecimal: Boolean read FTemCasaDecimal write FTemCasaDecimal;
  end;

implementation

{ TUnidadeMedida }

constructor TUnidadeMedida.Create(aSigala, aDescricao: string; aTemCasaDecima: Boolean);
begin
  FSigla:= aSigala;
  FDescricao:= aDescricao;
  FTemCasaDecimal:= aTemCasaDecima;  
end;

end.
