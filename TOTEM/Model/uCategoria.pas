unit uCategoria;

interface

uses
  {Delphi}
    FMX.Graphics,
    System.Classes,
    System.Types,
    System.SysUtils,
    System.JSON,
  {Model}
    uInterfaces,
  {Criador JSON}
    uCriadorJson,
    uCriadorJson.Interfaces;

type
  TCategoria = class(TInterfacedObject, IVerificavel, IPodeSerJson)
  private
    FDescricao: string;
    FLogo:      TBitmap;
    FID:        integer;
    FLogoStr:   string;
    procedure CarregarLogoTemp;
  public
    constructor Create(aId: integer; aDescricao, aImg64: string); overload;
    constructor Create; overload;
    destructor Destroy; override;

    property ID:        integer read FID        write FID;
    property Descricao: string  read FDescricao write FDescricao;
    property Logo:      TBitmap read FLogo      write FLogo;
    property LogoStr:   string  read FLogoStr   write FLogoStr;

    function Equals(Obj: TObject): Boolean; override;
    function VerificarSeExiste: Boolean;
    function AsJson: string;
  end;

implementation

uses
  {Repositorio}
    uRepositorio.Json,
    uRepositorio.Interfaces,
  {Model}
    uVerificador,
    uConfiguracoes,
  {Util}
    uLibUtil,
  {Helper}
    Json.Helper;

{ TCategoria }

function TCategoria.AsJson: string;
var
  criadorJson: TCriadorJson;
begin
  criadorJson:= TCriadorJson.Create;
  try
    criadorJson.Inicio;

    criadorJson.AddValue('id', ID);
    criadorJson.AddValue('descricao', Descricao);

    criadorJson.Fim;
    Result:= criadorJson.AsJsonStr;
  finally
    criadorJson.Free;
  end;
end;

procedure TCategoria.CarregarLogoTemp;
var
  resource: TResourceStream;
  nomeResource: string;
begin
  nomeResource:= 'NoImage';
  resource:= TResourceStream.Create(HInstance, nomeResource, RT_RCDATA);
  try
    Logo.LoadFromStream(resource);
  finally
    resource.Free;
  end;
end;

constructor TCategoria.Create;
var
  tamanhoX, tamanhoY: integer;
begin
  tamanhoX:= 512;
  tamanhoY:= 512;
  FDescricao:= EmptyStr;
  FLogo:= TBitmap.Create(tamanhoX, tamanhoY);
  CarregarLogoTemp;
end;

constructor TCategoria.Create(aId: integer; aDescricao, aImg64: string);
var
  tamanhoX: Integer;
  tamanhoY: Integer;
begin
  tamanhoX:= 512;
  tamanhoY:= 512;

  FDescricao:= aDescricao;
  FID:= aId;

  if aImg64.IsEmpty then
  begin
    FLogo:= TBitmap.Create(tamanhoX, tamanhoY);
    CarregarLogoTemp;
  end
  else
  begin
    FLogoStr:= aImg64;
    FLogo:= BitmapFromBase64(aImg64);
  end;
end;

destructor TCategoria.Destroy;
begin
  if Assigned(FLogo) then
    FLogo.Free;
  inherited;
end;

function TCategoria.Equals(Obj: TObject): Boolean;
begin
  result:= Assigned(obj) and
            ((obj as TCategoria).Descricao = Self.Descricao) and
            ((obj as TCategoria).ID = Self.ID);
end;

function TCategoria.VerificarSeExiste: Boolean;
var
  verificar: IVerificador;
  jsonResposta: TJSONObject;
  nomeRota: string;
  nomeCampo1: string;
  nomeCampo2: string;
begin
  verificar:= TVerificadorDados.Create();
  verificar.Ref;
  nomeRota:= 'categoria';
  nomeCampo1:= 'Descricao';
  nomeCampo2:= 'ID';

  jsonResposta:= TJSONObject.Parse(verificar.VerificarDados(ID, nomeRota));
  try
    if jsonResposta.GetValue<string>(nomeCampo1).Equals(Descricao) and
      (jsonResposta.GetValue<integer>(nomeCampo2) = ID) then
      result:= true
    else
      Result:= false;
  finally
    jsonResposta.Free;
  end;


end;

end.
