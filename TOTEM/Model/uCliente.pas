unit uCliente;

interface

uses
  {Model}
    uInterfaces,
    uComanda;

type
  TCliente = class(TInterfacedObject, ICliente)
  private
    FNome:    string;
    FContato: string;
    FID: Integer;

  public
    constructor Create;

    property ID:      Integer read FID      write FID;
    property Nome:    string  read FNome    write FNome;
    property Contato: string  read FContato write FContato;

    procedure IniciaCompra(comanda: TComanda);
    procedure FinalizarCompra(venda: IVenda);
    procedure CancelarCompra(venda: IVenda);
  end;

implementation

{ TCliente }

uses
{Model}
  uVenda;

procedure TCliente.CancelarCompra(venda: IVenda);
begin
  venda.CancelarVenda;
end;

constructor TCliente.Create;
begin
  FNome   := '';
  FContato:= '';
end;

procedure TCliente.FinalizarCompra(venda: IVenda);
begin
  venda.GravarVenda;
end;

procedure TCliente.IniciaCompra(comanda: TComanda);
begin
  { TODO : Re pensar }
end;

end.
