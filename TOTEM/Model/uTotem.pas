unit uTotem;

interface

uses
  {Model}
    uCardapio,
    uCardapio.Manager,
    uComanda,
    uInterfaces,
  {Ini}
    PersistenciaConfiguracao.Ini,
    PersistenciaConfiguracao.Interfaces,
    Factory.PersistenciaConfigucao, uEstabelecimento;

type
  TTotem = class(TInterfacedObject, ITotem, iConfiguravel)
  private
    FNome:     string;
    FIP:       string;
    FEstabelecimento: TEstabelecimento;
    procedure IniciarSistema;
    procedure GetInformacoesEstabelecimento();
  public
    constructor Create;
    destructor Destroy; override;

    procedure Salvar;
    procedure Carregar;
    function GetConfigurado: boolean;

    property Nome:                  string           read FNome;
    property IP:                    string           read FIP;
    property Estabelecimento:       TEstabelecimento read FEstabelecimento write FEstabelecimento;
  end;

implementation

{ TTotem }

uses
  {Jedi}
    JclSysInfo,
  {Util}
    Model.LibUtil,
  {Delphi}
    System.SysUtils, uRepositorio.Interfaces, System.JSON, uRepositorio.Json,
  uConfiguracoes


  ,

    System.Classes;

function CARREGARTESTE(ARQUIVO: string): string;
var
  json: TStringList;
begin
  json:= TStringList.Create();
  try
    json.LoadFromFile(ARQUIVO+'.json');
    Result:= json.Text;
  finally
    json.Free;
  end;
end;

procedure TTotem.Carregar;
begin
  IniciarSistema;
end;

constructor TTotem.Create;
begin
  FEstabelecimento:= TEstabelecimento.Create();
  FNome           := '';
  FIP             := '';
  GetInformacoesEstabelecimento();
  IniciarSistema;
  Salvar;
end;

destructor TTotem.Destroy;
begin
  FEstabelecimento.Free;
  inherited;
end;

function TTotem.GetConfigurado: boolean;
begin
  result:= not FNome.IsEmpty and
           not FIP.IsEmpty;
end;

procedure TTotem.GetInformacoesEstabelecimento;
var
  repositorio: IRepositorio<TEstabelecimento>;
  nomeRota: string;
  JsonStr: string;
  jsonResultado: TJSONObject;
  it: TJSONValue;
begin
  nomeRota:= 'estabelecimento';
  //nomeRota:= 'l6a6e';
  repositorio:= TRepositorioJson<TEstabelecimento>.Create(TConfiguracoes.GetIP);
  repositorio.Ref;

    JsonStr:=repositorio.GetJsonString(nomeRota);
 // JsonStr:= CARREGARTESTE(nomeRota);

  if JsonStr.Equals('{}') then
    exit();

  jsonResultado:= TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(JsonStr), 0) as TJSONObject;
  try
     Estabelecimento.Id:= jsonResultado.GetValue<Integer>('id');
     Estabelecimento.NomeFantasia:= jsonResultado.GetValue<string>('nomeFantasia');
     Estabelecimento.Logo64:= jsonResultado.GetValue<string>('logo');
  finally
    jsonResultado.Free;
  end;
end;

procedure TTotem.IniciarSistema;
begin
  FNome:= GetLocalComputerName;
  FIP:= getIP;
end;

procedure TTotem.Salvar;
var
  ini: iSalvaCarregaConfiguracao;
begin
  ini:= TFactoryPersistenciaConfiguracao.getPersistenciaConfiguracao(tpcIni);
  ini.salva(self);
end;

end.
