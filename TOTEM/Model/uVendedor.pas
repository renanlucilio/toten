unit uVendedor;

interface

uses
  {Model}
    uComanda,
    uVenda,
    uCliente,
  {Delphi}
    System.Generics.Collections;

type
  TVendedor = class
  private
    FComanda: TComanda;
    FVenda:   TVenda;
    FCliente: TCliente;
  public
    constructor Create;
    destructor Destroy; override;

    property Cliente: TCliente read FCliente write FCliente;
    property Comanda: TComanda read FComanda write FComanda;
    property Venda:   TVenda   read FVenda   write FVenda;
  end;

implementation

{ TVendedor }

constructor TVendedor.Create;
begin
  FComanda:= TComanda.Create();
  FVenda := TVenda.Create(FComanda);
end;

destructor TVendedor.Destroy;
begin
  FComanda.Free;
  FVenda.Free;
  inherited;
end;
end.
