unit uItem.Produto;

interface

uses
  {Resource}
    uResource.Strings,
  {Model}
    uItem,
    uItem.ItemExtra,
    uCategoria,
    uUnidadeMedida,
  {Delphi}
    System.Generics.Collections,
    System.Classes;

type
  TEvChangeItemExtra = procedure(Sender: TObject; const Item: TItemExtra; Action: TCollectionNotification) of object;

  TProduto = class(TItem)
  private
    FDetalhes:          string;
    FCategoria:         TCategoria;
    FItensExtra:        TObjectList<TItemExtra>;
    FObservacao:        TStringList;
    FUnidadeMedida: TUnidadeMedida;
  public
    constructor Create(aId: integer; aValor: Currency; aDescricao,aDetalhes: string;
      aCategoria: TCategoria; aUnidade:TUnidadeMedida); overload;
    constructor Create(); overload;
    destructor Destroy; override;

    property Detalhes:          string                  read FDetalhes          write FDetalhes;
    property Categoria:         TCategoria              read FCategoria         write FCategoria;
    property ItensExtra:        TObjectList<TItemExtra> read FItensExtra        write FItensExtra;
    property Observacao:        TStringList             read FObservacao        write FObservacao;     
    property UnidadeMedida:     TUnidadeMedida          read FUnidadeMedida     write FUnidadeMedida;


    function Equals(Obj: TObject): Boolean; override;
    function AsJson(): string;  override;
  end;

implementation

uses
  {Delphi}
    System.SysUtils,
    System.JSON,
  {Model}
    uVerificador,
    uInterfaces,
  {Helper}
    Json.Helper,
  {Criador JSON}
    uCriadorJson;

{ TProduto }

function TProduto.AsJson: string;
var
  criadorJson: TCriadorJson;
begin
  criadorJson:= TCriadorJson.Create;
  try
    criadorJson.Inicio;

    criadorJson.AddValue('id', ID);
    criadorJson.AddValue('descricao', Descricao);
    criadorJson.AddValue('valorUnitario', ValorUnitario);
    criadorJson.AddObj<TCategoria>('categoria', Categoria);
    criadorJson.AddValue('observacao', Observacao.Text);
    criadorJson.AddArray<TItemExtra>('itensExtra', ItensExtra.ToArray());

    criadorJson.Fim;
    Result:= criadorJson.AsJsonStr;
  finally
    criadorJson.Free;
  end;
end;

constructor TProduto.Create;
begin
  inherited Create();
  FItensExtra   := TObjectList<TItemExtra>.Create();
  FObservacao   := TStringList.Create();
  FUnidadeMedida:= TUnidadeMedida.Create('UN', 'Unidade', false);
end;

constructor TProduto.Create(aId: integer; aValor: Currency; aDescricao,aDetalhes: string;
  aCategoria: TCategoria;aUnidade:TUnidadeMedida);
begin
  if not Assigned(aCategoria) then
    raise Exception.Create(MsgErroCategoriaInv�lida);

  ID            := aId;
  Descricao     := aDescricao;
  FDetalhes     := aDetalhes;
  ValorUnitario := aValor;
  FCategoria    := aCategoria;
  FItensExtra   := TObjectList<TItemExtra>.Create();
  FObservacao   := TStringList.Create();
  FUnidadeMedida:= FUnidadeMedida;
end;

destructor TProduto.Destroy;
begin
  FCategoria.Free;
  FItensExtra.Clear;
  FItensExtra.Free;
  FObservacao.Free;
  inherited;
end;

function TProduto.Equals(Obj: TObject): Boolean;
begin

  result:= Assigned(obj) and
            (((obj as TProduto).Descricao = Self.Descricao) and
            ((obj as TProduto).ID = Self.ID));
end;

end.
