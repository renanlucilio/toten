unit uItem;

interface

uses
  {Model}
    uInterfaces,
  {Criador JSON}
    uCriadorJson,
    uCriadorJson.Interfaces;

type
  TItem = class(TInterfacedObject, IVerificavel, IPodeSerJson)
  private
    FDescricao:     string;
    FValorUnitario: Currency;
    FID: integer;
  public
    constructor Create;
    destructor Destroy; override;

    function VerificarSeExiste(): Boolean; virtual;

    property ID:            integer  read FID            write FID;
    property Descricao:     string   read FDescricao     write FDescricao;
    property ValorUnitario: Currency read FValorUnitario write FValorUnitario;

    function AsJson(): string; virtual; abstract;
  end;

implementation

uses
  {Model}
    uVerificador,
  {Delphi}
    System.JSON,
    System.SysUtils,
  {Helper}
    Json.Helper;

{ TItem }

constructor TItem.Create;
begin
  FDescricao    := '';
  FValorUnitario:= 0;
end;

destructor TItem.Destroy;
begin

  inherited;
end;

function TItem.VerificarSeExiste: Boolean;
var
  verificar: IVerificador;
  jsonResposta: TJSONObject;
  nomeRota: string;
  nomeCampo1: string;
  nomeCampo2: string;
begin
  verificar:= TVerificadorDados.Create();
  verificar.Ref;
  nomeRota:= 'item';
  nomeCampo1:= 'Descricao';
  nomeCampo2:= 'ID';

  jsonResposta:= TJSONObject.Parse(verificar.VerificarDados(ID, nomeRota));
  try
    if jsonResposta.GetValue<string>(nomeCampo1).Equals(Descricao) and
      (jsonResposta.GetValue<integer>(nomeCampo2) = ID) then
      result:= true
    else
      Result:= false;
  finally
    jsonResposta.Free;
  end;
end;



end.
