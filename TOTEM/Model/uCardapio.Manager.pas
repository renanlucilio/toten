﻿unit uCardapio.Manager;

interface

uses
  {Model}
    uCardapio,
    uInterfaces,
    uCategoria,
    uItem.Produto,
    uItem.Produto.Pizza,
    uItem.ItemExtra,
    uPromocao,
    uPizzaTamanho,
    uPizzaTipo,
  {Delphi}
    System.Generics.Collections,
    System.Json,
    System.SysUtils, uUnidadeMedida;

type
  TCardapioManager = class
  private
    function CarregarListaCategorias: TObjectList<TCategoria>;
    function CarregarListaProdutos(cardapio: TCardapio): TObjectList<TProduto>;
    function CarregarListaItensExtra(cardapio: TCardapio): TObjectList<TItemExtra>;
  public
    destructor Destroy; override;
    function CarregarCardapio(): TCardapio;

    function CarregarProdutosDeUmaCategoria(aCategoria: TCategoria; const cardapio: TCardapio): TObjectList<TProduto>;

  end;

implementation

uses
  {Model}
    uConfiguracoes,
  {Repositorio}
    uRepositorio.Json,
    uRepositorio.Interfaces,
  {Criador JSON}
    uCriadorJson,
  {Helper}
    Json.Helper,



    System.Classes;

{ TCardapioManager }


function TCardapioManager.CarregarCardapio: TCardapio;
begin
  result:= TCardapio.Create;

  try
    result.Categorias:= CarregarListaCategorias;

    if result.Categorias.Count <= 0 then
      raise Exception.Create('Categorias n�o foram carregadas');

    result.Produtos:= CarregarListaProdutos(result);

    result.ItensExtra:= CarregarListaItensExtra(result);

  except
    raise Exception.Create('Erro ao carregar o cardapio');
  end;
end;

function TCardapioManager.CarregarListaCategorias: TObjectList<TCategoria>;
var
  repositorio: IRepositorio<TCategoria>;
  nomeRota: string;
  JsonStr: string;
  jsonResultado: TJSONArray;
  it: TJSONValue;
begin
  result:= TObjectList<TCategoria>.Create();
  nomeRota:= 'categorias';
  repositorio:= TRepositorioJson<TCategoria>.Create(TConfiguracoes.GetIP);
  repositorio.Ref;

  JsonStr:=repositorio.GetJsonString(nomeRota);
  if JsonStr.Equals('{}') then
    exit(result);


  jsonResultado:= TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(JsonStr), 0) as TJSONArray;
  try
    for it in jsonResultado.AsArray do
    begin
      Result.Add(TCategoria.Create(
        it.GetValue<integer>('id'),
        it.GetValue<string>('descricao'),
        it.GetValue<string>('logo')
      ));
    end;
  finally
    jsonResultado.Free;
  end;
end;

function TCardapioManager.CarregarListaItensExtra(cardapio: TCardapio): TObjectList<TItemExtra>;
var
  repositorio: IRepositorio<TItemExtra>;
  nomeRota: string;
  JsonStr: string;
  jsonResultado: TJSONArray;
  it: TJSONValue;
  itemExtra: TItemExtra;
begin
  { TODO : REFATORAR ESSES M�TODOS DE REQUISI��O }
  result:= TObjectList<TItemExtra>.Create();

  nomeRota:= 'itensExtra';
  repositorio:= TRepositorioJson<TItemExtra>.Create(TConfiguracoes.GetIP);
  repositorio.Ref;

  JsonStr:=repositorio.GetJsonString(nomeRota);


  if JsonStr.Equals('{}') then
    exit(result);

  jsonResultado:= TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(JsonStr), 0) as TJSONArray;
  try
    for it in jsonResultado.AsArray do
    begin
      itemExtra:= TItemExtra.Create(
        it.GetValue<integer>('id'),
        it.GetValue<string>('descricao'),
        it.GetValue<Currency>('valorUnitario')
      );

      Result.Add(itemExtra);
    end;
  finally
    jsonResultado.Free;
  end;
end;

function TCardapioManager.CarregarListaProdutos(cardapio: TCardapio): TObjectList<TProduto>;
var
  repositorio: IRepositorio<TProduto>;
  nomeRota: string;
  JsonStr: string;
  jsonResultado: TJSONArray;
  it: TJSONValue;
  categoriaTemp: TCategoria;
  produtoTemp: TProduto;
  unidadeMedida: TUnidadeMedida;
begin
  result:= TObjectList<TProduto>.Create();

  nomeRota:= 'produtos';
  repositorio:= TRepositorioJson<TProduto>.Create(TConfiguracoes.GetIP);
  repositorio.Ref;

  JsonStr:=repositorio.GetJsonString(nomeRota);

  if JsonStr.Equals('{}') then
    exit(result);

  jsonResultado:= TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(JsonStr), 0) as TJSONArray;
  try
    for it in jsonResultado.AsArray do
    begin
      categoriaTemp:= cardapio.GetCategoria(it.GetValue<integer>('categoria'));

      if not Assigned(categoriaTemp) then
        Continue;

      if LowerCase(categoriaTemp.Descricao).Contains('pizza') then
      begin

        produtoTemp:= TPizza.Create(
          it.GetValue<integer>('id'),
          it.GetValue<Currency>('valorUnitario'),
          it.GetValue<string>('descricao'),
          it.GetValue<string>('detalhes'),
          categoriaTemp
        );

      end
      else
      begin
        unidadeMedida:= TUnidadeMedida.Create(
          it.GetValue<string>('siglaUnidade', 'UN'),
          it.GetValue<string>('descricaoUnidade', 'Unidade'),
          it.GetValue<Boolean>('temCasaDecimal', false)
        );

        produtoTemp:= TProduto.Create(
          it.GetValue<integer>('id'),
          it.GetValue<Currency>('valorUnitario'),
          it.GetValue<string>('descricao'),
          it.GetValue<string>('detalhes'),
          categoriaTemp,
          unidadeMedida
        );
      end;

      Result.Add(produtoTemp);
    end;
  finally
    jsonResultado.Free;
  end;
end;

function TCardapioManager.CarregarProdutosDeUmaCategoria(aCategoria: TCategoria; const cardapio: TCardapio): TObjectList<TProduto>;
var
  it: TProduto;
  produto: TProduto;
begin
  Result:= TObjectList<TProduto>.Create;
  for it in cardapio.Produtos do
  begin
    if it.Categoria.Equals(aCategoria) then
    begin
      produto:= cardapio.GetProduto(it.ID);

      if not Assigned(produto) then
        raise Exception.Create('Produto n�o foi localizado');

      Result.Add(produto);
    end;
  end;
end;

destructor TCardapioManager.Destroy;
begin

  inherited;
end;

end.
