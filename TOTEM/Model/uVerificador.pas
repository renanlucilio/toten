unit uVerificador;

interface

uses
  {Model}
    uInterfaces;

type
  TVerificadorDados = class(TInterfacedObject, IVerificador)
  private
  public
    constructor Create();
    destructor Destroy; override;

    function VerificarDados(const id: integer; const nameClass: string): string;
    function Ref:IVerificador;
  end;

implementation
uses
  {Repositorio}
    uRepositorio.Json,
    uRepositorio.Interfaces,
  {Criador JSON}
    uCriadorJson,
  {Model}
    uConfiguracoes,
  {Delphi}
    System.SysUtils;

{ TVerificadorDados }

constructor TVerificadorDados.Create();
begin
end;

destructor TVerificadorDados.Destroy;
begin

  inherited;
end;

function TVerificadorDados.Ref: IVerificador;
begin
  result:= self;
end;

function TVerificadorDados.VerificarDados(const id: integer; const nameClass: string): string;
var
  repositorio: IRepositorio<TObject>;
  json: string;
begin
  result:= '';
  repositorio:= TRepositorioJson<TObject>.Create(TConfiguracoes.GetIP);
  repositorio.Ref;

//  json:= TCriadorJson.New
//    .Inicio
//      .AddValue('Tipo', nameClass)
//      .AddValue('ID', id)
//    .Fim
//    .AsJsonStr;

  if repositorio.GetDisponivel then
  begin
    result:= repositorio.GetDadoSimples('dados', json).AsString;
  end;

end;

end.
