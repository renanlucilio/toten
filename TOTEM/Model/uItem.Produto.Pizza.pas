unit uItem.Produto.Pizza;

interface

uses
  {Model}
    uItem.Produto,
    uPizzaTamanho,
    uInterfaces,
    uCategoria,
    uUnidadeMedida, uItem.ItemExtra;

type
  TPizza = class(TProduto)
  private
    FTamanhoPizza: TPizzaTamanho;
    FValorPizza: Currency;
  public
    constructor Create(aId: integer; aValor: Currency; aDescricao,aDetalhes: string;aCategoria: TCategoria);
    destructor Destroy; override;

    procedure DefinirTamanhoPizza();

    property ValorPizza:   Currency read FValorPizza write FValorPizza;
    property TamanhoPizza: TPizzaTamanho read FTamanhoPizza write FTamanhoPizza;
    function AsJson(): string; override;
  end;

implementation

uses
  {Criador JSON}
    uCriadorJson;

{ TPizza }

function TPizza.AsJson: string;
var
  criadorJson: TCriadorJson;
begin
  criadorJson:= TCriadorJson.Create;
  try
    criadorJson.Inicio;

    criadorJson.AddValue('id', ID);
    criadorJson.AddValue('descricao', Descricao);
    criadorJson.AddValue('valoPizza', ValorPizza);
    criadorJson.AddObj<TCategoria>('categoria', Categoria);
    criadorJson.AddObj<TPizzaTamanho>('TamanhoPizza', TamanhoPizza);
    criadorJson.AddValue('observacao', Observacao.Text);
    criadorJson.AddArray<TItemExtra>('itensExtra', ItensExtra.ToArray());

    criadorJson.Fim;
    Result:= criadorJson.AsJsonStr;
  finally
    criadorJson.Free;
  end;
end;

constructor TPizza.Create(aId: integer; aValor: Currency; aDescricao,aDetalhes: string;aCategoria: TCategoria);
begin
  inherited Create(aId, aValor, aDescricao, aDetalhes, aCategoria, TUnidadeMedida.Create('UN', 'Unidade', false));
  FValorPizza:= ValorUnitario;
end;

procedure TPizza.DefinirTamanhoPizza;
begin
  TamanhoPizza.DefiniTamanho(ValorUnitario);
  ValorPizza:= TamanhoPizza.GetValorPizza;
end;

destructor TPizza.Destroy;
begin
  inherited Destroy;
end;


end.
