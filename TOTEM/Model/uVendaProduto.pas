unit uVendaProduto;

interface

uses
  {Model}
    uItem.Produto,
    uItem.Produto.Pizza,
    uItem.ItemExtra,
    uInterfaces,
    uPizzaTipo,
    uPromocao,
    uItem.Produto.Combinado,
  {Delphi}
    System.Classes,
  {Criador JSON}
    uCriadorJson,
    uCriadorJson.Interfaces;

type
  TVendaProduto = class(TInterfacedObject, IValorDefinivel, IPodeSerJson)
  private
    FQuantidade:        double;
    FValorAtualProduto: Currency;
    FProduto:           TProduto;
    FValorProduto:      Currency;
    FDescricao:         string;
    FDetalhes:          string;
    FID:                string;
    procedure DefinirValor();
    procedure DefinirDetalhesProduto();
    procedure DefinirDescricaoProduto();
  public
    constructor Create(aProduto: TProduto; aQuantidade:double);
    destructor Destroy; override;

    property Quantidade:        double      read FQuantidade write FQuantidade;
    property ValorAtualProduto: Currency    read FValorAtualProduto;
    property Produto:           TProduto    read FProduto;
    property ID:                string      read FID;

    function GetValor:  Currency;
    function GetDescricaoProduto: string;
    function GetDetalhesProduto: string;

    procedure ColocarPromocao(promocao: TPromocao);
    function AsJson(): string;
  end;

implementation

uses
  {System}
    System.SysUtils,
  {Resource}
    uResource.Strings,
  {LibUtil}
    uLibUtil;

{ TVendaProduto }

function TVendaProduto.AsJson: string;
var
  criadorJson: TCriadorJson;
begin
  criadorJson:= TCriadorJson.Create;
  try
    criadorJson.Inicio;

    criadorJson.AddValue('quantidade', Quantidade);
    criadorJson.AddObj<TProduto>('produtos', Produto);

    criadorJson.Fim;
    Result:= criadorJson.AsJsonStr;
  finally
    criadorJson.Free;
  end;
end;

procedure TVendaProduto.ColocarPromocao(promocao: TPromocao);
begin

end;

constructor TVendaProduto.Create(aProduto: TProduto; aQuantidade:double);
begin
  if not Assigned(aProduto) then
    raise Exception.Create(MsgErroProdutoNulo);

  FProduto          := aProduto;
  FQuantidade       := aQuantidade;
  FValorAtualProduto:= Produto.ValorUnitario;
  FValorProduto     := 0;
  FID               := GetIdUnique;
end;

procedure TVendaProduto.DefinirDescricaoProduto;
var
  casasASeremEleminadas: integer;
  descricao: string;
begin
  casasASeremEleminadas:= 30;     
  casasASeremEleminadas :=casasASeremEleminadas - (Length(FloatToStr(Quantidade)) + 5);

  if Assigned(Produto) and Assigned(Produto.Categoria) then
  begin
    descricao:= Produto.Descricao;
    if descricao.Length > casasASeremEleminadas then
      begin
        FDescricao:= descricao.Remove(casasASeremEleminadas);
        FDescricao:= FDescricao+ '...';
        FDescricao:= Concat(FDescricao,' x', Quantidade.ToString);
      end
      else
        FDescricao:= Concat(Produto.Descricao,' x', Quantidade.ToString);
  end
  else
    FDescricao:= '';
end;

procedure TVendaProduto.DefinirDetalhesProduto;
var
  it: TItemExtra;
  novaLinha: string;
  captionItemExtra: string;
  captionObervacao: string;
  detalhesProdutos: TStringList;
  it2: string;
begin
  detalhesProdutos:= TStringList.Create();
  try
    detalhesProdutos.Clear;
    captionItemExtra:= 'Item(ns) Extra: ';
    captionObervacao:= 'Obeservação: ';
    for it in Produto.ItensExtra do
    begin
      if Produto.ItensExtra.First = it then
      begin
        novaLinha:=Concat(captionItemExtra, it.Descricao);
        detalhesProdutos.Add(novaLinha)
      end
      else
      begin
        novaLinha:= Concat(detalhesProdutos[0], ', ', it.Descricao);
        detalhesProdutos[0]:= novaLinha;
      end;
    end;

    if detalhesProdutos.Count > 0 then
    begin
      if detalhesProdutos[0].Length > 60 then
      begin
        detalhesProdutos[0]:= detalhesProdutos[0].Remove(60);
        detalhesProdutos[0]:= detalhesProdutos[0]+ '...';
      end;
    end;

    for it2 in Produto.Observacao do
    begin
      if Produto.Observacao.IndexOf(it2) = 0 then
      begin
        novaLinha:= Concat(captionObervacao, it2);
        detalhesProdutos.Add(novaLinha)
      end
      else
      begin
        novaLinha:= Concat(detalhesProdutos[1], ', ', it2);
        detalhesProdutos[1] := novaLinha;
      end;
    end;

    if detalhesProdutos.Count > 1 then
    begin
      if detalhesProdutos[1].Length > 60 then
      begin
        detalhesProdutos[1]:= detalhesProdutos[1].Remove(60);
        detalhesProdutos[1]:= detalhesProdutos[1]+ '...';
      end;
    end;

    FDetalhes:= detalhesProdutos.Text;
  finally
    detalhesProdutos.Free;
  end;
end;

procedure TVendaProduto.DefinirValor;
var
  valorTotalItensExtra: Currency;
  it: TItemExtra;
begin
  FValorProduto:= 0;
  valorTotalItensExtra:= 0;
  FValorProduto:= Produto.ValorUnitario;
  for it in Produto.ItensExtra do
  begin
    valorTotalItensExtra:= valorTotalItensExtra + it.ValorUnitario;
  end;
  FValorProduto:= FValorProduto + valorTotalItensExtra;

  FValorProduto:= (FValorProduto*Quantidade);

  FValorProduto:= FValorProduto;
end;

destructor TVendaProduto.Destroy;
begin

//  if Produto is TProdutoCombinado then
//   TProdutoCombinado(Produto).Free
//  else
    Produto.Free;

  inherited;
end;

function TVendaProduto.GetDescricaoProduto: string;
begin
  DefinirDescricaoProduto();
  result:= FDescricao;
end;

function TVendaProduto.GetDetalhesProduto: string;
begin
  DefinirDetalhesProduto();
  result:= FDetalhes;
end;

function TVendaProduto.GetValor: Currency;
begin
  DefinirValor;
  result:= FValorProduto;
end;

end.
