unit uConfiguracoes;

interface

type
  TConfiguracoes = class
  private
    FIPServidor: string;
    procedure Carregar;
  public
    constructor Create;
    destructor Destroy; override;

    class function GetIP: string;
    property IPServidor: string read FIPServidor;
  end;

implementation

uses
  {Delphi}
    System.IniFiles,
    System.SysUtils;

{ TConfiguracoes }

procedure TConfiguracoes.Carregar;
var
  arquivoIni: TIniFile;
  nomeArquivo: string;
begin
  nomeArquivo:= ExtractFilePath(ParamStr(0)) + 'TotemServ.ini';

  if FileExists(nomeArquivo) then
  begin
    arquivoIni:= TIniFile.Create(nomeArquivo);
    try
      FIPServidor:= arquivoIni.ReadString('Serv', 'IP', '');
    finally
      arquivoIni.Free;
    end;
  end;
end;

constructor TConfiguracoes.Create;
begin
  FIPServidor:= '';
  Carregar;
end;

destructor TConfiguracoes.Destroy;
begin

  inherited;
end;

class function TConfiguracoes.GetIP: string;
var
  configuracoes: TConfiguracoes;
begin
  configuracoes:= TConfiguracoes.Create;
  try
    Result:= configuracoes.IPServidor;
  finally
    configuracoes.Free;
  end;
end;

end.
