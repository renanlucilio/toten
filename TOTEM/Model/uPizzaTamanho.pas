unit uPizzaTamanho;

interface

uses
  {Model}
    uInterfaces,
  {Criador JSON}
    uCriadorJson,
    uCriadorJson.Interfaces;

type
  TTipoCobrancaPorTamanho = (tctValorFixo, tctPorcentagem, tctValorMM);
  TTamanhoPizza = (tpBroto, tpMedia, tpGigante, tpNormal);

  TPizzaTamanho = class(TInterfacedObject, IValorDefinivel, IPodeSerJson)
  private
    FValorPizza:         Currency;
    FTamanhoPIzza:       TTamanhoPizza;
    FTipoCobranca:       TTipoCobrancaPorTamanho;
    FPorcentagemMedia:   Double;
    FValorBroto:         Currency;
    FPorcentagemBroto:   Double;
    FPorcentagemGigante: Double;
    FValorMedia:         Currency;
    FValorGigante:       Currency;
    FValorFixoMBroto:    Currency;
    FValorFixoMMedia:    Currency;
    FValorFixoMGigante:  Currency;


    procedure DefinirValor;
    procedure CalcularValor(porcentagem: double);
    procedure GetInformacoesTamanhoPizza();
  public

    constructor Create();
    destructor Destroy; override;

    procedure DefiniTamanho(aValorNormal: Currency);

    function GetValorPizza: Currency;

    property TamanhoPizza:       TTamanhoPizza           read FTamanhoPizza   write FTamanhoPizza;
    property ValorBroto:         Currency                read FValorBroto;
    property ValorMedia:         Currency                read FValorMedia;
    property ValorGigante:       Currency                read FValorGigante;
    property ValorFixoMBroto:    Currency                read FValorFixoMBroto;
    property ValorFixoMMedia:    Currency                read FValorFixoMMedia;
    property ValorFixoMGigante:  Currency                read FValorFixoMGigante;
    property PorcentagemBroto:   Double                  read FPorcentagemBroto;
    property PorcentagemMedia:   Double                  read FPorcentagemMedia;
    property PorcentagemGigante: Double                  read FPorcentagemGigante;
    property TipoCobranca:       TTipoCobrancaPorTamanho read FTipoCobranca;

    function AsJson(): string;
  end;

implementation

uses
  {Model}
    uConfiguracoes,
  {Repositorio}
    uRepositorio.Json,
    uRepositorio.Interfaces,
  {Delphi}
    System.Json,
    System.SysUtils,
    TypInfo,
  {Helper}
    Json.Helper, System.Classes;

{ TPizzaTamanho }


function TPizzaTamanho.AsJson: string;
var
  criadorJson: TCriadorJson;
begin
  criadorJson:= TCriadorJson.Create;
  try
    criadorJson.Inicio;

    criadorJson.AddValue('Tamanho', GetEnumName(TypeInfo(TTamanhoPizza),integer(TamanhoPizza)));

    criadorJson.Fim;
    Result:= criadorJson.AsJsonStr;
  finally
    criadorJson.Free;
  end;
end;

procedure TPizzaTamanho.CalcularValor(porcentagem: double);
var
  valorPorcentagem: Currency;
begin
  case TipoCobranca of
    tctPorcentagem:
    begin
      valorPorcentagem:= (porcentagem*FValorPizza) / 100;

      if TamanhoPizza = tpGigante then
        FValorPizza:= FValorPizza + valorPorcentagem
      else
        FValorPizza:= FValorPizza - valorPorcentagem;
    end;
    tctValorMM:
    begin
      case TamanhoPizza of
        tpBroto: FValorPizza:=  FValorPizza - ValorFixoMBroto;
        tpMedia: FValorPizza:= FValorPizza - ValorFixoMMedia;
        tpGigante: FValorPizza:= FValorPizza + PorcentagemGigante;
      end;

     if FValorPizza <= 0 then
      FValorPizza:= 0;
    end;
  end;

end;

constructor TPizzaTamanho.Create;
begin
  FTamanhoPizza  := tpNormal;
  FTipoCobranca  := tctValorFixo;
  GetInformacoesTamanhoPizza();
end;

procedure TPizzaTamanho.DefinirValor;
begin
  case TamanhoPizza of
    tpBroto:
    begin
      if TipoCobranca = tctValorFixo then
        FValorPizza:= ValorBroto
      else
        CalcularValor(PorcentagemBroto);
    end;
    tpMedia:
    begin
      if TipoCobranca = tctValorFixo then
        FValorPizza:= ValorMedia
      else
        CalcularValor(PorcentagemMedia);
    end;
    tpGigante:
    begin
      if TipoCobranca = tctValorFixo then
        FValorPizza:= ValorGigante
      else
        CalcularValor(PorcentagemGigante);
    end;
    tpNormal: FValorPizza:= FValorPizza;
  end;
end;

procedure TPizzaTamanho.DefiniTamanho(aValorNormal: Currency);
begin
  FValorPizza:= aValorNormal;
end;

destructor TPizzaTamanho.Destroy;
begin

  inherited;
end;

procedure TPizzaTamanho.GetInformacoesTamanhoPizza;
var
  repositorio: IRepositorio<TObject>;
  nomeRota: string;
  JsonStr: string;
  tipoStr: string;
  jsonResultado: TJSONObject;
begin
  nomeRota:= 'pizzaTamanho';

  repositorio:= TRepositorioJson<TObject>.Create(TConfiguracoes.GetIP);
  repositorio.Ref;

  JsonStr:=repositorio.GetJsonString(nomeRota);

  if not JsonStr.Equals('{}') then
  begin
    jsonResultado:= TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(JsonStr), 0) as TJSONObject;
    try
      FValorBroto        := jsonResultado.GetValue<Currency>('valorBroto', 0);
      FValorMedia        := jsonResultado.GetValue<Currency>('valorMedia', 0);
      FValorGigante      := jsonResultado.GetValue<Currency>('valorGigante', 0);
      FValorFixoMBroto   := jsonResultado.GetValue<Currency>('valorFixoMBroto', 0);
      FValorFixoMMedia   := jsonResultado.GetValue<Currency>('valorFixoMMedia', 0);
      FValorFixoMGigante := jsonResultado.GetValue<Currency>('valorFixoMGigante', 0);
      FPorcentagemMedia  := jsonResultado.GetValue<Double>('porcentagemMedia', 0);
      FPorcentagemBroto  := jsonResultado.GetValue<Double>('porcentagemBroto', 0);
      FPorcentagemGigante:= jsonResultado.GetValue<Double>('porcentagemGigante', 0);
      tipoStr            := jsonResultado.GetValue<string>('tipoCobranca', '');

      if tipoStr.Equals('valorFixo') then
        FTipoCobranca:= tctValorFixo
      else if tipoStr.Equals('porcentagem') then
        FTipoCobranca:= tctPorcentagem
      else if tipoStr.Equals('valorMM') then
        FTipoCobranca:= tctValorMM;

    finally
      jsonResultado.Free;
    end
  end;
end;

function TPizzaTamanho.GetValorPizza: Currency;
begin
  DefinirValor;
  result:= FValorPizza;
end;


end.
