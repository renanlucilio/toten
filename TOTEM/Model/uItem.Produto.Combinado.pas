unit uItem.Produto.Combinado;

interface

uses
  {Delphi}
    System.Generics.Collections,
    System.SysUtils,
  {Model}
    uItem.Produto,
    uItem.ItemExtra,
    uItem.Produto.Pizza,
    uPizzaTamanho,
    uCategoria;

type
  TProdutoCombinado = class(TProduto)
  private
    Fprodutos: TObjectList<TProduto>;
    function GetDescricao: string;
    procedure CriarProduto(aValorUnitario: Currency);
  public
    constructor Create(aCategoria: TCategoria ;aProdutos: TArray<TProduto>; aValorUnitario: Currency);
    destructor Destroy; override;

    property Produtos: TObjectList<TProduto> read Fprodutos write Fprodutos;
    function Equals(Obj: TObject): Boolean; override;
    function AsJson: string; override;
  end;

implementation

uses
  uCriadorJson;

{ TProdutoCombinado }


function TProdutoCombinado.AsJson: string;
var
  criadorJson: TCriadorJson;
begin
  criadorJson:= TCriadorJson.Create;
  try
    criadorJson.Inicio;

    criadorJson.AddValue('id', ID);
    criadorJson.AddValue('descricao', Descricao);
    criadorJson.AddValue('valorUnitario', ValorUnitario);
    criadorJson.AddObj<TCategoria>('categoria', Categoria);
    criadorJson.AddArray<TProduto>('produtos', Produtos.ToArray);
    criadorJson.AddValue('observacao', Observacao.Text);
    criadorJson.AddArray<TItemExtra>('itensExtra', ItensExtra.ToArray());

    criadorJson.Fim;
    Result:= criadorJson.AsJsonStr;
  finally
    criadorJson.Free;
  end;
end;

constructor TProdutoCombinado.Create(aCategoria: TCategoria ;aProdutos: TArray<TProduto>; aValorUnitario: Currency);
begin
  inherited Create;
  FProdutos:= TObjectList<TProduto>.Create;
  Fprodutos.AddRange(aProdutos);
  Categoria:= aCategoria;
  CriarProduto(aValorUnitario);
end;

procedure TProdutoCombinado.CriarProduto(aValorUnitario: Currency);
begin
  Descricao:= GetDescricao;
  ID       := Produtos.First.ID;
  Detalhes := '';
  ValorUnitario:= aValorUnitario;
end;

destructor TProdutoCombinado.Destroy;
begin

  Fprodutos.Clear;
  Fprodutos.Free;
  inherited Destroy;
end;

function TProdutoCombinado.Equals(Obj: TObject): Boolean;
begin
  result:= Assigned(obj) and
            ((obj as TProduto).Descricao = Self.Descricao);
end;

function TProdutoCombinado.GetDescricao: string;
var
  it: TProduto;
  resultTemp: string;
begin
  resultTemp:= '';
  for it in Produtos do
  begin
    resultTemp:= resultTemp + it.Descricao + ', ';
  end;

  if resultTemp.EndsWith(', ') then
   resultTemp:=resultTemp.Remove(pred(Length(resultTemp)-1), 2);

  if Produtos.First is TPizza then
  case TPizza(Produtos.First).TamanhoPizza.TamanhoPizza of
    tpBroto: resultTemp:= 'Broto ' + resultTemp;
    tpMedia: resultTemp:= 'M�dia ' + resultTemp;
    tpGigante: resultTemp:= 'Gigante ' + resultTemp;
  end;

  Result:= resultTemp;
end;

end.
