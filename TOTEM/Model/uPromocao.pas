unit uPromocao;

interface

uses
  {Model}
    uItem.Produto,
    uCategoria,
    uInterfaces,
    uConfiguracoes;

type
  TOndeSeraAplicadoPromocao = (SobreVenda, SobreOsItens);
  TTipoPromocao = (tpCategoria, tpProduto);

  TPromocao = class(TInterfacedObject, IPromocao)
  private
    FProduto:        TProduto;
    FValorPromocao:  Currency;
    FCategoria:      TCategoria;
    FAtiva:          Boolean;
    FTipoPromocao:   TTipoPromocao;
    FResumoPromocao: string;
    FID: Integer;
    FCupomDesconto: string;
    FOndeSeraPromocao: TOndeSeraAplicadoPromocao;


  public
    constructor Create(aProduto: TProduto); overload;
    constructor Create(aCategoria: TCategoria); overload;
    constructor Create; overload;
    destructor Destroy; override;

    property ID:               Integer                   read FID               write FID;
    property ResumoPromocao:   string                    read FResumoPromocao   write FResumoPromocao;
    property TipoPromocao:     TTipoPromocao             read FTipoPromocao;
    property Produto:          TProduto                  read FProduto;
    property ValorPromocao:    Currency                  read FValorPromocao    write FValorPromocao;
    property Categoria:        TCategoria                read FCategoria;
    property Ativa:            Boolean                   read FAtiva;
    property CupomDesconto:    string                    read FCupomDesconto;
    property OndeSeraPromocao: TOndeSeraAplicadoPromocao read FOndeSeraPromocao write FOndeSeraPromocao;

    procedure VerificarPromocao();
    procedure VerificarCupomDesconto(aCupom: string);
  end;

implementation

uses
  {Repositorio}
    uRepositorio.Json,
    uRepositorio.Interfaces,
  {Criador JSON}
    uCriadorJson,
  {Delphi}
    System.Json,
  {Helper}
    Json.Helper;

{ TPromocao }

constructor TPromocao.Create(aProduto: TProduto);
begin
  FProduto         := aProduto;
  FTipoPromocao    := tpProduto;
  FValorPromocao   := 0;
  FCategoria       := nil;
  FAtiva           := false;
  FOndeSeraPromocao:= SobreOsItens;
  FCupomDesconto   := '';
  FResumoPromocao  := '';
  FID              := 0;
end;

constructor TPromocao.Create(aCategoria: TCategoria);
begin
  FCategoria       := aCategoria;
  FTipoPromocao    := tpCategoria;
  FOndeSeraPromocao:= SobreOsItens;
  FValorPromocao   := 0;
  FAtiva           := false;
  FOndeSeraPromocao:= SobreOsItens;
  FProduto         := nil;
  FCupomDesconto   := '';
  FResumoPromocao  := '';
  FID              := 0;
end;

constructor TPromocao.Create;
begin
  FCategoria       := nil;
  FTipoPromocao    := tpCategoria;
  FOndeSeraPromocao:= SobreVenda;
  FValorPromocao   := 0;
  FAtiva           := false;
  FOndeSeraPromocao:= SobreOsItens;
  FProduto         := nil;
  FCupomDesconto   := '';
  FResumoPromocao  := '';
  FID              := 0;
end;

destructor TPromocao.Destroy;
begin
  if Assigned(FCategoria) then
    FCategoria.Free;

  if Assigned(FProduto) then
    FProduto.Free;

  inherited;
end;

procedure TPromocao.VerificarCupomDesconto(aCupom: string);
var
  repositorio: IRepositorio<TPromocao>;
  json: string;
  campo1: string;
  campo2: string;
  nomeRota: string;
  resultadoJson: TJSONObject;
begin
  resultadoJson:= nil;
  campo1:= 'ID';
  campo2:= 'cupom';
  nomeRota:= 'verificarCupom';

  repositorio:= TRepositorioJson<TPromocao>.Create(TConfiguracoes.GetIP);
  repositorio.Ref;

//  json := TCriadorJson.New
//    .Inicio
//      .AddValue(campo1, ID)
//      .AddValue(campo2, aCupom)
//    .Fim
//    .AsJsonStr();

  resultadoJson.Parse(repositorio.GetDadoSimples(nomeRota, json).AsString);

  FAtiva:= resultadoJson.GetValue<Boolean>('ativa', false);

  if FAtiva then
    FCupomDesconto:= aCupom
  else
    FCupomDesconto:= '';
end;

procedure TPromocao.VerificarPromocao;
var
  repositorio: IRepositorio<TPromocao>;
  json: string;
  campo1: string;
  campo2: string;
  campo3: string;
  nomeRota: string;
  resultadoJson: TJSONObject;
begin
  resultadoJson:= nil;
  campo1:= 'ID';
  campo2:= 'ResumoPromocao';
  campo3:= 'ValorPromocao';
  nomeRota:= 'verificarPromocao';

  repositorio:= TRepositorioJson<TPromocao>.Create(TConfiguracoes.GetIP);
  repositorio.Ref;
//
//  json := TCriadorJson.New
//    .Inicio
//      .AddValue(campo1, ID)
//      .AddValue(campo2, ResumoPromocao)
//      .AddValue(campo3, ValorPromocao)
//    .Fim
//    .AsJsonStr();

  resultadoJson.Parse(repositorio.GetDadoSimples(nomeRota, json).AsString);

  FAtiva:= resultadoJson.GetValue<Boolean>('ativa', false);
end;

end.
