unit uCardapio;

interface

uses
  {Delphi}
    System.Generics.Collections,
  {Model}
    uItem.ItemExtra,
    uItem.Produto,
    uCategoria,
    uPromocao,
    uItem.Produto.Pizza,
    uItem.Produto.Combinado,
    uPizzaTipo,
    uPizzaTamanho;

type
  TCardapio = class
  private
    FCategorias: TObjectList<TCategoria>;
    FProdutos:   TObjectList<TProduto>;
    FPromocoes:  TObjectList<TPromocao>;
    FItensExtra: TObjectList<TItemExtra>;
    FPizzaCombinada: TPizzaTipo;
    FTamanhoPizza: TPizzaTamanho;

    procedure SetTamanhoPizza();
    function GetProdutos: TObjectList<TProduto>;
    procedure SetProdutos(const Value: TObjectList<TProduto>);
  public
    constructor Create();
    destructor Destroy; override;

    function GetCategoria(id: integer):TCategoria;
    function GetProduto(id: integer): TProduto;
    function GetItemExtra(id: integer): TItemExtra;

    function CriarPizzaCombinada: TProdutoCombinado;


    property Categorias:     TObjectList<TCategoria> read FCategorias     write FCategorias;
    property Produtos:       TObjectList<TProduto>   read GetProdutos     write SetProdutos;
    property Promocoes:      TObjectList<TPromocao>  read FPromocoes      write FPromocoes;
    property ItensExtra:     TObjectList<TItemExtra> read FItensExtra     write FItensExtra;
    property PizzaCombinada: TPizzaTipo              read FPizzaCombinada write FPizzaCombinada;
  end;

implementation

uses
  System.SysUtils;

{ TCardapio }

constructor TCardapio.Create();
begin
  FPizzaCombinada:= TPizzaTipo.Create;
  FTamanhoPizza:= TPizzaTamanho.Create();
end;


function TCardapio.CriarPizzaCombinada: TProdutoCombinado;
var
 categoria: TCategoria;
 it, produto: TProduto;
 produtos: TList<TProduto>;
begin
  if PizzaCombinada.Pizzas.Count > 0 then
  begin
    categoria:= GetCategoria(PizzaCombinada.Pizzas.First.Categoria.ID);

    if not Assigned(categoria) then
      raise Exception.Create('Categoria n�o localizada');

    produtos:= TList<TProduto>.Create();
    try
      for it in PizzaCombinada.Pizzas do
      begin
        produto:= GetProduto(it.ID); 
        produtos.Add(produto); 
      end;
      
      Result:= TProdutoCombinado.Create(
        categoria ,
        produtos.ToArray,
        PizzaCombinada.GetValorPizza);
    finally
      PizzaCombinada.LimparPizzaCombinada();
      produtos.Free;
    end;
  end
  else
    result:= nil;
end;

destructor TCardapio.Destroy;
begin
  if Assigned(FCategorias) then
  begin
    FCategorias.Clear;
    FCategorias.Free;
  end;

  if Assigned(FProdutos) then
  begin
    FProdutos.Clear;
    FProdutos.Free;
  end;

  if Assigned(FPromocoes) then
  begin
    FPromocoes.Clear;
    FPromocoes.Free;
  end;

  if Assigned(FItensExtra) then
  begin
    FItensExtra.Clear;
    FItensExtra.Free;
  end;

  FPizzaCombinada.Free;
  FTamanhoPizza.Free;
  inherited;
end;

function TCardapio.GetCategoria(id: integer): TCategoria;
var
  it: TCategoria;
  categoria: TCategoria;
begin
  categoria:= nil;
  for it in Categorias do
  begin
    if it.ID = id then
    begin
     categoria := it;
     break;
    end;
  end;

  if Assigned(categoria) then
  begin
    Result:= TCategoria.Create(
      categoria.ID,
      categoria.Descricao,
      categoria.LogoStr);
  end
  else
    Result:= nil;
end;

function TCardapio.GetItemExtra(id: integer): TItemExtra;
var
  it, itemExtra: TItemExtra;
begin
  itemExtra:= nil;
  for it in ItensExtra do
  begin
    if it.ID = id then
    begin
     itemExtra := it;
     break;
    end;
  end;

  if Assigned(itemExtra) then
  begin
    Result:= TItemExtra.Create(
      itemExtra.ID,
      itemExtra.Descricao,
      itemExtra.ValorUnitario);
  end
  else
    Result:= nil;
end;

function TCardapio.GetProduto(id: integer): TProduto;
var
  it, produto: TProduto;
  categoria: TCategoria;
begin
  Result:= nil;
  produto:= nil;
  for it in Produtos do
  begin
    if it.ID = id then
    begin
     produto := it;
     break;
    end;
  end;

  if Assigned(produto) then
  begin
    categoria:= GetCategoria(produto.Categoria.ID);

    if  not Assigned(categoria) then
      raise Exception.Create('Categoria n�o foi localizada');

    if produto is TPizza then
    begin
      Result:= TPizza.Create(
        produto.ID,
        produto.ValorUnitario,
        produto.Descricao,
        produto.Detalhes,
        categoria
      );
      TPizza(Result).TamanhoPizza:= FTamanhoPizza;
      exit(result);
    end;

    if produto is TProduto then
    begin
      Result:= TProduto.Create(
        produto.ID,
        produto.ValorUnitario,
        produto.Descricao,
        produto.Detalhes,
        categoria,
        produto.UnidadeMedida
      );

      exit(result);
    end;
  end
  else
    Result:= nil;
end;

function TCardapio.GetProdutos: TObjectList<TProduto>;
begin
  result:= FProdutos;
end;

procedure TCardapio.SetTamanhoPizza;
var
  it: TProduto;
begin
  for it in Produtos do
  begin
    if it is TPizza then
      TPizza(it).TamanhoPizza:= FTamanhoPizza;
  end;
end;

procedure TCardapio.SetProdutos(const Value: TObjectList<TProduto>);
begin
  FProdutos:= value;
  SetTamanhoPizza;
end;

end.
