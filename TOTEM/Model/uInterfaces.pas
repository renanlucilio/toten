unit uInterfaces;

interface

type

  IComanda = interface
    ['{541AA4AE-C8B7-41CD-A608-7A0A9BEE7784}']
  end;

  IVerificavel = interface
    ['{A1C074AB-D485-4E7F-AEA9-717FD7E6C5E2}']
    function VerificarSeExiste(): Boolean;
  end;

  IVerificador = interface
    ['{19B73FA1-D304-47DD-B273-C3502D146B1F}']
    function VerificarDados(const id: integer; const nameClass: string): string;
    function Ref:IVerificador;
  end;

  IValorDefinivel = interface
    ['{81DA223E-24CF-4839-ABE0-E8537670DBFF}']
    procedure DefinirValor();
  end;

  IVenda = interface
    ['{86B7BA77-8F02-4731-9616-D1DBA4822DD3}']
    function ComecaVender(): Boolean;
    function GravarVenda(): Boolean;
    function CancelarVenda(): Boolean;
  end;

  IPromocao = interface
    ['{B7C3303C-44E1-4B27-8A9A-5C8594639D24}']
    procedure VerificarPromocao();
    procedure VerificarCupomDesconto(aCupom: string);
  end;

  ITotem = interface
    ['{1D281F60-7621-4B9B-81C9-36320CB241D4}']
    procedure IniciarSistema;
  end;

  ICliente = interface
    ['{46314C12-80BD-4A87-956D-E3C6090442A6}']
  end;

implementation

end.
