unit uItem.ItemExtra;

interface

uses
  {Model}
    uItem,
  {Criador JSON}
    uCriadorJson,
    uCriadorJson.Interfaces;

type
  TItemExtra = class(TItem)
  private
  public
    constructor Create(aId: integer; aDescricao: string; aValor: Currency);
    destructor Destroy; override;

    function AsJson(): string; override;
  end;
implementation



{ TItemExtra }

function TItemExtra.AsJson: string;
var
  criadorJson: TCriadorJson;
begin
  criadorJson:= TCriadorJson.Create;
  try
    criadorJson.Inicio;

    criadorJson.AddValue('id', ID);
    criadorJson.AddValue('descricao', Descricao);
    criadorJson.AddValue('valorUnitario', ValorUnitario);

    criadorJson.Fim;
    Result:= criadorJson.AsJsonStr;
  finally
    criadorJson.Free;
  end;
end;

constructor TItemExtra.Create(aId: integer; aDescricao: string; aValor: Currency);
begin
  ID:= aId;
  Descricao:= aDescricao;
  ValorUnitario:= aValor;
end;

destructor TItemExtra.Destroy;
begin
  inherited;
end;


end.
