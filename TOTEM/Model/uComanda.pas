unit uComanda;

interface

uses
  {Model}
    uVendaProduto,
    uInterfaces,
    uItem.Produto,
    uItem.Produto.Combinado,
    uItem.ItemExtra,
  {Delphi}
    System.Generics.Collections,
  {Criador JSON}
    uCriadorJson,
    uCriadorJson.Interfaces;

type
  TComanda = class(TInterfacedObject, IComanda, IValorDefinivel, IPodeSerJson)
  private

    FItensVendidos: TObjectList<TVendaProduto>;
    FTotalVenda:    Currency;
    procedure DefinirValor();
  public
    constructor Create();
    destructor Destroy; override;

    property ItensVendidos: TObjectList<TVendaProduto> read FItensVendidos;
    function GetTotalVenda: Currency;

    procedure AdicionarProduto(produto: TProduto; quatidade: Double);
    procedure RemoverProduto(const id: string);
    function GetProduto(const id: string):TVendaProduto;
    procedure AtualizarProduto(produto: TVendaProduto);


    function ProdutoJaEstaNaComanda(produto: TProduto; out quantidade: double): Boolean;

    function AsJson(): string;
  end;

implementation


uses
  {Model}
    uCliente;

{ TComanda }

procedure TComanda.AdicionarProduto(produto: TProduto; quatidade: Double);
var
  itemVendido: TVendaProduto;
begin

  itemVendido:= TVendaProduto.Create(produto, quatidade);
  ItensVendidos.Add(itemVendido);

end;

function TComanda.AsJson: string;
var
  criadorJson: TCriadorJson;
begin
  criadorJson:= TCriadorJson.Create;
  try
    criadorJson.Inicio;

    criadorJson.AddArray<TVendaProduto>('itens', ItensVendidos.ToArray);

    criadorJson.Fim;
    Result:= criadorJson.AsJsonStr;
  finally
    criadorJson.Free;
  end;
end;

procedure TComanda.AtualizarProduto(produto: TVendaProduto);
begin
  if Assigned(produto) then
    ItensVendidos.Add(produto);
end;

constructor TComanda.Create();
begin
  FItensVendidos:= TObjectList<TVendaProduto>.Create();
  FTotalVenda   := 0;
end;

procedure TComanda.DefinirValor;
var
  it: TVendaProduto;
  valorT: currency;
begin
  valorT:= 0;
  for it in ItensVendidos do
  begin
    valorT:= valorT + it.GetValor;
  end;
  FTotalVenda:= valorT;
end;

destructor TComanda.Destroy;
begin
  FItensVendidos.OnNotify:= nil;
  FItensVendidos.Clear;
  FItensVendidos.Free;
  inherited;
end;

function TComanda.GetProduto(const id: string): TVendaProduto;
var
  it: TVendaProduto;
  produto: TVendaProduto;
begin
  Result:= nil;
  for it in ItensVendidos do
  begin
    if it.ID = id then
    begin
      produto:= ItensVendidos.Extract(it);
      exit(produto);
    end;
  end;

end;

function TComanda.GetTotalVenda: Currency;
begin
  DefinirValor();
  Result:= FTotalVenda;
end;

function TComanda.ProdutoJaEstaNaComanda(produto: TProduto; out quantidade: double): Boolean;
var
  it: TVendaProduto;
begin
  for it in ItensVendidos do
  begin
    if it.Produto.Equals(produto) then
    begin
      quantidade:= it.Quantidade;
      exit(true);
    end;
  end;

  result:= false;
end;

procedure TComanda.RemoverProduto(const id: string);
var
  it: TVendaProduto;
begin
  for it in ItensVendidos do
  begin
    if it.ID = id then
    begin
      ItensVendidos.Remove(it);
      exit;
    end;
  end;
end;

end.
