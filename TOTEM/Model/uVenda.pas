unit uVenda;

interface

uses
  {Model}
    uCliente,
    uInterfaces,
    uComanda,
  {Criador JSON}
    uCriadorJson,
    uCriadorJson.Interfaces;

type
  TVenda = class(TInterfacedObject, IValorDefinivel, IVenda, IPodeSerJson)
  private
    FValorTotal: Currency;
    FData:       TDate;
    FHora:       TTime;
    FComanda:    TComanda;
    FIdentify:   string;
    procedure DefinirValor();


  public
    constructor Create(aComanda : TComanda);
    destructor Destroy; override;

    property Identify:   string   read FIdentify;
    property Comanda:    TComanda read FComanda;
    property Data:       TDate read FData;
    property Hora:       TTime read FHora;

    function ComecaVender(): Boolean;
    function GravarVenda(): Boolean;
    function CancelarVenda(): Boolean;

    function GetValorTotal: Currency;
    function AsJson(): string;
  end;

implementation

uses
  {System}
    System.SysUtils,
  {Model}
    uVendaProduto,
    uConfiguracoes,
  {Util}
    uLibUtil,
  {Repositorio}
    uRepositorio.Json,
    uRepositorio.Interfaces,
  {Resource}
    uResource.Strings;

{ TVenda }

function TVenda.AsJson: string;
var
  criadorJson: TCriadorJson;
begin
  criadorJson:= TCriadorJson.Create;
  try
    criadorJson.Inicio;

    criadorJson.AddValue('cod', Identify);
    criadorJson.AddValue('data', Data);
    criadorJson.AddValue('hora', Hora);
    criadorJson.AddObj<TComanda>('comanda', Comanda);

    criadorJson.Fim;
    Result:= criadorJson.AsJsonStr;
  finally
    criadorJson.Free;
  end;
end;

function TVenda.CancelarVenda: Boolean;
//var
//  repositorio: IRepositorio<TVenda>;
//  json: string;
//  campo1: string;
//  nomeRota: string;
begin
  Result:= true;
    { TODO : Implementar confirmação de cancelar a venda }
//  campo1:= 'cod';
//  nomeRota:= 'cancelarRota';
//  repositorio:= TRepositorioJson<TVenda>.Create(TConfiguracoes.GetIP);
//  repositorio.Ref;
//  json := TCriadorJson.New
//    .Inicio
//      .AddValue(campo1, Identify)
//    .Fim
//    .AsJsonStr();
//
//  repositorio.DeletaDado(nomeRota, json);
end;

function TVenda.ComecaVender: Boolean;
//var
//  repositorio: IRepositorio<TVenda>;
//  json: string;
//  campo1: string;
//  nomeRota: string;
begin
  Result:= true;
    { TODO : Implementar confirmação de inicio da venda }
//  campo1:= 'cod';
//  nomeRota:= 'comecaVenda';
//  repositorio:= TRepositorioJson<TVenda>.Create(TConfiguracoes.GetIP);
//  repositorio.Ref;
//  json := TCriadorJson.New
//    .Inicio
//      .AddValue(campo1, Identify)
//    .Fim
//    .AsJsonStr();
//
//  if not repositorio.SetDadoSimples(nomeRota, json) then
//    raise Exception.Create(MsgErroComecaVenda);
end;

constructor TVenda.Create(aComanda : TComanda);
begin
  FIdentify  := GetIdUnique();
  FComanda   := aComanda;
  FValorTotal:= 0;
  FData      := Now();
  FHora      := Now();
end;

procedure TVenda.DefinirValor;
var
  it: TVendaProduto;
  valorT: currency;
begin
  valorT:= 0;
  for it in Comanda.ItensVendidos do
  begin
    valorT:= valorT + it.GetValor;
  end;
  FValorTotal:= valorT;
end;

destructor TVenda.Destroy;
begin
  inherited;
end;

function TVenda.GetValorTotal: Currency;
begin
  DefinirValor;
  Result:= FValorTotal;
end;

function TVenda.GravarVenda: Boolean;
var
  repositorio: IRepositorio<TVenda>;
  json: string;
  nomeRota: string;
begin
  result:= True;
    { TODO : Implementar confirmação de venda }
  nomeRota:= 'vender';
  repositorio:= TRepositorioJson<TVenda>.Create(TConfiguracoes.GetIP);
  repositorio.Ref;
  json := AsJson();
  //raise Exception.Create(json);


//  if not repositorio.SetDadoSimples(nomeRota, json) then
//    raise Exception.Create(MsgErroVender);
end;

end.
