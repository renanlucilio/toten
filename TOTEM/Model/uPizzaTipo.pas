unit uPizzaTipo;

interface

uses
  {Model}
   uItem.Produto,
   uInterfaces,
   uItem.Produto.Combinado,
    uItem.Produto.Pizza,
  {Delphi}
    System.Generics.Collections,
    FMX.Graphics;

type
  TTipoCobrancaPorCombinada = (tccMedia, tccValorDaMaior);
  TTipos = (tMeia, tTerco, tQuadra, tNormal);

  TEvAdicionadoPizza = procedure(tipoPizza:TTipos ; saborAtual: integer; produtoAtual: TPizza; valorAtual: Currency;
    Action: TCollectionNotification) of object;

  TPizzaTipo = class(TInterfacedObject, IValorDefinivel)
  private
    FValorPizza:        Currency;
    FPizzas:            TList<TPizza>;
    FTipoCobranca:      TTipoCobrancaPorCombinada;
    FTipo:              TTipos;
    FOnAtualizaLista:   TEvAdicionadoPizza;

    procedure DoNotifiPizzas(Sender: TObject; const Item: TPizza; Action: TCollectionNotification);

    procedure calcularValorMedia;
    procedure calcularValorMaior;

    procedure DefinirValor;
    procedure GetInformacoesTipoPizza;
  public
    constructor Create;
    destructor Destroy; override;

    function VerificarSeTodosSaboresForamPreenchidos(): Boolean;
    procedure DefinirTipo();
    procedure IniciarEscolhaSabores();
    function GetValorPizza: Currency;

    procedure RemoverPizza(aId: integer);
    procedure AdicionarPizza(pizza: TPizza);
    procedure LimparPizzaCombinada();

    property OnAtualizaLista:   TEvAdicionadoPizza        read FOnAtualizaLista   write FOnAtualizaLista;
    property Pizzas:            TList<TPizza>             read FPizzas;
    property TipoCobranca:      TTipoCobrancaPorCombinada read FTipoCobranca;
    property Tipo:              TTipos                    read FTipo              write FTipo;
  end;

implementation

uses
  {Resource}
    uResource.Strings,
  {Model}
    uConfiguracoes,
  {Repositorio}
    uRepositorio.Json,
    uRepositorio.Interfaces,
  {Delphi}
    System.Json,
    System.SysUtils,
  {Helper}
    Json.Helper, uPizzaTamanho;

{ TPizzaTipo }

procedure TPizzaTipo.calcularValorMedia;
var
  valorTotalDivido: currency;
  valorTotal: Currency;
  it: TPizza;
  divisao: integer;
begin
  divisao:= 0;
  valorTotalDivido:= 0;
  valorTotal:= 0;

  case Tipo of
    tMeia: divisao:= 2;
    tTerco: divisao:= 3;
    tQuadra: divisao:= 4;
    tNormal: divisao:= 1;
  end;

  for it in pizzas do
  begin
    valorTotal:= valorTotal + it.ValorPizza;
  end;

  valorTotalDivido:= valorTotal / divisao;
  FValorPizza:= valorTotalDivido;
end;

procedure TPizzaTipo.AdicionarPizza(pizza: TPizza);
begin
   FPizzas.Add(pizza);
end;

procedure TPizzaTipo.calcularValorMaior;
var
  it: TPizza;
  maiorValor: Currency;
begin
  maiorValor:= 0;
  for it in pizzas do
  begin
    if maiorValor < it.ValorPizza then
      maiorValor:= it.ValorPizza;
  end;
  FValorPizza:= maiorValor;
end;

constructor TPizzaTipo.Create;
begin
  FTipoCobranca     := tccValorDaMaior;
  FTipo             := tNormal;
  FPizzas:= TList<TPizza>.Create;
  FPizzas.OnNotify:= DoNotifiPizzas;
  GetInformacoesTipoPizza();
end;

procedure TPizzaTipo.DefinirTipo();
begin
  case FTipo of
    tMeia:   Pizzas.Capacity := 2;
    tTerco:  Pizzas.Capacity := 3;
    tQuadra: Pizzas.Capacity := 4;
    tNormal: Pizzas.Capacity := 1;
  end;

  if Pizzas.Count > 0 then
    FValorPizza:= Pizzas.First.ValorPizza
  else
    FValorPizza:= 0;
end;

procedure TPizzaTipo.DefinirValor;
begin
  case Tipo of
    tMeia:
    begin
      case TipoCobranca of
        tccMedia: calcularValorMedia;
        tccValorDaMaior: calcularValorMaior;
      end;
    end;
    tTerco:
    begin
      case TipoCobranca of
        tccMedia: calcularValorMedia;
        tccValorDaMaior: calcularValorMaior;
      end;
    end;
    tQuadra:
    begin
      case TipoCobranca of
        tccMedia: calcularValorMedia;
        tccValorDaMaior: calcularValorMaior;
      end;
    end;
    tNormal:
    begin
      if Pizzas.Count > 0 then
        FValorPizza:= Pizzas.First.ValorPizza
      else
        FValorPizza:= 0;
    end;
  end;
end;

destructor TPizzaTipo.Destroy;
begin
    FOnAtualizaLista:= nil;
    FPizzas.OnNotify:=nil;
    FPizzas.Free;
  inherited;
end;

procedure TPizzaTipo.DoNotifiPizzas(Sender: TObject; const Item: TPizza;
  Action: TCollectionNotification);
begin
  if Assigned(FOnAtualizaLista) then
  begin
    if Action = cnAdded then
      OnAtualizaLista(Tipo, pizzas.Count, Item, GetValorPizza, Action)
    else
      OnAtualizaLista(Tipo, pizzas.Count, Item, GetValorPizza, Action)
  end;
end;


procedure TPizzaTipo.GetInformacoesTipoPizza;
var
  repositorio: IRepositorio<TObject>;
  nomeRota: string;
  JsonStr: string;
  tipoStr: string;
  jsonResultado: TJSONObject;
begin
  nomeRota:= 'pizzaTipo';

  repositorio:= TRepositorioJson<TObject>.Create(TConfiguracoes.GetIP);
  repositorio.Ref;

  JsonStr:=repositorio.GetJsonString(nomeRota);

  if not JsonStr.Equals('{}') then
  begin
    jsonResultado:= TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(JsonStr), 0) as TJSONObject;
    try
      tipoStr := jsonResultado.GetValue<string>('tipoCobranca', '');

      if tipoStr.Equals('media') then
        FTipoCobranca:= tccMedia
      else if tipoStr.Equals('valorMaior') then
        FTipoCobranca:= tccValorDaMaior;

    finally
      jsonResultado.Free;
    end
  end;
end;

function TPizzaTipo.GetValorPizza: Currency;
begin
  if not VerificarSeTodosSaboresForamPreenchidos then
    Result:= 0
  else
  begin
    DefinirValor;
    Result:= FValorPizza;
  end;
end;

procedure TPizzaTipo.IniciarEscolhaSabores;
var
  produtoAtual: TPizza;
  it: TObject;
begin
  produtoAtual:= nil;

  pizzas.Clear;
  if Assigned(FOnAtualizaLista) then
  begin
    if Pizzas.Count > 0 then
      produtoAtual:= Pizzas.Items[Pred(Pizzas.Count)];

    OnAtualizaLista(Tipo, pizzas.Count, produtoAtual, GetValorPizza, cnAdded);
  end;
end;

procedure TPizzaTipo.LimparPizzaCombinada;
var
  it: TPizza;
begin
  for it in FPizzas do
  begin
    it.Free;
  end;
  FPizzas.Clear;
end;

procedure TPizzaTipo.RemoverPizza(aId: integer);
var
  it: TPizza;
begin
  for it in Pizzas do
  begin
    if it.ID = aId then
    begin
      FPizzas.Remove(it);
      exit;
    end;
  end;
end;

function TPizzaTipo.VerificarSeTodosSaboresForamPreenchidos: Boolean;
begin
  case FTipo of
    tMeia: result:= Pizzas.Count = 2;
    tTerco: result:= Pizzas.Count = 3;
    tQuadra: result:= Pizzas.Count = 4;
    tNormal: result:= Pizzas.Count = 1;
    else
    result:= false;
  end


end;

end.
