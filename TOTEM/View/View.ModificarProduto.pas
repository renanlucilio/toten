unit View.ModificarProduto;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Objects, FMX.Layouts, FMX.TabControl, FMX.ListView.Types,
  FMX.ListView.Appearances, FMX.ListView.Adapters.Base, FMX.ListView,
  FMX.Controls.Presentation, FMX.StdCtrls, uVendaProduto, uItem.Produto,
   uItem.Produto.Combinado, uItem.ItemExtra;

type
  TfrmModificaProduto = class(TForm)
    Layout1: TLayout;
    Rectangle1: TRectangle;
    Layout2: TLayout;
    Rectangle2: TRectangle;
    Layout3: TLayout;
    Layout20: TLayout;
    Rectangle17: TRectangle;
    txtTituloProduto: TText;
    Layout10: TLayout;
    btnCancelar: TLayout;
    Rectangle9: TRectangle;
    Rectangle12: TRectangle;
    txtCancelar: TText;
    btnConfirmar: TLayout;
    Rectangle18: TRectangle;
    Rectangle19: TRectangle;
    txtConfirmar: TText;
    Layout4: TLayout;
    Layout5: TLayout;
    Layout7: TLayout;
    Rectangle3: TRectangle;
    Text1: TText;
    txtObservacoes: TText;
    Layout6: TLayout;
    Layout8: TLayout;
    Rectangle4: TRectangle;
    Text2: TText;
    txtItensExtra: TText;
    Layout9: TLayout;
    Rectangle6: TRectangle;
    txtValor: TText;
    btnConfirmarNaoEdita: TLayout;
    Rectangle5: TRectangle;
    Rectangle10: TRectangle;
    txtAdicionarObservacao: TText;
    Layout11: TLayout;
    tbcPrincipal: TTabControl;
    TabItem1: TTabItem;
    StyleBook1: TStyleBook;
    tabPergunta: TTabItem;
    Layout14: TLayout;
    Layout15: TLayout;
    Rectangle14: TRectangle;
    txtTituloPizza: TText;
    Rectangle22: TRectangle;
    Layout21: TLayout;
    Rectangle23: TRectangle;
    Rectangle24: TRectangle;
    Text5: TText;
    Layout12: TLayout;
    Rectangle7: TRectangle;
    Rectangle8: TRectangle;
    Text3: TText;
    Rectangle11: TRectangle;
    Image4: TImage;
    Rectangle13: TRectangle;
    Rectangle15: TRectangle;
    procedure btnConfirmarNaoEditaClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
    procedure btnConfirmarSaboresClick(Sender: TObject);
    procedure Layout12Click(Sender: TObject);
  private
    produto: TVendaProduto;

    procedure MostraInformacoesProduto();
    procedure DeixarTabDefault();
  public
    constructor Create(AOwner: TComponent; aProduto: TVendaProduto); reintroduce;
    { Public declarations }
  end;

var
  frmModificaProduto: TfrmModificaProduto;

implementation

{$R *.fmx}

uses View.Principal, uVM.Cardapio;

{ TfrmModificaProduto }

procedure TfrmModificaProduto.btnCancelarClick(Sender: TObject);
begin
  close;
  ModalResult:= mrAbort;
end;

procedure TfrmModificaProduto.btnConfirmarClick(Sender: TObject);
begin
  if produto.Produto is TProdutoCombinado then
  begin
    tbcPrincipal.TabIndex:= 1;
  end
  else
  begin
    close;
    ModalResult:= mrOk;
  end;
end;

procedure TfrmModificaProduto.btnConfirmarNaoEditaClick(Sender: TObject);
begin
  close;
  ModalResult:= mrContinue;
end;

procedure TfrmModificaProduto.btnConfirmarSaboresClick(Sender: TObject);
begin
  close;
  ModalResult:= mrOk;
end;

constructor TfrmModificaProduto.Create(AOwner: TComponent; aProduto: TVendaProduto);
begin
  inherited Create(AOwner);
  DeixarTabDefault;
  produto:= aProduto;
  MostraInformacoesProduto;
end;


procedure TfrmModificaProduto.DeixarTabDefault;
var
  I: integer;
begin
  for I := 0 to Pred(ComponentCount) do
  begin
    if Components[I] is TTabControl then
    begin
      (Components[I] as TTabControl).TabIndex:=0;
    end;
  end;
end;

procedure TfrmModificaProduto.Layout12Click(Sender: TObject);
begin
  close;
  ModalResult:= mrAll;
end;

procedure TfrmModificaProduto.MostraInformacoesProduto;
var
  it: string;
  it2: TItemExtra;
begin
  if Assigned(produto) then
  begin
    txtTituloProduto.Text:= produto.GetDescricaoProduto;

    for it in produto.Produto.Observacao do
    begin
      txtObservacoes.Text:= txtObservacoes.Text + it + #13;
    end;

    for it2 in produto.Produto.ItensExtra do
    begin
      txtItensExtra.Text:= txtItensExtra.Text + it2.Descricao + #13;
    end;

    txtValor.Text:= 'Valor: R$' + FormatCurr('###,###,##0.00;1;', Produto.GetValor);
  end;
end;

end.
