unit View.Principal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Objects, FMX.Layouts, FMX.Controls.Presentation, FMX.StdCtrls,
  FMX.TabControl, FMX.Edit, FMX.ListView.Types, FMX.ListView.Appearances,
  FMX.ListView.Adapters.Base, FMX.ListView, System.ImageList, FMX.ImgList,
  uVM.Venda, System.Classes;

type
  TfrmPrincipal = class(TForm)
    tbcPrincipal: TTabControl;
    tabInicio: TTabItem;
    rctBackgroundInicio: TRectangle;
    Layout1: TLayout;
    txtEstabelecimento: TText;
    Layout3: TLayout;
    txtSite: TText;
    txtVersao: TText;
    Layout2: TLayout;
    btnFazerPedido: TLayout;
    Rectangle2: TRectangle;
    Rectangle3: TRectangle;
    txtBtnFazerPedido: TText;
    tabLista: TTabItem;
    Rectangle4: TRectangle;
    Layout5: TLayout;
    Layout4: TLayout;
    tbcCategoriasProdutos: TTabControl;
    tabCategorias: TTabItem;
    Layout6: TLayout;
    Layout7: TLayout;
    Rectangle6: TRectangle;
    Rectangle7: TRectangle;
    edtPesquisaCategorias: TEdit;
    Layout8: TLayout;
    Rectangle5: TRectangle;
    txtTituloCategorias: TText;
    Layout9: TLayout;
    lsvCategorias: TListView;
    tabCategoriaProduto: TTabItem;
    Layout10: TLayout;
    Layout11: TLayout;
    Rectangle8: TRectangle;
    Rectangle9: TRectangle;
    edtPesquisarProdutoCategoria: TEdit;
    Layout12: TLayout;
    Rectangle10: TRectangle;
    txtProdutoCategoria: TText;
    Layout13: TLayout;
    lsvProdutos: TListView;
    btnVoltaCategorias: TLayout;
    Rectangle11: TRectangle;
    Rectangle12: TRectangle;
    txtVoltarCategorias: TText;
    Layout15: TLayout;
    Layout16: TLayout;
    btnConfirmar: TLayout;
    btnCancelar: TLayout;
    Rectangle13: TRectangle;
    Rectangle14: TRectangle;
    Rectangle15: TRectangle;
    Rectangle16: TRectangle;
    txtCancelar: TText;
    txtConfirmar: TText;
    Layout19: TLayout;
    txtTituloTotal: TText;
    txtTotal: TText;
    lsvComanda: TListView;
    Layout20: TLayout;
    Rectangle17: TRectangle;
    Text8: TText;
    tabFim: TTabItem;
    Rectangle18: TRectangle;
    Layout21: TLayout;
    txtMensagemFinal: TText;
    Layout22: TLayout;
    txtSiteFinal: TText;
    txtVersaoFinal: TText;
    Layout23: TLayout;
    btnVoltarParaInicio: TLayout;
    Rectangle19: TRectangle;
    Rectangle20: TRectangle;
    txtVoltarParaInicio: TText;
    Rectangle21: TRectangle;
    tabCategoriaPizza: TTabItem;
    Rectangle1: TRectangle;
    Layout14: TLayout;
    Rectangle22: TRectangle;
    txtPizzas: TText;
    btnCategoriasPizza: TLayout;
    Rectangle23: TRectangle;
    Rectangle24: TRectangle;
    txtBtnCategoriasPizza: TText;
    lPesquisaPizza: TLayout;
    Rectangle25: TRectangle;
    Rectangle26: TRectangle;
    edtPesquisaPizza: TEdit;
    Layout25: TLayout;
    imgListPizzas: TImageList;
    lsvPizzas: TListView;
    Line1: TLine;
    imgPesquisarCategoriaProduto: TImage;
    imgPesquisarCategorias: TImage;
    Image1: TImage;
    Layout24: TLayout;
    Rectangle27: TRectangle;
    imgTipoAtual: TImage;
    Layout26: TLayout;
    lPizza1: TLayout;
    txtPizza1: TText;
    imgPizza1: TImage;
    lPizza2: TLayout;
    txtPizza2: TText;
    imgPizza2: TImage;
    Layout27: TLayout;
    lPizza3: TLayout;
    txtPizza3: TText;
    imgPizza3: TImage;
    lPizza4: TLayout;
    txtPizza4: TText;
    imgPizza4: TImage;
    Layout17: TLayout;
    txtCobrancaTamanho: TText;
    txtCobrancaTipo: TText;
    Line2: TLine;
    Line3: TLine;
    Line4: TLine;
    Line5: TLine;
    Rectangle28: TRectangle;
    Rectangle29: TRectangle;
    Image2: TImage;
    imgLogoDoEstabelecimento: TImage;
    procedure FormCreate(Sender: TObject);
    procedure btnFazerPedidoClick(Sender: TObject);
    procedure btnVoltaCategoriasClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
    procedure btnVoltarParaInicioClick(Sender: TObject);
    procedure lsvPizzasUpdateObjects(const Sender: TObject;
      const AItem: TListViewItem);
    procedure lsvPizzasItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure lsvCategoriasItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure lsvProdutosItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lsvComandaItemClick(const Sender: TObject;
      const AItem: TListViewItem);
  private
    procedure RemoverScrollBars();
    procedure DeixarOsTabControlDefault();
    procedure AjustasLsvProdutosItem(aItem: TListViewItem);
    { Private declarations }
  public
    procedure AjustaLsvProdutos(lsv: TListView);
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

uses
  {Relatorio}
    uRelatorio.Linhas,
  {View Model}
    uVM.Cardapio,
  {Util}
    Model.LibUtil,
  {Exception}
    TratandoErro;

{$R *.fmx}

{ TFormTeste }

procedure TfrmPrincipal.AjustaLsvProdutos(lsv: TListView);
var
  I: Integer;
begin
  for I := 0 to Pred(lsv.Items.Count) do
  begin
    AjustasLsvProdutosItem(TListViewItem(lsv.Items.Item[I]));
  end;
end;


procedure TfrmPrincipal.AjustasLsvProdutosItem(aItem: TListViewItem);
var
  it: string;
  linhaQtd: integer;
  descricao, detalhes: TListItemText;
  descricaoStr, detalhesStr: string;
begin
  linhaQtd:= 0;
  AItem.Height:= 80;

  descricao:= TListItemText(AItem.Objects.FindDrawable('Descricao'));
  detalhes:= TListItemText(AItem.Objects.FindDrawable('Detalhes'));

  if Assigned(descricao) then
  begin
    descricaoStr:= descricao.Text;
    descricao.Text:= EmptyStr;


    for it in TTratamentoLinhas.New.TratarLinha(descricaoStr, 50) do
    begin
      descricao.Text:= descricao.Text + it + #13;
      inc(linhaQtd);

      if linhaQtd>2 then
      begin
        descricao.Height:= descricao.Height + 25;
        AItem.Height:= AItem.Height+25;
        detalhes.PlaceOffset.Y:= detalhes.PlaceOffset.Y + 25;
      end;
    end;
  end;

  if Assigned(detalhes) then
  begin
    detalhesStr:= detalhes.Text;
    detalhes.Text:= EmptyStr;

    for it in TTratamentoLinhas.New.TratarLinha(detalhesStr, 90) do
    begin
      detalhes.Text:= detalhes.Text + it + #13;
      inc(linhaQtd);

      if linhaQtd>2 then
      begin
        detalhes.Height:= detalhes.Height + 18;
        AItem.Height:= AItem.Height+18;
      end;
    end;
  end;
end;

procedure TfrmPrincipal.btnCancelarClick(Sender: TObject);
begin
  DeixarOsTabControlDefault;
end;

procedure TfrmPrincipal.btnConfirmarClick(Sender: TObject);
begin
  dmVenda.FinalizaVenda;
  tbcPrincipal.TabIndex:= 2;
end;

procedure TfrmPrincipal.btnFazerPedidoClick(Sender: TObject);
begin
  tbcPrincipal.TabIndex:= 1;
  dmVenda.ComecaVenda;
end;

procedure TfrmPrincipal.btnVoltaCategoriasClick(Sender: TObject);
begin
  tbcCategoriasProdutos.TabIndex:=0;
  dmCardapio.VoltarParaCategorias;
end;

procedure TfrmPrincipal.btnVoltarParaInicioClick(Sender: TObject);
begin
  DeixarOsTabControlDefault;
end;

procedure TfrmPrincipal.DeixarOsTabControlDefault;
var
  I: Integer;
begin
  dmCardapio.CancelarProdutosCombinados();
  if dmVenda.EmVenda then
    dmVenda.CancelarVenda;

  for I := 0 to Pred(ComponentCount) do
  begin
    if Components[I] is TTabControl then
    begin
      (Components[I] as TTabControl).TabIndex:=0;
    end;
  end;

  txtTotal.Text:= EmptyStr;

end;

procedure TfrmPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  DeixarOsTabControlDefault;

end;

procedure TfrmPrincipal.FormCreate(Sender: TObject);
begin
  RemoverScrollBars;
  DeixarOsTabControlDefault;
  txtVersao.Text:= GetVersaoPrograma(ParamStr(0));
  txtVersaoFinal.Text:= GetVersaoPrograma(ParamStr(0));
  txtPizza1.TagString:= 'pizza';
  txtPizza2.TagString:= 'pizza';
  txtPizza3.TagString:= 'pizza';
  txtPizza4.TagString:= 'pizza';
  Application.OnException:= Tratamento.TratamentoDeErro;
end;

procedure TfrmPrincipal.FormShow(Sender: TObject);
begin
  dmCardapio.IniciarCardapios;
  tbcPrincipal.TabPosition:= TTabPosition.None;
  tbcCategoriasProdutos.TabPosition:= TTabPosition.None;
end;

procedure TfrmPrincipal.lsvCategoriasItemClick(const Sender: TObject;
  const AItem: TListViewItem);
begin
  dmCardapio.ListaProdutosCategoria(AItem);
end;

procedure TfrmPrincipal.lsvComandaItemClick(const Sender: TObject;
  const AItem: TListViewItem);
begin
  dmVenda.EditarExcluirProduto(AItem);
end;

procedure TfrmPrincipal.lsvPizzasItemClick(const Sender: TObject;
  const AItem: TListViewItem);
begin
  dmCardapio.AdcRemovePizza(AItem);
end;

procedure TfrmPrincipal.lsvPizzasUpdateObjects(const Sender: TObject;
  const AItem: TListViewItem);
var
  img: TListItemImage;
begin
  img:= TListItemImage(AItem.Objects.FindDrawable('Selecionavel'));
  img.Bitmap:= imgListPizzas.Bitmap(TSizeF.Create(512, 512), 10);

end;

procedure TfrmPrincipal.lsvProdutosItemClick(const Sender: TObject;
  const AItem: TListViewItem);
begin
  dmCardapio.DefinirProduto(AItem);
end;

procedure TfrmPrincipal.RemoverScrollBars;
var
  item: TControl;
  I: Integer;
begin
  for I := 0 to Pred(ComponentCount) do
  begin
    if Components[I] is TListView then
    begin
      for item in (Components[I] as TListView).Controls do
      begin
        if item is TScrollBar then
          item.Width := 0;
      end;
    end;
  end;
end;

end.

