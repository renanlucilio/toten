unit View.TipoTamanhoPizza;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Layouts, FMX.Objects, FMX.Controls.Presentation, FMX.StdCtrls,
  FMX.Effects;

type
  TfrmTamanhoTipoPizza = class(TForm)
    Layout1: TLayout;
    Rectangle1: TRectangle;
    Layout2: TLayout;
    Rectangle2: TRectangle;
    Layout3: TLayout;
    Layout20: TLayout;
    Rectangle17: TRectangle;
    txtTituloPizza: TText;
    Layout4: TLayout;
    Layout5: TLayout;
    Rectangle3: TRectangle;
    Text1: TText;
    Layout6: TLayout;
    Rectangle4: TRectangle;
    rbnBroto: TRadioButton;
    Layout7: TLayout;
    Rectangle5: TRectangle;
    rbnGigante: TRadioButton;
    Layout8: TLayout;
    Rectangle6: TRectangle;
    rbnNormal: TRadioButton;
    Layout9: TLayout;
    Rectangle7: TRectangle;
    rbnMedia: TRadioButton;
    btnMeia: TLayout;
    Rectangle8: TRectangle;
    rbnMeia: TRadioButton;
    btnQuarto: TLayout;
    Rectangle10: TRectangle;
    rbnQuarto: TRadioButton;
    btnTerco: TLayout;
    Rectangle11: TRectangle;
    rbnTerco: TRadioButton;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Layout10: TLayout;
    btnCancelar: TLayout;
    Rectangle9: TRectangle;
    Rectangle12: TRectangle;
    txtCancelar: TText;
    btnConfirmar: TLayout;
    Rectangle18: TRectangle;
    Rectangle19: TRectangle;
    txtConfirmar: TText;
    btnUmSabor: TLayout;
    Rectangle13: TRectangle;
    rbnUmSabor: TRadioButton;
    Image4: TImage;
    GlowMeia: TGlowEffect;
    GlowQuarto: TGlowEffect;
    GlowTerco: TGlowEffect;
    GlowUmSabor: TGlowEffect;
    GlowBroto: TInnerGlowEffect;
    GlowGigante: TInnerGlowEffect;
    GlowNormal: TInnerGlowEffect;
    GlowMedia: TInnerGlowEffect;
    Timer1: TTimer;
    Rectangle14: TRectangle;
    Image5: TImage;
    procedure btnMeiaClick(Sender: TObject);
    procedure btnTercoClick(Sender: TObject);
    procedure btnQuartoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
    procedure btnUmSaborClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);

  private
    { Private declarations }
    procedure SetFocus(const tag: integer);
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

procedure TfrmTamanhoTipoPizza.btnCancelarClick(Sender: TObject);
begin
  self.Close;
  ModalResult:= mrNo;
end;

procedure TfrmTamanhoTipoPizza.btnConfirmarClick(Sender: TObject);
begin
  self.Close;
  ModalResult:= mrYes;
end;

procedure TfrmTamanhoTipoPizza.btnMeiaClick(Sender: TObject);
begin
  rbnMeia.IsChecked:= true;
end;

procedure TfrmTamanhoTipoPizza.btnQuartoClick(Sender: TObject);
begin
  rbnQuarto.IsChecked:= true;
end;

procedure TfrmTamanhoTipoPizza.btnTercoClick(Sender: TObject);
begin
  rbnTerco.IsChecked:= true;
end;

procedure TfrmTamanhoTipoPizza.btnUmSaborClick(Sender: TObject);
begin
  rbnUmSabor.IsChecked:= true;
end;

procedure TfrmTamanhoTipoPizza.SetFocus(const tag: integer);
begin
  case tag of
    1:
    begin
      GlowBroto.Enabled:= true;

      GlowGigante.Enabled:= false;
      GlowNormal.Enabled:= false;
      GlowMedia.Enabled:= false;
    end;
    2:
    begin
     GlowGigante.Enabled:= true;

      GlowBroto.Enabled:= false;
      GlowNormal.Enabled:= false;
      GlowMedia.Enabled:= false;
    end;
    3:
    begin
      GlowNormal.Enabled:= true;

      GlowBroto.Enabled:= false;
      GlowGigante.Enabled:= false;
      GlowMedia.Enabled:= false;
    end;
    4:
    begin
      GlowMedia.Enabled:= true;

      GlowBroto.Enabled:= false;
      GlowGigante.Enabled:= false;
      GlowNormal.Enabled:= false;
    end;
    5:
    begin
      GlowMeia.Enabled:= true;

      GlowQuarto.Enabled:= false;
      GlowUmSabor.Enabled:= false;
      GlowTerco.Enabled:= false;
    end;
    6:
    begin
      GlowQuarto.Enabled:= true;

      GlowMeia.Enabled:= false;
      GlowUmSabor.Enabled:= false;
      GlowTerco.Enabled:= false;
    end;
    7:
    begin
      GlowTerco.Enabled:= true;

      GlowMeia.Enabled:= false;
      GlowQuarto.Enabled:= false;
      GlowUmSabor.Enabled:= false;
    end;
    8:
    begin
     GlowUmSabor.Enabled:= true;

     GlowMeia.Enabled:= false;
     GlowQuarto.Enabled:= false;
     GlowTerco.Enabled:= false;
    end;
  end;
end;

procedure TfrmTamanhoTipoPizza.Timer1Timer(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to ComponentCount-1 do
    if Components[I] is TRadioButton then
    begin
      if (Components[i] as TRadioButton).IsChecked then
        SetFocus((Components[i] as TRadioButton).Tag);
    end;
end;

end.
