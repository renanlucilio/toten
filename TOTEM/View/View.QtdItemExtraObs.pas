unit View.QtdItemExtraObs;

interface

uses
  {Delphi}
    System.SysUtils,
    System.Types,
    System.UITypes,
    System.Classes,
    System.Variants,
    System.Generics.Collections,
    FMX.Types,
    FMX.Controls,
    FMX.Forms,
    FMX.Graphics,
    FMX.Dialogs,
    FMX.Layouts,
    FMX.Objects,
    FMX.Ani,
    FMX.TabControl,
    System.Math.Vectors,
    FMX.Controls3D,
    FMX.Layers3D,
    FMX.Controls.Presentation,
    FMX.Edit,
    FMX.ListView.Types,
    FMX.ListView.Appearances,
    FMX.ListView.Adapters.Base,
    FMX.ListView,
    FMX.ScrollBox,
    FMX.Memo,
  {Model}
    uItem.Produto,
    uItem.ItemExtra,
    uItem.Produto.Combinado,
    uItem.Produto.Pizza,
  {Teclado}
    View.Teclado,
    View.TecladoNumerico;

type
  TfrmQtdItemExtraObs = class(TForm)
    Layout1: TLayout;
    Layout2: TLayout;
    Layout3: TLayout;
    tbcPrincipal: TTabControl;
    tabQuantidade: TTabItem;
    Rectangle2: TRectangle;
    Layout20: TLayout;
    Rectangle17: TRectangle;
    txtTituloQuantidadeProduto: TText;
    Layout5: TLayout;
    btnDiminuir: TLayout;
    Rectangle14: TRectangle;
    Rectangle15: TRectangle;
    txtMais: TText;
    btnAumentar: TLayout;
    Rectangle13: TRectangle;
    Rectangle16: TRectangle;
    txtMenos: TText;
    edtQuantidade: TEdit;
    btnAdicionarItemExtra: TLayout;
    Rectangle7: TRectangle;
    Rectangle8: TRectangle;
    txtAdicionarItemExtra: TText;
    Layout7: TLayout;
    btnCancelarQuantidade: TLayout;
    Rectangle11: TRectangle;
    Rectangle12: TRectangle;
    txtCancelarQuantidade: TText;
    btnConfirmarQuantidade: TLayout;
    Rectangle18: TRectangle;
    Rectangle19: TRectangle;
    txtConfirmarQuantidade: TText;
    btnAdicionarObservacao: TLayout;
    Rectangle9: TRectangle;
    Rectangle10: TRectangle;
    txtAdicionarObservacao: TText;
    tabItensExtra: TTabItem;
    Rectangle3: TRectangle;
    Layout4: TLayout;
    Rectangle5: TRectangle;
    txtTituloItemExtra: TText;
    Layout13: TLayout;
    btnCancelarItenExtra: TLayout;
    Rectangle20: TRectangle;
    Rectangle21: TRectangle;
    txtCancelarItemExtra: TText;
    btnConfirmarItemExtra: TLayout;
    Rectangle22: TRectangle;
    Rectangle23: TRectangle;
    txtConfirmarItemExtra: TText;
    lsvItensExtra: TListView;
    tabObervacoes: TTabItem;
    Rectangle4: TRectangle;
    Layout16: TLayout;
    Rectangle6: TRectangle;
    txtTituloObservacoesProduto: TText;
    Layout17: TLayout;
    btnCancelarObservacoes: TLayout;
    Rectangle24: TRectangle;
    Rectangle25: TRectangle;
    txtCancelarObservacoes: TText;
    btnConfirmarObervacoes: TLayout;
    Rectangle26: TRectangle;
    Rectangle27: TRectangle;
    txtConfirmarObservacoes: TText;
    lstDeItensExtraAdicionados: TLayout;
    Rectangle28: TRectangle;
    lsvItensAdicionados: TListView;
    Layout22: TLayout;
    Rectangle29: TRectangle;
    txtItensAdicionados: TText;
    edtObervacoes: TEdit;
    btnLimparObservacoes: TLayout;
    Rectangle30: TRectangle;
    Rectangle31: TRectangle;
    txtLimparObservacoes: TText;
    tmrMostraItensAdicionados: TTimer;
    Rectangle1: TRectangle;
    StyleBook1: TStyleBook;
    Layout6: TLayout;
    Layout8: TLayout;
    Layout19: TLayout;
    Rectangle32: TRectangle;
    txtTituloTotal: TText;
    txtTotal: TText;
    rectFundo: TRectangle;
    Rectangle33: TRectangle;
    lTeclado: TLayout;
    procedure FormCreate(Sender: TObject);
    procedure btnAdicionarObservacaoClick(Sender: TObject);
    procedure btnAdicionarItemExtraClick(Sender: TObject);
    procedure tmrMostraItensAdicionadosTimer(Sender: TObject);
    procedure btnCancelarItenExtraClick(Sender: TObject);
    procedure btnConfirmarItemExtraClick(Sender: TObject);
    procedure btnLimparObservacoesClick(Sender: TObject);
    procedure btnCancelarObservacoesClick(Sender: TObject);
    procedure btnConfirmarObervacoesClick(Sender: TObject);
    procedure btnDiminuirClick(Sender: TObject);
    procedure btnAumentarClick(Sender: TObject);
    procedure btnCancelarQuantidadeClick(Sender: TObject);
    procedure btnConfirmarQuantidadeClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtQuantidadeChangeTracking(Sender: TObject);
    procedure lsvItensExtraItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure FormDestroy(Sender: TObject);
    procedure lsvItensAdicionadosItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure edtQuantidadeEnter(Sender: TObject);
    procedure edtQuantidadeExit(Sender: TObject);
    procedure edtObervacoesEnter(Sender: TObject);
    procedure edtObervacoesExit(Sender: TObject);
  private
    FQuantidade: Double;
    FProduto: TProduto;
    FItensAdicionado: TList<TItemExtra>;
    FItensExtra: TObjectList<TItemExtra>;
    FLinhas: TStringList;
    FTeclado: TfrmTecladoAlphaNumeric;
    FTecladoNumerico: TfrmTecladoNumerico;

    procedure iniciaObs();
    procedure RemoverScrollBars();
    procedure DeixarOsTabControlDefault();

    procedure CarregarObs;
    procedure MostraQuantidade();
    procedure MostraDadosProduto();
    { Private declarations }
  public
    procedure IniciaItensAdicionados(itens: TArray<TItemExtra>);
    constructor Create(AOwner: TComponent; aProduto: TProduto; quantidade: double = 1); reintroduce;
    property Quantidade: Double read FQuantidade write FQuantidade;
    property ItensAdicionado: TList<TItemExtra> read FItensAdicionado;
    property ItensExtra: TObjectList<TItemExtra> read FItensExtra write FItensExtra;

    property Linhas: TStringList read FLinhas;
    property Produto: TProduto read FProduto;

    procedure AumentarQuantidade;
    procedure DiminuirQuantidade;
    { Public declarations }
  end;

implementation

uses
  FMX.StdCtrls, uVM.Cardapio;

{$R *.fmx}

{ TfrmQtdItemExtraObs }

procedure TfrmQtdItemExtraObs.AumentarQuantidade;
begin
  ActiveControl:= nil;
  FQuantidade:= FQuantidade + 1;
  MostraQuantidade;
end;

procedure TfrmQtdItemExtraObs.btnAdicionarItemExtraClick(Sender: TObject);
begin
  tbcPrincipal.TabIndex:= 1;
end;

procedure TfrmQtdItemExtraObs.btnAdicionarObservacaoClick(Sender: TObject);
begin
  tbcPrincipal.TabIndex:= 2;
  edtObervacoes.SetFocus;
  ActiveControl:= edtObervacoes;
end;

procedure TfrmQtdItemExtraObs.btnAumentarClick(Sender: TObject);
begin
  AumentarQuantidade;
end;

procedure TfrmQtdItemExtraObs.btnCancelarItenExtraClick(Sender: TObject);
begin
  lsvItensAdicionados.Items.Clear;
  FItensAdicionado.Clear;
  DeixarOsTabControlDefault;
end;

procedure TfrmQtdItemExtraObs.btnCancelarObservacoesClick(Sender: TObject);
begin
  edtObervacoes.Text:= EmptyStr;
  FLinhas.Clear;
  DeixarOsTabControlDefault;
end;

procedure TfrmQtdItemExtraObs.btnCancelarQuantidadeClick(Sender: TObject);
begin
  self.Close;
  ModalResult:= mrNo;
end;

procedure TfrmQtdItemExtraObs.btnConfirmarItemExtraClick(Sender: TObject);
begin
  DeixarOsTabControlDefault;
end;

procedure TfrmQtdItemExtraObs.btnConfirmarObervacoesClick(Sender: TObject);
begin
  DeixarOsTabControlDefault;
end;

procedure TfrmQtdItemExtraObs.btnConfirmarQuantidadeClick(Sender: TObject);
begin
  CarregarObs();
  self.Close;
  ModalResult:= mrYes;
end;

procedure TfrmQtdItemExtraObs.btnDiminuirClick(Sender: TObject);
begin
  DiminuirQuantidade;
end;

procedure TfrmQtdItemExtraObs.btnLimparObservacoesClick(Sender: TObject);
begin
  edtObervacoes.Text:= EmptyStr;
end;

procedure TfrmQtdItemExtraObs.CarregarObs;
begin
  if not edtObervacoes.Text.IsEmpty then
    FLinhas.Add(edtObervacoes.Text);
end;

constructor TfrmQtdItemExtraObs.Create(AOwner: TComponent; aProduto: TProduto; quantidade: double);
begin
  inherited Create(AOwner);
  FItensExtra:= TObjectList<TItemExtra>.Create();
  FItensAdicionado:= TList<TItemExtra>.Create();
  FLinhas:= TStringList.Create();
  FQuantidade:= quantidade;
  FProduto:= aProduto;


  if FProduto.UnidadeMedida.TemCasaDecimal then
    edtQuantidade.FilterChar:= '0123456789' + FormatSettings.DecimalSeparator
  else
    edtQuantidade.FilterChar:= '0123456789';


  MostraDadosProduto();
  MostraQuantidade;
end;

procedure TfrmQtdItemExtraObs.DeixarOsTabControlDefault;
var
  I: Integer;
begin
  for I := 0 to Pred(ComponentCount) do
  begin
    if Components[I] is TTabControl then
    begin
      (Components[I] as TTabControl).TabIndex:=0;
    end;
  end;
end;

procedure TfrmQtdItemExtraObs.DiminuirQuantidade;
begin
  if Quantidade > 1 then
    FQuantidade:= FQuantidade - 1;

  MostraQuantidade;
  ActiveControl:= nil;
end;

procedure TfrmQtdItemExtraObs.edtObervacoesEnter(Sender: TObject);
begin
   FTeclado:= TfrmTecladoAlphaNumeric.Create(
    edtObervacoes,
    alphaNumerico,
    TAlphaColorRec.Black,
    TAlphaColorRec.White,
    TAlphaColorRec.Gray
  );

  lTeclado.Size.Height:= FTeclado.lTeclado.Height;
  lTeclado.Size.Width:= FTeclado.lTeclado.Width;
  lTeclado.AddObject(FTeclado.lTeclado);

  lTeclado.Visible:= true;
end;

procedure TfrmQtdItemExtraObs.edtObervacoesExit(Sender: TObject);
begin
  try
    edtObervacoes.Enabled:= false;
    edtObervacoes.Enabled:= true;
    lTeclado.Height:= 0;
    if Assigned(FTeclado) then
      FTeclado.Free;
  except

  end;
end;

procedure TfrmQtdItemExtraObs.edtQuantidadeChangeTracking(Sender: TObject);
begin
  if not edtQuantidade.Text.IsEmpty then
  begin
    if edtQuantidade.Text.CountChar(FormatSettings.DecimalSeparator) > 1 then
      edtQuantidade.Text:= StringReplace(edtQuantidade.Text, FormatSettings.DecimalSeparator, '', []);

    if edtQuantidade.Text.EndsWith(FormatSettings.DecimalSeparator) then
      exit;

    if edtQuantidade.Text = '0' then
      edtQuantidade.Text:= '1';

    if StrToFloat(edtQuantidade.Text) > 100000 then
      edtQuantidade.Text:= '1';

    FQuantidade:= StrToFloat(edtQuantidade.Text);
    MostraQuantidade;
  end
  else
  begin
    edtQuantidade.Text:= '1';

    FQuantidade:= StrToInt(edtQuantidade.Text);
    MostraQuantidade;
  end;
end;

procedure TfrmQtdItemExtraObs.edtQuantidadeEnter(Sender: TObject);
begin
  FTecladoNumerico:= TfrmTecladoNumerico.Create(
    edtQuantidade,
    Produto.UnidadeMedida.TemCasaDecimal,
    TAlphaColorRec.Black,
    TAlphaColorRec.White,
    TAlphaColorRec.Gray
  );

  lTeclado.Size.Height:= FTecladoNumerico.lTeclado.Height;
  lTeclado.Size.Width:= FTecladoNumerico.lTeclado.Width;
  lTeclado.AddObject(FTecladoNumerico.lTeclado);
end;

procedure TfrmQtdItemExtraObs.edtQuantidadeExit(Sender: TObject);
begin
  try
    edtQuantidade.Enabled:= false;
    edtQuantidade.Enabled:= true;
    lTeclado.Height:= 0;
    if Assigned(FTecladoNumerico) then
      FTecladoNumerico.Free;


  except

  end;
end;

procedure TfrmQtdItemExtraObs.FormCreate(Sender: TObject);
begin
  RemoverScrollBars;
  DeixarOsTabControlDefault;
  tbcPrincipal.TabPosition:= TTabPosition.None;
end;

procedure TfrmQtdItemExtraObs.FormDestroy(Sender: TObject);
begin
  FItensExtra.Clear;
  FItensExtra.Free;
  FItensAdicionado.Clear;
  FItensAdicionado.Free;
  FLinhas.Free;
end;

procedure TfrmQtdItemExtraObs.FormShow(Sender: TObject);
begin
  iniciaObs();
end;

procedure TfrmQtdItemExtraObs.IniciaItensAdicionados(itens: TArray<TItemExtra>);
var
  novoItem: TListViewItem;
  descricao, valor: TListItemText;
  it, it2, ItemExtra: TItemExtra;
  I: Integer;
begin
  lsvItensAdicionados.Items.Clear;
  ItemExtra:= nil;
  for it in itens do
  begin
    try
      for it2 in ItensExtra do
      begin
        if it2.ID = it.ID then
        begin
          ItemExtra:= it2;
        end;
      end;

      if Assigned(ItemExtra) then
      begin
        FItensAdicionado.Add(ItemExtra);

        novoItem:= lsvItensAdicionados.Items.Add;

        descricao:= TListItemText(novoItem.Objects.FindDrawable('Descricao'));
        valor:= TListItemText(novoItem.Objects.FindDrawable('Valor'));

        novoItem.Checked:= true;
        novoItem.Tag:= ItemExtra.ID;
        descricao.Text:= ItemExtra.Descricao;
        valor.Text:=  FormatCurr('R$ ###,###,##0.00;1; ', ItemExtra.ValorUnitario);

        for I := 0 to Pred(lsvItensExtra.Items.Count) do
        begin
           if lsvItensExtra.Items.Item[i].tag = ItemExtra.ID then
              TListViewItem(lsvItensExtra.Items.Item[i]).Checked:= true;
        end;
      end;
    finally
      it.Free;
    end;
  end;

end;

procedure TfrmQtdItemExtraObs.iniciaObs;
begin

  edtObervacoes.Text:= Produto.Observacao.Text;
  Produto.Observacao.Clear;
end;


procedure TfrmQtdItemExtraObs.lsvItensAdicionadosItemClick(
  const Sender: TObject; const AItem: TListViewItem);
var
  it: TItemExtra;
  I, id: integer;
begin
  id:= -1;
  if AItem.Checked then
  begin
    for it in FItensAdicionado do
    begin
      if it.ID = AItem.Tag then
      begin
        id:= it.ID;
        FItensAdicionado.Remove(it);
        break
      end;
    end;
  end;


  for I := 0 to Pred(lsvItensExtra.Items.Count) do
  begin
     if lsvItensExtra.Items.Item[i].tag = id then
        TListViewItem(lsvItensExtra.Items.Item[i]).Checked:= false;
  end;


  MostraQuantidade;
end;

procedure TfrmQtdItemExtraObs.lsvItensExtraItemClick(const Sender: TObject;
  const AItem: TListViewItem);
var
  it: TItemExtra;
begin
  if AItem.Checked then
  begin
    for it in FItensExtra do
    begin
      if it.ID = AItem.Tag then
      begin
        FItensAdicionado.Remove(it);
        break
      end;
    end;
    AItem.Checked:= false;
  end
  else
  begin
    for it in FItensExtra do
    begin
      if it.ID = AItem.Tag then
      begin
        FItensAdicionado.Add(it);
        break
      end;
    end;
    AItem.Checked:= true;
  end;

  MostraQuantidade;
end;

procedure TfrmQtdItemExtraObs.MostraDadosProduto;
begin
  txtTotal.Text:= FormatCurr('###,###,##0.00;1;', Produto.ValorUnitario);

  
  txtTituloQuantidadeProduto.Text:= 'Quantidade de ' + Produto.Descricao;
  txtTituloItemExtra.Text:= 'Itens Extra para ' + Produto.Descricao;
  txtTituloObservacoesProduto.Text:= 'Observações do(a) ' + Produto.Descricao;
end;

procedure TfrmQtdItemExtraObs.MostraQuantidade;
var
  it: TItemExtra;
  valor: Currency;
begin
  edtQuantidade.Text:= FloatToStr(Quantidade);
  txtTotal.Text:= EmptyStr;
  valor:=0;
  for it in ItensAdicionado do
  begin
    valor:= valor + it.ValorUnitario;
  end;
  valor:= valor + Produto.ValorUnitario;
  valor:= valor * Quantidade;

  txtTotal.Text:= FormatCurr('###,###,##0.00;1;', valor);
end;

procedure TfrmQtdItemExtraObs.RemoverScrollBars;
var
  item: TControl;
  I: Integer;
begin
  for I := 0 to Pred(ComponentCount) do
  begin
    if Components[I] is TListView then
    begin
      for item in (Components[I] as TListView).Controls do
      begin
        if item is TScrollBar then
          item.Width := 0;
      end;
    end;
  end;
end;

procedure TfrmQtdItemExtraObs.tmrMostraItensAdicionadosTimer(
  Sender: TObject);
begin
  lstDeItensExtraAdicionados.Visible:= tbcPrincipal.TabIndex = 1;
end;

end.
